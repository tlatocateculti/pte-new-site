<?php
	header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
	header("Pragma: no-cache"); //HTTP 1.0
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	session_start();
	require_once('config.php');
	//CHWILOWO ZAWSZE MOŻNA WEJŚC DO PANELU!!!
	if (/*isset($_GET['id']) ||*/ stripos($_SERVER['QUERY_STRING'], 'go2') !== false) {
		$_SESSION['unconfirm'] = true;
	}
	//w tej chwili aby zalogować się do panelu administracyjnego odpowiednie zmienne sesyjne muszą
	//zostać ustawione. Tylko w ten sposób można przejść do panelu!
// 	//moze okazac sie krytyczne rozbicie ciagu zwracanego wlasnie TUTAJ
// 	$tmp = explode('/', $_SERVER['QUERY_STRING']);
	if (isset($_SESSION['unconfirm'])) {
		//stara wersja panelu - login i hasło; w niedalekiej przyszłości będzie to prowadziło do nowego panelu z logowaniem, a stary zostanie usunięty
		if ($_SESSION['unconfirm'] && $_SERVER['QUERY_STRING'] === 'go' && !isset($_SESSION['sid'])) {
			header('Location: 4gtr4tghw5/panel.php');
		}
		//nowa wersja panelu z logowaniem przez pocztę - ten warunek tworzy wiadomość, w której znajdzie się odnośnik 
		else if ($_SESSION['unconfirm'] && stripos($_SERVER['QUERY_STRING'], 'go2') !== false) {
			//echo 'TUTAJ';
			require_once('config_mail.php');
			require_once('./scripts/utilityFunctions.php');
			define('SITELOGIN', $_BASE_LOCATION[0]. "/?id=");
			if ($f = @fopen(__DIR__ . '/data/login.dat', 'r')) { 
				$m = @explode('/', $_SERVER['QUERY_STRING'])[1];
				
				$sRead = fread($f, filesize("./data/login.dat"));
				fclose($f);
				$sRead = explode('|##|', $sRead);
				if ($m === '' || is_null($m))
					$m = explode('||', decodePhrase($sRead[0]))[2];
				for ($i = 0; $i < count($sRead); $i++) {
					$data = explode('||', decodePhrase($sRead[$i]));
					if (stripos($data[2], $m) !== false) {
						$mail = $data[2];
						$id = $i;
						break;
					}
				}
			}
			if (isset($mail)) {
				$_SESSION['sid'] = session_id() . '&&' . $mail . '&&' . $id;				
				sendMail(decodePhrase(MAIL_LOG), 'PTE - AUTOMAT', $mail, 'AUTORYZACJA - POTWIERDZENIE',
				"Poniższy odnośnik pozwoli na przeprowadzenie administracji stroną czestochowa.pte.pl. W celu potwierdzenia swojej tożsamości należy kliknąć\nw link poniżej celem kontynuacji operacji. Proszę pamiętać by odnośnik otworzyć w oknie przeglądarki, z której zostało wysłane zapytanie!\n\n\n" . SITELOGIN . base64_encode($_SESSION['sid']));
			}
		}
		//jeżeli ktoś kliknął w odnośnik z poczty,a nie jest jeszcze dopuszczony do panelu to tutaj sprawdzone zostaną jego uprawnienia
		else if ($_SESSION['unconfirm'] && isset($_GET['id'])) {
			if (isset($_SESSION['sid']) && isset($_GET['id']))
				if ($_SESSION['sid'] === base64_decode($_GET['id']))
					header('Location: __panel__/panel.php');
		}
		else {
			unset($_SESSION['unconfirm']);
			unset($_SESSION['sid']);
		}
		
		session_regenerate_id(true);
	}
	
	$variant = 'main';
	$site = 'main';
	$subSites = '';
	$galleryID = -1;
	if (isset($_SERVER['QUERY_STRING'])) {
		$tmp = explode('/', $_SERVER['QUERY_STRING']);
		for ($i = 0; $i < count($tmp); $i++) {
			if (strpos($tmp[$i], 'm_')  === 0) {
				$site = explode('_', $tmp[$i])[1];
			}
			else if (strpos($tmp[$i], 'gid_') === 0) {
				$galleryID = explode('_', $tmp[$i])[1];
			}
			if (strpos($tmp[$i], 'sub_') === 0) {
				$subSites = explode('_', $tmp[$i])[1];
			}
			
		}
		
	} 
	include "./sites/news.php";
	include "./sites/content.php";
?>
<!DOCTYPE html>
<html>
  <head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Polskie Towarzystwo Ekonomiczne w Częstochowie</title>
    <meta content=""/>
    <link rel="stylesheet" href="./css/font-awesome.min.css"/>
    <link rel="stylesheet" href="./css/main.css"/>
    <link rel="stylesheet" href="./css/main_colors.css"/>
  </head>
  <body>
	  <nav>
			<img src="img/pte.png" alt="Logo PTE"/>
			<div id="mainMenuIconDiv"><i class="fa fa-list-alt" aria-hidden="true"></i></div>
			<ul>
				<li data-name="m_main#news"><p>Aktualności</li>
				<li data-name="m_main#about"><p>O nas</li>
				<li data-name="m_main#congress"><p>Kongres</li>
				<li data-name="m_main#auth"><p>Władze oddziału</li>
				<li data-name="m_archive"><p>Archiwum</li>
				<li data-name="m_courses"><p>Kursy</li>
				<li data-name="m_materials"><p>Materiały</li>
				<li data-name="m_main#contact"><p>Kontakt</li>
			</ul>
		</nav>
		<div id="mainContener">
			<?php
				if ($site === 'archive')  
					include('./sites/archiveSite.php');
				else if ($site === 'courses') 
					include('./sites/coursesSite.php');
				else if ($site === 'materials')
					include('./sites/materialsSite.php');
				else
					include('./sites/mainSite.php');
			?>
			<div id="upClick"><i class="fa fa-arrow-up" aria-hidden="true"></i></div>
			<footer>
				<p>© Polskie Towarzystwo Ekonomiczne 2017, Wszelkie prawa zastrzeżone</p>
			</footer>
		</div>
		<div id="newsDivBack">			
			<div class="main">
				<i id="exit" class="fa fa-times-circle" aria-hidden="true"></i>
				<div>Przykładowy tekst</div>
			</div>
		</div>
		<div id="tipLabel"><p>Przykładowy tekst</p></div>
		<script src="./js/upload.js"></script>
		<script src="./js/main.js"></script>
		<script src="./js/events.js"></script>
		<script></script>
  </body>
</html>
