h<?php	
	session_start();
	require_once('../config.php');
	require_once('../database.php');
	require_once('../utilityFunctions.php');
	//print_r($_SESSION);

	if (!isset($_SESSION['rebase'])) {
		header('Location: ' . BASE_LOCATION);
	}
	if ($_SESSION['rebase'] == 'set_base') {
		$base = new Database();
		$base->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME);
		
		$base->addDbName('clinics');
		$base->addDbName('gallery');
		$base->addDbName('maps');
		$base->addDbName('mode_level');
		$base->addDbName('sites');
		$base->addDbName('site_config');
		$base->addDbName('slides');
		$base->addDbName('users');
		$base->addDbName('users_options');
		$base->addDbName('visits');
		
		$base->executeScript(file_get_contents("../databases/lekarz_test_24_02_2016.sql"));
	}
	else {
		unset($_SESSION['rebase']);
		header('Location: ' . BASE_LOCATION);
	}
