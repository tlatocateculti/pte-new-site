//funkcja pobiera zawartość strony (ewentualne parametry można przesłać poprzez GET)
//NIE MA MOŻLIWOŚCI PRZESŁANIA DANYCH TYPU POST
function readFileAsync(fileName, wait, rec) {
	if (wait !== '' && wait !== undefined)
		wait();
	var contentFile;
	if (window.XMLHttpRequest)
		contentFile = new XMLHttpRequest();
	else {
		console.log('Stara przeglądarka! Nie obsługuję!');
		return;
	}
	// 	else
	// 		contentFile = new ActiveXObject("Microsoft.XMLHTTP");
	
	contentFile.onreadystatechange = function () {
		var answer = "";
		var retMsg = false;
		if(contentFile.readyState === 4) {				    
			if(contentFile.status === 200 || contentFile.status === 0) {
				answer = contentFile.responseText;
				retMsg = true;
			}
		}
		if (retMsg) {
			if (answer.length > 0)
				rec(answer);
			else
				rec(-1);
		}
	}
	
	contentFile.open("GET", fileName, true);
	contentFile.send();	
}

function toggleSubMenu(caller) {
	var subHandler = document.getElementById(caller);
	if (subHandler.style.display == "none")
		subHandler.style.display = "block";
	else
		subHandler.style.display = "none";
}

function hideAllSubs(active) {
	var i = 0;
	for (i; i < document.getElementById('menu').children[0].children.length; i++) {
		if (document.getElementById('menu').children[0].children[i].id.indexOf('Sub') > -1)
			document.getElementById('menu').children[0].children[i].style.display = "none";
	}
	if (active !== undefined) {
		document.getElementById(active).style.display = "block";
	}
}

//funkcja nadaje odpowiednie właściwości przesłanej tabeli z pliku sites_maintenance.php
//można pokusić się o późniejszą zmianę sztywnego nadawania koloru/operacji
//na odpowiednie nazwy klas (zmiany w CSS)
function tableMaintenace() {
	var tdOptions = document.getElementsByName('dataContent');
	var showDeleted = false;
// 	if (document.getElementsByName('showDeleted')[0].checked)
// 		showDeleted = true;
	var changeIDNum = false;
	for (var i = 0; i < tdOptions.length; i++) {
		//console.log(tdOptions[i].parentNode);
		if (tdOptions[i].dataset.option == 3 && !showDeleted) {
			tdOptions[i].parentNode.style.display = "none";
			changeIDNum = true;
		}
		else if (tdOptions[i].dataset.option == 3 && showDeleted) {
			tdOptions[i].parentNode.style.background = "black";
			tdOptions[i].parentNode.style.color = "white";
		}
		else if (tdOptions[i].dataset.option == 2)
			tdOptions[i].parentNode.style.background = "red";
		else if (tdOptions[i].dataset.option == 1)
			tdOptions[i].parentNode.style.background = "green";
	}
	if (changeIDNum) {
		var idsNum = 1;
		var ids = document.getElementsByName('idNum');
		for (var j = 0; j < ids.length; j++) {
			if (ids[j].parentNode.style.display !== "none")
				ids[j].innerHTML = (idsNum++);
		}
		var buttonParagraph = document.getElementById('tableButtonsP');
		if (idsNum < 10) {
			buttonParagraph.style.display = "none";
		}
		else {
			var tmpElem = document.getElementById('prevTableButton');
			if (tmpElem)
				tmpElem.dataset.jump = parseInt(tmpElem.dataset.jump) - idsNum;
			tmpElem = document.getElementById('nextTableButton');
			if (tmpElem)
				tmpElem.dataset.jump = parseInt(tmpElem.dataset.jump) - idsNum;
// 			document.getElementById('prevTableButton').dataset.jump = parseInt(buttonsDataset[1].dataset.jump) - idsNum;
// 			document.getElementById('nextTableButton').dataset.jump = parseInt(buttonsDataset[2].dataset.jump) - idsNum;
		}
	}
	else {
		var tableRows = document.getElementsByTagName('tr');
		if (tableRows.length > 10) {
			//byc może lepiej jest kasować...
			for (var z = 11; z < tableRows.length; z++)
				tableRows[z].style.display = "none";
			var tmpElem = document.getElementById('prevTableButton');
			if (tmpElem)
				tmpElem.dataset.jump = parseInt(tmpElem.dataset.jump) - (tableRows.length - 11);
			tmpElem = document.getElementById('nextTableButton');
			if (tmpElem) {
				console.log(tmpElem.dataset.jump);
				tmpElem.dataset.jump = parseInt(tmpElem.dataset.jump) - (tableRows.length - 11);
				console.log(tmpElem.dataset.jump);
			}
		}
	}
}

function adminCommunication(site, post, get, waitCode, fRev) {
	if (waitCode !== '' && waitCode !== undefined)
		waitCode();
	if (post === undefined)
		return;
	var contentFile;
	if (window.XMLHttpRequest)
		contentFile = new XMLHttpRequest();
	else
		contentFile = new ActiveXObject("Microsoft.XMLHTTP");
	
	contentFile.onreadystatechange = function () {		
		if(contentFile.readyState === 4) {				    
			if(contentFile.status === 200 || contentFile.status === 0) {
				console.log('Otrzymałem te dane!!');
				fRev(contentFile.responseText);
				return;
			}
		}
		
	}	
	if (get !== undefined && get !== '')
		site += '?' + get;
	contentFile.open("POST", site, true);
	contentFile.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	contentFile.send(post);
}

// function newsTableAddEdit(edit) {
// 	var sendData = '';
// 	var fieldsID = '';
// 	if (edit !== undefined && edit) {
// 		var fieldsToEdit = document.getElementsByName('selectVisitBox');
// 		for (var i = 0;  i < fieldsToEdit.length; i++) {
// 			if (fieldsToEdit[i].checked)
// 				fieldsID += fieldsToEdit[i].value + ',';
// 		}
// 		fieldsID = fieldsID.slice(0, fieldsID.length - 1);
// 		sendData += 'fields=' + fieldsID;
// 	}
// 	adminCommunication("./sites_addedit.php", sendData, '', '',
// 			   function(r) {
// 				   if (r == -100) {
// 					   document.getElementById("tableContent").innerHTML = "Błąd! Nic nie zaznaczyłeś!";
// 					   setTimeout(getNewsTable, 1000);  
// 					   return;
// 				   }
// 				   document.getElementById("tableContent").innerHTML = r;
// 				   document.getElementById('actionButtons').style.display = 'none';
// 				   
// 			   }); 
// 		
// }

// function newsTableShowChange() {
// 	//po prostu przeładowanie tabeli od zera!
// 	getNewsTable();
// }

function newsTableConfirm() {
	var confirmId = '';
	var confChbox = document.getElementsByName('selectVisitBox');
	for (var i = 0; i < confChbox.length; i++) {
		if (confChbox[i].checked) {
			confirmId += confChbox[i].value + ',';
		}
	}
	confirmId = confirmId.slice(0, confirmId.length - 1);
	var sendMsg = "confirmId=" + confirmId;
	adminCommunication("./sites_maintenance.php", sendMsg, '', '',
			   function(r) {
				   document.getElementById("tableContent").innerHTML = r;
				   tableMaintenace();
				   document.getElementById('actionButtons').style.display = 'block';
			   });
}

function visitTableDisable() {
	var disableId = '';
	var disableChbox = document.getElementsByName('selectVisitBox');
	for (var i = 0; i < disableChbox.length; i++) {
		if (disableChbox[i].checked) {
			disableId += disableChbox[i].value + ',';
		}
	}
	disableId = disableId.slice(0, disableId.length - 1);
	var sendMsg = "disableId=" + disableId;
	adminCommunication("./sites_maintenance.php", sendMsg, '', '',
			   function(r) {
				   document.getElementById("tableContent").innerHTML = r;
				   tableMaintenace();
				   document.getElementById('actionButtons').style.display = 'block';
			   });
}



function getNewsTable(startNum) {
	if (startNum === undefined)
		startNum = 0;
	var sendMsg = "startNum=" + startNum;
	adminCommunication("./sites_maintenance.php", sendMsg, '', 
				function () {
					document.getElementById("tableContent").innerHTML = 'Ładuję dane...';
				},
				function(r) {
					//console.log('Jestem w funkcji, dane to: ' + r);
					document.getElementById("tableContent").innerHTML = r;
					tableMaintenace();
					document.getElementById('actionButtons').style.display = 'block';
				});
}

function getUserTable(startNum) {
	if (startNum === undefined)
		startNum = 0;
	var sendMsg = "startNum=" + startNum;
	adminCommunication("./users_maintenance.php", sendMsg, '', 
			   function () {
				   document.getElementById("userUtilityDiv").innerHTML = 'Ładuję dane...';
			   },
		    function(r) {
			    //console.log('Jestem w funkcji, dane to: ' + r);
			    document.getElementById("userUtilityDiv").innerHTML = r;
			    //tableMaintenace();
			    //document.getElementById('actionButtons').style.display = 'block';
		    });
}

function userTableEdit() {
	var tmp = document.getElementsByName('userCheck');
	var id = -1;
	for (var i = 0; i < tmp.length; i++) {
		if (tmp[i].checked) {
			id = tmp[i].value;
			break;
		}
	}
	var sendMsg = "editIDUser=" + id;
	adminCommunication("./users_addedit.php", sendMsg, '', 
			   function () {
				   document.getElementById("userUtilityDiv").innerHTML = 'Ładuję dane...';
			   },
		    function(r) {
			    document.getElementById("userUtilityDiv").innerHTML = r;
			    userTableMaintenance();
			    document.getElementById('tableUsersButtons').style.display = 'none';
		    });
}

function sendSiteContent(target) {
	var contentHTML = document.getElementById('content');
	var sendMsg = "newCode=" + contentHTML.innerHTML;
	var contentFile;
	adminCommunication("./edit_mech.php?edit/", sendMsg, target, 
				function() {
					document.getElementById("content").innerHTML = "Zapytanie jest realizowane...";
				},
				function(r) {
					document.getElementById("content").innerHTML = answer;});
}

function fullTextKeyUpEvent(caller, num) {
	//console.log(caller);
	var s = document.getElementsByName('summary-' + num)[0];
	//prawdopodobnie mamy do czynienia z formularzem bez skrótu
	if (s === undefined) return;
	if (s.dataset.rewrite == 1 && s.value.length < 151) {
		s.value = caller.value.replace(/(<([^>]+)>)/gi, '');
		//console.log(caller.value);
	}
	if (s.value.length > 150)
		s.value = s.value.substring(0, 150);
	
// 	var s = document.getElementsByName('summary-' + num)[0];
// 	if (s.dataset.null != 0 && s.value.length < 151)
// 		s.value = caller.value;
// 	if (s.value.length > 150)
// 		s.value = s.value.substring(0, 150);
}

function summaryKeyUpEvent(caller) {
	if (caller.value != '')
		caller.dataset.rewrite = 0;
	else 
		caller.dataset.rewrite = 1;
}

function checkFileUploadState() {
	if (window.File && window.FileReader && window.FileList && window.Blob) {
		//console.log('Przeglądarka zgodna!'); 
		return true;
	} else {
		//console.log('Wczytywanie plików niedostępne!'); 
		return false;
	}
}

//funkcja przechwytuje i przesyła wskazany obraz na serwer
//ponieważ do PHP trafia jedynie 500000 bajtów to żadne ograniczenia nie są straszne (przeważnie
//serwery mają ograniczenia ok 2 MB).
function uploadFile(event, mini) {
	if (!checkFileUploadState()) return;
	var name = event.target.files[0].name;
	var f = new FileReader();
	f.onload = function() {
		//poniższa linia ma za zadanie wkleić 'przesłany' obraz jako podgląd do wskazanego elementu na stronie
		//nie ma wpływu na sam przepływ pliku!
		var prvID = event.target.parentNode.parentNode.dataset.id;
		if (mini !== undefined) document.getElementById('previewMini-' + prvID).src = f.result;
		else document.getElementById('preview-' + prvID).src = f.result;
		//przechwytujemy folder galerii plików 
		var folder = document.getElementsByName('gallery_folder')[0].value;
		//obliczamy ile kawałków będzie do przesłania
		var chunk = Math.ceil(f.result.length/500000);
		//var progress = 0;
		var gauge = document.getElementById('uploadGauge').children[0];
		gauge.style.display = 'block';
		var gaugeText = gauge.children[0].innerHTML;
		gauge.style.width = '0';
		gauge.children[0].innerHTML = 'Przesłano: ' + gauge.style.width;
		for (var i = 0; i < chunk; i++) {
			var startRead = (i*500000);
			var send = 'filePath=../galleries/' + folder + '/' + name + '&data=' + f.result.slice(startRead,(startRead+500000)) + '&mode=w'  +
			'&secquence=' + i + '&total=' + chunk;
			adminCommunication("./phps/galleries_save.php", send, '', '', function(r) {
				//jeżeli wszystkie części zostały przesłane to wykonuje wysyłany jest do
				//skryptu odpowiedni komunikat; ponieważ nie wymagamy potwierdzenia z serwera
				//toteż nic nie odbieramy (pusta funkcja function(e) {} )
				if ((parseInt(r) + 1) >= chunk) {
					gauge.style.width = '100%';
					gauge.children[0].innerHTML = 'Przesłano: ' + gauge.style.width;
					gauge.style.display = '';
					delete event.target.parentNode.parentNode.dataset.null;
					//console.log('WARTOŚĆ: ' + gaugeText + ' ' + gauge.children[0].innerHTML);
					var sendMsg = 'transfer=../galleries/' + folder + '/' + name + '&total=' + chunk;
					adminCommunication("./phps/galleries_save.php", sendMsg, '', '', function(e) {
						if (mini === undefined) {
							var ftmp = "newelem[gallery_folder][0]=" + folder + '&elemID=' + (parseInt(prvID) + 1) + '&setID=' + document.getElementsByName('gallery_id')[0].value;
								adminCommunication("./phps/galleries_addedit.php", ftmp, '', '', function(r) { 
								var obj = document.createElement('div');
								obj.innerHTML = r;
								document.getElementById('gallerySetElementsForm').appendChild(obj.firstChild);
							});
						}
					});
				}
				else {
					gauge.style.width = (parseInt(gauge.style.width) + (i * chunk)) + '%';
					//console.log('WARTOŚĆ: ' + gaugeText);
					gauge.children[0].innerHTML = 'Przesłano: ' + gauge.style.width;
				}
			});
		}
	}
	if (event.target.files[0].type.indexOf('image/') > -1)
		f.readAsDataURL(event.target.files[0]);
}

function gallerySiteBelong(t) {
	var sn = document.getElementsByName('site_name')[0];
	if (t.value == 0) {
		sn.parentNode.style.display = "block";
	}
	else
		sn.parentNode.style.display = "none";
	console.log(t.value);
}

function openSiteGellaryWindow(mini) {
	
    var w;

    w = document.getElementById('galleryWindow');
    w.style.display = 'block';
    w.style.width = '90%';//(window.innerWidth - 100) + 'px';
    w.style.height = '90%';//(window.innerHeight - 100) + 'px';
    w.style.right = '50px';
    w.style.top = '50px';
    w.style.left = '';
    w.style.position = '';
	var sendMsg = "startNum=0";
	adminCommunication("./phps/galleries_maintenace.php?s=1", sendMsg, '', 
			   function () {
				   document.getElementById("galleriesDiv").innerHTML = 'Ładuję dane...';
			   },
		    function(r) {
			    var galleriesDiv = document.getElementById("galleriesDiv");
			    galleriesDiv.innerHTML = r;
			    var btnok = document.getElementById('addButtonGalleryWindow');
			    if (mini !== undefined) {
				    w.style.position = "fixed";
				    w.style.zIndex = "100000000000";
				    w.style.width = "70%";
				    w.style.right = '0';
				    w.style.left = '15%';
				    window.currentMini = mini;
				
				btnok.setAttribute("onClick", "addPhotoToMini(this);");  
			    }
			    else btnok.setAttribute("onClick", "addPhotoToText(this);");  
			    var elem = document.querySelectorAll("[data-gallery-icon-action]");
			    for (var i = 0; i < elem.length; i++) {
				elem[i].children[1].className += ' elemActionBtn';
				elem[i].children[2].className += ' elemActionBtn';
			    }
			    //tableMaintenace();
// 			    if (!elements)
// 				document.getElementById('actionButtons').style.display = 'block';
		    });	
    //document.getElementsByName('telType')[0].children[0].selected = 'selected';
    //generateTelForm();
}

function closeWindow(t) {
	//document.getElementById('windowDiv').style.zIndex = '';
	if (window.currentMini) window.currentMini = '';
    if (t)
        t.parentNode.style.display = 'none';
    //document.getElementById('localizationWindows').style.display = 'none';
}

function addPhotoToMini(t) {
	var g = document.getElementsByClassName('photosSet');
	for(var i = 0; i < g.length; i++) {
            for (var j = 0; j < g[i].children.length; j++) {
                if (g[i].children[j].dataset.select) {
			var imgAddr = g[i].children[j].children[0].dataset.mini;
			var address = '';
			var addressParts = window.location.href.split('/');
			for(var z = 0; z < addressParts.length; z++) {
				if (addressParts[z] === '__panel__')
					break;
				address += addressParts[z] + '/';
			}
			var miniImg = document.createElement('img');
			miniImg.src = address + imgAddr;
			while (window.currentMini.children.length > 0)
				window.currentMini.removeChild(window.currentMini.lastChild);
			window.currentMini.appendChild(miniImg);
			window.currentMini.innerHTML += '<i onclick="removeMiniSite(event);" class="fa fa-times-circle-o deleteMini" aria-hidden="true"></i>';
			window.currentMini.dataset.mini = address + imgAddr;
			window.currentMini.dataset.full = address + g[i].children[j].children[0].dataset.full;
			window.currentMini.dataset.id = g[i].children[j].dataset.id;
			addNewMiniToSite(window.currentMini);
			closeWindow(t);                    
                }
            }
    }	
}

function addPhotoToText(t) {
    var g = document.getElementsByClassName('photosSet');
    var ta = window.frames['text'].document || window.frames['full_text-0'].document || window.frames['objectives-0'].document || window.frames['additional_info-0'].document || window.frames['prices-0'].document;//.contentDocument || window.frames['full_text-0'].contentWindow.document;
    ta = ta.getElementsByName('galleryItem')[0];
    for(var i = 0; i < g.length; i++) {
            for (var j = 0; j < g[i].children.length; j++) {
                if (g[i].children[j].dataset.select) {
			var imgAddr = g[i].children[j].children[0].dataset.full;//.style.background.split(' ');
			var address = '';
			var addressParts = window.location.href.split('/');
			for(var z = 0; z < addressParts.length; z++) {
				if (addressParts[z] === '__panel__')
					break;
				address += addressParts[z] + '/';
			}
			ta.value = address + imgAddr;//ta.value.slice(0, ta.selectionStart) + '<img src="' + imgAddr + '" alt="Dodane zdjęcie"/>' + ta.value.slice(ta.selectionStart);
			closeWindow(t);
                    
                }
            }
    }
}

function showInfoTip(t, hide) {
	if (t === undefined) return;
	if (hide === undefined) hide = false;
	var tmp = document.getElementById('tipLabel');
	if (tmp === undefined) return;
	if (!hide) {
		tmp.style.display = 'block';
		tmp.children[0].innerHTML = t.target.dataset.text;
		tmp.style.top = (t.clientY + (document.getElementById('windowDiv').scrollTop) + 40) + 'px';
		tmp.style.left = (t.clientX -600) + 'px';
	}
	else
		tmp.style.display ='';
}

function toggleSettingsDiv(t) {
// 	if (t.parentNode.style.height === undefined ||
// 		t.parentNode.style.height == '')
// 	t.parentNode.style.height = "40px";
	if (parseInt(t.parentNode.style.height) == 40) {
		t.parentNode.style.height = "";
	}
	else
		t.parentNode.style.height = "40px";
}
