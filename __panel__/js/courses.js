function getCoursesTable(startNum, training) {
	var address = "./phps/courses_maintenance.php";
	if (startNum === undefined)
		startNum = 0;
	if (training !== undefined && training) {
		address += "?s=1";
	}	
	var sendMsg = "startNum=" + startNum;
	adminCommunication(address, sendMsg, '', 
			   function () {
				   if (startNum == 0)
				  document.getElementById("tableContent").innerHTML = 'Ładuję dane...';
			   },
		    function(r) {
			    //console.log('Jestem w funkcji, dane to: ' + r);
			     if (startNum > 0) {
			        var ct = document.getElementById('courseTraining');
				document.getElementById('moreCourses').parentNode.removeChild(document.getElementById('moreCourses'));
				var newCt = document.createElement('div');
				document.body.appendChild(newCt);
				newCt.innerHTML = r;
				ct.parentNode.appendChild(newCt.lastChild);
				while(newCt.lastChild) {
					ct.appendChild(newCt.firstChild);
				}
				document.body.removeChild(newCt);
			    }
			    else
				document.getElementById("tableContent").innerHTML = r;
			    //tableMaintenace();
			    //document.getElementById('actionButtons').style.display = 'block';
		    });	
}

function coursesTableAddEdit(training, edit, t) {
	if (t === undefined) return;
	var sendData = '';
	//var fieldsID = '';
	var address = "./phps/courses_addedit.php";
	while (t.className.indexOf('settingsBody') == -1 && t.className.indexOf('headerSetName') == -1 ) {
		if (t.parentNode) 
			t = t.parentNode; 
		else return;
		if (t.className === undefined) t.className = '';
		//console.log(t);
	}
	if (t.parentNode.lastChild.id === 'siteEditDiv') {
		t.parentNode.lastChild.style.display = '';
		t.parentNode.removeChild(t.parentNode.lastChild);
		return;
	}
	if (training !== undefined && training) {
		address += "?s=1";
	}	
	if (edit !== undefined && edit) {
// 		var fieldsToEdit = document.getElementsByName('selectCourseBox');
// 		for (var i = 0;  i < fieldsToEdit.length; i++) {
// 			if (fieldsToEdit[i].checked)
// 				fieldsID += fieldsToEdit[i].value + ',';
// 		}
// 		fieldsID = fieldsID.slice(0, fieldsID.length - 1);
		sendData += 'fields=' + t.dataset.id;
	}
	//console.log(training + ' ' + edit + ' ' + address + ' ' + sendData);
	adminCommunication(address, sendData, '', '',
			   function(r) {
				   if (r == -100) {
					   document.getElementById("tableContent").innerHTML = "Błąd! Nic nie zaznaczyłeś!";
					   setTimeout(getNewsTable, 1000);  
					   return;
				   }
// 				   document.getElementById("tableContent").innerHTML = r;
// 				   document.getElementById('actionButtons').style.display = 'none';
				   var d = document.getElementById('siteEditDiv');
				   if (d === undefined || d === null) d = document.createElement('div');
				   d.id = 'siteEditDiv';				   
				   d.style.display = 'block';
				   d.innerHTML = r;
				   t.parentNode.appendChild(d);
// 				   var summary = document.getElementsByName('summary-0')[0];
// 				   if (summary !== undefined) {
// 					if (summary.value !== '')
// 						summary.dataset.rewrite = 0;
// 					else 
// 						summary.dataset.rewrite = 1;
// 				   }
// // 				   var editorName = '';
// 				   if (news) editorName = 'full_text-0';
// 				   else editorName = 'text';
				   document.getElementById('objectives-0').addEventListener('load', function() {
					   var iFrameObjectives = document.getElementById('objectives-0'); 
					   var frameInner = this.contentDocument || this.contentWindow.document;
					   iFrameObjectives.prevelem = frameInner.getElementById('preview');
					   var bodyFrame = frameInner.body;
					   var prevCode = frameInner.getElementById('prevCode');
					   if (iFrameObjectives) iFrameObjectives.prevelem.innerHTML = t.children[2].innerHTML;
					   prevCode.addEventListener('keyup', function() {iFrameObjectives.style.height = (bodyFrame.offsetHeight + 50) + 'px'; });
					   bodyFrame.onresize = function() {iFrameObjectives.style.height = (bodyFrame.offsetHeight + 50/* + prevCode.offsetHeight*/) + 'px'; };
					   frameInner.addEventListener('keyup', function() {iFrameObjectives.style.height = (bodyFrame.offsetHeight + 50) + 'px'; });
				    });
				   document.getElementById('additional_info-0').addEventListener('load', function() {
					   var iFrameInfo = document.getElementById('additional_info-0');
					   var frameInner = this.contentDocument || this.contentWindow.document;
					   iFrameInfo.prevelem = frameInner.getElementById('preview');
					   var bodyFrame = frameInner.body;
					   var prevCode = frameInner.getElementById('prevCode');
					   if (iFrameInfo) iFrameInfo.prevelem.innerHTML = t.children[3].innerHTML;
					   prevCode.addEventListener('keyup', function() {iFrameInfo.style.height = (bodyFrame.offsetHeight + 50) + 'px'; });
					   bodyFrame.onresize = function() {iFrameInfo.style.height = (bodyFrame.offsetHeight + 50/* + prevCode.offsetHeight*/) + 'px'; };
					   frameInner.addEventListener('keyup', function() {iFrameInfo.style.height = (bodyFrame.offsetHeight + 50) + 'px'; });
				    });
				   document.getElementById('prices-0').addEventListener('load', function() {
					   var iFramePrices = document.getElementById('prices-0');
					   var frameInner = this.contentDocument || this.contentWindow.document;
					   iFramePrices.prevelem = frameInner.getElementById('preview');
					   var bodyFrame = frameInner.body;
					   var prevCode = frameInner.getElementById('prevCode');
					   if (iFramePrices) iFramePrices.prevelem.innerHTML = t.children[4].innerHTML;
					   prevCode.addEventListener('keyup', function() {iFramePrices.style.height = (bodyFrame.offsetHeight + 50) + 'px'; });
					   bodyFrame.onresize = function() {iFramePrices.style.height = (bodyFrame.offsetHeight + 50/* + prevCode.offsetHeight*/) + 'px'; };
					   frameInner.addEventListener('keyup', function() {iFramePrices.style.height = (bodyFrame.offsetHeight + 50) + 'px'; });
				    });
				   
				   
			   }); 
	
}

function saveCourse() {
// 	var forms = document.getElementById('siteForm');
// 	var sendData = '';
	
	//trzeba by jeszcze przesylac id kursu... gdyby byl edytowany!
	//NAPRAWIC (takze w formularzu)
	
	var forms = document.getElementsByName('courseForms');
	var sendData = '';
    var courseType = false;
	for (var i = 0; i < forms.length; i++) {
		//console.log(forms[i].id.split('-')[1]);
		var tmp = document.getElementsByName('category-name-' + i);
		if (tmp[0].value == -1) {
			sendData += 'form[' + i + '][category-name]=' + tmp[1].value + '&';
			sendData += 'form[' + i + '][category-description]=' + document.getElementsByName('category-description-' + i)[0].value + '&';
		}
		else
			sendData += 'form[' + i + '][category-name]=' + tmp[0].value + '&';
		tmp = document.getElementsByName('subcategory-name-' + i);
		if (tmp[0].value == -1) {
			sendData += 'form[' + i + '][subcategory-name]=' + tmp[1].value + '&';
			sendData += 'form[' + i + '][subcategory-description]=' + document.getElementsByName('subcategory-description-' + i)[0].value + '&';
		}
		else
			sendData += 'form[' + i + '][subcategory-name]=' + tmp[0].value + '&';
		tmp = document.getElementsByName('category_type-' + i)[0].value;
		sendData += 'form[' + i + '][category_type]=' + tmp + '&';
		if (tmp == 0) {
			sendData += 'form[' + i + '][course_startdate]=' + document.getElementsByName('course_startdate-' + i)[0].value + '&';
			sendData += 'form[' + i + '][end_date]=' + document.getElementsByName('end_date-' + i)[0].value + '&';
		}
		else {
			sendData += 'form[' + i + '][course_startdate]=0000/00/00&';
			sendData += 'form[' + i + '][end_date]=0000/00/00&';
		}
		if (tmp == 1) courseType = true;
		tmp = document.getElementsByName('topic-' + i)[0].value;
		tmp = tmp.replace(/\+/g, '||..||');
			sendData += 'form[' + i + '][topic]=' + tmp + '&';
		tmp = document.getElementsByName('objectives-' + i)[0].prevelem.innerHTML;
		tmp = tmp.replace(/\+/g, '||..||');
			sendData += 'form[' + i + '][objectives]=' + tmp + '&';
		tmp = document.getElementsByName('additional_info-' + i)[0].prevelem.innerHTML;
		tmp = tmp.replace(/\+/g, '||..||');
			sendData += 'form[' + i + '][additional_info]=' + tmp + '&';
		tmp = document.getElementsByName('prices-' + i)[0].prevelem.innerHTML;
		tmp = tmp.replace(/\+/g, '||..||');
			sendData += 'form[' + i + '][prices]=' + tmp + '&';
			sendData += 'form[' + i + '][active]=' + document.getElementsByName('active-' + i)[0].value + '&';
			sendData += 'form[' + i + '][user_id]=' + document.getElementsByName('user_id-' + i)[0].value + '&';
			if (document.getElementsByName('course_id-' + i)[0])
				sendData += 'form[' + i + '][course_id]=' + document.getElementsByName('course_id-' + i)[0].value + '&';
		sendData += 'form[' + i + '][show_all_photo]=' + document.getElementsByName('show_all_photo-' + i)[0].value + '&';
	}	
	sendData = sendData.slice(0, sendData.length - 1);
	adminCommunication("./phps/courses_save.php", sendData, '', '',
			   function(r) {
				   document.getElementById("tableContent").innerHTML = r;
                   setTimeout(function() { getCoursesTable(0, courseType); }, 1100); 
				   //tableMaintenace();
				   //document.getElementById('actionButtons').style.display = 'block';
			   });
}

function showHideCourses(t) {
	if (t === undefined) return;
	while (t.className.indexOf('settingsBody') == -1 && t.className.indexOf('headerSetName') == -1 ) {
		if (t.parentNode) 
			t = t.parentNode; 
		else return;
		if (t.className === undefined) t.className = '';
	}
	if(t.dataset.active == 1) t.dataset.active = 0; else t.dataset.active = 1;
	var address = "./phps/courses_save.php";
	var sendMsg = "hideValue=" + t.dataset.active + '&hideID=' + t.dataset.id;
	adminCommunication(address, sendMsg, '', '',
			   function(r) {
				//console.log(t.children[1].children[1].children[0]);
				if (t.dataset.active == 1) {
					t.children[1].children[1].children[0].innerHTML = '<i aria-hidden="true" class="fa fa-eye-slash"></i>';
					t.children[1].children[1].children[0].dataset.tip = 'Ukryj wiadomość';
					t.children[0].children[0].innerHTML = t.children[0].children[0].innerHTML.replace('(nieaktywna)', '(aktywna)');
				}
				else {
					t.children[1].children[1].children[0].innerHTML = '<i aria-hidden="true" class="fa fa-eye"></i>';
					t.children[1].children[1].children[0].dataset.tip = 'Pokaż wiadomość';
					t.children[0].children[0].innerHTML = t.children[0].children[0].innerHTML.replace('(aktywna)', '(nieaktywna)');
				}
				return;
			   });
}

function coursesTableDelete(t, training) {
	if (t === undefined) return;
	if (training === undefined) training = false;
	while (t.className.indexOf('settingsBody') == -1 && t.className.indexOf('headerSetName') == -1 ) {
		if (t.parentNode) 
			t = t.parentNode; 
		else return;
		if (t.className === undefined) t.className = '';
		//console.log(t);
	}
	//var delId = '';
	//var delChbox = document.getElementsByName('selectCourseBox');
	//for (var i = 0; i < delChbox.length; i++) {
		//if (delChbox[i].checked) {
		//	delId += delChbox[i].value + ',';
		//}
	//}
	//delId = delId.slice(0, delId.length - 1);
	var sendMsg = "deleteId=" + t.dataset.id;//delId;
	adminCommunication("./phps/courses_maintenance.php", sendMsg, '', '',
			   function(r) {
				   getCoursesTable(0, training);
				   //document.getElementById("tableContent").innerHTML = r;
				   //tableMaintenace();
				   //document.getElementById('actionButtons').style.display = 'block';
			   });
}

function courseCategoryChange(t, num) {
	if (t.value != -1) {
		document.getElementsByName('category-name-' + num)[1].parentNode.style.display = "none";
		document.getElementsByName('category-description-' + num)[0].parentNode.style.display = "none";
	}
	else {
		document.getElementsByName('category-name-' + num)[1].parentNode.style.display = "block";
		document.getElementsByName('category-description-' + num)[0].parentNode.style.display = "block";
	}
}

function subCourseCategoryChange(t, num) {
	if (t.value != -1 || t.value == 0) {
		document.getElementsByName('subcategory-name-' + num)[1].parentNode.style.display = "none";
		document.getElementsByName('subcategory-description-' + num)[0].parentNode.style.display = "none";
	}
	else {
		document.getElementsByName('subcategory-name-' + num)[1].parentNode.style.display = "block";
		document.getElementsByName('subcategory-description-' + num)[0].parentNode.style.display = "block";
	}
}