function getSitesTable(startNum, news) {
	var address = "./phps/sites_maintenance.php";
	if (news === undefined || !news)
		address += "?s=1";
	if (startNum === undefined)
		startNum = 0;
	var sendMsg = "startNum=" + startNum;
	adminCommunication(address, sendMsg, '', 
			   function () {
				   document.getElementById("tableContent").innerHTML = 'Ładuję dane...';
			   },
		    function(r) {
			    //console.log('Jestem w funkcji, dane to: ' + r);
			    document.getElementById("tableContent").innerHTML = r;
			    //tableMaintenace();
			    //document.getElementById('actionButtons').style.display = 'block';
		    });	
}

function sitesTableAddEdit(edit, news, t) {
	if (t === undefined) return;
	//console.log(t);
	while (t.className.indexOf('settingsBody') == -1 && t.className.indexOf('headerSetName') == -1 ) {
		if (t.parentNode) 
			t = t.parentNode; 
		else return;
		if (t.className === undefined) t.className = '';
		//console.log(t);
	}
	if (t.parentNode.lastChild.id === 'siteEditDiv') {
		t.parentNode.lastChild.style.display = '';
		t.parentNode.removeChild(t.parentNode.lastChild);
		return;
	}
	var sendData = '';
// 	var fieldsID = '';
	var address = "./phps/sites_addedit.php";
	if (news === undefined || !news) 
		address += "?s=1";
	if (edit !== undefined && edit) {
// 		var fieldsToEdit = document.getElementsByName('selectSitesBox');
// 		for (var i = 0;  i < fieldsToEdit.length; i++) {
// 			if (fieldsToEdit[i].checked)
// 				fieldsID += fieldsToEdit[i].value + ',';
// 		}
// 		fieldsID = fieldsID.slice(0, fieldsID.length - 1);
		sendData += 'fields=' + t.dataset.id;
	}
	adminCommunication(address, sendData, '', '',
			   function(r) {
				   if (r == -100) {
					   document.getElementById("tableContent").innerHTML = "Błąd! Nic nie zaznaczyłeś!";
					   setTimeout(getNewsTable, 1000);  
					   return;
				   }
				   var d = document.getElementById('siteEditDiv');
				   if (d === undefined || d === null) d = document.createElement('div');
				   d.id = 'siteEditDiv';				   
				   d.style.display = 'block';
				   d.innerHTML = r;
				   t.parentNode.appendChild(d);
// 				   var summary = document.getElementsByName('summary-0')[0];
// 				   if (summary !== undefined) {
// 					if (summary.value !== '')
// 						summary.dataset.rewrite = 0;
// 					else 
// 						summary.dataset.rewrite = 1;
// 				   }
				   var editorName = 'text';
// 				   if (news) editorName = 'full_text-0';
// 				   else editorName = 'text';
				   if (editorName === undefined) return;
				   //else return;
				   document.getElementById(editorName).addEventListener('load', function() {
						var iFrame = document.getElementById(editorName);
						var frameInner = this.contentDocument || this.contentWindow.document;
						iFrame.prevelem = frameInner.getElementById('preview');
						var bodyFrame = frameInner.body;
						var prevCode = frameInner.getElementById('prevCode');
						iFrame.style.height = (bodyFrame.offsetHeight + 50) + 'px';
						if (t.children[2] !== undefined)
							iFrame.prevelem.innerHTML = t.children[2].innerHTML;
						window.frames[editorName].galleryOpen = openSiteGellaryWindow;
						//console.log(this.contentWindow.document.body);
						//iFrame.onresize = function() {iFrame.style.height = (bodyFrame.offsetHeight + 50) + 'px'; };
						//prevCode.onresize = function() {iFrame.style.height = (bodyFrame.offsetHeight + 50) + 'px'; };
						prevCode.addEventListener('keyup', function() {fullTextKeyUpEvent(prevCode, 0);iFrame.style.height = (bodyFrame.offsetHeight + 50) + 'px'; });
						bodyFrame.onresize = function() {iFrame.style.height = (bodyFrame.offsetHeight + 50/* + prevCode.offsetHeight*/) + 'px'; };
						frameInner.addEventListener('keyup', function() {fullTextKeyUpEvent(prevCode, 0);iFrame.style.height = (bodyFrame.offsetHeight + 50) + 'px'; });
				   });
			   }); 
}

function showHidesites(news, t) {
	if (t === undefined) return;
	while (t.className.indexOf('settingsBody') == -1 && t.className.indexOf('headerSetName') == -1 ) {
		if (t.parentNode) 
			t = t.parentNode; 
		else return;
		if (t.className === undefined) t.className = '';
		console.log(t);
	}
// 	if (news) {
// 		if(t.dataset.show == 1) t.dataset.show = 2; else t.dataset.show = 1;
// 	}
// 	else {
		if(t.dataset.active == 1) t.dataset.active = 0; else t.dataset.active = 1;
// 	}
	var address = "./phps/sites_save.php";
	if (news === undefined || !news)
		address += "?s=1";
// 	var delChbox = document.getElementsByName('selectSitesBox');
// 	for (var i = 0; i < delChbox.length; i++) {
// 		if (delChbox[i].checked) {
// 			delId += delChbox[i].value + ',';
// 		}
// 	}
// 	delId = delId.slice(0, delId.length - 1);
	var sendMsg = "hideValue=" + (t.dataset.show || t.dataset.active) + '&hideID=' + t.dataset.id;
	adminCommunication(address, sendMsg, '', '',
			   function(r) {
				//console.log(t.children[1].children[1].children[0]);
				if (t.dataset.show == 1 || t.dataset.active == 1) {
					t.children[1].children[1].children[0].innerHTML = '<i aria-hidden="true" class="fa fa-eye-slash"></i>';
					t.children[1].children[1].children[0].dataset.tip = 'Ukryj wiadomość';
					t.children[0].children[0].innerHTML = t.children[0].children[0].innerHTML.replace('(nieaktywna)', '(aktywna)');
				}
				else {
					t.children[1].children[1].children[0].innerHTML = '<i aria-hidden="true" class="fa fa-eye"></i>';
					t.children[1].children[1].children[0].dataset.tip = 'Pokaż wiadomość';
					t.children[0].children[0].innerHTML = t.children[0].children[0].innerHTML.replace('(aktywna)', '(nieaktywna)');
				}
				return;
			   });
}

function sitesTableDelete(news, t) {
	if (t === undefined) return;
	while (t.className.indexOf('settingsBody') == -1 && t.className.indexOf('headerSetName') == -1 ) {
		if (t.parentNode) 
			t = t.parentNode; 
		else return;
		if (t.className === undefined) t.className = '';
		console.log(t);
	}
	//var delId = '';
	var address = "./phps/sites_save.php";
	if (news === undefined || !news)
		address += "?s=1";
	var sendMsg = "deleteId=" + t.dataset.id;
	adminCommunication(address, sendMsg, '', '',
			   function(r) {
					getSitesTable(0, news);
// 				   document.getElementById("tableContent").innerHTML = r;
// 				   //tableMaintenace();
// 				   document.getElementById('actionButtons').style.display = 'block';
			   });
}

function saveSite() {
	//var forms = document.getElementById('siteForm');
	var sendData = '';
	sendData += 'addDate=' + document.getElementsByName('addDate')[0].value + '&';
	var fullText = document.getElementsByName('text')[0].prevelem.innerHTML;
	sendData += 'text=' + btoa(encodeURIComponent(encodeURI(fullText))) + '&';
	sendData += 'description=' + document.getElementsByName('description')[0].value + '&';
	sendData += 'header=' + document.getElementsByName('header')[0].value + '&';
// 	sendData += 'subsites=' + document.getElementsByName('subsites')[0].value + '&'; 
	sendData += 'siteBelong=' + document.getElementsByName('siteBelong')[0].value + '&';
	sendData += 'active=' + document.getElementsByName('active')[0].value + '&';
// 	sendData += 'btnname=' + document.getElementsByName('btnname')[0].value + '&';
// 	sendData += 'btncategory=' + document.getElementsByName('btncategory')[0].value + '&';
// 	sendData += 'mainbtncategory=' + document.getElementsByName('mainbtncategory')[0].value + '&';
	sendData += 'b64=1&'; //flaga okreslajaca, ze strona zostala zakodowana do b64 (trzeba odkodowac)
	sendData += 'user_id=' + document.getElementsByName('user_id')[0].value;
// 	var miniGalleries = document.getElementsByClassName('addSiteMiniGalleryContent')[0].children;
// 	for (var i = 0; i < miniGalleries.length; i++) {		
// 		if (miniGalleries[i].dataset.id) {
// 			sendData += '&gallery[' + i + '][full]=' + miniGalleries[i].dataset.full + '&';
// 			sendData += 'gallery[' + i + '][mini]=' + miniGalleries[i].dataset.mini + '&';
// 			sendData += 'gallery[' + i + '][id]=' + miniGalleries[i].dataset.id;
// 		}
// 	}
	if (document.getElementsByName('site_id')[0])
		sendData += '&' + 'site_id=' + document.getElementsByName('site_id')[0].value;
	//console.log(sendData);
	//sendData = sendData.slice(0, sendData.length - 1);
	adminCommunication("./phps/sites_save.php", sendData, '', '',
			   function(r) {
				   //document.getElementById("tableContent").innerHTML = r;
				   document.getElementById("tableContent").innerHTML = "Dodano/zmodyfikowano stronę!";
				   setTimeout(getSitesTable, 1000);  
				   return;
				   //tableMaintenace();
				   //document.getElementById('actionButtons').style.display = 'block';
			   });
}

function saveNews() {
	var sendData = '';
	sendData += 'addDate=' + document.getElementsByName('addDate')[0].value + '&';
	var fullText = document.getElementsByName('text')[0].prevelem.innerHTML;
	sendData += 'text=' + btoa(encodeURIComponent(encodeURI(fullText))) + '&';
	sendData += 'description=' + document.getElementsByName('description')[0].value + '&';
	sendData += 'newsHeader=' + document.getElementsByName('newsHeader')[0].value + '&';
	sendData += 'shortText=' + document.getElementsByName('shortText')[0].value + '&';
	sendData += 'active=' + document.getElementsByName('active')[0].value + '&';
	sendData += 'b64=1&'; //flaga okreslajaca, ze strona zostala zakodowana do b64 (trzeba odkodowac)
	sendData += 'user_id=' + document.getElementsByName('user_id')[0].value;
	var miniGalleries = document.getElementsByClassName('addSiteMiniGalleryContent')[0].children;
	for (var i = 0; i < miniGalleries.length; i++) {		
		if (miniGalleries[i].dataset.id) {
			sendData += '&gallery[' + i + '][full]=' + miniGalleries[i].dataset.full + '&';
			sendData += 'gallery[' + i + '][mini]=' + miniGalleries[i].dataset.mini + '&';
			sendData += 'gallery[' + i + '][id]=' + miniGalleries[i].dataset.id;
		}
	}
	if (document.getElementsByName('site_id')[0])
		sendData += '&' + 'site_id=' + document.getElementsByName('site_id')[0].value;
	adminCommunication("./phps/sites_save.php", sendData, '', '',
			   function(r) {
				   //document.getElementById("tableContent").innerHTML = r;
				   document.getElementById("tableContent").innerHTML = "Dodano/zmodyfikowano wiadomość!";
					setTimeout(function() { getSitesTable(0, true); }, 1000);  
				   return;
				   //tableMaintenace();
				   //document.getElementById('actionButtons').style.display = 'block';
			   });
}

function siteChangeSiteBelong(t) {	
	var sb = document.getElementsByName('siteBelong')[0];
	var rs = '';
	(t.value == 1) ? rs = 'sub_' : rs = 'm_';
	rs += sb.value.split('_')[1];
	sb.value = rs;
}

function addNewMiniToSite(mini) {
	var newMini = document.createElement('div');
	newMini.setAttribute("onClick", "openSiteGellaryWindow(this);");
	newMini.className = "addSiteMiniPhoto";
	newMini.innerHTML = '<i class="fa fa-plus-circle" aria-hidden="true"></i><p style="">DODAJ MINIATURĘ ZDJĘCIA</p><!--<i onclick="removeMiniSite(event);" class="fa fa-times-circle-o deleteMini" aria-hidden="true"></i>-->';
	
	mini.parentNode.appendChild(newMini);
	if (mini.parentNode.style.width === '' || mini.parentNode.style.width === undefined)
		mini.parentNode.style.width = "320px";
	console.log(mini.parentNode.style.width);
	mini.parentNode.style.width = (parseInt(mini.parentNode.style.width) + 315) + "px";
	var miniContentWidth = parseInt(mini.parentNode.style.width);
// 	if (miniContentWidth > mini.parentNode.parentNode.offsetWidth) {
// 		var scroll = document.getElementsByClassName('addSiteMiniGalleryScroll')[0].children[0];
// 		var scrollWidth = scroll.parentNode.offsetWidth;
// 		if (parseInt(scroll.style.width) > 0 && scroll.style.width !== undefined)
// 			scrollWidth = parseInt(scroll.style.width);
// 		//byc moze trzeba usunąc minus...
// 		document.getElementsByClassName('addSiteMiniGalleryContent')[0].scrollOffset = (parseInt(mini.parentNode.style.width) - mini.parentNode.parentNode.offsetWidth) / scroll.parentNode.offsetWidth * 100;
// 		scrollWidth -= (parseInt(mini.parentNode.style.width) - mini.parentNode.parentNode.offsetWidth) / scroll.parentNode.offsetWidth * 100;
// 		if (scrollWidth < 10)
// 			scrollWidth = 10;
// 		scroll.style.width = scrollWidth + 'px';
// 	}
}

function removeMiniSite(e) {
	e.stopPropagation();
	var panel = e.target.parentNode.parentNode;
	var mini = e.target.parentNode;
	panel.style.width = (parseInt(panel.style.width) - 310) + "px";
	panel.removeChild(mini);
}

// function getMouseScrollAction(up, e) {
// 	var scrollPanel =  document.getElementsByClassName('addSiteMiniGalleryContent')[0];
// 	if (scrollPanel.mouseXStart === undefined) scrollPanel.mouseXStart = 0;
// 	if (!up)
// 		scrollPanel.mouseXStart = e.clientX;
// 	else {
// 		
// 		scrollPanel.mouseXStart = 0;
// 		console.log('KOŃCZE!!');
// 	}
// 	if (scrollPanel.style.left === '' || scrollPanel.style.left === undefined) scrollPanel.style.left = 0;
// 	console.log("Wywołanie myszy " + scrollPanel.mouseXStart + " " + scrollPanel.mouseXOffset);
// }
// 
// function getMouseScrollMove(e) {
// 	var scrollPanel =  document.getElementsByClassName('addSiteMiniGalleryContent')[0];
// 	console.log(e.pageX + ' ' + e.clientX + ' ' + scrollPanel.scrollOffset);
// 	if (scrollPanel.mouseXStart !== 0 && scrollPanel.mouseXStart !== undefined ) {
// 		scrollPanel.mouseXOffset++;// = (e.clientX - scrollPanel.mouseXStart) * scrollPanel.scrollOffset;
// 		if (parseInt(scrollPanel.style.left) > -1)
// 			scrollPanel.style.left = (parseInt(scrollPanel.style.left) + scrollPanel.mouseXOffset) + 'px';
// 		else if (parseInt(scrollPanel.style.left) > scrollPanel.offsetWidth)
// 			scrollPanel.style.left = -scrollPanel.scrollOffset + 'px';
// 		else
// 			scrollPanel.style.left = 0;
// 		//console.log("Wywołanie myszy " + scrollPanel.mouseXStart + " " + scrollPanel.mouseXOffset);
// 	}
// }
// 
// function siteMiniGalleryScroll(e) {	
// 	var scrollPanel =  document.getElementsByClassName('addSiteMiniGalleryContent')[0];
// 	if (scrollPanel.style.left === '' || scrollPanel.style.left === undefined) scrollPanel.style.left = 0;
// 	//console.log(scrollPanel.style.left);
// 	scrollPanel.style.left = (parseInt(scrollPanel.style.left) - scrollPanel.scrollOffset) + 'px';
// 	//console.log(scrollPanel.style.left + ' ' + (parseInt(scrollPanel.style.left) - 10));
// }

//document.addEventListener('mouseup', function() {getMouseScrollAction(true, event);});