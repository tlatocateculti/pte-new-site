var timeoutHandler = '';

function getCategories(param) {
	//node, nodeID
	var get = '';
	if (param !== undefined) {			
		get = 'node=' + ( parseInt(param.split('_')[0]) + 1) + '&nodeID=' + param.split('_')[1];
	}
	adminCommunication("./phps/categories_maintenanceNew.php", '', get, '',
			function(r) {
			if (r === undefined || r == '') return;
			var o = document.getElementById("output");
			o.innerHTML = r;
			});
}

//Wejście:
// t - uchwyt do obiektu wywołującego funkcję
// s - opcjonalny; podpowiada funkcji skąd została wywołana (zmienia ścieżkę odwołania do obiektów)
function saveListNode(t, s) {
	var editField;
	if (s === undefined) {
		editField = t;
	}
	else if (s == 1) {
		editField = t.parentNode;
	}
	editField.parentNode.style.border = '';
	editField.contentEditable = 'false';
	var childID = 2;
	if (editField.parentNode.children[0].dataset.name == 'dummyElement' &&
	editField.parentNode.children[1].dataset.name == 'iconElement') childID += 1;
	editField.parentNode.children[childID].children[0].style.display = 'none';
}

function editListNode(t, l) {
	if (t === undefined) return;
	if (l === undefined) l = -1;
	//alert(t.parentNode.parentNode.id);
	//contenteditable='true' 
	var childID = 1;
	if (t.parentNode.children[0].dataset.name == 'dummyElement' &&
	t.parentNode.children[1].dataset.name == 'iconElement') childID += 1;
	t.parentNode.parentNode.style.display = '2px solid black';
	t.parentNode.parentNode.children[childID].contentEditable = 'true';
	console.log(t.parentNode.parentNode.children[0].name);
	t.parentNode.parentNode.children[childID].focus();
	//przycisk zapisz
	t.parentNode.children[0].style.display = "block";
}

function showHideNode(t) {
	var idNode = t.parentNode.parentNode.id.split('_');
	var get = 'showHideNode=' + idNode[0] + '&id=' + idNode[1];
	adminCommunication("./categories_maintenance.php", '', get, '',
		function(r) {
			//var json = eval(r)[0];
			getCategories();
		});
}

function deleteListNode(t) {
	var n = t.parentNode.parentNode.dataset.id || t.parentNode.parentNode.dataset.name;
// 	if (t.parentNode.dataset.pos == 2) {
// 		console.log('przycisk');
// 	}
// 	else if (t.parentNode.dataset.pos == 1) {
// 		console.log('podkategoria');
// 	}
// 	else if (t.parentNode.dataset.pos == 0) {
// 		console.log('kategoria');
// 	}
	//console.log(n);
	confirmDelete(n, function() {
		//alert('Kasujemy!! ' +  t.parentNode.parentNode.id);
		
		//var idNode = t.parentNode.parentNode.id.split('_');
		var post = 'deleteNode=1&id=' + n;
		if (t.parentNode.parentNode.dataset.sub)
			post += '&sub=' + t.parentNode.parentNode.dataset.sub;
		if (t.parentNode.parentNode.dataset.cat)
			post += '&cat=' + t.parentNode.parentNode.dataset.cat;
		console.log(post);
		adminCommunication("./phps/categories_save.php", post, '', '',
			function(r) {
				//var json = eval(r)[0];
				//if (json['result'] == 0)
					getCategories();
				var tip = document.getElementById('dialogBox');
				tip.className += ' infoOperation';
				tip.addEventListener('click', function() {hideTip(tip);});
				tip.innerHTML = "<p>" + r + "</p>";
				createTipDialog(tip, '25%', '25%');
				timeoutHandler = window.setTimeout(function() {hideTip(tip);}, 3000);
			});
		});
}

function confirmDelete(n, f, o) {
	if (o === undefined) {
		var tip = document.getElementById('dialogBox');
		tip.innerHTML = "<p>Czy na pewno chcesz usunąć <b>" + n + "</b>? Od tej operacji nie będzie odwrotu!</p>";
		var btn = document.createElement('button');
		btn.innerHTML = 'Tak';
		btn.className = 'tipButton';
		btn.addEventListener('click', function() {confirmDelete(n, f, true)});
		tip.appendChild(btn);
		btn = document.createElement('button');
		btn.innerHTML = 'Nie';
		btn.className = 'tipButton';
		btn.addEventListener('click', function() {confirmDelete(n, f, false)});
		tip.appendChild(btn);
		createTipDialog(tip, '25%', '25%');
	}
	else {
		if (o) 
			f();
		hideTip(document.getElementById('dialogBox'));
	}
}

function showTip(t, e) {
	//console.log(t);
	if (t.children[0].dataset.tip == undefined) return;
	var tipDiv = document.getElementById('tipLabel');
	if (tipDiv.style.display == 'block') return;		
	tipDiv.innerHTML = '<p>' + t.children[0].dataset.tip + '</p>';
	createTipDialog(tipDiv, (e.clientY + 30) + 'px', (e.clientX - 170) + 'px');
// 		tipDiv.style.display = 'block';
	
// 		tipDiv.style.top = (e.clientY + 10) + 'px';
// 		tipDiv.style.left = e.clientX + 'px';
	
	timeoutHandler = window.setTimeout(function() {hideTip(tipDiv);}, 3000);
}

function createTipDialog(handle, t, l, w, h) {
	handle.style.top = t;
	handle.style.left = l;
	if (w) handle.style.width = w;
	if (h) handle.style.height = h;
	handle.style.zIndex = 1000;
	handle.style.display = 'block';
}

function hideTip(tipDiv) {		
	if (typeof tipDiv === 'string' || tipDiv instanceof String)
		tipDiv = document.getElementById(tipDiv);
	if (tipDiv === undefined) return;
	window.clearTimeout(timeoutHandler); 
	tipDiv.style.display = '';
	tipDiv.style.top = '';
	tipDiv.style.left = '';
	tipDiv.style.width = '';
	tipDiv.style.height = '';
	tipDiv.style.zIndex = '';
	var c = tipDiv.className.split(' ');
	tipDiv.className = '';
	for(var i = 0; i < c.length - 1; i++)
		tipDiv.className += c[i];
	
}

function showSubNodes(t, e) {
//alert(t.contentEditable);
	if (t.contentEditable == 'true') e.stopPropagation();
	else {
		if (t.parentNode.dataset.subDiv === undefined) {getCategories(t.parentNode.id); return;}
		var elem = document.getElementById(t.parentNode.dataset.subDiv);
		if (elem.style.display == 'block') {
			elem.style.display = 'none';
			rotateArrow(t.parentNode);
		}
		else {
			elem.style.display = 'block';
			rotateArrow(t.parentNode, true);
		}
	};
}

function rotateArrow(t, direction) {
	var angle = '180deg';
	if (direction === undefined || !direction) angle = '0deg';
	for (var i = 0; i < t.children.length; i++) {
		//console.log(t.parentNode.children[i]);
		if (t.children[i].dataset.name == 'iconElement') {
			if (t.children[i].style.WebkitTransform) t.children[i].style.WebkitTransform = 'rotate3d(0,0,1,' + angle + ')';
			t.children[i].style.transform = 'rotate3d(0,0,1,' + angle + ')';
			if (angle == '180deg') {
				t.children[i].style.top = '12px';
				t.children[i].style.left = '-8px';
			}
			else {
				t.children[i].style.top = '';
				t.children[i].style.left = '';
			}
			break;
		}
	}
}