<?php
	//pliki wymagane, jednak nie ma sensu podłaczać ich wielokrotnie
	//require_once "../scripts/database.php";
	//require_once "Mail.php";
	require_once('./utilityFunctions.php');
	require_once "../../config.php";
	require_once "../../scripts/utilityFunctions.php";
	
 	if (!isset($_SESSION['limit']['localization']))
		$_SESSION['limit']['localization'] = 0;
		
		
	function createActionButtons($hide = 0, $n = [], $icons = [], $actions = []) {
		$btn = createNodeButtons($hide, $n, $icons, [], $actions);
		$btnTxt = '';
		for ($i = 0; $i < count($btn); $i++) {
			$btnTxt .= "<div style=\"position: relative;\"";
			if (isset($btn[$i]['style']))
				$btnTxt .= " class=\"{$btn[$i]['style']}\"";
			if (is_array($btn[$i]['events'])) {
				$keys = array_keys($btn[$i]['events']);
				for ($j = 0; $j < count($keys); $j++) {
					$btnTxt .= " {$keys[$j]}='" . $btn[$i]['events'][$keys[$j]] . ";'";
				}
			}
			$btnTxt .= ">";
			if (isset($btn[$i]['icon'])) {
				$btnTxt .= "<p";
				if ($btn[$i]['name'])
					$btnTxt .= " data-tip='{$btn[$i]['name']}'";
				$btnTxt .= ">{$btn[$i]['icon']}</p>";
			}
			else 
				$btnTxt .= "<p>{$btn[$i]['name']}</p>";
			$btnTxt .= "</div>";
		}
		return $btnTxt;
	}
	
	
	function createLocalizationPanel() {
		echo '<div class="settingsSetDiv">
			<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="localizationAddEdit(false, this);">
				<div style="float: left;"><p>Dodaj nową lokalizację</p></div>
				<div style="float: right; width: 170px; height: 100%; overflow:hidden; font-size: 15px;">' . 
				createActionButtons(false, ['Dodaj lokalizację'], ['<i class="fa fa-plus" aria-hidden="true"></i>'], ['onclick' => array('localizationAddEdit(false, this)')]) .'</div>
			</div>
		</div>';
		$btnIcons = [0 => '<i class="fa fa-cog" aria-hidden="true"></i>', 2 => '<i class="fa fa-trash" aria-hidden="true"></i>'];
		$btnName = [0 => 'Edytuj lokalizację', 2 => 'Usuń lokalizację (NA ZAWSZE)'];
		$btnActions = ['onclick' => array("event.stopPropagation(); localizationAddEdit(true, this);", 
						"event.stopPropagation(); deleteSelectedLocalization(0, this)", 
						"event.stopPropagation(); deleteSelectedLocalization(-1, this)") ];
		$file = readEntries('../../sites', 'contact.json');
		for ($i = 0; $i < count($file); $i++) {
			if ($file[$i]['deleted'] == 0) {
				$btnIcons[1] = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
				$btnName[1] = 'Ukryj lokalizację';
			}
			else {
				$btnIcons[1] = '<i class="fa fa-eye" aria-hidden="true"></i>';
				$btnName[1] = 'Pokaż lokalizację';
			}
			$address = explode(';;', $file[$i]['address']);
			echo '<div class="settingsSetDiv">
				<div class="settingsBody" data-addressname="' . $file[$i]['addressname'] . '" data-comment="' . $file[$i]['comment'] . '" data-streetprefix="' . $address[0]  . '" data-street="' .$address[1] . '" data-placenum="' . $address[2] . '" data-localnum="' . $address[3] . '" data-postalcode="' . $file[$i]['postalcode'] . '" data-postoffice="' . $file[$i]['postoffice'] . '" data-town="' . $file[$i]['town'] . '" data-tels="' . $file[$i]['tels'] . '" data-opens="' . $file[$i]['workHours'] . '" data-lat="' . $file[$i]['lat'] . '" data-lon="' . $file[$i]['lon'] . '" data-zoom="' . $file[$i]['zoom'] . '" data-deleted="' . $file[$i]['deleted'] . '" data-id="' . $i . '" onclick="localizationAddEdit(true, this);">
					<div style="float: left;"><p>' . ($file[$i]['addressname'] !== '' ? $file[$i]['addressname'] : '{brak opisu}')  . ($file[$i]['deleted'] == 0 ? ' (aktywna)' : ' (nieaktwyna)') . '</p></div>
					<div class="buttonIconDiv">' . 
					createActionButtons($file[$i]['deleted'], $btnName, $btnIcons, $btnActions) .'</div>
					<div style="display: none;">' . $file[$i]['text'] . '</div>
				</div>
			</div>';
		}
	}
	createLocalizationPanel();
	