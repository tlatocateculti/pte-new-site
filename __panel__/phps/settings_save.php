<?php
	require_once('./utilityFunctions.php');
// 	require_once(__DIR__ . "/../../scripts/database.php");
	require_once(__DIR__ . "/../../config.php");
	require_once(__DIR__ . "/../../scripts/utilityFunctions.php");
	confirmSession();
	
	function saveEncodeToFile($bu, $bp, $sl, $sp, $ss, $mp) {
		saveToFile($bu, 'w');
		saveToFile($bp);
		saveToFile($sl);
		saveToFile($sp);
		saveToFile($ss);
		saveToFile($mp);
	}
	
// 	function saveEncodeFile($bs) {
// 		saveToFile($bs->getResults(0)['baseUser'][0], 'w');
// 		saveToFile($bs->getResults(0)['basePass'][0]);
// 		saveToFile($bs->getResults(0)['siteLogin'][0]);
// 		saveToFile($bs->getResults(0)['sitePass'][0]);
// 		saveToFile($bs->getResults(0)['siteServer'][0]);
// 		saveToFile($bs->getResults(0)['sitePort'][0]);
// 	}
	
	function saveToFile($data, $mode = 'a') {
		$f = fopen('../../data/data.dat', $mode);
		$pass = encodePhrase($data);
		fwrite($f, $pass . "||");
		fclose($f);
	}
	$baseWork = false;
	//$bs = new Database();
	//if ($bs->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME)) $baseWork = true;
	if (isset($_POST['command'])) {
		if ($_POST['command'] == 0) {
// 			if ($_POST['reconfig'] != -1 && $baseWork) {
// 				$bs->buildConditionQuery(array('site_config', 'id'), array($_POST['reconfig']), DataEnum::EQUAL);
// 				$bs->update('site_config', array('baseUser' => $_POST['baseUser'],
// 				'basePass' => $_POST['basePass'],
// 				'baseName' => $_POST['baseName'],
// 				'siteLogin' => $_POST['siteLogin'],
// 				'sitePass' => $_POST['sitePass'],
// 				'siteServer' => $_POST['siteServer'],
// 				'sitePort' => $_POST['sitePort'],
// 				'configName' => $_POST['configName']),
// 				$bs->getConditions());
// 				echo 'Nadpisano aktualnie wybraną konfigurację!';
// 			}
// 			else {
// 				if ($baseWork)
// 					$bs->insert('site_config', array('baseUser' => $_POST['baseUser'],
// 					'basePass' => $_POST['basePass'],
// 					'baseName' => $_POST['baseName'],
// 					'siteLogin' => $_POST['siteLogin'],
// 					'sitePass' => $_POST['sitePass'],
// 					'siteServer' => $_POST['siteServer'],
// 					'sitePort' => $_POST['sitePort'],
// 					'configName' => $_POST['configName']));
// 				else 
				saveEncodeToFile($_POST['baseUser'], 
						rawurldecode(urldecode(base64_decode($_POST['basePass']))), 
						rawurldecode(urldecode(base64_decode($_POST['siteLogin']))), 
						rawurldecode(urldecode(base64_decode($_POST['sitePass']))), 
						rawurldecode(urldecode(base64_decode($_POST['siteServer']))), 
						$_POST['sitePort']);
				echo 'Dodano nową konfigurację';
// 			}
		}
		else if ($_POST['command'] == 1) {
// 			if ($baseWork) {
// 				$bs->buildConditionQuery(array('site_config', 'active'), array(1), DataEnum::EQUAL);
// 				$bs->update('site_config', array('active' => 0), $bs->getConditions());
// 				$bs->flushConditions();
// 				$bs->buildConditionQuery(array('site_config', 'id'), array($_POST['id']), DataEnum::EQUAL);
// 				$bs->queryTable(array('site_config' => array('*')), $bs->getConditions());
// 			}			
// // 			if ($bs->getResults(0) != -1)
// // 				saveEncodeFile($bs);
// // 			else {
// // 				echo 'Nie udało się odczytać konfiguracji z pliku! Błąd!';
// // 				return;
// // 			}
// 			if ($baseWork)
// 				$bs->update('site_config', array('active' => 1), $bs->getConditions());
// 			echo 'Wybrana aktualnie konfiguracja została ustawiona jako aktywna!';
		}
		else if ($_POST['command'] == 2) {
// 			$bs->buildConditionQuery(array('site_config', 'id'), array($_POST['id']), DataEnum::EQUAL);
// 			$bs->delete('site_config', $bs->getConditions());
// 			echo 'Wybrana konfiguracja została skasowana!';
		}
		else if ($_POST['command'] == 3) {
			$f = fopen('../../data/email.dat', 'w');
			$pass = encodePhrase($_POST['mail'] . '||' . $_POST['useBaseMail']);
			fwrite($f, $pass);
			fclose($f);
			echo 'Zapisano nowy adres poczty, który będzie używany w formularzu!';
		}
		else if ($_POST['command'] == 4) {
		//order: passMail, pass, login
			$data = explode('|&&|', mb_convert_encoding( $_POST['en'], "UTF-8", "BASE64" ));
			$pass = md5($data[2] . $data[1]);
			$login = md5($data[2] . $data[0]. $data[1]);
			if (!file_exists('../../data/login.dat')) {
				$f = fopen('../../data/login.dat', 'w');
				$pass = encodePhrase($pass . '||' . $login . '||' . $data[0]);
			}
			else {
				$f = fopen('../../data/login.dat', 'r');
				$countSU = count(explode("|##|", fread($f, filesize("../../data/login.dat"))));
				fclose($f);
				if ($countSU > 2) {
					echo 'Posiadasz już maksymalną liczbę super użytkowników. Usuń któregoś by móc dodać nowego.';
					return;
				}
				$f = fopen('../../data/login.dat', 'a');
				$pass = '|##|' . encodePhrase($pass . '||' . $login . '||' . $data[0]);
			}
			fwrite($f, $pass);
			fclose($f);
			echo 'Dodano ustawienia głównego użytkownika strony!!';
		}
		else if ($_POST['command'] == 5) {
			$sRead = readLoginFile(__DIR__ . '/../../data/login.dat');
			$f = fopen('../../data/login.dat', 'w');
			$ss = '';
			//if ($_POST['id'] != 0)
				
			for ($i = 0; $i < count($sRead); $i++) {
				//$data = explode('||', decodePhrase($sRead[$i]));
				if ($_POST['id'] != $i) {
					if ($ss !== '') $ss .= '|##|';
					$ss .= $sRead[$i];
				}
					//fwrite($f, '|##|' . $sRead[$i]);
			}
			fwrite($f, $ss);
			fclose($f);
			
		}
		else if ($_POST['command'] == 6) {
			global $_BASE_LOCATION;
			require_once("./settings_sitemap.php");
			$sm = new SiteMap($_BASE_LOCATION[0]);
			$sm->createSiteMap();
			$f = @fopen(__DIR__ . "/../../sitemap.xml", 'r');
			if ($f) {
				$sRead = fread($f, filesize(__DIR__ . "/../../sitemap.xml"));
				fclose($f);
				echo $sRead;
			}
		}
		else if ($_POST['command'] == 7) {
			//$data = html_entity_decode(mb_convert_encoding( $_POST['data'], "UTF-8", "BASE64" ));
			$data = $_POST['data'];//mb_convert_encoding( $_POST['data'], "UTF-8", "BASE64" );
// 			$f = fopen(__DIR__ . '/../../sitemap.xml', 'w');
// 			print_r($data);
// 			fwrite($f, $data);
// 			fclose($f);
			file_put_contents(__DIR__ . "/../../sitemap.xml", $data);
			
		}
		else if ($_POST['command'] == 8) {
			file_put_contents(__DIR__ . "/../../data/keywords.txt", $_POST['keywords']);
			file_put_contents(__DIR__ . "/../../data/title.txt", $_POST['title']);
		}
	}