<style>
	.buttonCSS {
		background: blue;
		border: 2px solid lightblue;
		color: white;
		float: left;
		padding: 0 20px;
		cursor: pointer;
		overflow: hidden;
	}
	.buttonCSS:hover p{
/* 		font-weight: bold; */
	}
	.buttonListStyle {
		width: 40px;
		height: 100%;
		margin-right: 15px;
		float: left;
		overflow: hidden;
		text-align: center;
		font-size: 20px;
	}
	.buttonListStyle p i {
		position: relative;
		top: -10px;
	}
/*	.mainNode .textEditField::before {
		content: '<b>Tekst_test</b>';
	}*/
	.inactive {
		background: gray;
	}
	.mainNode {
		position: relative;
		clear: both;
		height: 40px;
		width: 95%;
		overflow: hidden;
	}
	.mainNode p {
		cursor: pointer;
		position: relative;
		top: -5px;
	}
	.mainNode:hover {
		border: 2px solid black;
	}
	.hideBtn {
		display: none;
	}
	.listIconStyle {
		width: 40px;
		position: relative;
/* 		font-size: 24px; */
	}
	.listIconStyle i {
		font-size: 24px;		
		position: absolute;
		top: -10px;
		left: 8px;
	}
	.infoOperation {
		background: red;
		font-weight: bold;
		font-size: 40px;
		max-width: 50%;
	}
	.infoOperation p {
		background: red;
	}
	#output {
		clear: both;
	}
	#output::after, .buttonCSS::after, .mainNode::after {
		content: '';
		clear: both;
		display: table;
	}
	#tipLabel, #dialogBox {
		position: absolute;
		border: 2px solid gray;
		display: none;
		background: white;
	}
</style>
<?php 

	include "../html/categories.html";
	require_once "../scripts/forms.php";
	
	//createDivButton('searchEmptyBtn', 'Wyświetl wszystkie nieużywane kategorie i podkategorie', 'Kliknij by wyszukać nieużyteczne pola celem szybszego uporządkowania', 'buttonCSS', array('onclick' => "alert('Działam!')"));
?>
<link rel="stylesheet" href="./css/elements.css"/>
<link rel="stylesheet" href="./css/categories.css"/>
<div id="output"></div>
<div id="tipLabel"></div>
<div id="dialogBox"></div>
<script>
	//getCategories();
</script>