<?php
	require_once('./utilityFunctions.php');
	confirmSession();
	//pliki wymagane, jednak nie ma sensu podłaczać ich wielokrotnie
	require_once "../../scripts/database.php";
	require_once "../../config.php";
	require_once "../../scripts/utilityFunctions.php";
	
	if (!isset($_SESSION['limit']['courseHandler']))
		$_SESSION['limit']['courseHandler'] = 0;
	if (isset($_POST['startNum'])) 
		$_SESSION['limit']['courseHandler'] = $_POST['startNum'];
	
	$resultsArray = [];
	$limit = 50;
	$totalCourses = 0;
	
	$sitesShow = false;
	
	if (isset($_GET['s'])) {
		if ($_GET['s'] == 1)
			$sitesShow = true;
	}
	
	function createActionButtons($hide = 0, $n = [], $icons = [], $actions = []) {
		$btn = createNodeButtons($hide, $n, $icons, [], $actions);
		$btnTxt = '';
		for ($i = 0; $i < count($btn); $i++) {
			$btnTxt .= "<div style=\"position: relative;\"";
			if (isset($btn[$i]['style']))
				$btnTxt .= " class=\"{$btn[$i]['style']}\"";
			if (is_array($btn[$i]['events'])) {
				$keys = array_keys($btn[$i]['events']);
				for ($j = 0; $j < count($keys); $j++) {
					$btnTxt .= " {$keys[$j]}='" . $btn[$i]['events'][$keys[$j]] . ";'";
				}
			}
			$btnTxt .= ">";
			if (isset($btn[$i]['icon'])) {
				$btnTxt .= "<p";
				if ($btn[$i]['name'])
					$btnTxt .= " data-tip='{$btn[$i]['name']}'";
				$btnTxt .= ">{$btn[$i]['icon']}</p>";
			}
			else 
				$btnTxt .= "<p>{$btn[$i]['name']}</p>";
			$btnTxt .= "</div>";
		}
		return $btnTxt;
	}
	
	function indexOf($table, $search) {
		$maxCount = count($table);
		$max = (((int) ($maxCount--/2)) + 1);
		for ($i = 0; $i < $max; $i++) {
			if ($table[$i] == $search)
				return $i;
			if ($table[$maxCount - $i] == $search)
				return $maxCount - $i;
		}
		return -1;
	}
	
	function coursesDatabaseMaintenance(&$resultsArray, &$totalCourses, $training = false) {
		$base = new Database();
		$base->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME);
		//setExpiredDate($base);
		if (isset($_POST['deleteId'])) {
			$visitDelId = explode(',', $_POST['deleteId']);
			$base->buildConditionQuery(array('courses', 'course_id'), $visitDelId, DataEnum::IN);
			$base->delete('courses', $base->getConditions());
			//$base->update('visits', array('deleted' => 1), $base->getConditions());
			$base->flushConditions();
		}
// 		if (isset($_POST['confirmId'])) {
// 			$visitDelId = explode(',', $_POST['confirmId']);
// 			$base->buildConditionQuery(array('visits', 'id'), $visitDelId, DataEnum::IN);
// 			$base->update('visits', array('confirmed' => 1), $base->getConditions());
// 			$base->flushConditions();
// 		}
// 		if (isset($_POST['disableId'])) {
// 			$visitDelId = explode(',', $_POST['disableId']);
// 			$base->buildConditionQuery(array('visits', 'id'), $visitDelId, DataEnum::IN);
// 			$base->update('visits', array('editable' => 1), $base->getConditions());
// 			$base->flushConditions();
// 		}
		$base->buildConditionQuery(array('categories', 'active'), array(1), DataEnum::EQUAL);
		if (!$training)
			$base->buildConditionQuery(array('categories', 'category_type'), array(0), DataEnum::EQUAL, DataEnum::DAND);
		else 
			$base->buildConditionQuery(array('categories', 'category_type'), array(1), DataEnum::EQUAL, DataEnum::DAND);
		$base->queryTable(array('categories' => array('name', 'category_id')), $base->getConditions(), -1);
		if ($base->getResults(0) == -1)
			return;
		$base->flushConditions();
		$base->buildConditionQuery(array('subcategories', 'active'), array(1), DataEnum::EQUAL);
		$base->buildConditionQuery(array('subcategories', 'category_id'), $base->getResults(0)['category_id'], DataEnum::IN, DataEnum::DAND);
		$base->queryTable(array('subcategories' => array('name', 'category_id', 'subcat_id')), $base->getConditions(), -1);
		if ($base->getResults(1) == -1)
			return;
		$base->flushConditions();
		$base->buildConditionQuery(array('courses', 'subcat_id'), $base->getResults(1)['subcat_id'], DataEnum::IN);
		
		//poniżej kod trzeba będzie poprawić; aktualnie sprawdza ilość wszystkich kursów, w tym 
		//szkoleń; powinno sprawdzać tylko wybraną kategorię!!
		$totalCourses = $base->countQuery(array('courses' => array('course_id')), $base->getConditions());
		if(isset($_SESSION['limit']['courseHandler'])) {
			if($_SESSION['limit']['courseHandler'] == -1)
				$_SESSION['limit']['courseHandler'] = $totalCourses - 50;	
		}
		else {
			$_SESSION['limit']['courseHandler'] = 0;
		}
		if ($totalCourses > $_SESSION['limit']['courseHandler'] + 50) 
			$_SESSION['limit']['nextSite'] = true;
		else
			$_SESSION['limit']['nextSite'] = false;
		$base->queryTable(array('courses' => array('*')), $base->getConditions(), 50, $_SESSION['limit']['courseHandler']);
		if ($base->getResults(2) == -1)
			return;
		$base->flushConditions();
		
		for ($i = 0; $i < count($base->getResults(2)['course_id']); $i++) {
			$resultsArray[$i]['course_id'] = $base->getResults(2)['course_id'][$i];
			$resultsArray[$i]['start'] = $base->getResults(2)['course_startdate'][$i];
			$resultsArray[$i]['end'] = $base->getResults(2)['end_date'][$i];
			$resultsArray[$i]['active'] = $base->getResults(2)['active'][$i];
			$resultsArray[$i]['topic'] = $base->getResults(2)['topic'][$i];
			$resultsArray[$i]['objectives'] = $base->getResults(2)['objectives'][$i];
			$resultsArray[$i]['additional_info'] = $base->getResults(2)['additional_info'][$i];
			$resultsArray[$i]['show_all_photo'] = $base->getResults(2)['show_all_photo'][$i];
			$resultsArray[$i]['prices'] = $base->getResults(2)['prices'][$i];
			$resultsArray[$i]['user_id'] = $base->getResults(2)['user_id'][$i];
			$resultsArray[$i]['subcat_id'] = $base->getResults(2)['subcat_id'][$i];
			$subID = indexOf($base->getResults(1)['subcat_id'], $base->getResults(2)['subcat_id'][$i]);
			if ($subID != -1) {
				$resultsArray[$i]['subcategory'] = $base->getResults(1)['name'][$subID];
				$resultsArray[$i]['subcategoryID'] = $subID;
				$catID = indexOf($base->getResults(0)['category_id'], $base->getResults(1)['category_id'][$subID]);
				if ($catID != -1) {
					$resultsArray[$i]['category'] = $base->getResults(0)['name'][$catID];
					$resultsArray[$i]['categoryID'] = $catID;
				}
				else {
					$resultsArray[$i]['category'] = '';
					$resultsArray[$i]['categoryID'] = -1;
				}
			}
			else {
				$resultsArray[$i]['category'] = '';
				$resultsArray[$i]['categoryID'] = -1;
				$resultsArray[$i]['subcategory'] = '';
				$resultsArray[$i]['subcategoryID'] = -1;
			}
		}
	}
	
	function coursesCreateTableContent(&$resultsArray, &$limit, $training = false) {
		if ($_SESSION['limit']['courseHandler'] == 0) {
			echo '<div class="settingsSetDiv">
				<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="' . (($training == false) ? 'coursesTableAddEdit(false, false, this);' : 'coursesTableAddEdit(true, false, this);') . '">
					<div style="float: left;"><p>' . (($training == false) ? 'Dodaj kurs' : 'Dodaj szkolenie') . '</p></div>
					<div style="float: right; width: 170px; height: 100%; overflow:hidden; font-size: 15px;">' . 
					createActionButtons(false, [(($training == false) ? 'Dodaj kurs' : 'Dodaj szkolenie')], ['<i class="fa fa-plus" aria-hidden="true"></i>'], 
					['onclick' => array((($training == false) ? 'coursesTableAddEdit(false, false, this);' : 'coursesTableAddEdit(true, false, this);'))]) .'</div>
				</div>
			</div>';
			echo "<div>Filtruj<input type='search' name='filter' onchange=''/></div>";
		}
		if ($_SESSION['limit']['courseHandler'] == 0)
			echo '<div id="courseTraining">';
// 		else 
// 			echo '<div id="newCourseTraining">';
		$btnIcons = [0 => '<i class="fa fa-cog" aria-hidden="true"></i>', 2 => '<i class="fa fa-trash" aria-hidden="true"></i>'];
		$btnName = [0 => 'Edytuj wiadomość', 2 => 'Usuń wiadomość (NA ZAWSZE)'];
		$btnActions = ['onclick' => array("event.stopPropagation(); coursesTableAddEdit(" . (($training === false) ? "false" : "true") . ", true, this)", 
						"event.stopPropagation(); showHideCourses(this)", 
						"event.stopPropagation(); coursesTableDelete(this, " . (($training === false) ? "false" : "true") . ")") ];
		for ($i = 0; $i < count($resultsArray); $i++) {
			if ($resultsArray[$i]['active'] == 1) {
				$btnIcons[1] = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
				$btnName[1] = 'Ukryj kurs';
			}
			else {
				$btnIcons[1] = '<i class="fa fa-eye" aria-hidden="true"></i>';
				$btnName[1] = 'Pokaż kurs';
			}
			echo '<div class="settingsSetDiv">
				<div class="settingsBody" data-subcatid="' . $resultsArray[$i]['subcat_id'] . '" data-active="' . $resultsArray[$i]['active'] . '" data-userid="' . $resultsArray[$i]['user_id'] . '" data-id="' . $resultsArray[$i]['course_id'] . '" data-topic="' . $resultsArray[$i]['topic'] . '" data-datestart="' . $resultsArray[$i]['start'] . '" data-dateexp="' . $resultsArray[$i]['end'] . '" data-showphoto="' . $resultsArray[$i]['show_all_photo'] . '" onclick="coursesTableAddEdit(' . (($training === false) ? "false" : "true") . ', true, this);">
					<div style="float: left;"><p>' . ($resultsArray[$i]['topic'] !== '' ? $resultsArray[$i]['topic'] : '{brak tematu}')  . ($resultsArray[$i]['active'] == 1 ? ' (aktywna)' : ' (nieaktwyna)') . '</p></div>
					<div class="buttonIconDiv">' . 
					createActionButtons($resultsArray[$i]['active'], $btnName, $btnIcons, $btnActions) .'</div>
					<div style="display: none;">' . $resultsArray[$i]['objectives'] . '</div>
					<div style="display: none;">' . $resultsArray[$i]['additional_info'] . '</div>
					<div style="display: none;">' . $resultsArray[$i]['prices'] . '</div>
				</div>
			</div>';
		}
		if ($_SESSION['limit']['courseHandler'] == 0)
			echo '</div>';
		if ($_SESSION['limit']['nextSite']) {
			echo '<div id="moreCourses" class="settingsSetDiv">
			<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="getCoursesTable(' . ($_SESSION['limit']['courseHandler'] + 50) . ', ' . (($training === false) ? 'false' : 'true') . ');">
				<div style=""><p>Następne '. (($training === false) ? 'kursy' : 'szkolenia') . '</p></div>				
			</div>';
		}
		
	}
	
	if (!$sitesShow) 
		coursesDatabaseMaintenance($resultsArray, $totalCourses);
	else
		coursesDatabaseMaintenance($resultsArray, $totalCourses, true);
	if (!$sitesShow) 
		coursesCreateTableContent($resultsArray, $totalCourses);
	else
		coursesCreateTableContent($resultsArray, $totalCourses, true);
	?>

