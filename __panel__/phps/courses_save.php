<?php 
//DO ZROBIENIA!!!!
	require_once('./utilityFunctions.php');
	require_once "../../scripts/database.php";
	require_once "../../config.php";
	require_once "../../scripts/utilityFunctions.php";
	require_once "../../scripts/search.php";
	confirmSession();
	
	//print_r($_POST);
	if (isset($_POST['form'])) {
		$mod = false;
		$bs = new Database();
		$bs->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME);
		if (count($_POST['form']) > 0)
			$k = array_keys($_POST['form'][0]);
		else
			return 'Nic nie zostało przesłane do dodania/aktualizacji!';

		for ($i = 0; $i < count($_POST['form']); $i++) {
			$catID = -1;
			$subCatID = -1;
			$data = $_POST['form'][$i];
			
			if (!is_numeric($data['category-name'])) {				
				$categoryArray = array('name' => $data['category-name'],
						       'description' => $data['category-description'],
						       'category_type' => $data['category_type'],
						       'user_id' => $data['user_id'],
						       'active' => 1);
				$bs->insert('categories',$categoryArray);
				$bs->buildConditionQuery(array('categories', 'category_id'), array(), DataEnum::ORDERDESC);
				$bs->queryTable(array('categories' => array('category_id')), $bs->getConditions(), 1);
				$bs->flushConditions();
				$catID = $bs->getResults(0)['category_id'][0];
				$bs->flushResults();				
			}
			else	
				$catID = (int)$data['category-name'];
			if (!is_numeric($data['subcategory-name'])) {
				$categoryArray = array('name' => $data['subcategory-name'],
						       'description' => $data['subcategory-description'],
						       'category_id' => $catID,
						       'user_id' =>$data['user_id'],
						       'active' => 1);
				$bs->insert('subcategories',$categoryArray);
				$bs->buildConditionQuery(array('subcategories', 'subcat_id'), array(), DataEnum::ORDERDESC);
				$bs->queryTable(array('subcategories' => array('subcat_id')), $bs->getConditions(), 1);
				$bs->flushConditions();
				$subCatID = $bs->getResults(0)['subcat_id'][0];
				$bs->flushResults();			
			}
			//odpowiada za wstawienie subkategorii IDENTYCZNEJ do kategorii;
			//przypadek gdy kurs/szkolenie nie jest przypisane do subkategorii (raczej rzadkość)
			//SPRAWDZIC TEN KOD CZY JEST W PORZĄDKU!!!!!!!!!!
			//ZMIENIC WYSWIETLANIE KATEGORII NA STRONIE!!!
			else if (is_numeric($data['subcategory-name']) && $data['subcategory-name'] == 0) {
			//echo 'TUTAJ!!!!!!';
// 				$bs->buildConditionQuery(array('subcategories', 'category_id'), array($catID), DataEnum::EQUAL);
// 				$bs->buildConditionQuery(array('subcategories', 'no_sub_cat'), array(1), DataEnum::EQUAL, DataEnum::DAND);
// 				//$bs->buildConditionQuery(array('subcategories', 'name'), array($data[ $keys[0] ]), DataEnum::EQUAL, DataEnum::DAND);
// 				$bs->queryTable(array('subcategories' => array('subcat_id')), $bs->getConditions(), 1);
// 				
// 				if ($bs->getResults(0) < 0) {
					$bs->flushResults();
					$categoryArray = array(/*'name' => $data['category-name'],*/
// 						       'description' => $data['category-description'],
						       'category_id' => $catID,
						       'no_sub_cat' => 1,
						       'user_id' => $data['user_id'],
						       'active' => 1);
					$bs->insert('subcategories',$categoryArray);
					$bs->buildConditionQuery(array('subcategories', 'subcat_id'), array(), DataEnum::ORDERDESC);
					$bs->queryTable(array('subcategories' => array('subcat_id')), $bs->getConditions(), 1);
					$bs->flushConditions();
					//print_r($bs->getResults(0)['subcat_id'][0]);
					$subCatID = $bs->getResults(0)['subcat_id'][0];
					//$bs->flushResults();
/*				}
				else {
					$subCatID = $bs->getResults(0)['subcat_id'][0];
					//$bs->flushResults();
				}*/	
				$bs->flushResults();
			}
			else
				$subCatID = $data['subcategory-name'];
			//dorobic jakis znacznik dlaczego przerywamy tutaj dzialanie!!
			if ($subCatID == -1 || $catID == -1)
				return;
            $data['topic'] = str_replace('||..||','+',$data['topic']);
            $data['objectives'] = str_replace('||..||','+',$data['objectives']);
            $data['additional_info'] = str_replace('||..||','+',$data['additional_info']);
            $data['prices'] = str_replace('||..||','+',$data['prices']);
			$addArray = array('user_id' => $data['user_id'],
							'subcat_id' => $subCatID,
							'course_startdate' => $data['course_startdate'],							
							'objectives' => $data['objectives'],
							'additional_info' => $data['additional_info'],
							'prices' => $data['prices'],
							'topic' => $data['topic'],
							'show_all_photo' => $data['show_all_photo'],
							//'category_type' => $_POST['form'][$i]['category_type'],
// 							'user_id' => $data[ $keys[10] ],
							'active' => $data['active']);
            if (isset($data['end_date']) && $data['end_date'] != '')
                $mod = 0;
                $addArray['end_date'] = $data['end_date'];
			if (isset($data['course_id'])) {
				$mod = $data['course_id'];
				$bs->buildConditionQuery(array('courses', 'course_id'), array($data['course_id']), DataEnum::EQUAL);
				$bs->update('courses', $addArray, $bs->getConditions());
				$bs->flushConditions();
			}
			else
				$bs->insert('courses',$addArray);
		}
		
		if ($mod > 0)
			echo "Zmodyfikowano wskazane kursy/szkolenia!";
		else {
			$bs->buildConditionQuery(array('courses', 'topic'), array($data['topic']), DataEnum::EQUAL);
			$bs->buildConditionQuery(array('courses', 'subcat_id'), array($subCatID), DataEnum::EQUAL, DataEnum::DAND);
			$bs->buildConditionQuery(array('courses', 'objectives'), array($data['objectives']), DataEnum::EQUAL, DataEnum::DAND);
			$bs->buildConditionQuery(array('courses', 'additional_info'), array($data['additional_info']), DataEnum::EQUAL, DataEnum::DAND);
			$bs->queryTable(array('courses' => array('course_id')), $bs->getConditions(), -1);
			$bs->flushConditions();
			$mod = $bs->getResults(0)['course_id'][ count($bs->getResults(0)['course_id']) - 1 ];
			$bs->flushResults();
			echo "Dodano nowy kurs/szkolenie!";
		}
// 		$bs->buildConditionQuery(array('words_sites', 'site'), array('courses/' . $mod), DataEnum::EQUAL);
// 		$bs->delete('words_sites', $bs->getConditions());
		$bs->flushConditions();
		$search = new IndexLib($bs);
// 		$data['additional_info'] = preg_replace('|<[^>]+>(.*)</[^>]+>|U', '', $data['additional_info']);
		$data['additional_info'] = preg_replace('|<[^>]+>|U', '', $data['additional_info']);
		$phraseAdd = $data['topic'] . ' ' . $data['objectives'] . ' ' . $data['additional_info'];
		if ($data['prices'] !== '') $phraseAdd .= ' ' . $data['prices'];
		if ($data['course_startdate'] !== '') $phraseAdd .= ' ' . $data['course_startdate'];
		$search->addPhrase(0, 'courses/' . $mod, $phraseAdd);
		
	}
	else if(isset($_POST['hideValue'])) {
		$bs = new Database();
		$bs->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME);
		$bs->buildConditionQuery(array('courses', 'course_id'), array($data['hideID']), DataEnum::EQUAL);
		$bs->update('courses', array('active' => $_POST['hideValue']), $bs->getConditions());
	}
