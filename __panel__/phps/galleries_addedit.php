<?php 
	require_once('./utilityFunctions.php');
// 	require_once "../../scripts/database.php";
	require_once "../../config.php";
// 	require_once "../mailFunctions.php";
	require_once "../../scripts/utilityFunctions.php";
	require_once "../scripts/forms.php";
	//confirmSession();
	
	function createGalleryForm($data = '', $sites = '') {
		$siteID = -1;
		if ($data !== '')
			if ($data['site_id'][0] !== '' || $data['site_id'][0] !== null || $data['site_id'][0] !== 0)
				$siteID = $data['site_id'][0];			
		echo "<div id='galleryForm' style='overflow: hidden;'>";
		echo createNewTextInput("name", ($data === '' ? '' : $data['name'][0]), 'Nazwa nowej galerii', 'Wpisz nazwę galerii', 'first afterBlock beforeBlock', 'Nazwa galerii nie będzie wyświetlana na stronie; służy jedynie jako nazwa ułatwiająca segregację zdjęć');
		echo createNewTextInput("description", ($data === '' ? '' : $data['description'][0]), 'Opis galerii (opcjonalny)', 'Dodatkowe informacje o galerii', 'afterBlock beforeBlock', 'Opis galerii ułatwiający jej identyfikację/przeznaczenie. Nie pojawia się na stronie docelowej');
		echo createNewSelectInput("site_id", ($sites === '' ? '' : $sites), ($data === '' || $siteID === '0' ? '-1' : $siteID), 'Strona, do której dowiązana ma zostać galeria', array('onchange' => 'gallerySiteBelong(this);'), 'afterBlock beforeBlock', 'To ustawienie pozwala dowiązać galerię do dowolnej podstrony witryny. Przykładowo przywiązanie strony do kursów będzie wyświetlać zdjęcia z zakładanej galerii na podstronie kursów.');
		echo createNewTextInput("site_name", '', 'Nazwa nowej strony (np. m_contact)', 'Nazwa podstrony, do której przywiązana zostanie galeria', 'afterBlock beforeBlock none', 'Jeżeli strona dowiązania nie znajduje się na liście powyżej, to tutaj można ręcznie wprowadzić nazwę takowej. Nazwą jest np. m_promotions, m_contact itp.');
		echo createNewSelectInput("active", array(1 => 'Tak', 0 => 'Nie'), ($data === '' ? '1' : $data['active'][0]), 'Galeria aktywna', '', 'afterBlock beforeBlock', 'Wskazuje, czy galeria ma wyświetlać się w zakładce Galeria na stronie. Należy pamiętać, że nie eliminuje to wyświetlania się galerii przy wskazanej stronie ani wyświetlania z niej zdjęć np. w artykułach, wiadomościach itp.');
		
		createHiddenInput("user_id", 1);
		if (isset($data['gallery_id'][0])) 
			createHiddenInput("gallery_id", $data['gallery_id'][0]);
		echo "</div>";
	}
	
	function createGallerySetElementsForm($galleryData = '', $photosData = '', $dataID = 0) {
	//DOKONCZYC!!
		echo "<fieldset class='elementsDiv' " . (($photosData !== '') ? '' : "data-null='1'") . " data-id='{$dataID}' style='overflow: hidden;'>";
		echo "<legend onclick='removePicture(this);'><i class=\"fa fa-minus-square\" aria-hidden=\"true\" style='font-size: 17px; float:inherit;'></i>KLIKNIJ TU BY USUNĄĆ TO ZDJĘCIE!</legend>";
		echo createNewTextInput("description", (isset($photosData['alt']) == true ? $photosData['alt'] : ''), 'Opis zdjęcia (opcjonalny)', 'Dodatkowe informacje o zdjęciu', 'afterBlock beforeBlock', 'Opis zdjęcia, który widoczny będzie zarówno w galerii jak i jako opis alternatywny zdjęcia. Opisy alternatywne poprawiają pozycjonowanie w największych wyszukiwarkach internetowych (stąd warto go uzupełnić).');
		echo createNewFileInput("path", ''/*($photosData === '' ? '' : $photosData['path'][0])*/, 'Dodawane zdjęcie', 'Wskaż zdjęcie do dodania', 'afterBlock beforeBlock', array('onchange' => 'uploadFile(event);'), 'Wypełnienie tego pola powoduje automatyczne przesłanie wybranego pliku na serwer WWW. Zdjęcie zostaje trwale zapisane dopiero po kliknięciu przycisku Dodaj na końcu formularza.');
		echo createNewFileInput("path_mini", ''/*($photosData === '' ? '' : $photosData['path_mini'][0])*/, 'Miniatura zdjęcia', 'miniatura (opcjonalna)', 'afterBlock beforeBlock', array('onchange' => 'uploadFile(event, true);'), 'Tutaj można dodać własną miniaturę do przesyłanego zdjęcia. Jeżeli to pole pozostanie puste to miniatura wygeneruje się smoczynnie (zoptymalizowana). Miniatury zdjęć są o tyle ważne, że znacznie szybciej ładują się w galeria (w przeciwieństwie do dużych zdjęć).');
		createCheckBox('splash', '1', (isset($photosData['splash']) ? '1' : ''), 'Zdjęcie widoczne na głównej stronie', 'Jeżeli ta opcja zostanie zaznaczona to zdjęcie będzie jednym z widocznych na szczycie strony WWW');
		$addToCourse = -1;
		createHiddenInput("user_id", 1);
		if ($galleryData !== '')
			createHiddenInput("gallery_folder", $galleryData['gallery_folder'][0]);
		else
			createHiddenInput("gallery_folder", '');
		createHiddenInput("gallery_id", $_POST['setID']);
		if ($photosData !== '')
			createHiddenInput("photo_id", $dataID);
		//$photosData = explode('||', $photosData);
		echo "<div><div style='display: inline-block;'><img id='preview-{$dataID}' style='max-width: 500px; max-height: 300px;'";
		if (isset($photosData['photo']))
			echo " src='../galleries/". explode('/', $photosData['photo'])[2] . "'";
		echo "/></div>";
		echo "<div style='display: inline-block;'><img id='previewMini-{$dataID}' style='width: 300px; height: 200px;'";
		if (isset($photosData['mini']))
			echo " src='../galleries/". explode('/', $photosData['mini'])[2] . "'";
		echo "/></div>";
		echo "</div>";
		echo "</fieldset>";
	}
	
	function indexOf($table, $search) {
		$maxCount = count($table);
		$max = (((int) ($maxCount--/2)) + 1);
		for ($i = 0; $i < $max; $i++) {
			if ($table[$i] == $search)
				return $i;
			if ($table[$maxCount - $i] == $search)
				return $maxCount - $i;
		}
		return -1;
	}
	
	//TEN FRAGMENT MA SIĘ WYKONYWAĆ PRZED WSZYSTKIMI POZOSTAŁYMI - BLOKUJE ON TEŻ POZOSTAŁY KOD SKRYPTU!!
	if(isset($_POST['newelem'])) {
		createGallerySetElementsForm($_POST['newelem'], '', $_POST['elemID']);
		return;
	}
	
// 	$bs = new Database();
// 	$bs->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME);
	if (!isset($_GET['s'])) {
		//$bs->queryTable(array('sites' => array('siteBelong', 'description', 'site_id')), "", -1);
		//$sites = array('-1' => 'Żadna', '0' => 'Nowa');
		//if ($bs->getResults(0) != -1) {
		//	for ($i = 0; $i < count($bs->getResults(0)['site_id']); $i++) {
		//		$sites[ $bs->getResults(0)['site_id'][$i] ] = $bs->getResults(0)['siteBelong'][$i] . ' (' . $bs->getResults(0)['description'][$i] . ')';
		//	}
		//}
		//$bs->flushResults();
		
// 		if (isset($_POST['fields'])) {
// 			if ($_POST['fields'] == '') {
// 				//ponieważ żadna galeria nie została wybrana
// 				echo '-100';
// 				return;
// 			}
// 				//$fieldsArray = explode(',', $_POST['fields']);			
// 				////$bs->buildConditionQuery(array('galleries', 'gallery_id'), array($_POST['fields']), DataEnum::IN);
// 				//$bs->queryTable(array('galleries' => array('*')), $bs->getConditions(), -1);
// 				//$bs->flushConditions();	
// 				//w tej chwili nie działa uzupełnianie/edycja wielu galerii
// 				//jednak w toku późniejszym może się to przydać (np. nadal pobierane są wszystkie pasujące galerie)
// 				//dlatego poniższy for pozostaje
// 				//for ($i = 0; $i < count($base->getResults(0)['news_id']); $i++) 
// 					createGalleryForm($bs->getResults(0), $sites);
// 		}
// 		else {
			createGalleryForm();	
// 		}
		
		
	}
	else {
		//TUTAJ TRZEBA dorobić funkcjonalność umozliwiającą dodanie 
			//zdjęcia dla konkretnego kursu; jednak by to było w miarę wygodne 
			//(później może się to zmieni) należałoby stworzyć kaskadowe wyświetlanie na zasadzie
			//typ(kurs/szkolenie)->kategoria->(opcjonalnie) podkategoria->lista kursów/szkoleń
		//W TRAKCIE REALIZACJI!!!!!!
		
// 		$bs->buildConditionQuery(array('galleries', 'gallery_id'), array($_POST['setID']), DataEnum::EQUAL);
// 		$bs->queryTable(array('galleries' => array('*')), $bs->getConditions(), -1);
// 		$bs->flushConditions();	

		echo "<div id='gallerySetElementsForm' style='overflow: hidden;'>";
		//echo "<h2>Elementy dla galerii: {$bs->getResults(0)['name'][0]}</h2>";
		if (isset($_POST['fields'])) {
			if ($_POST['fields'] == '') {
				
				//ponieważ żadna galeria nie została wybrana
				echo '-100';
				return;
			}
			$photos = explode(',', $_POST['fields']);
			$jsonPhoto = readGalleryEntries('../../json');// 				
			for ($i = 0; $i < count($photos); $i++) 
				createGallerySetElementsForm('', $jsonPhoto[$photos[$i]], $photos[$i]);
		}
		else 			
			createGallerySetElementsForm('');
		echo "</div>";
	}
?>
<style>
	.afterBlock:after, .beforeBlock:before {
		content: '';
		overflow: auto;
		display: table;
		clear: both;
	}
	.selectList {
		overflow: hidden;
	}
	.expired {
		color: red;
	}
	.none {
		display: none;
	}
	.first {
		margin-top: 30px;
	}
</style>

<button onclick="<?php if (!isset($_GET['s'])) echo 'saveGallery();'; else echo 'saveGalleryElements();'; ?>">
<?php if (isset($_POST['fields'])) echo 'Zapisz zmiany'; else {
	if (!isset($_GET['s'])) 
		echo 'Dodaj album'; 
	else
		echo 'Dodaj elementy';
	}?></button>
<button onclick="getGalleriesTable(<?php if (isset($_GET['s'])) echo 'true'; ?>);">Anuluj</button>
<?php if (isset($_GET['s'])) : ?>
<div id='uploadGauge' style="height: 20px; width: 50%; margin-left: auto; margin-right: auto;"><div style='background: blue; height: 100%; width: 0; text-align: center;'><p>Przesłano: 0%</p></div></div>
<?php endif; ?>