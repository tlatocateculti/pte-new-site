<?php 
	if (file_exists(__DIR__ . '/../data/login.dat')) {
		header("Location: index.php");
	}
	header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
	header("Pragma: no-cache"); //HTTP 1.0
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
	</head>
	<body>
		<?php 
			$date = new DateTime("now");
			$date->setTimezone(new DateTimezone("Europe/Warsaw"));
			echo $date->format("d/m/Y H:i");

			if (isset($_POST['en'])) {
				$data = explode('|&&|', mb_convert_encoding( $_POST['en'], "UTF-8", "BASE64" ));
				$pass = md5($data[2] . $data[1]);
				$login = md5($data[2] . $data[0]. $data[1]);
				if (!file_exists('../../data/login.dat')) {
					$f = fopen('../../data/login.dat', 'w');
					$pass = encodePhrase($pass . '||' . $login . '||' . $data[0]);
					fwrite($f, $pass);
					fclose($f);
				}
			}
		?>
		<script src="./js/functions.js"></script>
		<script src="./js/settings.js"></script>
		<div id='mainUserSettingsForm'>
			<div><span>Login</span><input type='password' name='login'/></div>
			<div><span>Hasło</span><input type='password' name='pass'/></div>
			<div><span>Potwierdzenie hasła</span><input type='password' name='repass'/></div>
			<div><span>Adres poczty (na wypadek zgubienia hasła)</span><input type='text' name='passMail'/></div>
			<div><span>Adres poczty (potwierdzenie)</span><input type='text' name='rePassMail'/></div>
			<div><button onclick='appendUserSettings();'>Zatwierdź</button><button onclick="resetSettings('mainUserSettingsForm');">Wyczyść pola</button></div>
		</div>
	</body>
</html>