<?php	
	require_once('../config.php');
	require_once('./utilityFunctions.php');
	//confirmSession();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex, nofollow"/>
	<link rel="icon" type="image/x-icon" href="ikona.ico"/>
	<title>Lange</title>
	<link type="text/css" href="../css/font-awesome_panel.css" rel="stylesheet">
	<!--<link type="text/css" href="../styl.css" rel="stylesheet">-->
	<link type="text/css" href="./css/main.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Domine&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<!-- 	<script src="../scripts.js"></script> -->
	<!--<script src="../functions.js"></script>-->
	<script src="./js/functions.js"></script>
<!-- 	<script src="http://maps.google.com/maps/api/js" onload=""></script> -->
	<!--<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.2/dist/leaflet.css" />
	<script src="https://unpkg.com/leaflet@1.0.2/dist/leaflet.js"></script>-->
	<link rel="stylesheet" href="https://openlayers.org/en/v3.20.1/css/ol.css" type="text/css">
	<script src="https://openlayers.org/en/v3.20.1/build/ol.js"></script>
	<script src="../js/maps.js" onload=""></script>
</head>

<body>	
	<div style="width: 100%; height: 100%;">
		<div id="content">
			<?php 
				//if (/*$_SERVER['QUERY_STRING'] == "pos" &&*/ (isset($_SESSION['login']) && isset($_SESSION['unconfirm'])) ) {
					if ($_SERVER['QUERY_STRING'] == "") {
						//echo $_SESSION['login'];
						include('./html/welcome.html');
					}
					else
						loadSite($_SERVER['QUERY_STRING']);
				//}
				//else if ((!isset($_SESSION['login']) || !isset($_SESSION['unconfirm']))) 
				//	include('./html/login.html');
				//else
				//	echo "<p>PUSTA STRONA!</p>";
			?>
		</div>
<!--		<div id="floatingPanel">
			<p>Panel pomocniczy
			
		</div>-->
	</div>
</body>
</html>