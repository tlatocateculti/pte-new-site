<?php
/* Plik odpowiada za przesyłanie poczty z formularza na wskazany przez nasz adres. Dobrym pomysłem byłoby dodanie
   zmiany poczty docelowej przez panel administracyjny
   
   TRZEBA NIEC ZOPTYMALIZOWAĆ KOD!
*/
	//error_reporting(E_ALL);
	require_once ('./utilityFunctions.php');
// 	$recaptcha = true;

	function buildFormJSON($json) {
		$jsonKeys = array_keys($json);
		for ($i = 0; $i < count($jsonKeys); $i++) {
			if ($json[$jsonKeys[$i]]['type'] === 'fieldset') {
				$json = array_merge($json, $json[$jsonKeys[$i]]['fields']);
				unset($json[$jsonKeys[$i]]);
				return buildFormJSON($json);
			}
		}
		return $json;
	}

	$emptyValues = [];
	$destMail = getSettingEmail();
	
	//print_r($destMail);
	if (!$destMail)
		return;
		
	$destMail = explode('||', $destMail);
	
	$keys = array_keys($_POST); 
	if (count($keys) == 0) return;
	$json = readEntries('../json', 'form.json');
	$json = buildFormJSON($json);
	
	for ($i = 0; $i < count($keys); $i++) {
		$_POST[ $keys[$i] ] = str_replace('@@', '+', $_POST[ $keys[$i] ]);
		if (isset($json[$keys[$i]]['mandatory']) && $json[$keys[$i]]['type'] !== 'file' && $_POST[ $keys[$i] ] == "")
			$emptyValues[count($emptyValues)] = $keys[$i];
		else if (isset($json[$keys[$i]]['mandatory']) && isset($json[$keys[$i]]['regexp'])) {
			$matchReg = '/(*UTF8)' . $json[$keys[$i]]['regexp'] . '/' . ((isset($json[$keys[$i]]['regexpParam'])) ? $json[$keys[$i]]['regexpParam'] : '');
			//echo $matchReg . ' ';
			if (!preg_match($matchReg, $_POST[$keys[$i]]))
				$emptyValues[count($emptyValues)] = $keys[$i];
		}
	}
	if (count($emptyValues) > 0) {
		if (isset($_POST['fileSend']))
			removeDir($_POST['fileSend'][0]);
		$retMsg = "error:";
		for ($i = 0; $i < count($emptyValues); $i++)
			$retMsg .= $emptyValues[$i] . '||';
		$retMsg = substr($retMsg, 0, -2);		
		//gdyby dodać recaptcha
// 				if(!$recaptcha)
// 			$retMsg = "<p>Błąd weryfikacji antyspamowej. Spróbuj ponownie.</p>
		echo $retMsg;
		return;
	}
	$mailSON = readEntries('../json', 'mailSettings.json');
	$sendMail = $_POST["email"];
	$sendName = $_POST["name"] . ' ' . $_POST["surname"];
	$message = $_POST["message"];
	$topic = $mailSON['topic'];
	$tmpTopic = explode('%%', $mailSON['topic']);
	for ($i = 1; $i < count($tmpTopic); $i+=2) {
		$topic = str_replace('%%' . $tmpTopic[$i] . '%%', $_POST[$tmpTopic[$i]], $topic);
	}
	for ($i = 0; $i <  count($keys); $i++) {
		if ($keys[$i] !== 'sendMode' && $keys[$i] !== 'lang' && $keys[$i] !== 'message' && $keys[$i] !== 'fileSend')
			$message .= "\n\n" . $json[$keys[$i]]['pl-PL']['placeholder'] . ": " . $_POST[$keys[$i]];
	}
// 	$message = $_POST["message"] . "\n\nNumer telefonu: {$_POST["phone"]}";
	if (isset($destMail[1]))
		if ($destMail[1] == 1) {
			$sendMail = '';
			$sendName = $mailSON['senderName'];//"FORMULARZ ZE STRONY CZESTOCHOWA.PTE.PL";
// 			$message .= "\n\nAdres poczty: {$_POST["email"]}\n\nOsoba przesyłająca: {$_POST["name"]} {$_POST["surname"]}";
		}
	if (!isset($_POST['fileSend']))
		$ret = sendMail($sendMail, 
			$sendName, $destMail[0], 
			$topic,
			//'[' . mb_strtoupper($_POST["topic"]) . '] - czestochowa.pte.pl', 
			//'[CZESTOCHOWA.PTE.PL] - ' . mb_strtoupper($_POST["topic"]), 
			$message);
	else {
		$ret = sendMailAttachment($sendMail, 
			$sendName, $destMail[0], 
			$topic,
			//'[' . mb_strtoupper($_POST["topic"]) . '] - czestochowa.pte.pl', 
// 			'[CZESTOCHOWA.PTE.PL] - ' . mb_strtoupper($_POST["topic"]), 
			$message,
			$_POST['fileSend']);
	}
	$retMsgText = "";
	
	if ($ret === true)
		$retMsgText .= $mailSON[$_POST['lang']]['sent'];//'Wiadomość została przesłana na nasz adres korespondencji. Dziękujemy za kontakt!';
	else
		$retMsgText .= $mailSON[$_POST['lang']]['notSent'] . $ret;
// 		echo 'Wiadomość nie mogła zostać przesłana z powodu błędu serwera. Prosimy ponowić przesłanie wiadomości bądź skorzystanie z tradycyjnego kontaktu (wysłanie wiadomości ze swojej skrzynki pocztowej). ' . $ret;


	$sendMail = 'czestochowa@pte.pl';
	$sendName = $mailSON[$_POST['lang']]['redirectName'];
	//$message = $_POST["message"];
	$message = $mailSON[$_POST['lang']]['redirectHeader'] . "\n\n" . $message;
	$topic = $mailSON[$_POST['lang']]['redirectTopic'];
	$destMail[0] = $_POST["email"];
	if (!isset($_POST['fileSend']))
		$ret = sendMail($sendMail, 
			$sendName, 
			$destMail[0], 
			$topic,
			$message);
	else {
		$ret = sendMailAttachment($sendMail, 
			$sendName, 
			$destMail[0], 
			$topic,
			$message,
			$_POST['fileSend']);
	}
	
	if ($ret === true)
		$retMsgText .= '<br/><br/>' . $mailSON[$_POST['lang']]['redirectSent'];
	else
		$retMsgText .= '<br/><br/>' . $mailSON[$_POST['lang']]['redirectnotSent'] . $ret;
	if(isset($_POST['fileSend']))
		removeDir($_POST['fileSend'][0]);
	echo $retMsgText;
