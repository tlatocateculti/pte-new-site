<?php 
	require_once "./utilityFunctions.php";
	$siteTitle['sub'] = 'Nasza oferta';
	$json = readEntries('../sites', 'sites.json');
	$sites = array_keys($json);
	$subSitesCount = 1;
	if(isset($_POST['subCount']))
		$subSitesCount = $_POST['subCount'];
	else
		$subSitesCount = count($sites);
	$ret = [];
	$i = count($sites) - 2;
	for ($i; $i >= 0; $i--) {
		if (strpos($json[$sites[$i]]['siteBelong'], 'sub_') !== flase) {
			$pos = count($ret);
			$ret[$pos]['header'] = $json[$sites[$i]]['description'];
			$ret[$pos]['text'] = (isset($json[$sites[$i]]['b64']) ? rawurldecode(urldecode(base64_decode($json[$sites[$i]]['text']))) : $json[$sites[$i]]['text']);
			$ret[$pos]['link'] = 'offer/' . $json[$sites[$i]]['siteBelong'];
			if (isset($json[$sites[$i]]['gallery'])) {
				$ret[$pos]['pic'] = $json[$sites[$i]]['gallery'][0]['mini'];
			}
			$subSitesCount--;
		}
		if ($subSitesCount === 0)
			break;
	}
	echo '['.json_encode($ret, JSON_FORCE_OBJECT).']';
