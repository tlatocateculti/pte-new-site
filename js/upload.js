var upload = new Object();

upload.prototype = window.Event.prototype;

upload.processFiles = function(data, cf, rootFolder) {
	upload.data = data;
	upload.cf = cf;
	upload.rootFolder = rootFolder;
	upload.uploadedFiles = [];
	upload.fileFields = [];
	for (var i = 0; i < upload.data.length; i++) {
		if (upload.data[i].type === 'file') upload.fileFields[upload.fileFields.length] = data[i];
	}
	for (var i = 0; i < upload.fileFields.length; i++)
		upload.uploadFile(upload.fileFields[i]);
}

if ( typeof window.CustomEvent === "function" )
	upload.finished = new CustomEvent("fileUploaded");

document.addEventListener("fileUploaded", function(e) {
	if (upload.uploadedFiles.length === upload.fileFields.length)
		sendMailAction(upload.data, upload.cf, upload.uploadedFiles);
});

//funkcja przechwytuje i przesyła wskazany obraz na serwer
//ponieważ do PHP trafia jedynie 512000 bajtów to żadne ograniczenia nie są straszne (przeważnie
//serwery mają ograniczenia ok 2 MB).
upload.uploadFile = function(fileField) {
	if (!checkFileUploadState()) return;
	var f = new FileReader();
	f.onload = function() {

		(function(fileField) {
			var name = fileField.files[0].name;
			var folder = upload.rootFolder;
			//obliczamy ile kawałków będzie do przesłania
			var chunk = Math.ceil(f.result.length/512000);
			//var progress = 0;
			var gauge = document.getElementById('uploadGauge').children[0];
			gauge.style.display = 'block';		
			var gaugeLength = gauge.parentNode.getBoundingClientRect().width;
			var gaugeChunk = gaugeLength/chunk;
			//var gaugeText = gauge.children[0].innerHTML;
			gauge.style.width = '0';
			gauge.children[1].innerHTML = gauge.style.width;
			var i = 0;
			
			function animGauge() {
				gauge.style.width = Math.floor((gauge.getBoundingClientRect().width + gaugeChunk) / gaugeLength * 100) + '%';
				gauge.children[1].innerHTML = gauge.style.width;
				if (++i < chunk)
					setTimeout(sendChunk, 0);
			}
			
			function sendChunk() {
				var startRead = (i*512000);
				var send = 'filePath=../tmp_data/' + folder + '/' + name + '&data=' + f.result.slice(startRead,(startRead+512000)) + '&mode=w'  +
				'&secquence=' + i + '&total=' + chunk;
				
				communication("./scripts/upload.php", send, '', '', function(r) {
					//jeżeli wszystkie części zostały przesłane to wykonuje wysyłany jest do
					//skryptu odpowiedni komunikat; ponieważ nie wymagamy potwierdzenia z serwera
					//toteż nic nie odbieramy (pusta funkcja function(e) {} )
					if ((parseInt(r) + 1) >= chunk) {
						//gauge.style.width = '100%';
						//gauge.children[1].innerHTML = gauge.style.width;
						gauge.style.display = '';
						var sendMsg = 'transfer=../tmp_data/' + folder + '/' + name + '&total=' + chunk;
						communication("./scripts/upload.php", sendMsg, '', '', function(e) {
							gauge.style.width = '100%';
							gauge.children[1].innerHTML = gauge.style.width;
							upload.uploadedFiles[upload.uploadedFiles.length] = '../tmp_data/' + folder + '/' + name;
							document.dispatchEvent(upload.finished);
						});
					}
				});
				
				window.requestAnimationFrame(animGauge);

			}
			sendChunk();
			
		})(fileField);
	}
	//if (event.target.files[0].type.indexOf('image/') > -1)
	//można sprawdzadź typy przesyłanych plików; ponadto można by tutaj rozbić plik na mniejsze części (mniejsze obciążenie RAM po stronie klienta)
		f.readAsDataURL(fileField.files[0]);
}