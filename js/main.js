var lang = "";
/*var mainSlide = document.getElementsByTagName('figure')[0];
var currSlide = 0;


var mainNav = document.getElementsByTagName('nav')[0];
var upClickBtn = document.getElementById('upClick');
var mainDiv = document.getElementById('mainContener');
var congressDiv = document.getElementById('congress');
var sections = document.getElementsByTagName('sections');
var tipLabel = document.getElementById('tipLabel');
var newsDiv = document.getElementById('newsDivBack');*/

//funkcja odpowiada za przesyłanie danych do skryptu na serwerze odraz odbiór odpowiedzi z tegoż 
//skryptu.
//Wejście:
//site - adres, na który przesyłamy dane (np. POST z formularza bądź GET)
//post - utworzony ciąg znakowy przesyłany w postaci tablicy POST
//get - opcjonalne parametry skryptu dodawane do jego adresu
//waitCode - pozwala na dodanie funkcji, której kod będzie wykonywał się do chwili otrzymania odpowiedzi z serwera
//fRev - funkcja, której kod ma się wykonać po otrzymaniu odpowiedzi ze skryptu. Przyjmuje jeden parametr, którym jest odpowiedź skryptu
function communication(site, post, get, waitCode, fRev) {
	if (waitCode !== '' && waitCode !== undefined)
		waitCode();
	if (post === undefined)
		return;
	var contentFile;
	if (window.XMLHttpRequest)
		contentFile = new XMLHttpRequest();
	else
		contentFile = new ActiveXObject("Microsoft.XMLHTTP");
	contentFile.onreadystatechange = function () {		
		if(contentFile.readyState === 4) {
			
			if(contentFile.status === 200 || contentFile.status === 0) {
				fRev(contentFile.responseText);
				return;
			}
			else {
				fRev(-1);
				return;
			}
		}
		
	}	
	if (get !== undefined && get !== '')
		site += get;
	contentFile.open("POST", site, true);
	contentFile.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	contentFile.send(post);
}

/*function getHeaders(f) {
	communication('./scripts/headersToJS.php', '', '', '', function(r) {
			if (r) {
				obj = new Function('return ' + r)()[0];
				f(obj);
			}
		}
	)
}*/

function getLanguage(f) {
	lang = window.navigator.language;
	lang = (lang === 'pl') ? 'pl-PL' : 'en-UK'
	f()
	//console.log(lang)
	/*getHeaders(function(obj) {
		lang = (window.navigator.languages) ? window.navigator.languages[0] : obj['HTTP_ACCEPT_LANGUAGE'].split(',')[0];
		if (lang.indexOf('-') !== -1) lang = lang.split('-')[0];
		if (lang !== 'pl' && lang !== 'en') lang = 'en-UK';
		else lang = (lang === 'pl') ? 'pl-PL' : 'en-UK';
		f();
	});*/
}

function popup() {
	communication('./sites/popup.html', '', '', '', function(r) {
		if (r===-1) return;
		const tip= document.querySelector('#tipLabel')
		tip.innerHTML=r
		tip.style="display: block;position:absolute;width:50vw;height:50vh;left:25vw;top:12vh;z-index:2000;overflow:hidden;overflow-y:auto;"
		//console.log(tip)
		tip.querySelector('#closepopup').onclick=function() {tip.style="";tip.innerHTML='<p></p>'}
		tip.querySelector('input[name=nodisplay]').onchange=function(event) {window.localStorage.setItem('rebuildInfo',event.target.checked ? 'yes' : 'no')}
	})
}

/*function scrollVerticalAnimation(o, time, f, t) {
	if (o === undefined) return;
	if (time === undefined) time = 1000;
	if (t === undefined) t = 0;
	var currTop = (f === undefined) ? o.scrollTop : f;
	var chunk = currTop*60 / time;
	var steps = Math.ceil(currTop/chunk);
	if (f < t) {
		chunk = (t - f)*60/time * -1;
		steps = (t-f)/chunk *-1;
	}	
	var startTime = performance.now();
	window.bodyAnim = function(ts) {
		while (--steps > 0) {
			o.scrollTop -= chunk;	
			return window.requestAnimationFrame(window.bodyAnim);
		}
		window.bodyAnim = '';
		if (o.scrollTop !== t)
			o.scrollTop = t;
	}	
	window.requestAnimationFrame(window.bodyAnim);
}

function scrollToElement(e, time, offset) {
	if (e === undefined) return;
	if (time === undefined) time = 1000;
	if (offset === undefined) offset[0] = 0;
	var currPos = e.getBoundingClientRect().top - offset[0];
	var chunk = currPos*60 / time;
	var startTime = performance.now();
	var steps = Math.ceil(currPos/chunk);
	var se = document.scrollingElement || document.body;
	++se.scrollTop;
	if (se.scrollTop === 0)
		se = document.documentElement;
	window.bodyAnim = function ba(ts) {
// 		window.reqAnimID = true;
		while (--steps > 0) {
			se.scrollTop += chunk;	
			return window.requestAnimationFrame(ba);
		}
		window.bodyAnim = '';
		if (offset.length > 1 && e.getBoundingClientRect().top != 0)
			se.scrollTop += e.getBoundingClientRect().top - offset[1];
	}	
	window.requestAnimationFrame(window.bodyAnim);
}

function moveMouseWheel(e) {
	var st = 0, topVal = '10vh';
	if (document.scrollingElement === undefined || document.scrollingElement === '') {
		topVal = '100px';
		st = window.pageYOffset;
	}
	else 
		st = document.scrollingElement.scrollTop ||  document.body.scrollTop || 
	document.documentElement.scrollTop;
	if (document.documentElement.scrollTop) topVal = '10vh';
	var congressDivPosY1 = congressDiv.getBoundingClientRect().top - mainNav.clientHeight * 2;
	var congressDivPosY2 = congressDiv.getBoundingClientRect().bottom - mainNav.clientHeight * 2;
	if (st  < 200)
		upClickBtn.style.display = 'none';
	else
		upClickBtn.style.display = 'block';
	if (congressDivPosY1 < 0 && congressDivPosY2 > 0)
		
		document.getElementById('congressNav').style.top = topVal;
	else
		document.getElementById('congressNav').style.top = "";
}*/

function displayTipLabel(e) {
	tipLabel.style.display = 'block';
	tipLabel.style.top = (e.pageY + 20) + 'px';
	tipLabel.style.left = e.pageX + 'px';
	if (e.target.dataset.tip) 
		tipLabel.children[0].innerHTML = e.target.dataset.tip;
	else
		tipLabel.children[0].innerHTML = 'Brak podpowiedzi';
}

//function closeNews(e) {
//	newsDiv.children[0].style.transform = "scale3d(0.1,0.1,0)";
//	window.setTimeout(function() {newsDiv.style.display = '';}, 2000);
//}

function checkDataIntegrity(data) {
	if (data === undefined || data.length === 0) data = document.formFields;
	var send = 1;
	var file = 1;
	//console.log(data);
	for (var i = 0; i < data.length; i++) {
		var matchTmp = [];
		if (data[i].type === 'file' && data[i].value !== "") file = 2; 
		if (data[i].dataset.regexp)
			var matchTmp = data[i].value.match(data[i].dataset.regexp, ((data[i].dataset.regexpparam) ? data[i].dataset.regexpparam : ""));
		if (data[i].dataset.man) {
			if (data[i].type !== 'checkbox' && data[i].type !== 'radio' && data[i].value === "") matchTmp = null;
			if ((data[i].type === 'checkbox' || data[i].type === 'radio') && !data[i].checked) matchTmp = null;
			
		}
		if (matchTmp === null) {
			data[i].parentNode.className += " error";
			data[i].dataset.error = 1;
			data[i].title = (data[i].dataset.titleTip) ? data[i].dataset.titleTip : ((lang === 'pl-PL') ? "To pole jest wymagane by wysłać wiadomość!" : "This field is mandatory!");
			if (data[i].dataset.exampleTip)
				data[i].placeholder = data[i].dataset.exampleTip;
			send = 0;
		}
		else if (data[i].dataset.error) {
			delete data[i].dataset.error;
			var ctmp = data[i].parentNode.className.split(' ');
			data[i].parentNode.className = "";
			for (var k = 0; k < ctmp.length; k++)
				if (ctmp[k] !== 'error')
					data[i].parentNode.className += ctmp[k] + " ";
			data[i].title = "";
			data[i].placeholder = data[i].dataset.tip;
		}
	}
	send = send * file;
	return send;
}

function sendMailAction(data, contentFile, filePath) {
	var sendMsg = "sendMode=0&lang=" + lang +"&";
	for (i = 0; i < data.length - 1; i++) {
		if (data[i].type !== 'file')
			sendMsg += data[i].name + "=" + data[i].value.replace('+', '@@') + "&";
	}
	if (filePath)
		for (var i = 0; i < filePath.length; i++) 
			sendMsg += "fileSend[" + i + "]=" + filePath[i] + "&";
	sendMsg = sendMsg.substring(0, sendMsg.length - 1);	
	
	contentFile.open("POST", "./scripts/mail.php", true);
	contentFile.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	contentFile.send(sendMsg);
}

function sendMailJS(sendMode) {
// 	var tmp = document.getElementsByName("accept")[0];
// 	if (!tmp.checked) {
// 		tmp.parentNode.style.border = "1px solid red";
// 		tmp.parentNode.title = "Musisz wyrazić zgodę na przetwarzanie danych osobowych by wysłać zapytanie!";
// 		return;
// 	}
// 	else tmp.parentNode.style.border = "";
	if (sendMode === undefined)
		sendMode = 0;
	var contentFile;	
	if (window.XMLHttpRequest)
		contentFile = new XMLHttpRequest();
	else
		contentFile = new ActiveXObject("Microsoft.XMLHTTP");
	var formContent = document.getElementById("formContactDiv").innerHTML.slice(0);
	
	contentFile.onreadystatechange = function () {		
		document.getElementById("formContactDiv").innerHTML = "<p style='font-size: 32px; font-weight: bold; color: white;'>Zapytanie jest realizowane...</p>";
		if(contentFile.readyState === 4) {				    
			if(contentFile.status === 200 || contentFile.status === 0) {
				var answer = contentFile.responseText;
				var errors = answer.split(':');
				console.log(errors);
				if (errors[0] !== "error") {
					document.getElementById("formContactDiv").innerHTML = "<p style='font-size: 32px; font-weight: bold; color: white;'>" +  answer + "</p>";
					setTimeout(function () {
						window.location.reload();
					}, 5000);
				}
				else {
					document.getElementById("formContactDiv").innerHTML = formContent;
					errors = errors[1].split('||');
					checkDataIntegrity();
// 					for (var i = 0; i < errors.length; i++) {					    
// 						//alert(errors[i]);
// 						document.getElementsByName(errors[i])[0].className += " error";
// 					}
				}
			}
		}				
	}
	var data = document.formFields;
	var send = 1;
	send = checkDataIntegrity(data);	
	if (send === 1) 
		sendMailAction(data, contentFile);
	else if (send === 2)
		upload.processFiles(data, contentFile, document.getElementsByName('sid')[0].value);
}

function checkFileUploadState() {
	if (window.File && window.FileReader && window.FileList && window.Blob) {
		//console.log('Przeglądarka zgodna!'); 
		return true;
	} else {
		//console.log('Wczytywanie plików niedostępne!'); 
		return false;
	}
}

function changeLang(e) {
	lang = e.target.dataset.lang;
	createForm(lang);
}

function createForm(lang, s) {
	if (lang === undefined) lang = 'pl-PL';
	if (s === undefined) s = 0;
	var formDiv = document.getElementById('formContactDiv');
	while(formDiv.children.length > 3) formDiv.removeChild(formDiv.children[0]);
	var uploadGauge = document.getElementById('uploadGauge');
	document.getElementsByClassName('contactLangTip')[0].children[0].innerHTML = (lang === 'pl-PL') ? 'Wybierz swój język: ' : 'Please select your language: ';
	formDiv.getElementsByTagName('button')[0].innerHTML = (lang === 'pl-PL') ? 'Wyślij' : 'Send';
	uploadGauge.getElementsByTagName('p')[0].innerHTML = (lang === 'pl-PL') ? 'Przesłano: ' : 'Sent: ';
	document.formFields = [];
	communication('./sites/jsonReader.php', '', '?s=contactFields', '', 
	function(r) {
		var json = new Function('return ' + r)()[0];
		(function(json, parentElem, beforeElem, lang, s) {
			function createFields(json, parentElem, beforeElem) {
				var jsonKeys = [];
				for(var tmp in json) jsonKeys[jsonKeys.length] = tmp;
				for (var i = 0; i < jsonKeys.length; i++) {
					var elem = json[jsonKeys[i]];
					if (elem.show && elem.show.indexOf(s) === -1) continue;
					var wrapDiv = document.createElement('div');
					var item = document.createElement(elem.type);
					if (elem.type === 'fieldset') {
						item.appendChild(document.createElement('legend').appendChild(document.createTextNode(elem[lang])));
						wrapDiv.appendChild(item);
						(beforeElem) ? parentElem.insertBefore(wrapDiv, beforeElem) : parentElem.appendChild(wrapDiv);
						createFields(elem.fields, item);
					}
					else {
						var append = false;
						if (elem[lang] && elem[lang].placeholder === '' && elem[lang].tip === '') continue; 
						item.placeholder = elem[lang].placeholder;
						item.dataset.tip = elem[lang].placeholder;
						item.name = jsonKeys[i];
						
						if (elem.event) {
							for (var j = 0; j < elem.event.length; j++) {
								var fStr = elem.event[j].function + "(";
								for (var jj = 0; jj < elem.event[j].params.length; jj++)
									fStr += elem.event[j].params[jj] + ",";
								fStr = fStr.slice(0, fStr.length - 1) + ")";
								//fStr += ")";
								var f = new Function(fStr);
								item.addEventListener(elem.event[j].action, f);
							}
						}
						
						if (elem.type === 'select') {
							if (!elem[lang].values) continue;
							for (var j = 0; j < elem[lang].values.length; j++) {
								var opt = document.createElement('option');
								opt.dataset.id = elem[lang].values[j].id;
								opt.value = elem[lang].values[j].value;
								opt.innerHTML = elem[lang].values[j].text;
								if (j === ((elem.default) ? elem.default : s)) opt.selected = "selected";
								item.appendChild(opt);
							}
							append = !append;
						}
						else if (elem.type === 'textarea') {
							if (elem.value)
								item.value = elem.value;
							append = !append;
						}
						else if (elem.subtype === 'text' || elem.subtype === 'file') {
							item.type = elem.subtype;
							if (elem.value)
								item.value = elem.value;
							append = !append;
						}
						else if (elem.subtype === 'checkbox') {
							item.type = elem.subtype;
							if (elem.checked)
								item.checked = 'checked';
							//wrapDiv.appendChild(item);
							
							append = !append;
						}
						if (append) {
							if (elem.showTitle) {
								var tmpP = document.createElement('p');
								tmpP.innerHTML = elem[lang].placeholder;
								wrapDiv.appendChild(tmpP);
							}
							if (elem.mandatory) {
								item.dataset.man = 1;
								if (elem.regexp) item.dataset.regexp = elem.regexp;
								if (elem.regexpParam) item.dataset.regexpparam = elem.regexpParam;
								if (elem[lang].titleTip) item.dataset.titleTip = elem[lang].titleTip;
								if (elem[lang].exampleTip) item.dataset.exampleTip = elem[lang].exampleTip;
							}
							wrapDiv.appendChild(item);
							if (elem.subtype === 'checkbox') {
								var tmpP = document.createElement('p');
								tmpP.innerHTML = elem[lang].placeholder;
								wrapDiv.appendChild(tmpP);
							}
							var span = document.createElement('div');
							span.className += ' span';
							span.dataset.tip = elem[lang].tip;
							span.innerHTML = '<i class="fa fa-question" aria-hidden="true"></i>';
							wrapDiv.appendChild(span);
							(beforeElem) ? parentElem.insertBefore(wrapDiv, beforeElem) : parentElem.appendChild(wrapDiv);
							document.formFields[document.formFields.length] = document.getElementsByName(jsonKeys[i])[0]; 
						}
					}
				}
			}
			createFields(json, parentElem, beforeElem);
			//ponizszy kod skopiowany z pliku events.js -> wymaga przebudowy do pelnego przeniesienia+optymalizacji
			helpIcons = document.getElementById('formContactDiv').getElementsByClassName('span');
			for (var i = 0; i < helpIcons.length; i++) {
				helpIcons[i].addEventListener('mouseover', displayTipLabel);
				helpIcons[i].addEventListener('click', displayTipLabel);
				helpIcons[i].addEventListener('mouseout', function(e) {
					tipLabel.style.display = '';
				});
			}
		})(json, formDiv, uploadGauge, lang, s);
		
		
	});
}



(
	function() {
// 		from MDN site https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
		function custonmEventIE9() {
			if ( typeof window.CustomEvent === "function" ) return false;

			function CustomEvent ( event, params ) {
			params = params || { bubbles: false, cancelable: false, detail: undefined };
			var evt = document.createEvent( 'CustomEvent' );
			evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
			return evt;
			}

			CustomEvent.prototype = window.Event.prototype;

			window.CustomEvent = CustomEvent;
			upload.finished = new CustomEvent("fileUploaded");
		}
		
		function loadSplashTexts() {
			communication('./sites/jsonReader.php', '', '?s=welcomeText', '', 
			function(r) {
				var json = new Function('return ' + r)()[0];
				if (json === undefined || json === '') return;
				var splash = document.getElementsByTagName('figcaption')[0];
				//future version will be with multiple splashes!
		// 				json.forEach(i, n) {
		// 					
		// 				}
				splash.innerHTML = "<h3>" + json[0][lang].header + "</h3>" + "<p>" + json[0][lang].text + "</p>";
			});
		}
		
		function loadImages() {	
			communication('./sites/jsonReader.php', '', '?s=splashImages', '', 
			function(r) {
				var json = new Function('return ' + r)()[0]["photos"];
				if (json === undefined || json === '') return;
	// 			mainSlide.removeChild(mainSlide.children[0]);
				var figCap = mainSlide.children[1];
				var removeSplashStart = false;
				json.forEach(function(i, n) {
					if (i.splash) {
						var nd = document.createElement('div');
						nd.style.backgroundImage = "url('" + i.photo + "')";
						mainSlide.insertBefore(nd, figCap);
						removeSplashStart = true;
					}
				});
				if (removeSplashStart)
					mainSlide.removeChild(mainSlide.children[0]);
				mainSlide.children[0].style.opacity = "1";
				mainSlide = mainSlide.getElementsByTagName('div');
				window.setInterval(function() {
					currSlide = (currSlide < mainSlide.length - 1) ? currSlide+1 :  0;
					for(var i = 0; i < mainSlide.length; i++) {
						if (currSlide == i) {
							mainSlide[i].style.opacity = "1";
						}
						else {
							mainSlide[i].style.opacity = "0";
						}
					}	
				}, 5000);
			});
		}
		
		/*if (window.location.toString().indexOf('m_') === -1 || window.location.toString().indexOf('m_contact') !== -1) {
			if (lang === '') 
				getLanguage(function() {
					//loadSplashTexts(); 
					createForm(lang);
				});
			//loadImages();
		}*/
		//custonmEventIE9();
		
	}
)()
/*function loadImages() {	
			communication('./sites/jsonReader.php', '', '?s=splashImages', '', 
			function(r) {
				var json = new Function('return ' + r)()[0]["photos"];
				if (json === undefined || json === '') return;
	// 			mainSlide.removeChild(mainSlide.children[0]);
				var figCap = mainSlide.children[1];
				var removeSplashStart = false;
				json.forEach(function(i, n) {
					if (i.splash) {
						var nd = document.createElement('div');
						nd.style.backgroundImage = "url('" + i.photo + "')";
						mainSlide.insertBefore(nd, figCap);
						removeSplashStart = true;
					}
				});
				if (removeSplashStart)
					mainSlide.removeChild(mainSlide.children[0]);
				mainSlide.children[0].style.opacity = "1";
				mainSlide = mainSlide.getElementsByTagName('div');
				window.setInterval(function() {
					currSlide = (currSlide < mainSlide.length - 1) ? currSlide+1 :  0;
					for(var i = 0; i < mainSlide.length; i++) {
						if (currSlide == i) {
							mainSlide[i].style.opacity = "1";
						}
						else {
							mainSlide[i].style.opacity = "0";
						}
					}	
				}, 5000);
			});
		}*/
//if (lang === '') 
//	getLanguage(function() {
//		//loadSplashTexts(); 
//		createForm(lang);
//	});
//loadImages();
