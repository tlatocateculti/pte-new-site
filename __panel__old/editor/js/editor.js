/*EDYTOR TREŚCI WIADOMOŚCI Z MOŻLIWOŚCIĄ EDYCJI WŁAŚCIWOŚCI ELEMENTÓW HTML
*
* Pierwsza wersja edytora tekstowego HTML z podręczną listą pozwalającą na wizualne modyfikowanie wpisywanej treści
* HTML bez znajomości znaczników HTML. Edytor pozwala na podgląd w czasie rzeczywistym wstawionych elementów HTML 
* oraz ręczną podmianę ich. Ponadto umożliwia wstawianie najbardziej potrzenych elementów HTML dla wiadomości (paragrafy, nagłówki, tabele, obrazy czy odnośniki)
* które później można modyfikować w nowym oknie.
* Edytor pozwala na podłączenie go do niemal każdej strony WWW (całość pisana w HTML + CSS). 
*
* Elementy do rozwinięcia:
* - dorobić przejmowanie właściwości edytowanych elementów (na razie zaimplementowane szczątkowo, dla niektórych kolorów)
* - zoptymalizować funkcje JS (obecnie spora ilość kodu się powtarza)
* - sprawdzić oraz zoptymalizować działanie na innych przeglądarkach niż Mozilla Firefox (aczkolwiek powinien działać na większości przeglądarek)
*/   

var closeRightMenu = true;
var currentSelect;
var fonts = [];

//prototyp funkcji do podłączenia wyboru zdjęcia z galerii
var galleryOpen = function() {console.log('BRA!');};

//otwiera menu kontekstowe
function openContext(e) {
	var tmp = document.getElementById('rightMenu');
	if (!tmp) return;
	if (e.which === 3) {
		e.preventDefault();
		e.stopPropagation();
		//console.log('Zdarzenie' + e.which + ' x ' + e.clientX + ' y ' + e.clientY);
// 			closeRightMenu = false;
		tmp.style.top = e.pageY + 'px';
		tmp.style.left = e.pageX + 'px';;
		/*if (!tmp)
			tmp.style.display = "block";
		else*/ if (tmp.style.display === "none" || tmp.style.display === "")
			tmp.style.display = "block";
// 			else 
// 				tmp.style.display = "";
	}
// 		else
// 			tmp.style.display = "";
}

//zamyka
function closeContext(e) {
	console.log('Menu ' + closeRightMenu);
	if (!closeRightMenu) {
		e.stopPropagation();
		closeRightMenu = true;
		return;
	}
	if (currentSelect !== undefined) {
		currentSelect.endOffset = 0;
		currentSelect.startOffset = 0;
	}
	document.getElementById('rightMenu').style.display = "";
	
}

//zabezpiecza przed otwarciem domyslnego prawego menu;
//jezeli przegladarka nie posiada tej opcji (IE) blokuje dostep do strony
//(bo po co się meczyc z dziadostwem)
function preventRightMenu(e) {
	if (!e.stopPropagation || !e.preventDefault)
		window.location = "https://microsoft.com";
	e.stopPropagation();
	e.preventDefault();
}

//prawdopodobnie do wywalenia
// 	function appendToPreview() {
// 		var tmp = document.getElementById('preview');
// 		var tDiv = document.createElement('div');
// 		tDiv.className = 'removable';
// 		//tDiv.appendChild(document.createElement('p'));
// 		//tDiv.innerHTML = document.getElementsByName('input')[0].value;
// 		var tP = document.createElement('p');
// 		tP.contentEditable = true;
// 		tDiv.appendChild(tP);
// 		//tDiv.contentEditable = true;
// 		tDiv.addEventListener('mousedown', function (e) {openContext(e);}, true);
// 		tmp.appendChild(tDiv);
// 		//tmp.innerHTML += "<div class='removable'>" +  + "</div>";
// 	}

function createButtonCheck(id, text, icon, value, classPress, classDepress, toElem, style, checked, showText) {
	if (id === undefined || text === undefined || icon === undefined || value === undefined) return -1;
	if (style === undefined || style === '') style = false;
	if (checked === undefined || checked === '') checked = false;
	if (showText === undefined || showText === '') showText = false;
	var btn = document.createElement('div');
	if (checked) {
		if (style) {
			btn.style = classPress;
// 				btn.data.depress = classDepress;
		}
		else {
			btn.className = classPress;
// 				btn.data.classDepress = classDepress;
		}
		btn.dataset.checked = 1;
	}
	else {
		if (style) {
			btn.style = classDepress;
// 				btn.data.depress = classDepress;
		}
		else {
			btn.className = classDepress;
// 				btn.data.classDepress = classDepress;
		}
		btn.dataset.checked = 0;
	}
	if (style) {
		btn.dataset.depress = classDepress;
		btn.dataset.press = classPress;
	}
	else {
		btn.dataset.classDepress = classDepress;
		btn.dataset.classPress = classPress;
	}
	btn.id = id;
	btn.innerHTML = icon;
	btn.addEvent = addEventHandler;
	btn.dataset.value = value;
	if (showText) {
		var desc = document.createElement('p');
		desc.innerHTML = text;
		btn.appendChild(desc);
	}
	else
		btn.title = text;
	if (toElem !== undefined && toElem !== '') toElem.appendChild(btn);
	return btn;
}

function buttonRadioCheckAction(textAP, t) {
	if (textAP === undefined) textAP = document.getElementById('textActionPreview');
	//console.log(event);
	//var t;
	//if (this ===undefined) t = document.getElementsByName('radioButtonPress') 
	if (t === undefined) t = this;
	var btnStyle = true;
	
	if (t.dataset.classPress !== undefined) btnStyle = false;
	//console.log(btnStyle);
	for (var i = 1; i < t.parentNode.children.length; i++) {
		if (t.parentNode.children[i] != t) {
			if (btnStyle)
				t.parentNode.children[i].style = t.parentNode.children[i].dataset.depress;
			else
				t.parentNode.children[i].className = t.parentNode.children[i].dataset.classDepress;
		}
		t.parentNode.children[i].dataset.checked = 0;
	}
	if (btnStyle) 
		t.style = t.dataset.press;	
	else 
		t.className = t.dataset.classPress;
	t.dataset.checked = 1;
	//var textAP = document.getElementById('textActionPreview');
	if (textAP === undefined) return;
	if (t.dataset.value == 0) textAP.style.textAlign = 'center';
	else if (t.dataset.value == 1) textAP.style.textAlign = 'left';
	else if (t.dataset.value == 3) textAP.style.textAlign = 'justify';
	else textAP.style.textAlign = 'right';
}

function headingSelectAction(headingType) {
	var newH;
	if (headingType.children[1].value == 1)
		newH = document.createElement('h2');
	else if (headingType.children[1].value == 2)
		newH = document.createElement('h3');
	else if (headingType.children[1].value == 3)
		newH = document.createElement('h4');
	else if (headingType.children[1].value == 4)
		newH = document.createElement('h5');
	else if (headingType.children[1].value == 5)
		newH = document.createElement('h6');
	else 
		newH = document.createElement('h1');
	return newH;
}

//dodaje nagłowek do edytowanego tekstu
function addHeading(bbm) {
	var btn = document.getElementsByClassName('fa-header')[0];
	btn.className = 'active ' + btn.className;
	bbm.style.display = 'block';
	//bbm.style.minWidth = '500px';
	createHeadingTitle('h3', 'Dodaj nagłówek', '', bbm);
	bbm.style.top = (btn.getBoundingClientRect().y | btn.getBoundingClientRect().top + btn.getBoundingClientRect().height) + 'px';
	var headingType = createSelectForm('Nagłówek typu', 'headingType', ['Nagłowek 1', 'Nagłówek 2', 'Nagłówek 3', 'Nagłówek 4', 'Nagłówek 5', 'Nagłówek 6'], bbm, '', true);
	headingType.addEvent(['change'], [function() {var h = headingSelectAction(this); h.innerHTML = 'Nagłówek'; pPrev.appendChild(h); pPrev.removeChild(pPrev.children[0]);}]);
	var hColor = createInputForm('color', 'Kolor tekstu', 'hColor', '#000000', '', bbm);
	hColor.addEvent(['change'], [function() {pPrev.style.color = hColor.children[1].value;}]);
	var hBackColor = createInputForm('color', 'Kolor tła', 'hBackColor', '#FFFFFF', '', bbm);
	hBackColor.addEvent(['change'], [function() {pPrev.style.background = hBackColor.children[1].value;}]);
	var mUp = createInputForm('number', 'Margines górny (px)', 'mUp', '', '(domyślny 18px)', bbm);
	mUp.addEvent(['change'], [function() {if (mUp.children[1].value === '') pPrev.style.marginTop = '18px'; else  pPrev.style.marginTop = parseInt(mUp.children[1].value) + 'px';}]);
	var mDown = createInputForm('number', 'Margines dolny (px)', 'mDown', '', '(domyślny 10px)', bbm);
	mDown.addEvent(['change'], [function() {if (mDown.children[1].value === '') pPrev.style.marginBottom = '10px'; else pPrev.style.marginBottom = parseInt(mDown.children[1].value) + 'px';}]);
	var hFontSize = createInputForm('number', 'Wielkość tekstu (px)', 'hFontSize', '', '(domyślny)', bbm);
	hFontSize.addEvent(['change'], [function() {if (hFontSize.children[1].value === '') pPrev.style.fontSize = ''; else pPrev.style.fontSize = parseInt(hFontSize.children[1].value) + 'px';}]);
	var btnDiv = document.createElement('fieldset');
	btnDiv.className = "buttonContener";
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Ułożenie tekstu";
	btnDiv.appendChild(btnLegend);
	createButtonCheck('centerH', 'Wyśrodkowany', '<i class="fa fa-align-center" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv, false, true).addEvent(['click'], [buttonRadioCheckAction]);
	createButtonCheck('leftH', 'Do lewej', '<i class="fa fa-align-left" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [buttonRadioCheckAction]);
	createButtonCheck('rightH', 'Do prawej', '<i class="fa fa-align-right" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [buttonRadioCheckAction]);
	bbm.appendChild(btnDiv);
	var pPrev = document.createElement('p');
	pPrev.id = "textActionPreview";
	pPrev.innerHTML = '<h1>Nagłówek</h1>';
	pPrev.style.marginTop = '18px';
	pPrev.style.marginBottom = '10px';
	pPrev.style.textAlign = 'center';
	bbm.appendChild(pPrev);
	createButton('Dodaj', 'button noselect', bbm).addEvent(['click'], [function () {
		var tmp = document.getElementById('preview');
		if (tmp === undefined) return;
		var newH = headingSelectAction(headingType);
		newH.dataset.name = 'heading';
		newH.style = pPrev.style.cssText;
		newH.addEventListener('mouseover', function() {
			newH.style.boxShadow = '0 0 10px #1A1A1A';
		}, true);
		newH.addEventListener('mouseout', function() {
			newH.style.boxShadow = '';
		}, true);
// 			newH.style.color = hColor.children[1].value;
// 			newH.style.background = hBackColor.children[1].value;
// 			newH.style.marginTop = mUp.children[1].value + 'px';
// 			newH.style.marginBottom = mDown.children[1].value + 'px';
// 			if (hFontSize.children[1].value !== '')
// 				newH.style.fontSize = hFontSize.children[1].value + 'px';
// 			if (document.getElementById('centerH').dataset.checked == 1) newH.style.textAlign = 'center';
// 			else if (document.getElementById('rightH').dataset.checked == 1) newH.style.textAlign = 'right';
// 			else newH.style.textAlign = 'left';
		newH.innerHTML = "[Wprowadź tekst nagłówka]";
		tmp.appendChild(newH);
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', bbm).addEvent(['click'], [buttonBarActionDestroy]);
}

function buttonCheckAction(elem) {
	if (elem === undefined) elem = this;
	if (elem.dataset.checked === undefined) return;
// 		console.log(elem);
	if (elem.dataset.classPress === undefined) {
		if (this.dataset.checked == 1)
			elem.style = elem.dataset.depress;
		else 
			elem.style = elem.dataset.press;

	}
	else {
		if (elem.dataset.checked == 1)
			elem.className = elem.dataset.classDepress;
		else 
			elem.className = elem.dataset.classPress;
	}
	if (elem.dataset.checked == 1) elem.dataset.checked = 0;
	else elem.dataset.checked = 1;
}

//dodaje paragraf do edytowanego tekstu
function addParagraph(bbm) {
	//console.log(window.getSelection());
	var btn = document.getElementsByClassName('fa-paragraph')[0];
	btn.className = 'active ' + btn.className;
	bbm.style.display = 'block';
	createHeadingTitle('h3', 'Dodaj paragraf', '', bbm);
	bbm.style.top = (btn.getBoundingClientRect().y | btn.getBoundingClientRect().top + btn.getBoundingClientRect().height) + 'px';
	var hColor = createInputForm('color', 'Kolor tekstu', 'hColor', '#000000', '(domyślny czarny)', bbm);
	hColor.addEvent(['change'], [function() {pPrev.style.color = hColor.children[1].value;}]);
	var hBackColor = createInputForm('color', 'Kolor tła', 'hBackColor', '#FFFFFF', '(domyślny biały)', bbm);
	hBackColor.addEvent(['change'], [function() {pPrev.style.background = hBackColor.children[1].value;}]);
	var mUp = createInputForm('number', 'Margines górny (px)', 'mUp', '', '(domyślny, ok. 16px)', bbm);
	mUp.addEvent(['change'], [function() {if (mUp.children[1].value === '') pPrev.style.marginTop = '18px'; else  pPrev.style.marginTop = parseInt(mUp.children[1].value) + 'px';}]);
	var mDown = createInputForm('number', 'Margines dolny (px)', 'mDown', '', '(domyślny, ok. 16px)', bbm);
	mDown.addEvent(['change'], [function() {if (mDown.children[1].value === '') pPrev.style.marginBottom = '10px'; else pPrev.style.marginBottom = parseInt(mDown.children[1].value) + 'px';}]);
	var hFontSize = createInputForm('number', 'Wielkość tekstu (px)', 'hFontSize', '', '(domyślny, ok. 19px)', bbm);
	hFontSize.addEvent(['change'], [function() {if (hFontSize.children[1].value === '') pPrev.style.fontSize = ''; else pPrev.style.fontSize = parseInt(hFontSize.children[1].value) + 'px';}]);
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Ułożenie tekstu";
	btnDiv.appendChild(btnLegend);
	createButtonCheck('centerT', 'Wyśrodkowany', '<i class="fa fa-align-center" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [buttonRadioCheckAction]);
	createButtonCheck('leftT', 'Do lewej', '<i class="fa fa-align-left" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv, false, true).addEvent(['click'], [buttonRadioCheckAction]);
	createButtonCheck('rightT', 'Do prawej', '<i class="fa fa-align-right" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [buttonRadioCheckAction]);
	createButtonCheck('justifyT', 'Równomierny', '<i class="fa fa-align-justify" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [buttonRadioCheckAction]);
	bbm.appendChild(btnDiv);
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Opcje tekstu";
	btnDiv.appendChild(btnLegend);
	var boldBtn = createButtonCheck('boldT', 'Pogrubienie', '<i class="fa fa-bold" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	boldBtn.addEvent(['click'],[function() {buttonCheckAction(boldBtn); if (boldBtn.dataset.checked == 1) pPrev.style.fontWeight = 'bold'; else pPrev.style.fontWeight = '';}]);
	var itaBtn = createButtonCheck('italicT', 'Pochylenie', '<i class="fa fa-italic" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	itaBtn.addEvent(['click'], [function() {buttonCheckAction(itaBtn); if (itaBtn.dataset.checked == 1) pPrev.style.fontStyle = 'italic'; else pPrev.style.fontStyle = '';}]);
	var underBtn = createButtonCheck('underT', 'Podkreślony', '<i class="fa fa-underline" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	underBtn.addEvent(['click'], [function() {buttonCheckAction(underBtn); if (underBtn.dataset.checked == 1) pPrev.style.textDecoration += ' underline'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('underline', '')}]);
	var strikeBtn = createButtonCheck('strikeT', 'Przekreślony', '<i class="fa fa-strikethrough" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	strikeBtn.addEvent(['click'], [function() {buttonCheckAction(strikeBtn); if (strikeBtn.dataset.checked == 1) pPrev.style.textDecoration += ' line-through'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('line-through', '');}]);
	var supBtn = createButtonCheck('supT', 'Indeks górny', '<i class="fa fa-superscript" aria-hidden="true"></i>', '4', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	supBtn.addEvent(['click'], [function() {buttonCheckAction(supBtn); if (supBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'super'; else pPrev.style.verticalAlign = '';}]);
	var subBtn = createButtonCheck('subT', 'Indeks dolny', '<i class="fa fa-subscript" aria-hidden="true"></i>', '5', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	subBtn.addEvent(['click'], [function() {buttonCheckAction(subBtn); if (subBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'sub'; else pPrev.style.verticalAlign = '';}]);
	bbm.appendChild(btnDiv);
	//TRZEBA DOKONCZYC!!!!
	var pPrev = document.createElement('p');
	pPrev.id = "textActionPreview";
	pPrev.innerHTML = 'Tekst';
	bbm.appendChild(pPrev);
	createButton('Dodaj', 'button noselect', bbm).addEvent(['click'], [function () {
		var tmp = document.getElementById('preview');
		if (tmp === undefined) return;
		var newP = document.createElement('p');
		newP.dataset.name = 'paragraph';
		newP.style = pPrev.style.cssText;
		newP.addEventListener('mouseover', function() {
			newP.style.boxShadow = '0 0 10px #1A1A1A';
		}, true);
		newP.addEventListener('mouseout', function() {
			newP.style.boxShadow = '';
		}, true);
		newP.innerHTML = "[Wprowadź tekst akapitu]";
		tmp.appendChild(newP);
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', bbm).addEvent(['click'], [buttonBarActionDestroy]);
}

function createSampleList(ol, iNum) {
	if (ol === undefined) return -1;
	if (ol.length == 0) return -2;
	if (iNum === undefined) iNum = 1;
	for (var i =0; i < iNum; i++) {
		for (var j = 0; j < ol.length; j++) {
			var ll = document.createElement('li');
			ll.innerHTML = 'Wpis ' + (i + 1);
			ol[j].appendChild(ll);
		}
	}
}

//funkcja ma za zadanie dodawać listę do edytowanego tekstu
function addList(bbm) {
	var btn = document.getElementsByClassName('fa-list')[0];
	btn.className = 'active ' + btn.className;
	bbm.style.display = 'block';
	createHeadingTitle('h3', 'Dodaj listę', '', bbm);
	bbm.style.top = (btn.getBoundingClientRect().y | btn.getBoundingClientRect().top + btn.getBoundingClientRect().height) + 'px';
	var lHeader = createInputForm('checkbox', 'Dodaj nagłówek do listy', 'lHeader', '', '', bbm);
	lHeader.addEvent(['change'], [function() {
		if (lHeader.children[1].checked) {
			//lHeaderText.style.display = ''; 
			pHead.style.display = '';
		}
		else {
			//lHeaderText.style.display = 'none';
			pHead.style.display = 'none';
		}
	}]);
	var lColor = createInputForm('color', 'Kolor tekstu', 'lColor', '#000000', '(domyślny czarny)', bbm);
	lColor.addEvent(['change'], [function() {pPrev.style.color = lColor.children[1].value;}]);
	var lBackColor = createInputForm('color', 'Kolor tła', 'lBackColor', '#FFFFFF', '(domyślny biały)', bbm);
	lBackColor.addEvent(['change'], [function() {pPrev.style.background = lBackColor.children[1].value;}]);
	var lFontSize = createInputForm('number', 'Wielkość tekstu (px)', 'hFontSize', '', '(domyślny, ok. 19px)', bbm);
	lFontSize.addEvent(['change'], [function() {if (hFontSize.children[1].value === '') pPrev.style.fontSize = ''; else pPrev.style.fontSize = parseInt(hFontSize.children[1].value) + 'px';}]);
	var listType = createSelectForm('Typ listy', 'listType', ['Uporządkowana (numerowana)', 'Nieuporządkowana', 'Opisowa'], bbm, '', true);
	listType.addEvent(['change'], [function() {
		if (listType.children[1].value == 1) {
			listOType.style.display = 'none';
			pOList.style.display = 'none';
			pDList.style.display = 'none';
			listUType.style.display = '';
			pUList.style.display = '';
		}
		else if (listType.children[1].value == 2) {
			listOType.style.display = 'none';
			pOList.style.display = 'none';
			pDList.style.display = '';
			listUType.style.display = 'none';
			pUList.style.display = 'none';
		}
		else {
			listOType.style.display = '';
			pOList.style.display = '';
			pDList.style.display = 'none';
			listUType.style.display = 'none';
			pUList.style.display = 'none';
		}
	}]);
	var listOType = createSelectForm('Oznaczenia pozycji', 'listOType', ['Brak', 'Liczby arabskie', 'Liczby arabskie z przewodnimi zerami', 'Liczby hebrajskie', 'Katakana', 'Katakana iroha', 
	'Hiragana', 'Hiragana iroha', 'Małe znaki', 'Duże znaki', 'Liczby rzymskie', 'Duże liczby rzymskie', 'Znaki greckie ', 'Liczby gregoriańskie', 'Liczby chińskie'], bbm, '', true);
	listOType.addEvent(['change'], [function() {
		if (listOType.children[1].value == 1)
			pOList.style.listStyleType = 'decimal';
		else if (listOType.children[1].value == 2)
			pOList.style.listStyleType = 'decimal-leading-zero';
		else if (listOType.children[1].value == 3)
			pOList.style.listStyleType = 'hebrew';
		else if (listOType.children[1].value == 4)
			pOList.style.listStyleType = 'katakana';
		else if (listOType.children[1].value == 5)
			pOList.style.listStyleType = 'katakana-iroha';
		else if (listOType.children[1].value == 6)
			pOList.style.listStyleType = 'hiragana';
		else if (listOType.children[1].value == 7)
			pOList.style.listStyleType = 'hiragana-iroha';
		else if (listOType.children[1].value == 8)
			pOList.style.listStyleType = 'lower-latin';
		else if (listOType.children[1].value == 9)
			pOList.style.listStyleType = 'upper-latin';
		else if (listOType.children[1].value == 10)
			pOList.style.listStyleType = 'lower-roman';
		else if (listOType.children[1].value == 11)
			pOList.style.listStyleType = 'upper-roman';
		else if (listOType.children[1].value == 12)
			pOList.style.listStyleType = 'lower-greek';
		else if (listOType.children[1].value == 13)
			pOList.style.listStyleType = 'georgian';
		else if (listOType.children[1].value == 14)
			pOList.style.listStyleType = 'cjk-ideographic';
		else
			pOList.style.listStyleType = 'none';
	}]);
	//moze kiedys kiedys dorobi sie obrazki?
	var listUType = createSelectForm('Oznaczenia pozycji', 'listOType', ['Brak', 'Dyski', 'Koła', 'Kwadraty'], bbm, '', true);
	listUType.addEvent(['change'], [function() {
		if (listUType.children[1].value == 1)
			pUList.style.listStyleType = 'disc';
		else if (listUType.children[1].value == 2)
			pUList.style.listStyleType = 'circle';
		else if (listUType.children[1].value == 3)
			pUList.style.listStyleType = 'square';
		else
			pUList.style.listStyleType = 'none';
	}]);
	listUType.style.display = 'none';
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Ułożenie tekstu w liście";
	btnDiv.appendChild(btnLegend);
	createButtonCheck('centerT', 'Wyśrodkowany', '<i class="fa fa-align-center" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [buttonRadioCheckAction]);
	createButtonCheck('leftT', 'Do lewej', '<i class="fa fa-align-left" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv, false, true).addEvent(['click'], [buttonRadioCheckAction]);
	createButtonCheck('rightT', 'Do prawej', '<i class="fa fa-align-right" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [buttonRadioCheckAction]);
	createButtonCheck('justifyT', 'Równomierny', '<i class="fa fa-align-justify" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [buttonRadioCheckAction]);
	bbm.appendChild(btnDiv);
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Opcje tekstu na liście";
	btnDiv.appendChild(btnLegend);
	var boldBtn = createButtonCheck('boldT', 'Pogrubienie', '<i class="fa fa-bold" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	boldBtn.addEvent(['click'],[function() {buttonCheckAction(boldBtn); if (boldBtn.dataset.checked == 1) pPrev.style.fontWeight = 'bold'; else pPrev.style.fontWeight = '';}]);
	var itaBtn = createButtonCheck('italicT', 'Pochylenie', '<i class="fa fa-italic" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	itaBtn.addEvent(['click'], [function() {buttonCheckAction(itaBtn); if (itaBtn.dataset.checked == 1) pPrev.style.fontStyle = 'italic'; else pPrev.style.fontStyle = '';}]);
	var underBtn = createButtonCheck('underT', 'Podkreślony', '<i class="fa fa-underline" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	underBtn.addEvent(['click'], [function() {buttonCheckAction(underBtn); if (underBtn.dataset.checked == 1) pPrev.style.textDecoration += ' underline'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('underline', '')}]);
	var strikeBtn = createButtonCheck('strikeT', 'Przekreślony', '<i class="fa fa-strikethrough" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	strikeBtn.addEvent(['click'], [function() {buttonCheckAction(strikeBtn); if (strikeBtn.dataset.checked == 1) pPrev.style.textDecoration += ' line-through'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('line-through', '');}]);
	var supBtn = createButtonCheck('supT', 'Indeks górny', '<i class="fa fa-superscript" aria-hidden="true"></i>', '4', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	supBtn.addEvent(['click'], [function() {buttonCheckAction(supBtn); if (supBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'super'; else pPrev.style.verticalAlign = '';}]);
	var subBtn = createButtonCheck('subT', 'Indeks dolny', '<i class="fa fa-subscript" aria-hidden="true"></i>', '5', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	subBtn.addEvent(['click'], [function() {buttonCheckAction(subBtn); if (subBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'sub'; else pPrev.style.verticalAlign = '';}]);
	bbm.appendChild(btnDiv);
	//TRZEBA DOKONCZYC!!!!
	var pPrev = document.createElement('div');
	pPrev.id = "textActionPreview";
	var pHead = document.createElement('p');
	pHead.style.display = 'none';
	pHead.innerHTML = 'Nagłówek listy';
	pPrev.appendChild(pHead);
	var pOList = document.createElement('ol');
	var pUList = document.createElement('ul');
	var pDList = document.createElement('dl');
	pUList.style.display = 'none';
	pDList.style.display = 'none';
	pUList.style.listStyleType = 'none';
	pOList.style.listStyleType = 'none';
	createSampleList([pUList, pOList], 10);
	pPrev.appendChild(pOList);
	pPrev.appendChild(pUList);
	for (var i = 0; i < 5; i++) {
		var ddt = document.createElement('dt');
		var ddd = document.createElement('dd');
		ddt.innerHTML = 'Pozycja ' + (i + 1);
		ddd.innerHTML = 'Podpozycja pozycji ' + (i + 1);
		pDList.appendChild(ddt);
		pDList.appendChild(ddd);
	}
	pPrev.appendChild(pDList);
	bbm.appendChild(pPrev);
	createButton('Dodaj', 'button noselect', bbm).addEvent(['click'], [function () {
		var tmp = document.getElementById('preview');
		if (tmp === undefined) return;
		var newP = document.createElement('div');
		newP.dataset.name = 'list';
		newP.style = pPrev.style.cssText;
		if (pHead.style.display !== 'none')
			newP.appendChild(pHead);
		if (pUList.style.display !== 'none')
			newP.appendChild(pUList);
		else if (pOList.style.display !== 'none')
			newP.appendChild(pOList);
		else
			newP.appendChild(pDList);
		newP.addEventListener('mouseover', function() {
			newP.style.boxShadow = '0 0 10px #1A1A1A';
		}, true);
		newP.addEventListener('mouseout', function() {
			newP.style.boxShadow = '';
		}, true);
		//newP.innerHTML = ;
		tmp.appendChild(newP);
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', bbm).addEvent(['click'], [buttonBarActionDestroy]);
}

function createTable(table, rows, cols, tHeaderPrev, tBodyPrev, rowStyle, colStyle) {
	if (table === undefined) return -1;
	if (rows === undefined)  row = 2;
	if (cols === undefined) cols = 2;
	var cssTmp = tHeaderPrev.style.cssText;
	tHeaderPrev = document.createElement('thead');
	var tHeadRow = document.createElement('tr');
	if (rowStyle !== undefined && rowStyle !== '') tHeadRow.style = rowStyle;
	tHeaderPrev.style = cssTmp;
	var headerStyle = '';
	if (colStyle !== undefined && colStyle !== '')  {
		var htmp = colStyle.split(';');
		for (var i = 0; i < htmp.length; i++) {
			if (htmp[i].indexOf('border') != -1) {
				headerStyle = htmp[i];
				break;
			}
		}
	}
	for (var i = 0; i < cols; i++) {
		var tHeadCol = document.createElement('th');
		tHeadCol.dataset.pos = i;
		if (headerStyle !== '') tHeadCol.style = headerStyle;
		tHeadCol.innerHTML = (i + 1);
		tHeadRow.appendChild(tHeadCol);
	}
	tHeaderPrev.appendChild(tHeadRow);
	cssTmp = tBodyPrev.style.cssText;
	tBodyPrev = document.createElement('tbody');
	tBodyPrev.style = cssTmp;
	for (var i = 0; i < rows; i++) {
		var tRow = document.createElement('tr');
		tRow.dataset.pos = i;
		if (rowStyle !== undefined) tRow.style = rowStyle;
		for (var j = 0; j < cols; j++) {
			var tCol = document.createElement('td');
			tCol.dataset.pos = j;
			if (colStyle !== undefined) tCol.style = colStyle;
			tCol.innerHTML = 'TEST';
			tRow.appendChild(tCol);
		}
		tBodyPrev.appendChild(tRow);
	}
	while(table.children.length > 0)
		table.removeChild(table.lastChild);
	table.appendChild(tHeaderPrev);
	table.appendChild(tBodyPrev);
}

function headerChange(t, rStyle, cStyle) {
	if (rStyle === undefined) rStyle = document.getElementsByTagName('tr')[0].style.cssText;
	if (cStyle === undefined) cStyle = document.getElementsByTagName('td')[0].style.cssText;
	var tp = document.getElementsByTagName('thead')[0];
	console.log(t.children[1].value);
	if (t.children[1].value == 0) {
		tp.style.display = 'none';
		return;
	}
	var r = document.getElementsByName('tRows')[0].value;

	tp.style.display = '';
	if (t.children[1].value == 2)
		r -= 1;
	createTable(document.getElementsByTagName('table')[0], 
			r,
			document.getElementsByName('tCols')[0].value, 
			tp, 
			document.getElementsByTagName('tbody')[0],
			rStyle,
			cStyle);
}

function addTable(bbm) {
	var btn = document.getElementsByClassName('fa-table')[0];
	btn.className = 'active ' + btn.className;
	bbm.style.display = 'block';
	createHeadingTitle('h3', 'Dodaj tabelę', '', bbm);
	bbm.style.top = (btn.getBoundingClientRect().y | btn.getBoundingClientRect().top + btn.getBoundingClientRect().height) + 'px';
	var tableRowTmp = document.createElement('tr');
	var tableColTmp = document.createElement('td');
	var tColor = createInputForm('color', 'Kolor tekstu', 'tColor', '#000000', '(domyślny czarny)', bbm);
	tColor.addEvent(['change'], [function() {
		if (tHeaderColorChange.children[1].checked) {
			tableColTmp.style.color = '';  
			tableColTmp.style.background = ''; 
			tableRowTmp.style.color = tColor.children[1].value;   
			tableRowTmp.style.background = tBackColor.children[1].value; 
		}
		else {
			tableRowTmp.style.color = '';  
			tableRowTmp.style.background = '';  
			tableColTmp.style.color = tColor.children[1].value;  
			tableColTmp.style.background = tBackColor.children[1].value; 
		}
		if (tHeadRow.children[1].checked)
			createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else if (tHeadRow1.children[1].checked)
			headerChange(tHeadRow1, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else
			headerChange(tHeadRow2, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		//createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev, tableRowTmp.style.cssText, tableColTmp.style.cssText);
	}]);
	var tBackColor = createInputForm('color', 'Kolor tła', 'tBackColor', '#FFFFFF', '(domyślny biały)', bbm);
	tBackColor.addEvent(['change'], [function() {
		if (tHeaderColorChange.children[1].checked) {
			tableColTmp.style.color = '';  
			tableColTmp.style.background = ''; 
			tableRowTmp.style.color = tColor.children[1].value;   
			tableRowTmp.style.background = tBackColor.children[1].value; 
		}
		else {
			tableRowTmp.style.color = '';  
			tableRowTmp.style.background = '';  
			tableColTmp.style.color = tColor.children[1].value;  
			tableColTmp.style.background = tBackColor.children[1].value; 
		}
		if (tHeadRow.children[1].checked)
			createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else if (tHeadRow1.children[1].checked)
			headerChange(tHeadRow1, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else
			headerChange(tHeadRow2, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		//createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev, tableRowTmp.style.cssText, tableColTmp.style.cssText);
	}]);
	var tHeaderColorChange = createInputForm('checkbox', 'Kolory dotyczą nagłówka', 'tHeaderColorChange', '1', '', bbm, '', true);
	tHeaderColorChange.addEvent(['change'], [function() {
		if (tHeaderColorChange.children[1].checked) {
			tableColTmp.style.color = '';  
			tableColTmp.style.background = ''; 
			tableRowTmp.style.color = tColor.children[1].value;   
			tableRowTmp.style.background = tBackColor.children[1].value; 
		}
		else {
			tableRowTmp.style.color = '';  
			tableRowTmp.style.background = '';  
			tableColTmp.style.color = tColor.children[1].value;  
			tableColTmp.style.background = tBackColor.children[1].value; 
		}
		if (tHeadRow.children[1].checked)
			createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else if (tHeadRow1.children[1].checked)
			headerChange(tHeadRow1, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else
			headerChange(tHeadRow2, tableRowTmp.style.cssText, tableColTmp.style.cssText);
	}]);
	var tColumnSpace = createInputForm('checkbox', 'Kolumny są ze sobą zwarte', 'tColumnSpace', '1', '', bbm);
	tColumnSpace.addEvent(['change'], [function() {
		if (tColumnSpace.children[1].checked) 
			tPrev.style.borderSpacing = '0';  
		else
			tPrev.style.borderSpacing = ''; 
	}]);
	var tBorderWidth = createInputForm('number', 'Grubość obramowania', 'tBorderWidth', '0', '(domyślnie 0)', bbm);
	tBorderWidth.addEvent(['change'], [function() {
// 			if (tColumnSpace.children[1].value) 
		tableColTmp.style.border = parseInt(tBorderWidth.children[1].value) + 'px solid ' + tBorderColor.children[1].value;  
// 			else
// 				tableColTmp.style.borderSpacing = ''; 
		if (tHeadRow.children[1].checked)
			createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else if (tHeadRow1.children[1].checked)
			headerChange(tHeadRow1, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else
			headerChange(tHeadRow2, tableRowTmp.style.cssText, tableColTmp.style.cssText);
	}]);
	var tBorderColor = createInputForm('color', 'Kolor obramowania', 'tBackColor', '#000000', '(domyślny czarny)', bbm);
	tBorderColor.addEvent(['change'], [function() {
// 			if (tColumnSpace.children[1].value) 
		tableColTmp.style.border = parseInt(tBorderWidth.children[1].value) + 'px solid ' + tBorderColor.children[1].value;  
// 			else
// 				tableColTmp.style.borderSpacing = ''; 
		if (tHeadRow.children[1].checked)
			createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else if (tHeadRow1.children[1].checked)
			headerChange(tHeadRow1, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else
			headerChange(tHeadRow2, tableRowTmp.style.cssText, tableColTmp.style.cssText);
	}]);
// 		var tFontSize = createInputForm('number', 'Wielkość tekstu (px)', 'tFontSize', '', '(domyślny, ok. 19px)', bbm);
// 		tFontSize.addEvent(['change'], [function() {if (tFontSize.children[1].value === '') pPrev.style.fontSize = ''; else pPrev.style.fontSize = parseInt(tFontSize.children[1].value) + 'px';}]);
	var tRows = createInputForm('number', 'Ilość wierszy', 'tRows', '2', '(domyślnie 2)', bbm);
	tRows.addEvent(['change'], [function() {
		if (tHeadRow.children[1].checked)
			createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else if (tHeadRow1.children[1].checked)
			headerChange(tHeadRow1, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else
			headerChange(tHeadRow2, tableRowTmp.style.cssText, tableColTmp.style.cssText);
	}]);
	var tCols = createInputForm('number', 'Ilość kolumn', 'tCols', '2', '(domyślnie 2)', bbm);
	tCols.addEvent(['change'], [function() {
		if (tHeadRow.children[1].checked)
			createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else if (tHeadRow1.children[1].checked)
			headerChange(tHeadRow1, tableRowTmp.style.cssText, tableColTmp.style.cssText);
		else
			headerChange(tHeadRow2, tableRowTmp.style.cssText, tableColTmp.style.cssText);
	}]);
	var fieldSet = document.createElement('fieldset');
	var fieldsetLegend = document.createElement('legend');
	fieldsetLegend.innerHTML = "Kolumna nagłówkowa";
	fieldSet.appendChild(fieldsetLegend);
	var tHeadRow = createInputForm('radio', 'Brak', 'tHeadRow', '0', '', fieldSet, '', true);
	tHeadRow.addEvent(['change'], [function(t) {headerChange(tHeadRow);}]);
	var tHeadRow1 = createInputForm('radio', 'Nagłówek oddzielny (dodatkowy wiersz)', 'tHeadRow', '1', '', fieldSet);
	tHeadRow1.addEvent(['change'], [function(t) {headerChange(tHeadRow1);}]);
	var tHeadRow2 = createInputForm('radio', 'Nagłówek w sumie wierszy', 'tHeadRow', '2', '', fieldSet);
	tHeadRow2.addEvent(['change'], [function(t) {headerChange(tHeadRow2);}]);
	bbm.appendChild(fieldSet);
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	var pPrev = document.createElement('div');
	pPrev.id = "textActionPreview";
	var tPrev = document.createElement('table');
// 		tPrev.style.border = '1px solid black';
	tPrev.style.width = '100%';
	var tHeaderPrev = document.createElement('thead');
	tHeaderPrev.style.display = 'none';
	var tBodyPrev = document.createElement('tbody');;
	
	createTable(tPrev, tRows.children[1].value, tCols.children[1].value, tHeaderPrev, tBodyPrev);
	console.log(document.getElementsByTagName('thead'));
	bbm.appendChild(tPrev);
	createButton('Dodaj', 'button noselect', bbm).addEvent(['click'], [function () {
		var tmp = document.getElementById('preview');
		if (tmp === undefined) return;
		var newP = document.createElement('div');
		newP.style = pPrev.style.cssText;
		newP.dataset.name = 'table';
		var newTable = document.createElement('table');
		newTable.style = tPrev.style.cssText;
		var newHeadT = document.getElementsByTagName('thead')[0];
		if (newHeadT.style.display !== 'none')
			newTable.appendChild(newHeadT);
		newTable.appendChild(document.getElementsByTagName('tbody')[0]);
		//newP.innerHTML = "[Wprowadź tekst akapitu]";
		newP.appendChild(newTable);
		newP.addEventListener('mouseover', function() {
			newP.style.boxShadow = '0 0 10px #1A1A1A';
		}, true);
		newP.addEventListener('mouseout', function() {
			newP.style.boxShadow = '';
		}, true);
		tmp.appendChild(newP);
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', bbm).addEvent(['click'], [buttonBarActionDestroy]);
}

//tworzenie 
function addLink(bbm) {
	var btn = document.getElementsByClassName('fa-link')[0];
	btn.className = 'active ' + btn.className;
	bbm.style.display = 'block';
	//bbm.style.minWidth = '500px';
	createHeadingTitle('h3', 'Dodaj odnośnik', '', bbm);
	bbm.style.top = (btn.getBoundingClientRect().y | btn.getBoundingClientRect().top + btn.getBoundingClientRect().height) + 'px';
	var hColor = createInputForm('color', 'Kolor tekstu', 'hColor', '#000000', '', bbm);
	hColor.addEvent(['change'], [function() {lPrev.style.color = hColor.children[1].value;}]);
	var hBackColor = createInputForm('color', 'Kolor tła', 'hBackColor', '#FFFFFF', '', bbm);
	hBackColor.addEvent(['change'], [function() {pPrev.style.background = hBackColor.children[1].value;}]);
	var hFontSize = createInputForm('number', 'Wielkość tekstu (px)', 'hFontSize', '', '(domyślny)', bbm);
	hFontSize.addEvent(['change'], [function() {if (hFontSize.children[1].value === '') pPrev.style.fontSize = ''; else pPrev.style.fontSize = parseInt(hFontSize.children[1].value) + 'px';}]);
	var lText = createInputForm('text', 'Tekst odnośnika', 'lText', '', '(wprowadź tekst)', bbm); 
	lText.addEvent(['keyup'], [function() {
		if (lText.children[1].value !== '') 
			lPrev.innerHTML = lText.children[1].value;
		else
			lPrev.innerHTML = '(wprowadź tekst)';
	}]);
	var lHref = createInputForm('text', 'Źródło (adres URL)', 'lHref', '', '(np. http://www.example.com)', bbm); 
	lHref.addEvent(['keyup'], [function() {
		if (lHref.children[1].value !== '') 
			lPrev.href = lHref.children[1].value;
		else
			lPrev.href = '#';
	}]);
	var lTarget = createInputForm('checkbox', 'Odnośnik w nowym oknie', 'lTarget', '', '(domyślny)', bbm);
	var pPrev = document.createElement('p');
	pPrev.id = "textActionPreview";
	var lPrev = document.createElement('a');
	lPrev.href = '#';
	lPrev.target ='_blank';
	lPrev.innerHTML = '(wprowadź tekst)';
	pPrev.appendChild(lPrev);
	bbm.appendChild(pPrev);
	createButton('Dodaj', 'button noselect', bbm).addEvent(['click'], [function () {
		var tmp = document.getElementById('preview');
		if (tmp === undefined) return;
		if (!lTarget.children[1].checked)
			lPrev.target ='_self';
		lPrev.addEventListener('mouseover', function() {
			lPrev.style.boxShadow = '0 0 10px #1A1A1A';
		}, true);
		lPrev.addEventListener('mouseout', function() {
			lPrev.style.boxShadow = '';
		}, true);
		tmp.appendChild(lPrev);
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', bbm).addEvent(['click'], [buttonBarActionDestroy]);
}

//prawdopodobnie do wywalenia
// 	function foo2() {
// 		var tmp = document.getElementById('preview');
// 		if (tmp === undefined) return;
// 		tmp = tmp.getElementsByTagName('div')[0];
// 		if (tmp === undefined) return;
// 		tmp = tmp.children[0];
// 		tmp.innerHTML += '<span style="color: green;">klik</span>';
// 		console.log(tmp);
// 	}

//przechwytywanie aktualnie zaznaczonego fragmentu tekstu do zmiennej globalnej currentSelect;
//przechwycony fragment można wykorzystywać np. do dodania paragrafu, nagłówka, oznaczonego tesktu itp.
function selectionHandle() {
	console.log(window.getSelection());
	currentSelect = window.getSelection().getRangeAt(0);		
}

//funkcja w zamierzeniu bedzie zwracac(?) przetworzone zaznaczenie
//byc moze do przebudowy/usuniecia
function getCurrentSelect() {
	if (currentSelect === undefined) return;
	//console.log(currentSelect);
	console.log('start ' + currentSelect.startOffset + ' koniec ' + currentSelect.endOffset + ' suma zaznaczenia ' + (currentSelect.endOffset - currentSelect.startOffset) + ' tekst ' + currentSelect.toString());
}

//funkcja pokazuje/ukrywa kod HTML wprowadzanej wiadomosci
function hideHTML(t) {
	if (t.className.indexOf('-slash') !== -1) {
		document.getElementById('prevCode').style.display = "none";
		t.className = 'fa fa-eye noselect';
		t.title = "Pokaż kod";
	}
	else {
		document.getElementById('prevCode').style.display = "";
		t.className = 'fa fa-eye-slash noselect';
		t.title = "Ukryj kod";
	}
}

//funkcja po pierwsze odznacza potencjalnie wybrane przyciski edycji tekstu (elementsButtonBar)
//po czym czyści warstwę akcji przycisków i chowa ją (buttonBarMenu)
function buttonBarActionDestroy() {
	var bbm = document.getElementById('elementsButtonBar');
	for(var i = 0; i < bbm.children.length; i++) 
		bbm.children[i].removeClassName('active');
	bbm = document.getElementById('buttonBarMenu');
	//console.log( bbm.children);
	while(bbm.children.length > 0) {
		//console.log( bbm.lastChild);
		bbm.removeChild(bbm.lastChild);
	}
	bbm.style.display = '';
	bbm.removeClassName('propertiesBack');
	return bbm;
}

//funkcja odpowiada za aktywowanie odpowiedniej opcji w menu
//kazda z opcji posiada swoj numer
function buttonBarAction(act) {
	if (act === undefined) return;
	var bbm = buttonBarActionDestroy(); //document.getElementById('buttonBarMenu');
	if (act == 0) {
		imageSetup(bbm);
	}
	else if (act == 1)
		addHeading(bbm);
	else if (act == 2)
		addParagraph(bbm);
	else if (act == 3)
		addList(bbm);
	else if (act == 4)
		addTable(bbm);
	else if (act == 5)
		addLink(bbm);
}

//funkcja tworzy pole typu INPUT, otoczone w warstwę DIV oraz z podpowiedzią (jako paragraf P)
//Wejście:
//type - typ pole (text, passwword, checkbox, radio itd)
//text - opis pola, ktory bedzie się przy nim wyświetlał
//name - nazwa pola, po której można je będzie zidentyfikować
//value - wartość domyślna w polu (zmienna może pozostać pusta)
//hint - podpowiedź do pola (wyświetla się w chwili gdy w polu nie znajduje się żadna wartość)
//toElem - jeżeli podano obiekt, to utworzone pole będzie jego dzieckiem 
//id - id dodawanego elementu (dotyczy całej warstwy)
//checked - jeżeli mamy do czynienia z polem checkbox/radio to możemy dodać wartość true (będzie domyślnie zaznaczone)
function createInputForm(type, text, name, value, hint, toElem, id, checked) {
	console.log(value);
	if (type === undefined || text === undefined || name === undefined) return -1;
	
	var newDiv = document.createElement('div');
	var newSpan = document.createElement('p');
	var newItem = document.createElement('input');
	newSpan.innerHTML = text;
	newSpan.className = 'inputP';
	newDiv.appendChild(newSpan);
	newItem.type = type;
	newItem.name = name;
	if (value !== undefined)
		newItem.value = value;
	if (checked !== undefined)
		newItem.checked = checked;
	if (hint !== undefined)
		newItem.placeholder = hint;
	if (id !== undefined && id !== false && id !== '')
		newDiv.id = id;
	newDiv.appendChild(newItem);
	newDiv.addEvent = addEventHandler;
	if (toElem !== undefined && toElem !== '')
		toElem.appendChild(newDiv);
// 		else
// 			return newDiv;
	return newDiv;
}

function createSelectForm(text, name, options, toElem, selectedNum, indexed) {
	if (text === undefined || name === undefined || options === undefined) return -1;
	if (selectedNum === undefined) selectedNum = -1;
	if (indexed === undefined || indexed === '') indexed = false;
	var newDiv = document.createElement('div');
	var newP = document.createElement('p');
	var newSel = document.createElement('select');
	newP.innerHTML = text;
	newP.className = 'inputP';
	newDiv.appendChild(newP);
	newSel.name = name;
	for (var i = 0; i < options.length; i++) {
		var newOpt = document.createElement('option');
		if (indexed)
			newOpt.value = i;
		else
			newOpt.value = options[i];
		newOpt.innerHTML = options[i];
		if (selectedNum == i)
			newOpt.selected = "selected";
		newSel.appendChild(newOpt);
	}
	newDiv.appendChild(newSel);
	newDiv.addEvent = addEventHandler;
	if (toElem !== undefined && toElem !== '')
		toElem.appendChild(newDiv);
	return newDiv;
}

//tworzy element-przycisk (tworzony z warstwy DIV oraz paragrafu P)
//Wejście:
//text - napis na przyciski
//btnClass - nazwa klasy przycisku (pozwala na dostosowanie wyglądu przycisku wedle swojej woli)
//toELem - jeżeli podano obiekt, to utworzone pole będzie jego dzieckiem 
function createButton(text, btnClass, toElem) {
	if (text === undefined || btnClass  === undefined) return -1;
	var newDiv = document.createElement('div');
	newDiv.className = btnClass;
	var newP = document.createElement('p');
	newP.innerHTML = text;
	newDiv.appendChild(newP);
	newDiv.addEvent = addEventHandler;
	if (toElem !== undefined && toElem !== '') 
		toElem.appendChild(newDiv);
// 		else
		
	return newDiv;
// 		return 1;
}

//prototype for dynamic create elements - adding by custom method to object
function addEventHandler(ev, evFunctions, b) {
	if (ev.length != evFunctions.length) return -2;
	if (b === undefined) b = true;
// 		console.log(ev);
	for (var i = 0; i < ev.length; i++) {
		this.addEventListener(ev[i], evFunctions[i], b);
	}
}

//funkcja wywoływana przy zmianie typu dodawanego obrazu; zmiena pole dodawania obazu do tekstu
//domyślnie pozwala na załadowanie dowolnego pliku z dysku osoby dodającej wiadomość (zamienia na format znakowy + 33% do objętości zdjęcia)
//drugą opcją jest dodanie zdjęcia jako odnośnika z serwera WWW/innej strony (zajmuje tylko adres - nic więcej)
//trzecia opcja to załadowanie zdjęcia z istniejącej już galerii na stronie
function changeAddImageType() {
	//console.log("ZDARZENIE " + this.children[1].name);	
	this.children[1].checked = true;
	var ifp = document.getElementById('imgFilePath');
	var bbm = ifp.parentNode;
	var newElem;
	
	//while(ifp.children.length > 0) ifp.removeChild(ifp.lastChild);
	if (this.children[1].value == 0) {
		newElem = createInputForm('file', 'Wskaż plik do dodania', 'fileItem', '', '', '', 'imgFilePath');
	}
	else if (this.children[1].value == 1) {
		newElem = createInputForm('text', 'Wpis/skopiuj adres do obrazka', 'linkItem', '', '', '', 'imgFilePath');
	}
	else if (this.children[1].value == 2) {
	//dopracowac po podlaczeniu do calego panelu!!
		newElem = createInputForm('text', 'Wskaż zdjęcie z galerii', 'galleryItem', '', '[kliknij by dodać zdjęcie]', '', 'imgFilePath');
	}
	newElem.addEvent(['change'], [imagePreview]);
	newElem.onclick = function() {galleryOpen(); console.log('TUTAJ');};
	//console.log(newElem);
	bbm.insertBefore(newElem, ifp);
	bbm.removeChild(ifp);
	document.getElementById('tmpImg').src = '';
}

//chyba do usuniecia
function moveImage() {
	
}

//funkcja usuwa wszystkie opcje z podręcznego spisu
function clearRightMenu() {
	var subMenu = document.getElementById('rightMenu');
	while(subMenu.children.length > 0)
		subMenu.removeChild(subMenu.lastChild);
	return subMenu;
}

//funkcja pozwala na dodawanie elementu do podręcznego spisu
//Wejście:
//t - nazwa dodawanej opcji
//toElem - jeżeli podano obiekt, to utworzone pole będzie jego dzieckiem 
function rightMenuAddItem(t, toElem) {
	var tmpC = document.createElement('p');
	tmpC.innerHTML = t;
	tmpC.addEvent = addEventHandler;
	if (toElem !== undefined && toElem !== '') 
		toElem.appendChild(tmpC);
	//console.log(toElem);
	return tmpC;
}

function imageProperties(item) {
	if (item === undefined) return;
	var bbm = buttonBarActionDestroy();
	bbm.style.display = 'block';
	bbm.className += ' propertiesBack';
	bbm.style.top = '0px';
	var divTmp = document.createElement('div');
	divTmp.style.background = 'white';
	divTmp.style.width = '50%';
	divTmp.style.height = '90%';
	divTmp.style.overflow = 'auto';
	divTmp.style.position = 'absolute';
	divTmp.style.top = '5%';
	divTmp.style.left = '25%';
	createHeadingTitle('h3', 'Właściwości listy', '', divTmp);
	bbm.appendChild(divTmp);
	var iWidthP = createInputForm('checkbox', 'Szerokość procentowa', 'iWidthP', '1', '', divTmp);
	iWidthP.addEvent(['change'], [function() {
		var m = 'px';
		if (iWidthP.children[1].checked) m = '%';
		if (iWidth.children[1].value === '') 
			pPrev.style.height = ''; 
		else 
			pPrev.style.height = parseInt(iWidth.children[1].value) + m;
		if (iProp.children[1].checked) {
			iHeight.children[1].value = '';
			pPrev.style.height = 'auto';
		}
	}]);
	var iWidth = createInputForm('number', 'Szerokość zdjęcia', 'iWidth', item.width, '(auto)', divTmp);
	iWidth.addEvent(['change'], [function() {
		
		var m = 'px';
		if (iWidthP.children[1].checked) m = '%';
		if (iWidth.children[1].value === '') 
			pPrev.style.width = ''; 
		else 
			pPrev.style.width = parseInt(iWidth.children[1].value) + m;
		if (iProp.children[1].checked) {
			iHeight.children[1].value = '';
			pPrev.style.height = 'auto';
		}
	}]);
	var iHeightP = createInputForm('checkbox', 'Wysokość procentowa', 'iHeightP', '1', '', divTmp);
	iHeightP.addEvent(['change'], [function() {
		var m = 'px';
		if (iHeightP.children[1].checked) m = '%';
		if (iHeight.children[1].value === '') 
			pPrev.style.height = ''; 
		else 
			pPrev.style.height = parseInt(iHeight.children[1].value) + m;
		if (iProp.children[1].checked) {
			iWidth.children[1].value = '';
			pPrev.style.width = 'auto';
			
		}
	}]);
	var iHeight = createInputForm('number', 'Wysokość zdjęcia', 'iHeight', item.height, '(auto)', divTmp);
	iHeight.addEvent(['change'], [function() {
		var m = 'px';
		if (iHeightP.children[1].checked) m = '%';
		if (iHeight.children[1].value === '') 
			pPrev.style.height = ''; 
		else 
			pPrev.style.height = parseInt(iHeight.children[1].value) + m;
		if (iProp.children[1].checked) {
			iWidth.children[1].value = '';
			pPrev.style.width = 'auto';
			
		}
	}]);
	var iProp = createInputForm('checkbox', 'Zachowaj proporcje zdjęcia', 'iProp', '1', '', divTmp);

	var divPrev = document.createElement('div');
	divPrev.style.width = '100%';
	divPrev.style.maxHeight = '300px';
	divPrev.style.overflow = 'auto';
	var pPrev = item.cloneNode();
	//pPrev.src = item.src;
	pPrev.style = item.style.cssText;
	//pPrev.dataset = item.dataset;
	divPrev.appendChild(pPrev);
	divTmp.appendChild(divPrev);
	createButton('Dodaj', 'button noselect', divTmp).addEvent(['click'], [function () {
		item.style = pPrev.style.cssText;
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', divTmp).addEvent(['click'], [buttonBarActionDestroy]);
}

//funkcja tworzy spis podręczny dla klikniętych prawym przyciskiem myszy zdjęć
//poniewaz zdjecia moga byc zawarte w elemencie wyzszym (np. figure) funkcja bierze to pod uwage
function imageRightClick(e) {
	var subMenu = clearRightMenu();
	var figureTmp;
	var imgTmp;
	var figCapTmp;
	if (e.target.tagName.toLowerCase() === 'figure') {
		figureTmp = e.target;
	}
	else {
		figureTmp = e.target.parentNode;
	}
	imgTmp = figureTmp.children[0];
	if (figureTmp.children.length == 2) 
		figCapTmp = figureTmp.children[1];
// 		rightMenuAddItem('Przemieść zdjęcie', subMenu).addEvent(['click'], [function() {
// 			//moveImage(e);
// 		}]);
	rightMenuAddItem('Właściwości zdjęcia', subMenu).addEvent(['click'], [function() {
		imageProperties(imgTmp);
	}]);
	rightMenuAddItem('Usuń zdjęcie',subMenu).addEvent(['click'], [function() {
		figureTmp.parentNode.removeChild(figureTmp);
// 			if (e.target.parentNode.tagName.toLowerCase() === 'figure')
// 				e.target.parentNode.parentNode.removeChild(e.target.parentNode);
// 			else
// 				e.target.parentNode.removeChild(e.target);
	}]);
	rightMenuAddItem('Opływ od lewej', subMenu).addEvent(['click'], [function() {
		imgTmp.style.float = "left";
		//e.target.style.float = "left";
	}]);
	rightMenuAddItem('Opływ od prawej', subMenu).addEvent(['click'], [function() {
		imgTmp.style.float = "right";
		//e.target.style.float = "right";
	}]);
	rightMenuAddItem('Brak opływu', subMenu).addEvent(['click'], [function() {
		imgTmp.style.float = "";
		imgTmp.style.position = "";
// 			e.target.style.float = "";
// 			e.target.style.position = "";
	}]);
	rightMenuAddItem('Tekst nad obrazem', subMenu).addEvent(['click'], [function() {
		imgTmp.style.zIndex = "-10";
		imgTmp.style.position = "absolute";
		figureTmp.style.position = "relative";
		//e.target.style.position = "absolute";
		//e.target.style.zIndex = "-10";
	}]);
	openContext(e);
}

function highlightTable(table, e, row, col, reverse) {
	if (table === undefined || e === undefined || row === undefined) return;
	if (reverse === undefined) reverse = '0.5'; else reverse = '';
	if (col === undefined) col = -1;
	if (row != -1 && col != -1) {
		e.style.opacity = reverse;
		if (reverse !== '') {
			e.dataset.backorg = e.style.background.toString();
			if (e.style.background === '')
				e.style.background = 'white';
		}
		else {
			e.style.background = e.dataset.backorg;
		}
	}
	else if (row != -1) {
		e[row].style.opacity = reverse;
	}
	else if (e == -1) {
		var tBody;
		if (table.children.length == 2) {
			table.children[0].children[0].children[col].style.opacity = reverse;
			if (reverse !== '') {
				table.children[0].children[0].children[col].dataset.backorg = table.children[0].children[0].children[col].style.background.toString();
				if (table.children[0].children[0].children[col].style.background === '')
					table.children[0].children[0].children[col].style.background = 'white';
			}
			else {
				table.children[0].children[0].children[col].style.background = table.children[0].children[0].children[col].dataset.backorg;
				//table.children[0].children[0].children[col].dataset.backOrg = '';
			}
			tBody = table.children[1];
		}
		else tBody = table.children[0];
		for (var i = 0; i < tBody.children.length; i++) {
			tBody.children[i].children[col].style.opacity = reverse;
			if (reverse !== '') {
				tBody.children[i].children[col].dataset.backorg = tBody.children[i].children[col].style.background;
				if (tBody.children[i].children[col].style.background === '')
				tBody.children[i].children[col].style.background = 'white';
			}
			else tBody.children[i].children[col].style.background = tBody.children[i].children[col].dataset.backorg;
		}
	}
}

function toHex(c) {
	return c.length == 1 ? '0' + c : c;
}

function hexColor(c) {
	if (c === undefined) return '#000';
	if (c.indexOf('#') != -1 && c.length < 9)
		return c;
	if (c.indexOf('rgb') != -1) {
		c = c.toString().split('(')[1].split(')')[0].split(',');
		return '#' + toHex(parseInt(c[0]).toString(16)) + toHex(parseInt(c[1]).toString(16)) + toHex(parseInt(c[2]).toString(16));
	}
	if (c === '') return '#fff';
}

function tableRightProperties(item, pt, row, col) {
	if (item === undefined) return;
	if (row === undefined) row = -1;
	var bbm = buttonBarActionDestroy();
	bbm.style.display = 'block';
	bbm.className += ' propertiesBack';
	bbm.style.top = '0';
	var divTmp = document.createElement('div');
	divTmp.style.background = 'white';
	divTmp.style.width = '50%';
	divTmp.style.overflow = 'auto';
	divTmp.style.height = '90%';
	divTmp.style.position = 'absolute';
	divTmp.style.top = '5%';
	divTmp.style.left = '25%';
	createHeadingTitle('h3', 'Właściwości elementu', '', divTmp);
	bbm.appendChild(divTmp);
	var colorTmp = createInputForm('color', 'Kolor tekstu', 'colorTmp', hexColor(item.style.color), '', divTmp);
	colorTmp.addEvent(['change'], [function() {pPrev.style.color = colorTmp.children[1].value;}]);
	var backColorTmp = createInputForm('color', 'Kolor tła', 'backColorTmp', hexColor(item.style.background), '', divTmp);
	backColorTmp.addEvent(['change'], [function() {pPrev.style.background = backColorTmp.children[1].value;}]);
	var tColumnSpace = createInputForm('checkbox', 'Kolumny są ze sobą zwarte', 'tColumnSpace', '1', '', divTmp);
	tColumnSpace.addEvent(['change'], [function() {
		if (tColumnSpace.children[1].checked) 
			pPrev.style.borderSpacing = '0';  
		else
			pPrev.style.borderSpacing = ''; 
	}]);
	var tborderStyle = createSelectForm('Styl obramowania', 'tborderStyle', ['Brak', 'Lity', 'Kropki', 'Kreski', 'Prążkowe', 'Podwójny lity', 'Wklęsłe', 'Wypukłe', 'Żłobione',], divTmp, '', true);
	tborderStyle.addEvent(['change'], [function() {
		var t = [];
		if (pPrev.tagName.toLowerCase() === 'tr')
			t = pPrev.children;
		else if (pPrev.tagName.toLowerCase() === 'tbody')
			for (var j = 0; j < pPrev.children.length; j++) {
				t[j] = pPrev.children[j].children[0];
				console.log(t);
			}
		else if (pPrev.tagName.toLowerCase() === 'table') {
			if (tTdChange !== undefined) if (tTdChange.children[1].checked)
				for (var i = 0; i < pPrev.lastChild.children.length; i++) 
					for (var j = 0; j < pPrev.lastChild.children[i].children.length; j++)
						t[i * pPrev.lastChild.children.length + j] = pPrev.lastChild.children[i].children[j];
			t[t.length] = pPrev;
		}
		else t = [pPrev];
		//else if
		for (var i = 0; i < t.length; i++)
			if (tborderStyle.children[1].value == 1)
				t[i].style.borderStyle = 'solid';
			else if (tborderStyle.children[1].value == 2)
				t[i].style.borderStyle = 'dotted';
			else if (tborderStyle.children[1].value == 3)
				t[i].style.borderStyle = 'dashed';
			else if (tborderStyle.children[1].value == 4)
				t[i].style.borderStyle = 'ridge';
			else if (tborderStyle.children[1].value == 5)
				t[i].style.borderStyle = 'double solid';
			else if (tborderStyle.children[1].value == 6)
				t[i].style.borderStyle = 'inset';
			else if (tborderStyle.children[1].value == 7)
				t[i].style.borderStyle = 'outset';
			else if (tborderStyle.children[1].value == 8)
				t[i].style.borderStyle = 'groove';
			else 
				t[i].style.borderStyle = 'none';
	}]);
	var tBorderWidth = createInputForm('number', 'Grubość obramowania', 'tBorderWidth', '0', '(domyślnie 0)', divTmp);
	tBorderWidth.addEvent(['change'], [function() {
		var t = [];
		if (pPrev.tagName.toLowerCase() === 'tr')
			t = pPrev.children;
		else if (pPrev.tagName.toLowerCase() === 'tbody')
			for (var i = 0; i < pPrev.children.length; i++)
				t[i] = pPrev.children[i].children[0];
		else if (pPrev.tagName.toLowerCase() === 'table') {
			if (tTdChange !== undefined) if (tTdChange.children[1].checked)
				for (var i = 0; i < pPrev.lastChild.children.length; i++) 
					for (var j = 0; j < pPrev.lastChild.children[i].children.length; j++)
						t[i * pPrev.lastChild.children.length + j] = pPrev.lastChild.children[i].children[j];
			t[t.length] = pPrev;
		}
		else t = [pPrev];
		//else if
		for (var i = 0; i < t.length; i++)
			t[i].style.borderWidth = parseInt(tBorderWidth.children[1].value) + 'px';// solid ' + tBorderColor.children[1].value;  
	}]);
	var tBorderColor = createInputForm('color', 'Kolor obramowania', 'tBackColor', hexColor(item.style.borderColor), '(domyślny czarny)', divTmp);
	tBorderColor.addEvent(['change'], [function() {
		var t = [];
		if (pPrev.tagName.toLowerCase() === 'tr')
			t = pPrev.children;
		else if (pPrev.tagName.toLowerCase() === 'tbody')
			for (var i = 0; i < pPrev.children.length; i++)
				t[i] = pPrev.children[i].children[0];
		else if (pPrev.tagName.toLowerCase() === 'table') {
			console.log(tTdChange.children[1].checked + ' tutaj');
			if (tTdChange !== undefined) if (tTdChange.children[1].checked)
				for (var i = 0; i < pPrev.lastChild.children.length; i++) 
					for (var j = 0; j < pPrev.lastChild.children[i].children.length; j++)
						t[i * pPrev.lastChild.children.length + j] = pPrev.lastChild.children[i].children[j];
			t[t.length] = pPrev;
		}
		else t = [pPrev];
		//else if
		for (var i = 0; i < t.length; i++)
			t[i].style.borderColor = tBorderColor.children[1].value;  
	}]);
	if (item.tagName.toLowerCase() === 'table')
		var tTdChange = createInputForm('checkbox', 'Zmień obramowanie komórek', 'tTdChange', '1', '', divTmp);
	var tFontSize = createInputForm('number', 'Wielkość tekstu (px)', 'tFontSize', '', '(domyślny)', divTmp);
	tFontSize.addEvent(['change'], [function() {if (tFontSize.children[1].value === '') pPrev.style.fontSize = ''; else pPrev.style.fontSize = parseInt(tFontSize.children[1].value) + 'px';}]);
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Ułożenie tekstu";
	btnDiv.appendChild(btnLegend);
	createButtonCheck('centerT', 'Wyśrodkowany', '<i class="fa fa-align-center" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	createButtonCheck('leftT', 'Do lewej', '<i class="fa fa-align-left" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv, false, true).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	createButtonCheck('rightT', 'Do prawej', '<i class="fa fa-align-right" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	createButtonCheck('justifyT', 'Równomierny', '<i class="fa fa-align-justify" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	divTmp.appendChild(btnDiv);
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Opcje tekstu";
	btnDiv.appendChild(btnLegend);
	var boldBtn = createButtonCheck('boldT', 'Pogrubienie', '<i class="fa fa-bold" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	boldBtn.addEvent(['click'],[function() {buttonCheckAction(boldBtn); if (boldBtn.dataset.checked == 1) pPrev.style.fontWeight = 'bold'; else pPrev.style.fontWeight = '';}]);
	var itaBtn = createButtonCheck('italicT', 'Pochylenie', '<i class="fa fa-italic" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	itaBtn.addEvent(['click'], [function() {buttonCheckAction(itaBtn); if (itaBtn.dataset.checked == 1) pPrev.style.fontStyle = 'italic'; else pPrev.style.fontStyle = '';}]);
	var underBtn = createButtonCheck('underT', 'Podkreślony', '<i class="fa fa-underline" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	underBtn.addEvent(['click'], [function() {buttonCheckAction(underBtn); if (underBtn.dataset.checked == 1) pPrev.style.textDecoration += ' underline'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('underline', '')}]);
	var strikeBtn = createButtonCheck('strikeT', 'Przekreślony', '<i class="fa fa-strikethrough" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	strikeBtn.addEvent(['click'], [function() {buttonCheckAction(strikeBtn); if (strikeBtn.dataset.checked == 1) pPrev.style.textDecoration += ' line-through'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('line-through', '');}]);
	var supBtn = createButtonCheck('supT', 'Indeks górny', '<i class="fa fa-superscript" aria-hidden="true"></i>', '4', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	supBtn.addEvent(['click'], [function() {buttonCheckAction(supBtn); if (supBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'super'; else pPrev.style.verticalAlign = '';}]);
	var subBtn = createButtonCheck('subT', 'Indeks dolny', '<i class="fa fa-subscript" aria-hidden="true"></i>', '5', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	subBtn.addEvent(['click'], [function() {buttonCheckAction(subBtn); if (subBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'sub'; else pPrev.style.verticalAlign = '';}]);
	divTmp.appendChild(btnDiv);
	if (item.tagName.toLowerCase() === 'th' || item.tagName.toLowerCase() === 'table')
		var tHeadChange = createInputForm('checkbox', 'Zmień także komórkę nagłówka', 'tHeadChange', '1', '', divTmp);

	var divPrev = document.createElement('div');
	divPrev.style.width = '100%';
	divPrev.style.maxHeight = '100px';
	divPrev.style.overflow = 'hidden';
	var prevTable;
	var prevRow;
	var pPrev;
	if (row != -1 && col !== undefined) {
		prevTable = document.createElement('table');
		divPrev.appendChild(prevTable);
		prevRow = document.createElement('tr');
		prevTable.appendChild(prevRow);
		pPrev = item.cloneNode();
		pPrev.innerHTML = item.innerHTML;
		prevRow.appendChild(pPrev);
	}
	else if (row != -1 || col !== undefined) {
		prevTable = document.createElement('table');
		divPrev.appendChild(prevTable);
		pPrev = item.cloneNode();
		pPrev.innerHTML = item.innerHTML;
		prevTable.appendChild(pPrev);
	}
	else  {
		pPrev = item.cloneNode();
		pPrev.innerHTML = item.innerHTML;
		divPrev.appendChild(pPrev);
	}
	divTmp.appendChild(divPrev);
	createButton('Dodaj', 'button noselect', divTmp).addEvent(['click'], [function () {
		if (row != -1 && col !== undefined) item.style = pPrev.style.cssText;
		else if (row != -1) {
			item.style = pPrev.style.cssText;
			for (var i = 0; i < pPrev.children.length; i++) {
				item.children[i].style = pPrev.children[i].style.cssText;
			}
		}
		else if (col !== undefined) {
			var tdpos =  pPrev.lastChild.lastChild.dataset.pos;
			var n = 0;
			if (pt.children.length == 2) n = 1;
			for (var i = 0; i < pPrev.children.length; i++) {
				pt.children[n].children[i].children[tdpos].style = pPrev.lastChild.lastChild.style.cssText + ' ' + pPrev.lastChild.style.cssText + ' ' + pPrev.style.cssText;
			}
			if (tHeadChange !== undefined)
				if (tHeadChange.children[1].checked && n == 1)
					pt.children[0].children[0].children[tdpos].style =  pPrev.lastChild.lastChild.style.cssText + ' ' + pPrev.lastChild.style.cssText + ' ' + pPrev.style.cssText;
		}
		else {
			item.style = pPrev.style.cssText;
			var n = 0;
			if (pPrev.children.length == 2) n = 1;
			if (tTdChange !== undefined) if (tTdChange.children[1].checked) {
				for (var i = 0; i < pPrev.children[n].children.length; i++) {
					for (var j = 0; j < pPrev.children[n].children[i].children.length; j++)
					item.children[n].children[i].children[j].style = pPrev.lastChild.lastChild.lastChild.style.cssText;
				}
				if (tHeadChange !== undefined)
				if (tHeadChange.children[1].checked && n == 1)
					for (var i = 0; i < pPrev.children[0].children[0].children.length; i++)
						item.children[0].children[0].children[i].style = pPrev.lastChild.lastChild.lastChild.style.cssText;
			}
		}
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', divTmp).addEvent(['click'], [buttonBarActionDestroy]);
}

function listProperties(item) {
	if (item === undefined) return;
	var bbm = buttonBarActionDestroy();
	bbm.style.display = 'block';
	bbm.className += ' propertiesBack';
	bbm.style.top = '0';
	var divTmp = document.createElement('div');
	divTmp.style.background = 'white';
	divTmp.style.width = '50%';
	divTmp.style.height = '90%';
	divTmp.style.overflow = 'auto';
	divTmp.style.position = 'absolute';
	divTmp.style.top = '5%';
	divTmp.style.left = '25%';
	createHeadingTitle('h3', 'Właściwości listy', '', divTmp);
	bbm.appendChild(divTmp);

	var lHeader = createInputForm('checkbox', 'Dodaj nagłówek do listy', 'lHeader', '', '', divTmp);
	lHeader.addEvent(['change'], [function() {
		if (lHeader.children[1].checked) {
			//lHeaderText.style.display = ''; 
			pHead.style.display = '';
		}
		else {
			//lHeaderText.style.display = 'none';
			pHead.style.display = 'none';
		}
	}]);
	var lColor = createInputForm('color', 'Kolor tekstu', 'lColor', '#000000', '(domyślny czarny)', divTmp);
	lColor.addEvent(['change'], [function() {pPrev.style.color = lColor.children[1].value;}]);
	var lBackColor = createInputForm('color', 'Kolor tła', 'lBackColor', '#FFFFFF', '(domyślny biały)', divTmp);
	lBackColor.addEvent(['change'], [function() {pPrev.style.background = lBackColor.children[1].value;}]);
	var lFontSize = createInputForm('number', 'Wielkość tekstu (px)', 'hFontSize', '', '(domyślny, ok. 19px)', divTmp);
	lFontSize.addEvent(['change'], [function() {if (hFontSize.children[1].value === '') pPrev.style.fontSize = ''; else pPrev.style.fontSize = parseInt(hFontSize.children[1].value) + 'px';}]);
	var listType = createSelectForm('Typ listy', 'listType', ['Uporządkowana (numerowana)', 'Nieuporządkowana', 'Opisowa'], divTmp, '', true);
	listType.addEvent(['change'], [function() {
		if (listType.children[1].value == 1) {
			listOType.style.display = 'none';
			pOList.style.display = 'none';
			pDList.style.display = 'none';
			listUType.style.display = '';
			pUList.style.display = '';
		}
		else if (listType.children[1].value == 2) {
			listOType.style.display = 'none';
			pOList.style.display = 'none';
			pDList.style.display = '';
			listUType.style.display = 'none';
			pUList.style.display = 'none';
		}
		else {
			listOType.style.display = '';
			pOList.style.display = '';
			pDList.style.display = 'none';
			listUType.style.display = 'none';
			pUList.style.display = 'none';
		}
	}]);
	var listOType = createSelectForm('Oznaczenia pozycji', 'listOType', ['Brak', 'Liczby arabskie', 'Liczby arabskie z przewodnimi zerami', 'Liczby hebrajskie', 'Katakana', 'Katakana iroha', 
	'Hiragana', 'Hiragana iroha', 'Małe znaki', 'Duże znaki', 'Liczby rzymskie', 'Duże liczby rzymskie', 'Znaki greckie ', 'Liczby gregoriańskie', 'Liczby chińskie'], divTmp, '', true);
	listOType.addEvent(['change'], [function() {
		if (listOType.children[1].value == 1)
			pOList.style.listStyleType = 'decimal';
		else if (listOType.children[1].value == 2)
			pOList.style.listStyleType = 'decimal-leading-zero';
		else if (listOType.children[1].value == 3)
			pOList.style.listStyleType = 'hebrew';
		else if (listOType.children[1].value == 4)
			pOList.style.listStyleType = 'katakana';
		else if (listOType.children[1].value == 5)
			pOList.style.listStyleType = 'katakana-iroha';
		else if (listOType.children[1].value == 6)
			pOList.style.listStyleType = 'hiragana';
		else if (listOType.children[1].value == 7)
			pOList.style.listStyleType = 'hiragana-iroha';
		else if (listOType.children[1].value == 8)
			pOList.style.listStyleType = 'lower-latin';
		else if (listOType.children[1].value == 9)
			pOList.style.listStyleType = 'upper-latin';
		else if (listOType.children[1].value == 10)
			pOList.style.listStyleType = 'lower-roman';
		else if (listOType.children[1].value == 11)
			pOList.style.listStyleType = 'upper-roman';
		else if (listOType.children[1].value == 12)
			pOList.style.listStyleType = 'lower-greek';
		else if (listOType.children[1].value == 13)
			pOList.style.listStyleType = 'georgian';
		else if (listOType.children[1].value == 14)
			pOList.style.listStyleType = 'cjk-ideographic';
		else
			pOList.style.listStyleType = 'none';
	}]);
	//moze kiedys kiedys dorobi sie obrazki?
	var listUType = createSelectForm('Oznaczenia pozycji', 'listOType', ['Brak', 'Dyski', 'Koła', 'Kwadraty'], divTmp, '', true);
	listUType.addEvent(['change'], [function() {
		if (listUType.children[1].value == 1)
			pUList.style.listStyleType = 'disc';
		else if (listUType.children[1].value == 2)
			pUList.style.listStyleType = 'circle';
		else if (listUType.children[1].value == 3)
			pUList.style.listStyleType = 'square';
		else
			pUList.style.listStyleType = 'none';
	}]);
	listUType.style.display = 'none';
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Ułożenie tekstu w liście";
	btnDiv.appendChild(btnLegend);
	createButtonCheck('centerT', 'Wyśrodkowany', '<i class="fa fa-align-center" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	createButtonCheck('leftT', 'Do lewej', '<i class="fa fa-align-left" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv, false, true).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	createButtonCheck('rightT', 'Do prawej', '<i class="fa fa-align-right" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	createButtonCheck('justifyT', 'Równomierny', '<i class="fa fa-align-justify" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	divTmp.appendChild(btnDiv);
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Opcje tekstu na liście";
	btnDiv.appendChild(btnLegend);
	var boldBtn = createButtonCheck('boldT', 'Pogrubienie', '<i class="fa fa-bold" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	boldBtn.addEvent(['click'],[function() {buttonCheckAction(boldBtn); if (boldBtn.dataset.checked == 1) pPrev.style.fontWeight = 'bold'; else pPrev.style.fontWeight = '';}]);
	var itaBtn = createButtonCheck('italicT', 'Pochylenie', '<i class="fa fa-italic" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	itaBtn.addEvent(['click'], [function() {buttonCheckAction(itaBtn); if (itaBtn.dataset.checked == 1) pPrev.style.fontStyle = 'italic'; else pPrev.style.fontStyle = '';}]);
	var underBtn = createButtonCheck('underT', 'Podkreślony', '<i class="fa fa-underline" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	underBtn.addEvent(['click'], [function() {buttonCheckAction(underBtn); if (underBtn.dataset.checked == 1) pPrev.style.textDecoration += ' underline'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('underline', '')}]);
	var strikeBtn = createButtonCheck('strikeT', 'Przekreślony', '<i class="fa fa-strikethrough" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	strikeBtn.addEvent(['click'], [function() {buttonCheckAction(strikeBtn); if (strikeBtn.dataset.checked == 1) pPrev.style.textDecoration += ' line-through'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('line-through', '');}]);
	var supBtn = createButtonCheck('supT', 'Indeks górny', '<i class="fa fa-superscript" aria-hidden="true"></i>', '4', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	supBtn.addEvent(['click'], [function() {buttonCheckAction(supBtn); if (supBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'super'; else pPrev.style.verticalAlign = '';}]);
	var subBtn = createButtonCheck('subT', 'Indeks dolny', '<i class="fa fa-subscript" aria-hidden="true"></i>', '5', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	subBtn.addEvent(['click'], [function() {buttonCheckAction(subBtn); if (subBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'sub'; else pPrev.style.verticalAlign = '';}]);
	divTmp.appendChild(btnDiv);
	//TRZEBA DOKONCZYC!!!!
	var pPrev = document.createElement('div');
	pPrev.dataset.name = 'list';
// 		pPrev.id = "textActionPreview";
// 		var pHead = document.createElement('p');
// 		pHead.style.display = 'none';
// 		pHead.innerHTML = 'Nagłówek listy';
// 		pPrev.appendChild(pHead);
	var pOList = document.createElement('ol');
	var pUList = document.createElement('ul');
	var pDList = document.createElement('dl');
	pOList.style = item.style.cssText;
	pUList.style = item.style.cssText;
	pDList.style = item.style.cssText;
	pOList.style.display = 'none';
	pUList.style.display = 'none';
	pDList.style.display = 'none';
	console.log(item);
	if (item.tagName.toLowerCase() === 'ol')
		pOList.style.display = 'block';
	else if (item.tagName.toLowerCase() === 'ul')
		pUList.style.display = 'block';
	else
		pDList.style.display = 'block';
	for (var i = 0; i < item.children.length; i++) {
		pOList.appendChild(item.children[i].cloneNode());
		pOList.lastChild.innerHTML = item.children[i].innerHTML;
		pUList.appendChild(item.children[i].cloneNode());
		pUList.lastChild.innerHTML = item.children[i].innerHTML;
		pDList.appendChild(item.children[i].cloneNode());
		pDList.lastChild.innerHTML = item.children[i].innerHTML;
	}
	
// 		pUList.style.listStyleType = 'none';
// 		pOList.style.listStyleType = 'none';
	//createSampleList([pUList, pOList], 10);
	pPrev.appendChild(pOList);
	pPrev.appendChild(pUList);
	pPrev.appendChild(pDList);
	divTmp.appendChild(pPrev);
	createButton('Dodaj', 'button noselect', divTmp).addEvent(['click'], [function () {
		var tp = item.parentNode.parentNode;
		tp.removeChild(item.parentNode);
		if (pOList.style.display === 'none')
			pPrev.removeChild(pOList);
		if (pUList.style.display === 'none')
			pPrev.removeChild(pUList);
		if (pDList.style.display === 'none')
			pPrev.removeChild(pDList);
		tp.appendChild(pPrev);
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', divTmp).addEvent(['click'], [buttonBarActionDestroy]);
}

function blockTextProperties(item) {
	if (item === undefined) return;
	var bbm = buttonBarActionDestroy();
	bbm.style.display = 'block';
	bbm.className += ' propertiesBack';
	bbm.style.top = '0';
	var divTmp = document.createElement('div');
	divTmp.style.background = 'white';
	divTmp.style.width = '50%';
	divTmp.style.height = '90%';
	divTmp.style.overflow = 'auto';
	divTmp.style.position = 'absolute';
	divTmp.style.top = '5%';
	divTmp.style.left = '25%';
	createHeadingTitle('h3', 'Właściwości bloku tekstu', '', divTmp);
	bbm.appendChild(divTmp);
	var colorTmp = createInputForm('color', 'Kolor tekstu', 'colorTmp', hexColor(item.style.color), '', divTmp);
	colorTmp.addEvent(['change'], [function() {pPrev.style.color = colorTmp.children[1].value;}]);
	var backColorTmp = createInputForm('color', 'Kolor tła', 'backColorTmp', hexColor(item.style.background), '', divTmp);
	backColorTmp.addEvent(['change'], [function() {pPrev.style.background = backColorTmp.children[1].value;}]);
	if (item.dataset.name === 'heading'){
		var headingType = createSelectForm('Nagłówek typu', 'headingType', ['Nagłowek 1', 'Nagłówek 2', 'Nagłówek 3', 'Nagłówek 4', 'Nagłówek 5', 'Nagłówek 6'], divTmp, '', true);
		headingType.addEvent(['change'], [function() {
			var h = headingSelectAction(this); 
			h.innerHTML = pPrev.innerHTML;
			h.dataset.name = pPrev.dataset.name;
			h.style = pPrev.style.cssText;
			divPrev.appendChild(h); 
			divPrev.removeChild(pPrev); 
			pPrev = divPrev.lastChild;
		}]);
	}
	var tborderStyle = createSelectForm('Styl obramowania', 'tborderStyle', ['Brak', 'Lity', 'Kropki', 'Kreski', 'Prążkowe', 'Podwójny lity', 'Wklęsłe', 'Wypukłe', 'Żłobione',], divTmp, '', true);
	tborderStyle.addEvent(['change'], [function() {
		if (tborderStyle.children[1].value == 1)
			pPrev.style.borderStyle = 'solid';
		else if (tborderStyle.children[1].value == 2)
			pPrev.style.borderStyle = 'dotted';
		else if (tborderStyle.children[1].value == 3)
			pPrev.style.borderStyle = 'dashed';
		else if (tborderStyle.children[1].value == 4)
			pPrev.style.borderStyle = 'ridge';
		else if (tborderStyle.children[1].value == 5)
			pPrev.style.borderStyle = 'double solid';
		else if (tborderStyle.children[1].value == 6)
			pPrev.style.borderStyle = 'inset';
		else if (tborderStyle.children[1].value == 7)
			pPrev.style.borderStyle = 'outset';
		else if (tborderStyle.children[1].value == 8)
			pPrev.style.borderStyle = 'groove';
		else 
			pPrev.style.borderStyle = 'none';
	}]);
	var tBorderWidth = createInputForm('number', 'Grubość obramowania', 'tBorderWidth', '0', '(domyślnie 0)', divTmp);
	tBorderWidth.addEvent(['change'], [function() {
		pPrev.style.borderWidth = parseInt(tBorderWidth.children[1].value) + 'px';// solid ' + tBorderColor.children[1].value;  
	}]);
	var tBorderColor = createInputForm('color', 'Kolor obramowania', 'tBackColor', hexColor(item.style.borderColor), '(domyślny czarny)', divTmp);
	tBorderColor.addEvent(['change'], [function() {
		pPrev.style.borderColor = tBorderColor.children[1].value;  
	}]);
	var tFontSize = createInputForm('number', 'Wielkość tekstu (px)', 'tFontSize', '', '(domyślny)', divTmp);
	tFontSize.addEvent(['change'], [function() {if (tFontSize.children[1].value === '') pPrev.style.fontSize = ''; else pPrev.style.fontSize = parseInt(tFontSize.children[1].value) + 'px';}]);
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Ułożenie tekstu";
	btnDiv.appendChild(btnLegend);
	createButtonCheck('centerT', 'Wyśrodkowany', '<i class="fa fa-align-center" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	createButtonCheck('leftT', 'Do lewej', '<i class="fa fa-align-left" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv, false, true).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	createButtonCheck('rightT', 'Do prawej', '<i class="fa fa-align-right" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	createButtonCheck('justifyT', 'Równomierny', '<i class="fa fa-align-justify" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv).addEvent(['click'], [function(event) {
		var s;
		if (event.target.tagName.toLowerCase() === 'i') s = event.target.parentNode;
		else s = event.target;
		buttonRadioCheckAction(pPrev, s);
	}]);
	divTmp.appendChild(btnDiv);
	var btnDiv = document.createElement('fieldset');
	var btnLegend = document.createElement('legend');
	btnLegend.innerHTML = "Opcje tekstu";
	btnDiv.appendChild(btnLegend);
	var boldBtn = createButtonCheck('boldT', 'Pogrubienie', '<i class="fa fa-bold" aria-hidden="true"></i>', '0', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	boldBtn.addEvent(['click'],[function() {buttonCheckAction(boldBtn); if (boldBtn.dataset.checked == 1) pPrev.style.fontWeight = 'bold'; else pPrev.style.fontWeight = '';}]);
	var itaBtn = createButtonCheck('italicT', 'Pochylenie', '<i class="fa fa-italic" aria-hidden="true"></i>', '1', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	itaBtn.addEvent(['click'], [function() {buttonCheckAction(itaBtn); if (itaBtn.dataset.checked == 1) pPrev.style.fontStyle = 'italic'; else pPrev.style.fontStyle = '';}]);
	var underBtn = createButtonCheck('underT', 'Podkreślony', '<i class="fa fa-underline" aria-hidden="true"></i>', '2', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	underBtn.addEvent(['click'], [function() {buttonCheckAction(underBtn); if (underBtn.dataset.checked == 1) pPrev.style.textDecoration += ' underline'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('underline', '')}]);
	var strikeBtn = createButtonCheck('strikeT', 'Przekreślony', '<i class="fa fa-strikethrough" aria-hidden="true"></i>', '3', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	strikeBtn.addEvent(['click'], [function() {buttonCheckAction(strikeBtn); if (strikeBtn.dataset.checked == 1) pPrev.style.textDecoration += ' line-through'; else pPrev.style.textDecoration = pPrev.style.textDecoration.replace('line-through', '');}]);
	var supBtn = createButtonCheck('supT', 'Indeks górny', '<i class="fa fa-superscript" aria-hidden="true"></i>', '4', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	supBtn.addEvent(['click'], [function() {buttonCheckAction(supBtn); if (supBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'super'; else pPrev.style.verticalAlign = '';}]);
	var subBtn = createButtonCheck('subT', 'Indeks dolny', '<i class="fa fa-subscript" aria-hidden="true"></i>', '5', 'radioButtonPress', 'radioButtonDepress', btnDiv);
	subBtn.addEvent(['click'], [function() {buttonCheckAction(subBtn); if (subBtn.dataset.checked == 1) pPrev.style.verticalAlign = 'sub'; else pPrev.style.verticalAlign = '';}]);
	divTmp.appendChild(btnDiv);

	var divPrev = document.createElement('div');
	divPrev.style.width = '100%';
	divPrev.style.maxHeight = '100px';
	divPrev.style.overflow = 'hidden';
	var pPrev = item.cloneNode();
	pPrev.innerHTML = item.innerHTML;
	pPrev.style = item.style.cssText;
	//pPrev.dataset = item.dataset;
	divPrev.appendChild(pPrev);
	divTmp.appendChild(divPrev);
	createButton('Dodaj', 'button noselect', divTmp).addEvent(['click'], [function () {
		item.style = pPrev.style.cssText;
		if (pPrev.dataset.name === 'heading') {
			var pt = item.parentNode;
			pt.appendChild(pPrev); 
			item.parentNode.removeChild(item); 
		}
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', divTmp).addEvent(['click'], [buttonBarActionDestroy]);
}

function blockTextRightClick(e) {
	var subMenu = clearRightMenu();
	rightMenuAddItem('Ustawienia tekstu', subMenu).addEvent(['click'], [function() {
		blockTextProperties(e.target);
		//divTableTmp.parentNode.removeChild(divTableTmp);
	}]);
	openContext(e);
	rightMenuAddItem('Usuń blok', subMenu).addEvent(['click'], [function() {
		e.target.parentNode.removeChild(e.target);
		//divTableTmp.parentNode.removeChild(divTableTmp);
		//e.target.style.float = "left";
	}]);
	openContext(e);
}

function listRightClick(e) {
	var subMenu = clearRightMenu();
	rightMenuAddItem('Ustawienia listy', subMenu).addEvent(['click'], [function() {
		var item = e.target;
		if (e.target.tagName.toLowerCase() === 'div')
			item = e.target.children[0];
		else if (e.target.tagName.toLowerCase() === 'li' || e.target.tagName.toLowerCase() === 'dt' || e.target.tagName.toLowerCase() === 'dd')
			item = e.target.parentNode;
		listProperties(item);
		//divTableTmp.parentNode.removeChild(divTableTmp);
	}]);
	openContext(e);
	rightMenuAddItem('Usuń blok', subMenu).addEvent(['click'], [function() {
		e.target.parentNode.removeChild(e.target);
		//divTableTmp.parentNode.removeChild(divTableTmp);
		//e.target.style.float = "left";
	}]);
	openContext(e);
}

function tableRightClick(e) {
	var subMenu = clearRightMenu();
	var divTableTmp;
	var tableTmp;
	var tHeadTmp;
	var tBodyTmp;
	var columnTmp;
	var rowTmp;
	if (e.target.tagName.toLowerCase() === 'table') 
		tableTmp = e.target;
	else if (e.target.tagName.toLowerCase() === 'div') 
		tableTmp = e.target.children[0];
	else if (e.target.tagName.toLowerCase() === 'tr') {
		tableTmp = e.target.parentNode.parentNode;
		if (e.target.lastChild.tagName.toLowerCase() === 'td')
			rowTmp = parseInt(e.target.dataset.pos);
	}
	else if (e.target.tagName.toLowerCase() === 'td' || e.target.tagName.toLowerCase() === 'th') {
		tableTmp = e.target.parentNode.parentNode.parentNode;
		columnTmp = parseInt(e.target.dataset.pos);
		if (e.target.tagName.toLowerCase() === 'td') rowTmp = parseInt(e.target.parentNode.dataset.pos);
	}
	else
		tableTmp = e.target.parentNode;
	divTableTmp = tableTmp.parentNode;
	if(tableTmp.children.length == 2) {
		tHeadTmp = tableTmp.children[0];
		tBodyTmp = tableTmp.children[1];
	}
	else
		tBodyTmp = tableTmp.children[0];
	//var ;
	if (tHeadTmp !== undefined) {
		rightMenuAddItem('Ustawienia nagłówka', subMenu).addEvent(['click'], [function() {
			tableRightProperties(tHeadTmp);
			//imgTmp.style.float = "left";
			//e.target.style.float = "left";
		}]);
		rightMenuAddItem('Usuń nagłówek', subMenu).addEvent(['click'], [function() {
			tableTmp.removeChild(tHeadTmp);
		}]);
	}
	else {
		rightMenuAddItem('Dodaj nagłówek', subMenu).addEvent(['click'], [function() {
			tHeadTmp = document.createElement('thead');
			var tt = document.createElement('tr');
			//tHeadTmp.style = tBodyTmp.lastChild.style.cssText;
			var colCount = tBodyTmp.lastChild.children.length;
			for (var  i = 0; i < colCount; i++) {
				var thtmp = document.createElement('th');
				thtmp.innerHTML = (i + 1);
				thtmp.dataset.pos = i;
				tt.appendChild(thtmp);
			}
			tHeadTmp.appendChild(tt);
			tableTmp.insertBefore(tHeadTmp, tBodyTmp);
			//e.target.style.float = "left";
		}]);
	}
	if (columnTmp !== undefined && rowTmp !== undefined) 
		rightMenuAddItem('Ustawienia wybranej komórki', subMenu).addEvent(['click','mouseover', 'mouseout'], [function() {
			highlightTable(tableTmp, e.target, rowTmp, columnTmp, true);
			tableRightProperties(e.target);
			//imgTmp.style.float = "left";
			//e.target.style.float = "left";
		},
		function() {highlightTable(tableTmp, e.target, rowTmp, columnTmp);},
		function() {highlightTable(tableTmp, e.target, rowTmp, columnTmp, true);}]);
	if (rowTmp !== undefined)
		rightMenuAddItem('Ustawienia wybranego wiersza', subMenu).addEvent(['click','mouseover', 'mouseout'], [function() {
			highlightTable(tableTmp, tBodyTmp.children, rowTmp, -1, true);
			tableRightProperties(tBodyTmp.children[rowTmp], tableTmp, rowTmp);
			//imgTmp.style.float = "left";
			//e.target.style.float = "left";
		},
		function() {highlightTable(tableTmp, tBodyTmp.children, rowTmp);},
		function() {highlightTable(tableTmp, tBodyTmp.children, rowTmp, -1, true);}]);
	if (columnTmp !== undefined)
		rightMenuAddItem('Ustawienia wybranej kolumny', subMenu).addEvent(['click','mouseover', 'mouseout'], [function() {
			highlightTable(tableTmp, -1, -1, columnTmp, true);
			var tb = document.createElement('tbody');
			for (var i = 0; i < tBodyTmp.children.length; i++) {
				var it = document.createElement('tr');
				var ic = tBodyTmp.children[i].children[columnTmp].cloneNode();
				ic.innerHTML = tBodyTmp.children[i].children[columnTmp].innerHTML;
				it.appendChild(ic);
				tb.appendChild(it);
			}
			tableRightProperties(tb, tableTmp, -1, columnTmp);
			//imgTmp.style.float = "left";
			//e.target.style.float = "left";
		},
		function() {highlightTable(tableTmp, -1, -1, columnTmp);},
		function() {highlightTable(tableTmp, -1, -1, columnTmp, true);}]);
	rightMenuAddItem('Ustawienia tabeli', subMenu).addEvent(['click'], [function() {
		tableRightProperties(tableTmp);
		//tableTmp.removeChild(tHeadTmp);
	}]);
	rightMenuAddItem('Dodaj wiersz', subMenu).addEvent(['click'], [function() {
		if (tBodyTmp.children.length <= 0) return;
		if (rowTmp === undefined) rowTmp = -1;
		if (tBodyTmp.children.length <= rowTmp || rowTmp == -1)
			rowTmp = tBodyTmp.children.length - 1;
		var trtmp = document.createElement('tr');
		trtmp.dataset.pos = rowTmp;
		var columnCount = tBodyTmp.lastChild.children.length;
		var colCSS;
		if (rowTmp < tBodyTmp.children.length -1) 
			colCSS = tBodyTmp.children[rowTmp + 1].lastChild.style.cssText;
		else
			colCSS = tBodyTmp.lastChild.lastChild.style.cssText;
		for (var i = 0; i < columnCount; i++) {
			var tdtmp = document.createElement('td');
			tdtmp.dataset.pos = i;
			tdtmp.style = colCSS;
			tdtmp.innerHTML = '[TEKST]';
			trtmp.appendChild(tdtmp);
		}
		if (rowTmp < tBodyTmp.children.length -1) {
			trtmp.style = tBodyTmp.children[rowTmp + 1].style.cssText;
			tBodyTmp.insertBefore(trtmp, tBodyTmp.children[rowTmp + 1]);
		}
		else {
			trtmp.style = tBodyTmp.lastChild.style.cssText;
			tBodyTmp.appendChild(trtmp);
		}
		for (var i = rowTmp + 1; i < tBodyTmp.children.length; i++) 
			tBodyTmp.children[i].dataset.pos = i;
	}]);
	var isTh = false;
	if (e.target.lastChild.tagName !== undefined)
		if (e.target.lastChild.tagName.toLowerCase() === 'th')
			isTh = true;
	if (rowTmp !== undefined || isTh)
		rightMenuAddItem('Usuń wiersz', subMenu).addEvent(['click'], [function() {
			if (rowTmp !== undefined) {
				tBodyTmp.removeChild(tBodyTmp.children[rowTmp]);
				for (var i = rowTmp; i < tBodyTmp.children.length; i++) 
					tBodyTmp.children[i].dataset.pos = i;
			}
			if (e.target.lastChild.tagName.toLowerCase() === 'th') {
				tHeadTmp.parentNode.removeChild(tHeadTmp);
			}
		}]);
	
	rightMenuAddItem('Dodaj kolumnę', subMenu).addEvent(['click'], [function() {
		if (tBodyTmp.children.length <= 0) return;
		if (columnTmp === undefined) columnTmp = -1;
		//console.log(tBodyTmp.children);
		if (tBodyTmp.children[0].children.length <= columnTmp || columnTmp == -1)
			columnTmp = tBodyTmp.children[0].children.length - 1;
		for (var i = 0; i < tBodyTmp.children.length; i++) {
			var tdtmp = document.createElement('td');
			tdtmp.dataset.pos = columnTmp;
			tdtmp.innerHTML = "[TEKST]";
			if (columnTmp < tBodyTmp.children[0].children.length - 1) {
				tdtmp.style = tBodyTmp.children[0].children[columnTmp + 1].style.cssText;
				tBodyTmp.children[i].insertBefore(tdtmp, tBodyTmp.children[i].children[columnTmp + 1]);
			}
			else {
				tdtmp.style = tBodyTmp.children[i].lastChild.style.cssText;
				tBodyTmp.children[i].appendChild(tdtmp);
			}
			for (var j = columnTmp + 1; j < tBodyTmp.children[0].children.length; j++) 
				tBodyTmp.children[i].children[j].dataset.pos = j;
		}
		if (tHeadTmp !== undefined) {
			var tdtmp = document.createElement('th');
			tdtmp.dataset.pos = columnTmp;
			tdtmp.innerHTML = (columnTmp + 1);
			if (columnTmp < tHeadTmp.children[0].children.length - 1)
				tHeadTmp.children[0].insertBefore(tdtmp, tHeadTmp.children[0].children[columnTmp + 1]);
			else
				tHeadTmp.children[0].appendChild(tdtmp);
			for (var j = columnTmp + 1; j < tHeadTmp.children[0].children.length; j++) {
				tHeadTmp.children[0].children[j].dataset.pos = j;
				tHeadTmp.children[0].children[j].innerHTML = (j + 1);
			}
		}
	}]);
	if (columnTmp !== undefined) {
		rightMenuAddItem('Usuń kolumnę', subMenu).addEvent(['click'], [function() {
			for (var i = 0; i < tBodyTmp.children.length; i++) {
				tBodyTmp.children[i].removeChild(tBodyTmp.children[i].children[columnTmp]);
				for (var j = columnTmp; j < tBodyTmp.children[i].children.length; j++)
					tBodyTmp.children[i].children[j].dataset.pos = j;
			}
			if (tHeadTmp !== undefined) {
				tHeadTmp.children[0].removeChild(tHeadTmp.children[0].children[columnTmp]);
				for (var j = columnTmp; j < tHeadTmp.children[0].children.length; j++)
					tHeadTmp.children[0].children[j].dataset.pos = j;
			}
		}]);
	}
	rightMenuAddItem('Usuń tabelę', subMenu).addEvent(['click'], [function() {
		divTableTmp.parentNode.removeChild(divTableTmp);
		//e.target.style.float = "left";
	}]);
	openContext(e);
}

//glowna funkcja wywolujaca spis podreczny edytora; dostosowuje się do elementów, dla których została wywołana
//
function defaultRightMenu(e) {
	var targetTag = e.target.tagName.toLowerCase();		
	if (targetTag === 'img' || targetTag === 'figure') {
		imageRightClick(e);
		return;
	}
	if (currentSelect !== undefined) 
		if ((currentSelect.endOffset - currentSelect.startOffset) > 0) {
			var subMenu = clearRightMenu();
// 				rightMenuAddItem('Opcja pierwsza', subMenu).addEvent(['click'], [function() {closeRightMenu = false; getCurrentSelect();}]);
// 				rightMenuAddItem('Opcja druga', subMenu).addEvent(['click'], [addParagraph]);
// 				rightMenuAddItem('Opcja trzecia', subMenu);//.addEvent(['click'], [moveImage]);
// 				openContext(e);
			return;
		}
	if (targetTag === 'a') {
		console.log('odnośnik');
		return;
	}
	if (targetTag === 'span') {
		console.log('odnośnik');
		return;
	}
	if (targetTag === 'h1' || targetTag === 'h2' || targetTag === 'h3' || targetTag === 'h4' || targetTag === 'h5' || targetTag === 'h6') {
		blockTextRightClick(e);
		return;
	}
	if (targetTag === 'p' || targetTag === 'div') {
		var targetName = e.target.dataset.name;
		if (targetName !== undefined) {
			if (targetName === 'table') {
				tableRightClick(e);
				return;
			}
			else if (targetName === 'paragraph') {
				blockTextRightClick(e);
				return;
			}
			else if (targetName === 'list') {
				listRightClick(e);
				return;
			}
		}
	}
	if (targetTag === 'td' || targetTag === 'tr' || targetTag === 'table' || targetTag === 'th' || targetTag === 'tbody' || targetTag === 'thead') {
		tableRightClick(e);
		return;
	}
	if (targetTag === 'ul' || targetTag === 'ol' || targetTag === 'dl' || targetTag === 'dd' || targetTag === 'dt' || targetTag === 'li') {
		listRightClick(e);
		return;
	}
}

function imagePreview() {
	//console.log(document.getElementById('imgFilePath').children[1]);
	
	if (this.children[1].type === 'file') {
		var im = document.getElementById('tmpImg');
		var imTmp;
		imTmp = getImageFromFile(document.getElementById('imgFilePath').children[1]);
		imTmp.style = im.style.cssText;
		console.log(im.style);
		imTmp.id = im.id;
		this.parentNode.insertBefore(imTmp, im);
		this.parentNode.removeChild(im);
		//document.getElementById('tmpImg').src = imTmp.src;
		//console.log(imTmp);
	}
	else if (this.children[1].type === 'text')
		document.getElementById('tmpImg').src = document.getElementById('imgFilePath').children[1].value;
	
	
}

function getImageFromFile(im, desc) {
	if (desc === undefined) desc = '';
	var img = document.createElement('img');
	var f = new FileReader();
	f.onload = function () {img.src = f.result; img.alt = desc;}
	if(im.files[0].type.indexOf('image/') > -1) {
		f.readAsDataURL(im.files[0]);
	}
	return img;
}

function createHeadingTitle(headingType, text, pos, toElem) {
	if (headingType === undefined || text === undefined) return -1;
	if (pos === undefined || pos === '') pos = 'center';
	var hInfo = document.createElement(headingType);
	hInfo.innerHTML = text;
	hInfo.style.textAlign = pos;
	if (toElem !== undefined && toElem !== '')
		toElem.appendChild(hInfo);
	return hInfo;
}

//tworzy elementy warstwy dla przycisku obrazu; pozwala na dodawanie nowego obrazu do edytowanego tekstu
//wewnątrz dwa przyciski posiadające funkcje nienazwane umożliwiające wykonanie dodania obrazu oraz anulowania akcji 
function imageSetup(bbm) {
	var btn = document.getElementsByClassName('fa-picture-o')[0];
	btn.className = 'active ' + btn.className;
	bbm.style.display = 'block';
	
	bbm.style.top = (btn.getBoundingClientRect().y | btn.getBoundingClientRect().top + btn.getBoundingClientRect().height) + 'px';
	createHeadingTitle('h3', 'Dodaj obraz', '', bbm);
	createInputForm('radio', 'Dodaj z pliku', 'fileFrom', '0', '', bbm, '', true).addEvent(['click'], [changeAddImageType]);
	createInputForm('radio', 'Dodaj z odnośnika', 'fileFrom', '1', '', bbm).addEvent(['click'], [changeAddImageType]);
	createInputForm('radio', 'Dodaj z galerii', 'fileFrom', '2', '', bbm).addEvent(['click'], [changeAddImageType]);
	createInputForm('file', 'Wskaż plik do dodania', 'fileItem', '', '', bbm, 'imgFilePath').addEvent(['change'], [imagePreview]);
	createInputForm('text', 'Opis alternatywny obrazu', 'fileDesc', '', '', bbm);
	createInputForm('text', 'Podpis obrazu (widoczny)', 'imageDesc', '', '', bbm);
	var tmpImg = document.createElement('img');
	tmpImg.id = 'tmpImg';
	tmpImg.style = 'display: block; width: 300px; height: 200px;';
	bbm.appendChild(tmpImg);
	createButton('Dodaj', 'button noselect', bbm).addEvent(['click'], [function () {
		var ifp = document.getElementById('imgFilePath');
		var prev = document.getElementById('preview');
		var desc = document.getElementsByName('fileDesc')[0].value;
		var imageDesc = document.getElementsByName('imageDesc')[0].value;
		var img /* = document.createElement('img')*/;
		//img.style.position = 'relative';
		if (desc === '')
			desc = "Dodane zdjęcie";
		if (ifp.children[1].name === 'fileItem') {
			img = getImageFromFile(ifp.children[1], desc);
// 				var f = new FileReader();
// 				f.onload = function () {img.src = f.result; img.alt = desc;}
// 				if(ifp.children[1].files[0].type.indexOf('image/') > -1) {
// 					f.readAsDataURL(ifp.children[1].files[0]);
// 				}
		}
		else if (ifp.children[1].name === 'linkItem') {
			img = document.createElement('img');
			img.src = ifp.children[1].value;
			img.alt = desc;
		}
		else if (ifp.children[1].name === 'galleryItem') {
			img = document.createElement('img');
			img.src = ifp.children[1].value;
			img.alt = desc;
			//do wykonania po polaczenu do calosci!!
			//trzeba dostosowac to co juz jest - bo w szczatkowej formie jest!!
		}
		var pTemp = document.createElement('figure');
		pTemp.dataset.name = 'image';
		pTemp.appendChild(img);
		if (imageDesc !== '') {
			var figCap = document.createElement('figcaption');
			figCap.innerHTML = imageDesc;
			pTemp.appendChild(figCap);
		}
		pTemp.addEventListener('mouseover', function() {
			pTemp.style.boxShadow = '0 0 10px #1A1A1A';
		}, true);
		pTemp.addEventListener('mouseout', function() {
			pTemp.style.boxShadow = '';
		}, true);
		//pTemp.addEventListener('mousedown', function(event) {imageRightClick(event);}, true);
		prev.appendChild(pTemp);
		buttonBarActionDestroy();
	}]);
	createButton('Anuluj', 'button noselect', bbm).addEvent(['click'], [buttonBarActionDestroy]);
}

//prototype for all HTML elements!
function removeClassName(n) {
	if (n === undefined) return;
	var c = this.className.split(' ');
	this.className = '';
	for(var i = 0; i < c.length; i++) {
		if (c[i] !== n)
			this.className += c[i] + ' ';
	}
	this.className.slice(0, this.className.length - 1);
}