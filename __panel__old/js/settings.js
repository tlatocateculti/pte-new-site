// function appendUserSettings() {
// 	if ((document.getElementsByName('pass')[0].value != document.getElementsByName('repass')[0].value)) {
// 		document.getElementsByName('pass')[0].style.background = 'red';
// 		document.getElementsByName('repass')[0].style.background = 'red';
// 		return;
// 	}
// 	if (document.getElementsByName('passMail')[0].value != document.getElementsByName('rePassMail')[0].value) {
// 		document.getElementsByName('passMail')[0].style.background = 'red';
// 		document.getElementsByName('rePassMail')[0].style.background = 'red';
// 		return;
// 	}
// 	var sendData = '';
// 	sendData += 'command=4&';
// 	var answer = document.getElementsByName('passMail')[0].value + '|&&|' + 
// 			document.getElementsByName('pass')[0].value + '|&&|' +
// 			document.getElementsByName('login')[0].value;
// 	sendData += "en=" + window.btoa(unescape(encodeURIComponent(answer)));
// 	adminCommunication('./settings_save.php', sendData, '', '',
// 				function(r) {
// // 					document.getElementById('settingsCommunicationDiv').style.display = 'block';
// // 					document.getElementById('settingsCommunicationDiv').innerHTML = r;
// // 					setTimeout(function() {
// // 						document.getElementById('settingsCommunicationDiv').style.display = 'none';
// // 					}, 5000);
// 				});
// }

function appendSettings() {
	var sendData = '';
	sendData += 'command=0&';
	sendData += 'baseUser=' + document.getElementsByName('baseUser')[0].value + '&';
	sendData += 'basePass=' + btoa(encodeURIComponent(encodeURI(document.getElementsByName('basePass')[0].value))) + '&';
	sendData += 'baseName=' + document.getElementsByName('baseName')[0].value + '&';
	sendData += 'siteLogin=' + btoa(encodeURIComponent(encodeURI(document.getElementsByName('siteLogin')[0].value))) + '&';
	sendData += 'sitePass=' + btoa(encodeURIComponent(encodeURI(document.getElementsByName('sitePass')[0].value))) + '&';
	sendData += 'siteServer=' + btoa(encodeURIComponent(encodeURI(document.getElementsByName('siteServer')[0].value))) + '&';
	sendData += 'sitePort=' + document.getElementsByName('sitePort')[0].value + '&';
	sendData += 'configName=' + document.getElementsByName('configName')[0].value + '&';
	if (document.getElementsByName('reconfig')[0].checked)
		sendData += 'reconfig=' + document.getElementsByName('setList')[0].value;
	else
		sendData += 'reconfig=-1';
	adminCommunication('./phps/settings_save.php', sendData, '', '',
				function(r) {
					document.getElementById('settingsCommunicationDiv').style.display = 'block';
					document.getElementById('settingsCommunicationDiv').innerHTML = r;
					setTimeout(function() {
						document.getElementById('settingsCommunicationDiv').style.display = 'none';
					}, 5000);
				});
}

function appendEmailSettings() {
	var sendData = '';
	sendData += 'command=3&';
	sendData += 'mail=' + document.getElementsByName('email')[0].value;
	if (document.getElementsByName('onemail')[0].checked)
		sendData += '&useBaseMail=1';
	else
		sendData += '&useBaseMail=0';
	adminCommunication('./phps/settings_save.php', sendData, '', '',
				function(r) {
					document.getElementById('settingsCommunicationDiv').style.display = 'block';
					document.getElementById('settingsCommunicationDiv').innerHTML = r;
					setTimeout(function() {
						document.getElementById('settingsCommunicationDiv').style.display = 'none';
					}, 5000);
				});
}

function appendUserSettings() {
	if ((document.getElementsByName('pass')[0].value != document.getElementsByName('repass')[0].value)) {
		document.getElementsByName('pass')[0].style.background = 'red';
		document.getElementsByName('repass')[0].style.background = 'red';
		return;
	}
	if (document.getElementsByName('passMail')[0].value != document.getElementsByName('rePassMail')[0].value) {
		document.getElementsByName('passMail')[0].style.background = 'red';
		document.getElementsByName('rePassMail')[0].style.background = 'red';
		return;
	}
	var sendData = '';
	sendData += 'command=4&';
	var answer = document.getElementsByName('passMail')[0].value + '|&&|' + 
			document.getElementsByName('pass')[0].value + '|&&|' +
			document.getElementsByName('login')[0].value;
	sendData += "en=" + window.btoa(unescape(encodeURIComponent(answer)));
	adminCommunication('./phps/settings_save.php', sendData, '', '',
				function(r) {
					document.getElementById('settingsCommunicationDiv').style.display = 'block';
					document.getElementById('settingsCommunicationDiv').innerHTML = r;
					setTimeout(function() {
						document.getElementById('settingsCommunicationDiv').style.display = 'none';
					}, 5000);
				});
}

function resetSettings(n) {
	if (n === undefined)
		return;
	var tmp = document.getElementById(n).getElementsByTagName('div');
	for (var i = 0; i < tmp.length; i++) {
		if (tmp[i].children[1].type != 'checkbox')
			tmp[i].children[1].value = '';
		else
			tmp[i].children[1].checked = '';
	}
}

function deleteSettings() {
	var sendData = '';
	sendData += 'command=2&';
	sendData += 'id=' + document.getElementsByName('setList')[0].value;
	adminCommunication('./phps/settings_save.php', sendData, '', '',
				function(r) {
					document.getElementById('settingsCommunicationDiv').style.display = 'block';
					document.getElementById('settingsCommunicationDiv').innerHTML = r;
					setTimeout(function() {
						document.getElementById('settingsCommunicationDiv').style.display = 'none';
					}, 5000);
				});
}

function selectSettings() {
	var sendData = '';
	sendData += 'command=1&';
	sendData += 'id=' + document.getElementsByName('setList')[0].value;
	adminCommunication('./phps/settings_save.php', sendData, '', '',
				function(r) {
					document.getElementById('settingsCommunicationDiv').style.display = 'block';
					document.getElementById('settingsCommunicationDiv').innerHTML = r;
					setTimeout(function() {
						document.getElementById('settingsCommunicationDiv').style.display = 'none';
					}, 5000);
				});
}

function showHideForm(n) {
	var tmp = document.getElementById(n);
	var fields = document.getElementsByTagName('fieldset');
	for (var i = 0; i < fields.length; i++) {
		if (fields[i].children[1] != tmp)
			fields[i].children[1].style.display = 'none';
	}
	if (tmp) {
		if (tmp.style.display == 'block')
			tmp.style.display = 'none';
		else
			tmp.style.display = 'block';
	}
}

function deleteSU(t) {
	var sendData = '';
	sendData += 'command=5&';
	console.log('ID ' + t.parentNode.dataset.id);
	sendData += 'mailU=' + t.parentNode.dataset.id;
	adminCommunication('./phps/settings_save.php', sendData, '', '',
				function(r) {
					document.getElementById('settingsCommunicationDiv').style.display = 'block';
					document.getElementById('settingsCommunicationDiv').innerHTML = r;
					setTimeout(function() {
						document.getElementById('settingsCommunicationDiv').style.display = 'none';
					}, 5000);
				});
}

function generateSiteMap() {
	var sendData = '';
	sendData += 'command=6';
	adminCommunication('./phps/settings_save.php', sendData, '', '',
				function(r) {
					document.getElementsByName('sitemap')[0].innerHTML = r;
				});
	
}

function saveSiteMap() {
	var sendData = '';
	sendData += 'command=7&';
// 	window.btoa(unescape(encodeURIComponent(answer)));
 	//sendData += 'data=' + window.btoa(unescape(encodeURIComponent(document.getElementsByName('sitemap')[0].value)));
	sendData += 'data=' + document.getElementsByName('sitemap')[0].value;
	adminCommunication('./phps/settings_save.php', sendData, '', '',
				function(r) {
// 					document.getElementById('settingsCommunicationDiv').style.display = 'block';
// 					document.getElementById('settingsCommunicationDiv').innerHTML = r;
// 					setTimeout(function() {
// 						document.getElementById('settingsCommunicationDiv').style.display = 'none';
// 					}, 5000);
				});
}

function saveKeywords() {
	var sendData = '';
	sendData += 'command=8&keywords=' + document.getElementsByName('keywords')[0].value + '&title=' + document.getElementsByName('siteTitle')[0].value;
	adminCommunication('./phps/settings_save.php', sendData, '', '',
				function(r) {
					//document.getElementsByName('sitemap')[0].innerHTML = r;
				});
	
}

function repairBase() {
	
}
