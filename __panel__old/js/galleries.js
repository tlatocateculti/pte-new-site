//funckja getCategoriesForPhoto wywoływana jest kilkukrotnie
//jednak powinna wykonać się jedynie RAZ; dlatego postawiony jest ten
//semafor - jeżeli zostanie ustawiony przez pierwszą funkcję na
//true to reszta wywołań zostanie przerwanych
var blockItem = false;
var blockEvent = false;

//funkcja ma za zadanie wyłączać niepotrzebne listy kategorii/podkategorii/kursów
//została tak skonstruowana, że wyłączany jest 'zasięg' list, nie zaś wszystkie
//Działanie 
// brak podanej wartości - przymowana jest najwyższa wartość liczbowa i usuwane są wszystkie listy
// wartość 1 - usuwana jest jedzynie lista kursów
// wartość 2 - usuwama jest lista podkategorii oraz kursów
// wartość 3 - synonim braku wartości (na tę chwilę)
function hideAllPhotoCategoryElements(type) {
	if (type === undefined) {
		//byle jaka, najwyższa liczba 
		type = Number.MAX_VALUE;
	}
	var tmp;
	if (type > 2) {
		tmp = document.getElementsByName('category')[0];
		tmp.innerHTML = '';
		tmp.parentNode.style.display = 'none';
		
	}
	if (type > 1) {
		tmp = document.getElementsByName('subcategory')[0];
		tmp.innerHTML = '';
		tmp.parentNode.style.display = 'none';
	}
	if (type > 0) {
		tmp = document.getElementsByName('courseID')[0];
		tmp.innerHTML = '';
		tmp.parentNode.style.display = 'none';
	}
}

function getCategoriesForPhoto(t) {
	//console.log(t);
	var sendData = 'type='; 
	if (t === undefined) {
		sendData += '3';	
		t = document.getElementsByName('courseID')[0];
	}
	else if (t.name == 'addToCourseType') {			
		sendData += '0';
		hideAllPhotoCategoryElements();
		if (t.value == -1) return;
	}
	else if (t.name == 'category') {		
		sendData += '1';
		hideAllPhotoCategoryElements(2);
	}
	else if (t.name == 'subcategory') {	
		if (t.value == -1) {
			t = document.getElementsByName('category')[0];
			sendData += '4';
		}
		else sendData += '2';
		hideAllPhotoCategoryElements(1);
	}
	sendData += '&value=' + t.value;
	//console.log(sendData);
	if (blockItem) return;
	else blockItem = true;
	adminCommunication('./galleries_functions.php', sendData, '', '',
				function(r) {
					blockItem = false;
					if (r === undefined) return;
					var resp = eval(r)[0];
					var tmp;
					var retName = '';
					
					if (resp['type'] == 0)
						retName = 'category';
					else if (resp['type'] == 1) 
						retName = 'subcategory';
					else if (resp['type'] == 2) 
						retName = 'courseID';
					else if (resp['type'] == 3) {
						hideAllPhotoCategoryElements();
						var i;
						tmp = document.getElementsByName('addToCourseType')[0].children;
						for (i = 0; i < tmp.length; i++) {
							if (tmp[i].value == resp['typePhoto']) {
								tmp[i].selected = 'selected';
								break;
							}
						}
						tmp = document.getElementsByName('category')[0];
						if (resp['category'] !== undefined) {
							tmp.parentNode.style.display = 'block';							
							for (i = 0; i < resp['category'].length; i++) 
								tmp.innerHTML += resp['category'][i];
						}
						tmp = document.getElementsByName('subcategory')[0];	
						if (resp['subcategory'] !== undefined) {
							if (resp['subcategory'].length > 1) {
								tmp.parentNode.style.display = 'block';
								for (i = 0; i < resp['subcategory'].length; i++) 
									tmp.innerHTML += resp['subcategory'][i];	
							}
						}
						tmp = document.getElementsByName('courseID')[0];	
						if (resp['courseID'] !== undefined) {
							tmp = document.getElementsByName('courseID')[0];
							tmp.parentNode.style.display = 'block';
							for (i = 0; i < resp['courseID'].length; i++) 
								tmp.innerHTML += resp['courseID'][i];
						}
						return;
					}
					else return;	
					tmp = document.getElementsByName(retName)[0];
					tmp.innerHTML = '';
					if (resp[retName] === undefined || resp[retName].length == 0) return;
					else tmp.parentNode.style.display = 'block';
					for (var i = 0; i < resp[retName].length; i++)
						tmp.innerHTML += resp[retName][i];					
				});
}

function getGalleriesTable(elements, startNum, galleryID) {
	var address = "./phps/galleries_maintenace.php";
	
// 	if (elements !== undefined && elements)
// 		address += "?s=1";
	if (startNum === undefined)
		startNum = 0;
	var sendMsg = "startNum=" + startNum;
	if (galleryID !== undefined)
		sendMsg = "&gid=" + galleryID;
	adminCommunication(address, sendMsg, '', 
				function () {
					document.getElementById("tableContent").innerHTML = 'Ładuję dane...';
				},
				function(r) {
					//console.log('Jestem w funkcji, dane to: ' + r);
					document.getElementById("tableContent").innerHTML = r;
					var elem = document.querySelectorAll("[data-gallery-icon-action]");
					for (var i = 0; i < elem.length; i++) {
						elem[i].children[1].className += ' elemActionBtn';
						elem[i].children[2].className += ' elemActionBtn';
					}
				});	
}

function galleriesTableAddEdit(elements, edit, t) {
	var address = "./phps/galleries_addedit.php";
	var sendData = '';
	var gid;
	if (t !== undefined)
		gid = t.parentNode.parentNode.parentNode.id.split('-')[1];
	if (elements !== undefined && elements) {
		address += "?s=1";
		if (t !== undefined)
			sendData += 'setID=' + gid;
	}
	
	var fieldsID = '';	
	if (edit !== undefined && edit) {
		if (!elements) {
			fieldsID = gid + ',';
// 			var fieldsToEdit = document.getElementsByName('selectGalleryBox');
// 			for (var i = 0;  i < fieldsToEdit.length; i++) {
// 				if (fieldsToEdit[i].checked)
// 					fieldsID += fieldsToEdit[i].value + ',';
// 			}
			
		}
		else {
			var tmp = t.parentNode.parentNode.parentNode.getElementsByClassName('photosSet')[0].children;
			//var tmp = document.getElementById('set-' + gid).getElementsByClassName('photosSet')[0].children;
			for (var i = 0; i < tmp.length; i++) {
				if (tmp[i].dataset.select) 
					fieldsID += tmp[i].dataset.id + ',';
			}
		}
		fieldsID = fieldsID.slice(0, fieldsID.length - 1);
		sendData += '&fields=' + fieldsID;
		console.log(sendData);
	}
	adminCommunication(address, sendData, '', '',
			   function(r) {
				if (r == -100) {
					document.getElementById("tableContent").innerHTML = "Błąd! Nic nie zaznaczyłeś!";
					setTimeout(getGalleriesTable, 1000);  
					return;
				}
				document.getElementById("tableContent").innerHTML = r;				
// 				if (!elements)
// 					document.getElementById('actionButtons').style.display = 'none';
				if (elements) {
					//jeżeli edytujemy zdjęcie to funkcja poniżej 
					//ustawi nam jego aktualną przynależność do kursu/szkolenia 
					//o ile do takiego jest przywiązane!
// 					if (document.getElementsByName('courseID')[0].value !== '')
// 						getCategoriesForPhoto();
					
				}
			   }); 
	
}

function showHideGallery(t) {
	var sendData = 'showHide=1&id=' + t.parentNode.parentNode.parentNode.id.split('-')[1];
	adminCommunication("./phps/galleries_save.php", sendData, '', '',
			   function(r) {
				   //console.log(r);
				var paragraph = t.parentNode.parentNode.children[0].children[0];
				//console.log(paragraph);
				if (r == 1) {
					t.children[0].innerHTML = '<i aria-hidden="true" class="fa fa-eye-slash"></i>';
					t.children[0].dataset.tip = 'Ukryj galerię';
					paragraph.innerHTML = paragraph.innerHTML.replace('(nieaktywna)', '(aktywna)');
				}
				else {
					t.children[0].innerHTML = '<i aria-hidden="true" class="fa fa-eye"></i>';
					t.children[0].dataset.tip = 'Pokaż galerię';
					paragraph.innerHTML = paragraph.innerHTML.replace('(aktywna)', '(nieaktywna)');
				}
				return;
			   });
}

function saveGallery() {
	//var forms = document.getElementsByName('galleryForm');
	var sendData = '';
    var galleryName = document.getElementsByName('name')[0].value;
	sendData += 'name=' + galleryName + '&';
	sendData += 'description=' + document.getElementsByName('description')[0].value + '&';
	sendData += 'description_show=0&';
	//sendData += 'description_show=' + document.getElementsByName('description_show')[0].value + '&';
	//sendData += 'gallery_folder=' + document.getElementsByName('gallery_folder')[0].value + '&';
	sendData += 'save_in_base=0&';
//     var tmp = document.getElementsByName('save_in_base')[0].value;
// 	sendData += 'save_in_base=' + tmp + '&';
    //if (tmp == 0) {
        var tmp = galleryName.split(' ');
        galleryName = '';
        for(var i = 0; i < tmp.length; i++) 
            galleryName += tmp[i] + '_';
        galleryName = galleryName.slice(0, galleryName.length - 1);
        //tmp = ['ą','ć','ę','ł','ń','ó','ś','ż','ź'];
        galleryName = galleryName.replace(/[ąćęłńóśżź+\'\"]/g, '_');
        sendData += 'gallery_folder=' + galleryName + '&';
   // }	
	tmp = document.getElementsByName('site_id')[0].value;
	if (tmp == 0) {
		sendData += 'site_name=' + document.getElementsByName('site_name')[0].value + '&';
		//dlatego, że warunek poniżej za diabła się nie wykona;
		//później można pomyśleć o optymalizacji całości!
		sendData += 'site_id=' + tmp + '&';
	}
	//najpierw zapytanie czy ustawione jest 0 (dopisanie nowej strony)
	//czy strona już istnieje (większe od -1 ale i od 0!!)
	else if (tmp > -1 || tmp == -1)
		sendData += 'site_id=' + tmp + '&';
	sendData += 'active=' + document.getElementsByName('active')[0].value + '&';
	sendData += 'user_id=' + document.getElementsByName('user_id')[0].value + '&';
	if (document.getElementsByName('gallery_id')[0])
		sendData += 'gallery_id=' + document.getElementsByName('gallery_id')[0].value + '&';
	sendData = sendData.slice(0, sendData.length - 1);
	adminCommunication("./galleries_save.php", sendData, '', '',
			   function(r) {
				   document.getElementById("tableContent").innerHTML = r;
				   setTimeout(function() { getGalleriesTable(); }, 1000);  
				   return;
			   });
}

function saveGalleryElements() {
	//var forms = document.getElementsByName('galleryForm');
	var sendData = 'saveGalleryElements=1&';
	var eTmp = document.getElementsByClassName('elementsDiv');
	var elemCount = eTmp.length;
	for (var i = 0; i < elemCount; i++)
		if (eTmp[i] !== undefined)
			if (eTmp[i].dataset.null) // <- TA WARTOSC USTAWIANA JEST PRZEZE MNIE - to nie jest wartosc domysla w JS
				eTmp[i].parentNode.removeChild(eTmp[i]);
	elemCount = eTmp.length;
	var photo_id = document.getElementsByName('photo_id');
	var description = document.getElementsByName('description');
	var path = document.getElementsByName('path');
	var path_mini = document.getElementsByName('path_mini');
	var user_id = document.getElementsByName('user_id');
	var gallery_id =  document.getElementsByName('gallery_id');
	var splash = document.getElementsByName('splash');
	for (var i = 0; i < elemCount; i++) {
// 		if (photo_id)
// 			var photoID = document.getElementsByName('photo_id')[0].value;
		sendData += 'elements[' + i + '][description]=' + description[i].value + '&';
		//sendData += 'elements[' + i + '][description_show]=' + description_show[i].value + '&';
		console.log("Foto " + path[i] + ' mini ' + path_mini[i]); 
		if (path[i].value !== '')
			sendData += 'elements[' + i + '][path]=' + path[i].value + '&';
		if (path_mini[i].value !== '')
			sendData += 'elements[' + i + '][path_mini]=' + path_mini[i].value + '&';
		//sendData += 'elements[' + i + '][active]=' + active[i].value + '&';
		sendData += 'elements[' + i + '][user_id]=' + user_id[i].value + '&';
		sendData += 'elements[' + i + '][gallery_id]=' + gallery_id[i].value + '&';
		if (splash[i].checked)
			sendData += 'elements[' + i + '][splash]=1&';
		if (photo_id[i])
			sendData += 'elements[' + i + '][id]=' + photo_id[i].value + '&';
		//if (courseID[i].value !== '')
		//	sendData += 'elements[' + i + '][course_id]=' + courseID[i].value + '&';
		//else 
			sendData += 'elements[' + i + '][course_id]=0&';
	}
// 	if (document.getElementsByName('photo_id')[0])
// 		var photoID = document.getElementsByName('photo_id')[0].value;
// 	sendData += 'description=' + document.getElementsByName('description')[0].value + '&';
// 	sendData += 'description_show=' + document.getElementsByName('description_show')[0].value + '&';
// 	if (document.getElementsByName('path')[0].value !== '')
// 		sendData += 'path=' + document.getElementsByName('path')[0].value + '&';
// 	if (document.getElementsByName('path_mini')[0].value !== '')
// 		sendData += 'path_mini=' + document.getElementsByName('path_mini')[0].value + '&';
// 	sendData += 'active=' + document.getElementsByName('active')[0].value + '&';
// 	sendData += 'user_id=' + document.getElementsByName('user_id')[0].value + '&';
// 	sendData += 'gallery_id=' + document.getElementsByName('gallery_id')[0].value + '&';
// 	if (document.getElementsByName('photo_id')[0])
// 		sendData += 'id=' + document.getElementsByName('photo_id')[0].value + '&';
// 	if (document.getElementsByName('courseID')[0].value !== '')
// 		sendData += 'course_id=' + document.getElementsByName('courseID')[0].value + '&';
// 	else 
// 		sendData += 'course_id=0&';
	sendData = sendData.slice(0, sendData.length - 1);
	adminCommunication("./phps/galleries_save.php", sendData, '', '',
			   function(r) {
				   setTimeout(function() { getGalleriesTable(true, 0, parseInt(r)); }, 1100); 
				   //document.getElementById("tableContent").innerHTML = r;
				   document.getElementById("tableContent").innerHTML = "Dodano/zmodyfikowano element galerii!";
				    
				   return;
				   //tableMaintenace();
				   //document.getElementById('actionButtons').style.display = 'block';
			   });
}

function gallerySetClick(t) {
	if (t.parentNode.style.height === undefined ||
		t.parentNode.style.height == '')
	t.parentNode.style.height = "40px";
	if (parseInt(t.parentNode.style.height) == 40) {
		console.log(t.parentNode.children[1].children[1]);
		if (t.parentNode.children[1].children.length > 1)
			if (t.parentNode.children[1].children[1].className.indexOf('photoMiniSet') != -1) {
			if (t.parentNode.children[1].children[1].children[0].style.cssText.indexOf('url') == -1) {
				for (var i = 1; i < t.parentNode.children[1].children.length; i++) {
					t.parentNode.children[1].children[i].children[0].style = t.parentNode.children[1].children[i].children[0].dataset.photo;
				}
			}
		}
		t.parentNode.style.height = "580px";
	}
	else
		t.parentNode.style.height = "40px";
	
}

function galleryPhotoClick(t) {
	if (t === undefined) return;
	if (t.children[0].style.border == '' || t.children[0].style.border === undefined)
		t.children[0].style.border = '0px';
	console.log(t.children[0].style.border);
	console.log(parseInt(t.children[0].style.border));
	if (parseInt(t.children[0].style.border) ==  0) {
		console.log(t.children[0].style.border);
		t.children[0].style.border = '2px solid blue';
		t.dataset.select = true;
		if (t.parentNode.parentNode.children[0].children[1].dataset.activeCount === undefined) 
			t.parentNode.parentNode.children[0].children[1].dataset.activeCount = 0;		
		if (t.parentNode.parentNode.children[0].children[1].dataset.activeCount == 0) {
			t.parentNode.parentNode.children[0].children[1].children[1].style.display = 'block';
			t.parentNode.parentNode.children[0].children[1].children[2].style.display = 'block';
		}
		t.parentNode.parentNode.children[0].children[1].dataset.activeCount++;
	}
	else {
		t.children[0].style.border = '0';
		delete t.dataset.select;
		t.parentNode.parentNode.children[0].children[1].dataset.activeCount--;
		if (t.parentNode.parentNode.children[0].children[1].dataset.activeCount == 0) {
			t.parentNode.parentNode.children[0].children[1].children[1].style.display = '';
			t.parentNode.parentNode.children[0].children[1].children[2].style.display = '';
		}
	}
}

function galleriesTableDelete(t) {
	if (t === undefined) return;
// 	var tmp = document.getElementsByName('selectGalleryBox');
// 	if (!tmp)
// 		return;
	var send = 'delGallery=1';
// 	var sendID = '';
// 	for (var i = 0;  i < tmp.length; i++) {
// 		if (tmp[i].checked)
// 			sendID += tmp[i].value + ',';
// 	}
// 	if (sendID == '') 
// 		return;
	send += '&ids=' + t.parentNode.parentNode.parentNode.id.split('-')[1]; //sendID.substr(0, sendID.length - 1);
	adminCommunication("./galleries_save.php", send, '', '', function(r) {		
		setTimeout(function() { getGalleriesTable(); }, 1100); 
		document.getElementById("tableContent").innerHTML = "Usunięto wskazane galerie!";		
		return;
	});
}

function galleryPhotosTableDelete(t) {
	var tmp = t.parentNode.parentNode.parentNode.getElementsByClassName('photosSet')[0].children;
	//delPhoto -> że kasuje zdjęcia a nie albumy
	var send = 'delPhoto=1&gallery=' + t.parentNode.parentNode.parentNode.id.split('-')[1];
	var photo = '&photo=';
	for (var i = 0; i < tmp.length; i++) {
		if (tmp[i].dataset.select) {
			photo += tmp[i].dataset.id + ',';
			//console.log(tmp[i].dataset.id + ' galeria ' + t.dataset.id);
		}
	}
	if (photo[photo.length - 1] === ',') {
		photo = photo.slice(0, photo.length - 1);
	}
	send += photo;
	adminCommunication("./phps/galleries_save.php", send, '', '', function(r) {		
		setTimeout(function() { getGalleriesTable(true, 0, parseInt(r)); }, 1100); 
		document.getElementById("tableContent").innerHTML = "Usunięto wskazane zdjęcia!";		
		return;
	});
}

function removePicture(t) {
	if (document.getElementsByClassName('elementsDiv').length == 1)
		getGalleriesTable();
	else
		t.parentNode.parentNode.removeChild(t.parentNode);
}
