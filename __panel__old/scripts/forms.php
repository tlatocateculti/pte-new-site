<?php 
//plik zawiera funkcje wspólne dla tworzenia formularzy (i nie tylko)

	function createCheckBox($name, $val = '', $check = '', $fieldTitle = '', $tipText = '', $classStyle = '') {
		echo "<p";
		if ($classStyle !== '') 
			echo " class='{$classStyle}'";		
		echo ">";
		
		echo "<input type='checkbox' name='{$name}'";
		if ($val != '')
			echo " value='{$val}'";
		if ($check != '')
			echo " checked='checked'";
		if ($tipText != '')
			echo " placeholder='{$tipText}'";
		echo "/>";
		if ($fieldTitle != '')
			echo $fieldTitle;
		echo "</p>";
	}
	
	function createTextInput($name, $val = '', $fieldTitle = '', $tipText = '', $classStyle = '') {
		echo "<p";
		if ($classStyle !== '') 
			echo " class='{$classStyle}'";		
		echo ">";
		if ($fieldTitle != '')
			echo "<span>{$fieldTitle}</span>";
		echo "<input type='text' name='{$name}'";
		if ($val != '')
			echo " value='{$val}'";
		if ($tipText != '')
			echo " placeholder='{$tipText}'";
		echo "/></p>";
	}
	
	function createTextArea($name, $val = '', $fieldTitle = '', $tipText = '', $classStyle = '', $rows = '', $cols = '', $maxlength = '', $events = '') {
		echo "<p";
		if ($classStyle !== '') 
			echo " class='{$classStyle}'";		
		echo ">";
		if ($fieldTitle != '')
			echo "<span>{$fieldTitle}</span>";
		echo "<textarea type='text' name='{$name}'";
		if ($rows != '')
			echo " rows='{$rows}'";
		if ($cols != '')
			echo " cols='{$cols}'";	
		if ($maxlength != '')
			echo " maxlength='{$maxlength}'";	
		if ($tipText != '')
			echo " placeholder='{$tipText}'";
		if (is_array($events)) {
			$keys = array_keys($events);
			//$num = explode('-', $name)[1];
			for ($i = 0; $i < count($keys); $i++) {
				echo " on{$keys[$i]}='{$events[$keys[$i]]}'";
				//echo " on{$keys[$i]}='setEventHandler(this, {$num});'";
			}
		}
		echo ">";
		if ($val != '')
			echo "{$val}";
		echo "</textarea></p>";
	}
	
	function createHiddenInput($name, $value) {
		echo "<input type='hidden' name='{$name}' value='{$value}'/>";
	}
	
	function createSelectInput($name, $val = '', $selectedVal = '', $fieldTitle = '', $events = '', $classStyle = '') {
		//if ($val == '' || !is_array($val))
		//	return;
		echo "<p";
		if ($classStyle !== '') 
			echo " class='{$classStyle}'";
		echo ">";
		if ($fieldTitle != '')
			echo "<span>{$fieldTitle}</span>";
		echo "<select name='{$name}'";
		
		if (is_array($events)) {
			$e = '';
			$k = array_keys($events);
			for ($i = 0; $i < count($k); $i++) {
				$e .= " {$k[$i]}=\"{$events[$k[$i]]}\"";
			}
			echo $e;
		}
		
		echo ">";
		$keys = array();
		if (is_array($val))
			$keys = array_keys($val);
		for ($i = 0; $i < count($keys); $i++) {			
			echo "<option value='{$keys[$i]}'";
			if ($selectedVal == $keys[$i]) 
				echo " selected='selected'";
			echo ">{$val[$keys[$i]]}</option>";
		}
		echo "</select></p>";
	}
	
	function createLabelInfo($labelID, $value, $tip, $classStyle = '') {
		echo "<p class='{$classStyle}' id='{$labelID}'><span>{$tip}</span><span>{$value}</span></p>";
	}
	
	function createButtonPickUp(/*$name, */$val = '', $fieldTitle = '', $events = '', $classStyle = '') {
		echo "<p";
		if ($classStyle !== '') 
			echo " class='{$classStyle}'";		
		echo ">";
		if ($fieldTitle != '')
			echo "<span>{$fieldTitle}</span>";
		echo "<button";
		if (is_array($events)) {
            $keys = array_keys($events);
            for ($i = 0; $i < count($keys); $i++) {
				echo " on{$keys[$i]}='{$events[$keys[$i]]}'";
				//echo " on{$keys[$i]}='setEventHandler(this, {$num});'";
			}
		}
		echo ">";
		echo $val . "</button></p>";
		
// 		echo "<input type='text' name='{$name}'";
// 		if ($val != '')
// 			echo " value='{$val}'";
// // 		if ($tipText != '')
// // 			echo " placeholder='{$tipText}'";
// 		echo "/></p>";
	}

	function createFileInput($name, $val = '', $fieldTitle = '', $tipText = '', $classStyle = '', $events = '') {
		echo "<p";
		if ($classStyle !== '') 
			echo " class='{$classStyle}'";		
		echo ">";
		if ($fieldTitle != '')
			echo "<span>{$fieldTitle}</span>";
		echo "<input type='file' name='{$name}'";
		//if ($val != '')
		         
			//echo " value='{$val}'";
		if ($tipText != '')
			echo " placeholder='{$tipText}'";
		if (is_array($events)) {
			$e = '';
			$k = array_keys($events);
			for ($i = 0; $i < count($k); $i++) {
				$e .= " {$k[$i]}=\"{$events[$k[$i]]}\"";
			}
			echo $e;
		}
		echo "/></p>";
// 		if ($val != '')
// 			echo '<script>appedFileToInput(
	}
	
	function createDivButton($id, $val = '', $tipText = '', $classStyle = '', $events = '') {
		echo "<div";
		if ($classStyle !== '')
			echo " class='{$classStyle}'";
		if (is_array($events)) {
			$keys = array_keys($events);
			for ($i = 0; $i < count($keys); $i++)
				echo " {$keys[$i]}=\"" . $events[$keys[$i]] . ";\"";
		}
		echo " id='{$id}'>";
		echo "<p>{$val}</p></div>";
	}

	//FUNKCJA TWORZONA WEDLE NOWYCH PRAW!! WSZYSTKIE MAJĄ BYĆ TWORZONE IDENTYCZNIE (ZWRACANY CIĄG ZNAKOWY, NIE ECHO!!)
	function createListNode ($id, $name, $val = '', $tipText = '', $classStyle = '', $buttons = '', $events = '', $nodeParams = '') {
		$ret = '';
		$ret .= "<div";
		//echo ">";
		if (isset($nodeParams['nodeEvents'])) {
			$keys = array_keys($nodeParams['nodeEvents']);
			for ($i = 0; $i < count($keys); $i++) $ret .= " {$keys[$i]}='{$nodeParams['nodeEvents'][$keys[$i]]}'";
		}
		if ($classStyle !== '')
			$ret .= " class='{$classStyle}'";
		$ret .= " id='{$id}'>";
		if (isset($nodeParams['nodeID'])) {
			$ret .= "<div data-name='dummyElement' style='float: left; height: 100%; width: " . (50 * $nodeParams['nodeID']) . "px;'></div>";
		}
		if (isset($nodeParams['nodeIcon'])) {
			$ret .= "<p data-name='iconElement' style='float: left;'";
			if (isset($nodeParams['nodeIconStyle'])) {
				$ret .= " class='{$nodeParams['nodeIconStyle']}'";
			}
			if (isset($nodeParams['nodeIconEvents'])) {
				$keys = array_keys($nodeParams['nodeIconEvents']);
				for ($i = 0; $i < count($keys); $i++) $ret .= " {$keys[$i]}='{$nodeParams['nodeIconEvents'][$keys[$i]]}'";
			}
			$ret .= ">{$nodeParams['nodeIcon']}</p>";
		}
		$ret .= "<p style='float: left;' data-name='{$name}'";
		if (is_array($events)) {
			$keys = array_keys($events);
			for ($i = 0; $i < count($keys); $i++)
				$ret .= " {$keys[$i]}='" . $events[$keys[$i]] . ";'";
		}
		$ret .= ">{$val}</p>";
		if (is_array($buttons)) {
// 			print_r($buttons);
			$ret .= "<div style='float: right; height: 100%; overflow: hidden;'>";
			for ($i = 0; $i < count($buttons); $i++) {
				$ret .= "<div style=\"position: relative;\"";
				if (isset($buttons[$i]['style']))
					$ret .= " class=\"{$buttons[$i]['style']}\"";
				if (is_array($buttons[$i]['events'])) {					
					$keys = array_keys($buttons[$i]['events']);
					for ($j = 0; $j < count($keys); $j++) {
						$ret .= " {$keys[$j]}='" . $buttons[$i]['events'][$keys[$j]] . ";'";
					}
				}
				$ret .= ">";
				if (isset($buttons[$i]['icon'])) {
					//chwilowo zmieniła się koncepcja z tej...
					//echo "<div style=\"background: url('{$buttons[$i]['icon']}'); position: absolute; width: 100%; height: 100%; left: 0; top: 0;\"></div>";
					//na tą
					$ret .= "<p";
					if ($buttons[$i]['name'])
						$ret .= " data-tip='{$buttons[$i]['name']}'";
					$ret .= ">{$buttons[$i]['icon']}</p>";
				}
				else 
					$ret .= "<p>{$buttons[$i]['name']}</p>";
				$ret .= "</div>";
					
					
			}
			$ret .= "</div>";
		}
		$ret .= "</div>";
		return $ret;
	}
	
	function createNewTextInput($name, $val = '', $fieldTitle = '', $tipText = '', $classStyle = '', $fieldInfo = '') {
		$ret = "<div";
		if ($classStyle !== '') 
			$ret .= " class='{$classStyle}'";		
		$ret .=  ">";
		if ($fieldTitle !== '')
			$ret .=  "<span>{$fieldTitle}</span>";
		$ret .=  "<input type='text' name='{$name}'";
		if ($val !== '')
			$ret .=  " value='{$val}'";
		if ($tipText !== '')
			$ret .=  " placeholder='{$tipText}'";
		$ret .=  "/>";
		if ($fieldInfo !== '')
			$ret .= '<i class="fa fa-question-circle" aria-hidden="true" data-text="' . $fieldInfo . '" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i>';
		$ret .= "</div>";
		return $ret;
	}
	
	function createNewRadioButton($name, $val = '', $fieldTitle = '', $tipText = '', $classStyle = '', $fieldInfo = '') {
		$ret = "<fieldset";
		if ($classStyle !== '') 
			$ret .= " class='{$classStyle}'";		
		$ret .=  ">";
		if ($fieldTitle !== '')
			$ret .=  "<legend>{$fieldTitle}</legend>";
		$keys = array_keys($val);
		for ($i = 1; $i < count($val); $i++) {
			$ret .=  "<p><input type='radio' name='{$name}'";
			if ($val !== '')
				$ret .=  " value='{$val[$keys[$i]]}'";
			if ($tipText !== '')
				$ret .=  " placeholder='{$tipText}'";
			if ($val[$keys[$i]] === $val['default'])
				$ret .= " checked='checked'";
			$ret .=  "/><span>{$keys[$i]}</span></p>";
		}
		
		if ($fieldInfo !== '')
			$ret .= '<i class="fa fa-question-circle" aria-hidden="true" data-text="' . $fieldInfo . '" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i>';
		$ret .= "</fieldset>";
		return $ret;
	}
	
	//NIE DZIAŁA NA SAFARI (i nie wiadomo czy będzie działać - mało mnie to...)
	function createNewEditSelectInput($name, $val = '', $defaultValID = '',  $fieldTitle = '', $tipText = '', $classStyle = '', $fieldInfo = '') {
		$ret = "<div";
		if ($classStyle !== '') 
			$ret .= " class='{$classStyle}'";		
		$ret .=  ">";
		if ($fieldTitle !== '')
			$ret .=  "<span>{$fieldTitle}</span>";
		$ret .=  "<input type='text' list='{$name}List' name='{$name}'";
		if ($defaultValID !== '')
			$ret .=  " value='{$val[$defaultValID]}'";
		if ($tipText !== '')
			$ret .=  " placeholder='{$tipText}'";
		$ret .=  "/>";
		$ret .= "<datalist id='{$name}List'>";
		for ($i = 0; $i < count($val); $i++) {
			$ret .= "<option value='{$val[$i]}'/>{$val[$i]}</option>";
		}
		$ret .= "</datalist>";
		if ($fieldInfo !== '')
			$ret .= '<i class="fa fa-question-circle" aria-hidden="true" data-text="' . $fieldInfo . '" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i>';
		$ret .= "</div>";
		return $ret;
	}
	
	function createNewInfoButton($fieldInfo = '') {
		$ret = '';
		if ($fieldInfo !== '')
			$ret .= '<i class="fa fa-question-circle" aria-hidden="true" data-text="' . $fieldInfo . '" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i>';
		return $ret;
	}
	
	function createNewSelectInput($name, $val = '', $selectedVal = '', $fieldTitle = '', $events = '', $classStyle = '', $fieldInfo = '') {
		//if ($val == '' || !is_array($val))
		//	return;
		$ret = "<div";
		if ($classStyle !== '') 
			$ret .= " class='{$classStyle}'";
		$ret .= ">";
		if ($fieldTitle !== '')
			$ret .= "<span>{$fieldTitle}</span>";
		$ret .= "<select name='{$name}'";
		
		if (is_array($events)) {
			$e = '';
			$k = array_keys($events);
			for ($i = 0; $i < count($k); $i++) {
				$e .= " {$k[$i]}=\"{$events[$k[$i]]}\"";
			}
			$ret .= $e;
		}
		
		$ret .= ">";
		$keys = array();
		if (is_array($val))
			$keys = array_keys($val);
		for ($i = 0; $i < count($keys); $i++) {			
			$ret .= "<option value='{$keys[$i]}'";
			if ($selectedVal == $keys[$i]) 
				$ret .= " selected='selected'";
			$ret .= ">{$val[$keys[$i]]}</option>";
		}
		$ret .= "</select>";
		if ($fieldInfo !== '')
			$ret .= '<i class="fa fa-question-circle" aria-hidden="true" data-text="' . $fieldInfo . '" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i>';
		$ret .= "</div>";
		return $ret;
	}
	
	function createNewFileInput($name, $val = '', $fieldTitle = '', $tipText = '', $classStyle = '', $events = '', $fieldInfo = '') {
		$ret = "<div";
		if ($classStyle !== '') 
			$ret .= " class='{$classStyle}'";		
		$ret .= ">";
		if ($fieldTitle != '')
			$ret .= "<span>{$fieldTitle}</span>";
		$ret .= "<input type='file' name='{$name}'";
		//if ($val != '')
		         
			//echo " value='{$val}'";
		if ($tipText != '')
			$ret .= " placeholder='{$tipText}'";
		if (is_array($events)) {
			$e = '';
			$k = array_keys($events);
			for ($i = 0; $i < count($k); $i++) {
				$e .= " {$k[$i]}=\"{$events[$k[$i]]}\"";
			}
			$ret .= $e;
		}
		$ret .= "/>";
		if ($fieldInfo !== '')
			$ret .= '<i class="fa fa-question-circle" aria-hidden="true" data-text="' . $fieldInfo . '" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i>';
		$ret .= "</div>";
		return $ret;
// 		if ($val != '')
// 			echo '<script>appedFileToInput(
	}
	
	function createNewTextArea($name, $val = '', $fieldTitle = '', $tipText = '', $classStyle = '', $rows = '', $cols = '', $maxlength = '', $events = '', $fieldInfo = '') {
		$ret = "<div";
		if ($classStyle !== '') 
			$ret .= " class='{$classStyle}'";		
		$ret .= ">";
		if ($fieldTitle != '')
			$ret .= "<span>{$fieldTitle}</span>";
		$ret .= "<textarea type='text' name='{$name}'";
		if ($rows != '')
			$ret .= " rows='{$rows}'";
		if ($cols != '')
			$ret .= " cols='{$cols}'";	
		if ($maxlength != '')
			$ret .= " maxlength='{$maxlength}'";	
		if ($tipText != '')
			$ret .= " placeholder='{$tipText}'";
		if (is_array($events)) {
			$keys = array_keys($events);
			//$num = explode('-', $name)[1];
			for ($i = 0; $i < count($keys); $i++) {
				$ret .= " on{$keys[$i]}='{$events[$keys[$i]]}'";
				//echo " on{$keys[$i]}='setEventHandler(this, {$num});'";
			}
		}
		$ret .= ">";
		if ($val != '')
			$ret .= "{$val}";
		$ret .= "</textarea>";
		if ($fieldInfo !== '')
			$ret .= '<i class="fa fa-question-circle" aria-hidden="true" data-text="' . $fieldInfo . '" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i>';
		$ret .= "</div>";
		return $ret;
	}
	
	function createNewTextAreaEditor($name, $val = '', $fieldTitle = '', $classStyle = '', $events = '', $fieldInfo = '') {
		$ret = "<div";
		if ($classStyle !== '') 
			$ret .= " class='{$classStyle}'";		
		$ret .= ">";
		if ($fieldTitle != '')
			$ret .= "<span>{$fieldTitle}</span>";
		$ret .= "<iframe id='{$name}' name='{$name}' style='width: 95%; min-height: 820px;height:auto; border: 0;' src='./editor/editor.html'";
		if (is_array($events)) {
			$keys = array_keys($events);
			for ($i = 0; $i < count($keys); $i++) {
				$ret .= " on{$keys[$i]}='{$events[$keys[$i]]}'";
			}
		}
		$ret .= "></iframe>";
		if ($fieldInfo !== '')
			$ret .= '<i class="fa fa-question-circle" aria-hidden="true" data-text="' . $fieldInfo . '" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i>';
		$ret .= "</div>";
		return $ret;
	}