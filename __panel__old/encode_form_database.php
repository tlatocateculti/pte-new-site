<?php 
	if (file_exists(__DIR__ . '/../data/data.dat')) {
		header("Location: index.php");
	}
	header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
	header("Pragma: no-cache"); //HTTP 1.0
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
	</head>
	<body>
		<?php 
			$date = new DateTime("now");
			$date->setTimezone(new DateTimezone("Europe/Warsaw"));
			echo $date->format("d/m/Y H:i");
			
			if (isset($_POST) && isset($_POST['baseUser'])) {
				require_once('./utilityFunctions.php');
				//require_once(__DIR__ . "/../scripts/database.php");
				require_once(__DIR__ . "/../config.php");
				require_once(__DIR__ . "/../scripts/utilityFunctions.php");
				saveToFile($_POST['baseUser'], 'w');
				saveToFile($_POST['basePass']);
				saveToFile($_POST['siteLogin']);
				saveToFile($_POST['sitePass']);
				saveToFile($_POST['siteServer']);
				saveToFile($_POST['sitePort']);
			}
			
			function saveToFile($data, $mode = 'a') {
				$f = fopen('../data/data.dat', $mode);
				$pass = encodePhrase($data);
				fwrite($f, $pass . "||");
				fclose($f);
			}
		?>
		<!--<script src="functions.js"></script>
		<script src="./js/settings.js"></script>
		<script>
			
		</script>-->
		<form method="post" id='mainUserSettingsForm'>
			<div><span>Login</span><input type='password' name='baseUser'/></div>
			<div><span>Hasło</span><input type='password' name='basePass'/></div>
			<div><span>Login poczty</span><input type='password' name='siteLogin'/></div>
			<div><span>Hasło poczty</span><input type='password' name='sitePass'/></div>
			<div><span>Serwer poczty</span><input type='password' name='siteServer'/></div>
			<div><span>Port poczty</span><input type='password' name='sitePort'/></div>
			<div><input type="submit" value="Zatwierdź"/></div>
		</form>
<!--		<form method="post">
			<input type="password" name="toHash"/><br/>
			<input type="checkbox" name="removeFile" value="1">Usuń poprzedni plik z hasłami<br/>
			<input type="submit" value="Wyślij"/>
		</form>-->
	</body>
</html>