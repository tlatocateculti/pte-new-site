<?php 
	require_once('./utilityFunctions.php');
	require_once "../../scripts/database.php";
	require_once "../../config.php";
// 	require_once "../mailFunctions.php";
	require_once "../../scripts/utilityFunctions.php";
	require_once "../scripts/forms.php";
	confirmSession();
	//$currentForm = 0;
		
	//funkcja tworzy pojedynczy dormularz dla edycji/dodawania wizyty;
	//$data - zbiór wartości dla kolejnych edytowanych pól
	function createCourseForm($data = '', $currentForm = 0, $courseType = 0) {
		echo "<div name='courseForms' id='form-{$currentForm}' style='overflow: hidden;'>";
		//createSelectInput("category-name-{$currentForm}", $data[1], ($data[0] === '' ? '-1' : $data[0]['category_id'][$currentForm]), 'Wybierz kategorię', array('onchange' => 'courseCategoryChange(this, ' . $currentForm . ');'), 'afterBlock beforeBlock');
		echo createNewSelectInput("category-name-{$currentForm}", $data[1], ($data[0] === '' ? '-1' : $data[0]['category_id'][$currentForm]), 'Wybierz kategorię', array('onchange' => 'courseCategoryChange(this, ' . $currentForm . ');'), 'afterBlock beforeBlock', 'Każdy kurs/szkolenie musi zostać przypisane do kategorii. Dzięki temu zabiegowi tworzy się katalog usług, który można łatwo przeglądać (np. szukając kursu fotograficznego użytkownik będzie przeglądał kursy przypisane do kategorii Grafika zamiast szukać wśród wszystkich kursów. Wybierając Nowa tworzymy nową kategorię kursu (bądź możemy wybrać jedną z już dodanych)');
		$classNames = 'afterBlock beforeBlock';
		if (isset($data[0]['category_id'][$currentForm])) 
			$classNames .= ' hidden';
		//createTextInput("category-name-{$currentForm}", '', 'Nazwa kategorii', 'Wpisz nazwę nowej kategorii', $classNames);
		echo createNewTextInput("category-name-{$currentForm}", '', 'Nazwa kategorii', 'Wpisz nazwę nowej kategorii', $classNames, 'Nazwa nowo dodawanej kategorii. Należy pamiętać, że może istnieć kilka kategorii z tą samą nazwą - nie są one identyfikowane po nazwach!');
// 		createTextInput("category-description-{$currentForm}", '', 'Opis kategorii', 'Opcjonalny opis kategorii', $classNames);
		echo createNewTextInput("category-description-{$currentForm}", '', 'Opis kategorii', 'Opcjonalny opis kategorii', $classNames, 'Dodawany w tym miejscu opis NIE BĘDZIE widoczny nigdzie na stronie - ma on zastosowanie jedynie informacyjne');
		
// 		createSelectInput("subcategory-name-{$currentForm}", $data[2], ($data[0] === '' ? '0' : $data[0]['subcat_id'][$currentForm]), 'Wybierz podkategorię', array('onchange' => 'subCourseCategoryChange(this, ' . $currentForm . ');'), 'afterBlock beforeBlock');
		echo createNewSelectInput("subcategory-name-{$currentForm}", $data[2], ($data[0] === '' ? '0' : $data[0]['subcat_id'][$currentForm]), 'Wybierz podkategorię', array('onchange' => 'subCourseCategoryChange(this, ' . $currentForm . ');'), 'afterBlock beforeBlock', 'Dodawany kurs/szkolenie może zostać przypisany do podkategorii. Tego typu katalogowanie zmieni jednak sposób wyświetlanie kursu/szkolenia (dołączy dodatkową listę). Dlatego domyślbnie ZALECA SIĘ POZOSTAWINIE TEGO POLA PUSTEGO (Brak).');
		$classNames = 'afterBlock beforeBlock';
		if (isset($data[0]['subcat_id'][$currentForm])) 
			$classNames .= ' hidden';
// 		createTextInput("subcategory-name-{$currentForm}", '', 'Nazwa podkategorii', 'Wpisz nazwę nowej podkategorii', $classNames . ' hidden');
		echo createNewTextInput("subcategory-name-{$currentForm}", '', 'Nazwa podkategorii', 'Wpisz nazwę nowej podkategorii', $classNames . ' hidden', 'Nazwa nowo dodawanej podkategorii.');
// 		createTextInput("subcategory-description-{$currentForm}", '', 'Opis podkategorii', 'Opcjonalny opis podkategorii', $classNames . ' hidden');
		echo createNewTextInput("subcategory-description-{$currentForm}", '', 'Opis podkategorii', 'Opcjonalny opis podkategorii', $classNames . ' hidden', 'Opcjonalny opis podkategorii (nie będzie on nigdzie widoczny)');
		
// 		createTextInput("topic-{$currentForm}", ($data[0] === '' ? '' : $data[0]['topic'][$currentForm]), 'Nazwa kursu/szkolenia', 'Wpisz nazwę kursu/szkolenia', 'afterBlock beforeBlock');
		echo createNewTextInput("topic-{$currentForm}", ($data[0] === '' ? '' : $data[0]['topic'][$currentForm]), 'Nazwa kursu/szkolenia', 'Wpisz nazwę kursu/szkolenia', 'afterBlock beforeBlock', 'Nazwa dodawanego kursu/szkolenia. Nazwa ta, chociaż nieobowiązkowa jest ZALECANA. To po niej użytkownik stroby może zidentyfikować kurs/szkolenie. Nazwy mogą być dowolen i zawierać więcej niż jedno słowo (ograniczeniem jest 100 znaków).');
		
		if ($courseType == 0) {
			//createTextInput("course_startdate-{$currentForm}", ($data[0] === '' ? '' : $data[0]['course_startdate'][$currentForm]), 'Data startu kursu', 'Format: YYYY/MM/DD', 'afterBlock beforeBlock');
			echo createNewTextInput("course_startdate-{$currentForm}", ($data[0] === '' ? '' : $data[0]['course_startdate'][$currentForm]), 'Data startu kursu', 'Format: YYYY/MM/DD', 'afterBlock beforeBlock', 'Pole opcjonalne. Można podać datę (przybliżoną datę) rozpoczęcia kursu (zarówno jako konkretny dzień jak i jako dzień oraz godzinę rozpoczęcia)');
// 			createTextInput("end_date-{$currentForm}", ($data[0] === '' ? '' : $data[0]['end_date'][$currentForm]), 'Data zakończenia kursu (opcjonalnie)', 'Format: YYYY/MM/DD', 'afterBlock beforeBlock');
			echo createNewTextInput("end_date-{$currentForm}", ($data[0] === '' ? '' : $data[0]['end_date'][$currentForm]), 'Data zakończenia kursu (opcjonalnie)', 'Format: YYYY/MM/DD', 'afterBlock beforeBlock', 'Pole opcjonalne. Można podać datę zakończenia kursu, dzięki czemu osoby zainteresowane kursem poznają orientacyjny czas trwania kursu');
		}
		
// // 		createTextArea("objectives-{$currentForm}", ($data[0] === '' ? '' : $data[0]['objectives'][$currentForm]), 'Cele kursu', 'Proszę podawać kolejne cele po średniku, np. ciekawy kierunek; szybka nauka; duży rynek pracy;', 'afterBlock beforeBlock', 6, 100);
		echo createNewTextAreaEditor("objectives-{$currentForm}", ($data[0] === '' ? '' : $data[0]['objectives'][$currentForm]), 'Cele kursu', 'afterBlock beforeBlock', '', 'W tym polu podaje się możliwe osiągnięcia, jakie potencjalny kadydat może zdobyć wybierając kurs/szkolenie.');
// 		createTextArea("additional_info-{$currentForm}", ($data[0] === '' ? '' : $data[0]['additional_info'][$currentForm]), 'Informacje o kursie', 'Pełne informacje o kursie (65535 znaków)', 'afterBlock beforeBlock', 6, 100);
		echo createNewTextAreaEditor("additional_info-{$currentForm}", ($data[0] === '' ? '' : $data[0]['additional_info'][$currentForm]), 'Informacje o kursie', 'afterBlock beforeBlock', '', 'W tym polu należy szczegółowo opisać kurs/szkolenie.');
// 		createTextArea("prices-{$currentForm}", ($data[0] === '' ? '' : $data[0]['prices'][$currentForm]), 'Cennik', 'Ceny można podawać jak w przypadku celów kursu lub podać pojedynczą cenę', 'afterBlock beforeBlock', 6, 100);
		echo createNewTextAreaEditor("prices-{$currentForm}", ($data[0] === '' ? '' : $data[0]['prices'][$currentForm]), 'Cennik', 'afterBlock beforeBlock', '', 'Pole nieobowiązkowe. Można przypisywać ceny dla kursu/szkolenia.');
// 		createSelectInput("show_all_photo-{$currentForm}", array(1 => 'Pokazuj domyślne dla kategorii', 0 => 'Przypisane do kursu', 2 => 'Wszystkie możliwe'), ($data[0] === '' ? '0' : $data[0]['show_all_photo'][$currentForm]), 'Tryb pokazywania miniatur dla kursu', '', 'afterBlock beforeBlock');
		echo createNewSelectInput("show_all_photo-{$currentForm}", array(1 => 'Pokazuj domyślne dla kategorii', 0 => 'Przypisane do kursu', 2 => 'Wszystkie możliwe'), ($data[0] === '' ? '0' : $data[0]['show_all_photo'][$currentForm]), 'Tryb pokazywania miniatur dla kursu', '', 'afterBlock beforeBlock', 'Lista pozwala na zadecydowanie, w jaki sposób do kursu/szkolenia mają być dobierane zdjęcia w galerii. Wybranie pierwszej opcji (domyślne dla kategorii) podowdować będzie, iż kurs przyjmie za zdjęckia wszystkie z albumu przypisanego do kursów/szmkoleń. Opcja druga pozowli zawęzić wybór jedynie do zdjęć dla kursu/szkolenia. Ostatnia opcja wyświetli wszystkie zdjęcia.');
// 		createSelectInput("active-{$currentForm}", array(1 => 'Tak', 0 => 'Nie'), ($data[0] === '' ? '' : $data[0]['active'][$currentForm]), 'Kurs widoczny', '', 'afterBlock beforeBlock');
		echo createNewSelectInput("active-{$currentForm}", array(1 => 'Tak', 0 => 'Nie'), ($data[0] === '' ? '' : $data[0]['active'][$currentForm]), 'Aktywny', '', 'afterBlock beforeBlock', 'Określa czy kurs/szkolenie będzie widoczne dla użytkowników strony');
		$warnings = '';
		if ($data[0] !== '') {
// 			if ($data[0]['show'][$currentForm] == 0)
// 				$warnings .= 'TO OGŁOSZENIE NIE JEST WYŚWIETLANE!<br/>';
// 			createLabelInfo("warningInfo-{$currentForm}", $warnings, 'UWAGI: ', 'expired beforeBlock afterBlock');
		
			createHiddenInput("course_id-{$currentForm}", $data[0]['course_id'][$currentForm]);
		}
		//zero oznacza, ze jest to kurs
		createHiddenInput("category_type-{$currentForm}", $courseType);
		createHiddenInput("user_id-{$currentForm}", 1);
		echo "</div>";
	}
	
	function indexOf($table, $search) {
		$maxCount = count($table);
		$max = (((int) ($maxCount--/2)) + 1);
		for ($i = 0; $i < $max; $i++) {
			if ($table[$i] == $search)
				return $i;
			if ($table[$maxCount - $i] == $search)
				return $maxCount - $i;
		}
		return -1;
	}
//WŁAŚCIWY KOD STRONY!!
	$formsCount = 1;
	$base = new Database();
	$base->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME);

	$data  = '';
	if (isset($_POST['fields'])) {
		if ($_POST['fields'] != '') {
			$fieldsArray = explode(',', $_POST['fields']);
			$base->buildConditionQuery(array('courses', 'course_id'), $fieldsArray, DataEnum::IN);
			//$base->queryTable(array('courses' => array('topic', 'course_startdate', 'objectives', 'additional_info', 'prices', 'dateExpired', 'show', 'course_id')), $base->getConditions(), -1);
			$base->queryTable(array('courses' => array('*')), $base->getConditions(), -1);
			$base->flushConditions();
			if ($base->getResults(0) != -1) {
				$data = $base->getResults(0);
				$base->flushResults();
				$base->buildConditionQuery(array('subcategories', 'subcat_id'), $data['subcat_id'], DataEnum::IN);
				$base->queryTable(array('subcategories' => array('category_id', 'subcat_id')), $base->getConditions(), -1);
				$base->flushConditions();
				//WYDAJNOŚĆ!! Tego zestawienia; trzeba będzie poprawić!
				if ($base->getResults(0) != -1) {
					for ($i = 0; $i < count($data['course_id']); $i++) {
						for ($j = 0; $j < count($base->getResults(0)['subcat_id']); $j++) {
							if ($data['subcat_id'][$i] == $base->getResults(0)['subcat_id'][$j])
								$data['category_id'][$i] = $base->getResults(0)['category_id'][$j];
						}
					}
				}
			}
			$base->flushResults();
			
		}
	}
	
	$base->buildConditionQuery(array('categories', 'active'), array(1), DataEnum::EQUAL);
	if (!isset($_GET['s']))
		$base->buildConditionQuery(array('categories', 'category_type'), array(0), DataEnum::EQUAL, DataEnum::DAND);
	else
		$base->buildConditionQuery(array('categories', 'category_type'), array(1), DataEnum::EQUAL, DataEnum::DAND);
	$base->queryTable(array('categories' => array('name', 'description', 'category_id')), $base->getConditions(), -1);
	$base->flushConditions();
	$base->buildConditionQuery(array('subcategories', 'category_id'), $base->getResults(0)['category_id'], DataEnum::IN);
	$base->buildConditionQuery(array('subcategories', 'active'), array(1), DataEnum::EQUAL, DataEnum::DAND);
	$base->queryTable(array('subcategories' => array('name', 'description', 'subcat_id', 'no_sub_cat')), $base->getConditions(), -1);
	$categories = array(-1 => "Nowa");
	$subcategories = array(-1 => "Nowa", 0 => "Brak");
	for($i = 0; $i < count($base->getResults(0)['name']); $i++) {
		$categories[ $base->getResults(0)['category_id'][$i] ] = $base->getResults(0)['name'][$i];
	}
	for($i = 0; $i < count($base->getResults(1)['name']); $i++) {
		if ($base->getResults(1)['no_sub_cat'][$i] != 1)
			$subcategories[ $base->getResults(1)['subcat_id'][$i] ] = $base->getResults(1)['name'][$i];
	}
// 	if (count($subcategories) == 2 && is_array($data)) {
// 		
// 	}
	if (is_array($data)) {
		$formsCount = count($data['course_id']);
	}
	//w tej chwili i tak obsługiwany jest jedynie JEDEN formularz na zaznaczenie!!
	//albo to trzeba zmienić (przerobić na możliwość zmiany wielu kursów na raz )
	//albo przerobić mechanizm na pojedynczy mechanizm!! (skłaniam się do drugiego)
	for ($i = 0; $i < $formsCount; $i++) {
		$individualSubCat = 0;
		if (is_array($data) && count($subcategories) == 2) {
			//$individualSubCat = true;
			$subcategories[ $data['subcat_id'][$i] ] = 'JAK NAZWA KURSU/SZKOLENIA';
		}
		if (!isset($_GET['s']))
			createCourseForm(array($data, $categories, $subcategories), $i);
		else 
			createCourseForm(array($data, $categories, $subcategories), $i, 1);
			//echo 'Właśnie chcesz dodać nową wizytę!';
		if ($individualSubCat > 0)
			unset($subcategories[$individualSubCat]);
	}
?>
<style>
	.afterBlock:after, .beforeBlock:before {
		content: '';
		overflow: auto;
		display: table;
		clear: both;
	}
	.selectList {
		overflow: hidden;
	}
	.expired {
		color: red;
	}
	
	.hidden {
		display: none;
	}
</style>

<button onclick="saveCourse();">
<?php if (isset($_POST['fields'])) echo 'Zapisz zmiany'; else {
	if (!isset($_GET['s'])) 
		echo 'Dodaj kurs'; 
	else
		echo 'Dodaj szkolenie';
	}?></button>
<button onclick="getCoursesTable();">Anuluj</button>
