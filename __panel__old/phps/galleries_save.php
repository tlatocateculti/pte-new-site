<?php
	namespace GallerySave;
/*
	Ogólnie rzecz biorąc plik jest już w stanie dodawać bądź aktualizować dane na temat pacjenta;
*/
//NIE DZIAŁA DODAWANIE ZDJĘĆ DO DRUGIEJ GALERII (I PEWNIE KOLEJNYCH!!)
	require_once "./utilityFunctions.php";
	require_once "../../scripts/utilityFunctions.php";
// 	require_once "../../scripts/database.php";
	require_once "../../config.php";
	confirmSession();

	//funkcja ma za zadanie ustalenie nazw plików dodawanych do galerii;
	//pierwotnie zdjęcia przesyłane są z całą scieżką, w tym nazwą dysku!
	function getGalleryFilesName(&$path = '', &$path_mini = '') {
		if ($path !== '') {
			$path = str_replace("\\", "/", $path); 
			$path = explode('/',$path);
			$path = $path[count($path) -1];
		}
		if ($path_mini) {
			$path_mini = str_replace("\\", "/", $path_mini); 
			$path_mini = explode('/',$path_mini);
			$path_mini = $path_mini[count($path_mini) -1];
		}
	}
	
	function createMini($name) {
		generateMiniImages(array($name), $out, '../../galleries/');
		return $out[0];
	}
	
	if (!isset($_POST))
		return;
	
	if (isset($_POST['filePath'])) {		
		$tmp = explode('/', $_POST['filePath']);
		$dir = '';
		$name = $tmp[count($tmp) - 1];
		for($i = 0; $i < (count($tmp) - 1); $i++)
			$dir .= $tmp[$i] . '/';
		
		//echo $dir . $name . '.tmp_' . $_POST['secquence'] . ' ';
		save_tmp_file($dir, $name . '.tmp_' . $_POST['secquence'], $_POST['data'], $_POST['mode']);
		if ( !isset($_SESSION['files'][$name]) ) {
			$_SESSION['files'][$name] = -1;
		}
		$_SESSION['files'][$name]++;

		echo $_SESSION['files'][$name];
		return;
	}
	else if (isset($_POST['transfer'])) {
		$tmp = explode('/', $_POST['transfer']);
		$dir = '';
		$name = $tmp[count($tmp) - 1];
		for($i = 0; $i < (count($tmp) - 1); $i++)
			$dir .= $tmp[$i] . '/';
		
		unset($_SESSION['files'][$name]);		
		saveFile($dir, $name, $_POST['total']);
		
	}
	else if (isset($_POST['delPhoto'])) {
		$photosID = explode(',', $_POST['photo']);
		$files = readGalleryEntries('../../json');
		for ($z = 0; $z < count($photosID); $z++) {
			$path = $files[$photosID[$z]]['photo'];
			$path_mini = $files[$photosID[$z]]['mini'];
			getGalleryFilesName($path, $path_mini);
			unlink('../../galleries/'. $path);
			unlink('../../galleries/'. $path_mini);
			unset($files[$photosID[$z]]);
		}
		$files['photos'] = array_values($files);
		file_put_contents('../../json/gallery.json', '[' . json_encode($files) . ']');
	}
	else if (isset($_POST['delGallery'])) {
		$galleryID = explode(',', $_POST['ids']);
		if (/*$bs->getResults(1) !==*/ -1) {
			$filesRemove = [];
		}
	}

	else if (isset($_POST['saveGalleryElements'])){	
		$files = readGalleryEntries('../../json');
		$existed_files = [];
		foreach($_POST['elements'] as $e) {
			$path = '';
			if (isset($e['path'])) 
				$path = $e['path'];
			if (isset($e['path_mini']))
				$path_mini = $e['path_mini'];
			else 
				$path_mini = '';
			getGalleryFilesName($path, $path_mini);
			if($path_mini === '' && $path !== '') {
				$path_mini = createMini($path);
			}
			if (isset($e['id'])) {
				if ($path) {
					$f = explode('/', $files[$e['id']]['photo']);
					$existed_files[count($existed_files)] = $f[count($f) - 1];
					$files[$e['id']]['photo'] = './galleries/' .$path;
				}
				if ($path_mini !== '') {
					$f = explode('/', $files[$e['id']]['mini']);
					$existed_files[count($existed_files)] = $f[count($f) - 1];
					$files[$e['id']]['mini'] = './galleries/' .$path_mini;
				}
				$files[$e['id']]['alt'] = $e['description'];
				if (isset($e['splash'])) $files[$e['id']]['splash'] = '1';
				else if (isset($files[$e['id']]['splash'])) unset($files[$e['id']]['splash']);
			}
			else {
				$tmp_id = count($files);
				$files[$tmp_id]['photo'] = './galleries/' .$path;
				$files[$tmp_id]['mini'] = './galleries/' .$path_mini;
				$files[$tmp_id]['alt'] = $e['description'];
				if (isset($e['splash'])) $files[$tmp_id]['splash'] = '1';
			}
		}
		if (count($existed_files) > 0)
			removeGalleryFiles('../../galleries', $existed_files, true);
		//bez dodania nowej zmiennej tablica ulega znieksztalceniu (usuniety zostanie pojemnik photos - dosyc wazny przy zastosowanej notacji)
		$files2['photos'] = $files;
		file_put_contents('../../json/gallery.json', '[' . json_encode($files2) . ']');
	}
	
