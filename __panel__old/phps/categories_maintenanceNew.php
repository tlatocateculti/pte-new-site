<?php
	require_once('./utilityFunctions.php');
	confirmSession();
	//pliki wymagane, jednak nie ma sensu podłaczać ich wielokrotnie
	require_once "../../config.php";
	require_once "../../scripts/utilityFunctions.php";
	
	if (!isset($_SESSION['limit']['categoryHandler']))
		$_SESSION['limit']['categoryHandler'] = 0;
	if (isset($_POST['startNum'])) 
		$_SESSION['limit']['categoryHandler'] = $_POST['startNum'];
		
	$keys = [];
	$limit = 50;
	$totalNews = 0;
	
	$sitesShow = false;
	
	if (isset($_GET['s'])) {
		if ($_GET['s'] == 1)
			$sitesShow = true;
	}
	
	function createActionButtons($hide = 0, $n = [], $icons = [], $actions = []) {
		$btn = createNodeButtons($hide, $n, $icons, [], $actions);
		$btnTxt = '';
		for ($i = 0; $i < count($btn); $i++) {
			$btnTxt .= "<div style=\"position: relative;\"";
			if (isset($btn[$i]['style']))
				$btnTxt .= " class=\"{$btn[$i]['style']}\"";
			if (is_array($btn[$i]['events'])) {
				$keys = array_keys($btn[$i]['events']);
				for ($j = 0; $j < count($keys); $j++) {
					$btnTxt .= " {$keys[$j]}='" . $btn[$i]['events'][$keys[$j]] . ";'";
				}
			}
			$btnTxt .= ">";
			if (isset($btn[$i]['icon'])) {
				$btnTxt .= "<p";
				if ($btn[$i]['name'])
					$btnTxt .= " data-tip='{$btn[$i]['name']}'";
				$btnTxt .= ">{$btn[$i]['icon']}</p>";
			}
			else 
				$btnTxt .= "<p>{$btn[$i]['name']}</p>";
			$btnTxt .= "</div>";
		}
		return $btnTxt;
	}
	
	function sitesCategoriesContent(&$base, &$keys,&$limit) {
// 		echo '<div class="settingsSetDiv">
// 			<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="sitesTableAddEdit(false, false, this);">
// 				<div style="float: left;"><p>Dodaj nową stronę</p></div>
// 				<div style="float: right; width: 170px; height: 100%; overflow:hidden; font-size: 15px;">' . 
// 				createActionButtons(true, ['Dodaj stronę'], ['<i class="fa fa-plus" aria-hidden="true"></i>'], ['onclick' => array('sitesTableAddEdit(false, false, this)')]) .'</div>
// 			</div>
// 		</div>';
		echo "<div>Filtruj<input type='search' name='filter' onchange=''/></div>";
		echo '<div id="sitesPanel">';
		$btnIcons = [0 => '<i class="fa fa-cog" aria-hidden="true"></i>', 1 => '<i class="fa fa-trash" aria-hidden="true"></i>'];
		$btnName = [0 => 'Edytuj stronę', 1 => 'Usuń (NA ZAWSZE)'];
		$btnActions = ['onclick' => array("event.stopPropagation(); sitesTableAddEdit(true, false, this)", 
// 						"event.stopPropagation(); showHidesites(false, this)", 
						"event.stopPropagation(); deleteListNode(this)") ];
		$file = readEntries('../../sites', 'categories.json');
		if (count($file) > $_SESSION['limit']['categoryHandler'] + 50) 
			$_SESSION['limit']['nextCategory'] = true;
		else
			$_SESSION['limit']['nextCategory'] = false;
		$fKeys = array_keys($file);
		for ($i = 0; $i < count($fKeys); $i++) {
			echo '<div class="settingsSetDiv">
				<div class="settingsBody" data-name="' . $fKeys[$i] . '">
					<div style="float: left;"><p>' . $fKeys[$i] . ' (kategoria)</p></div>
					<div class="buttonIconDiv" data-pos="0">' . 
					createActionButtons(false, $btnName, $btnIcons, $btnActions) .'</div>
				</div>';
				$subCat = array_keys($file[$fKeys[$i]]);
				for ($j = 0; $j < count($subCat); $j++) {
					echo '<div class="subclass">
						<div class="settingsBody" data-cat="' . $fKeys[$i] . '" data-name="' . $subCat[$j] . '" >
						<div style="float: left;"><p>' . ($subCat[$j] !== ' ' ? $subCat[$j] : '[brak_nazwy]') . ' (podkategoria)</p></div>
						<div class="buttonIconDiv" data-pos="1">' . 
						createActionButtons(false, $btnName, $btnIcons, $btnActions) .'</div>
						</div>';
					for ($k = 0; $k < count($file[$fKeys[$i]][$subCat[$j]]['buttons']); $k++) {
						echo '<div class="settingsBody btnname" data-cat="' . $fKeys[$i] . '" data-sub="' . $subCat[$j] . '" data-id="' . $k . '">
						<div style="float: left;"><p>' .$file[$fKeys[$i]][$subCat[$j]]["buttons"][$k]["name"] . ' (przycisk)</p></div>
						<div class="buttonIconDiv" data-pos="2">' . 
						createActionButtons(false, $btnName, $btnIcons, $btnActions) .'</div>
						</div>';
					}
					echo '</div>';
				}
			echo '</div>';
		}
		echo '</div>';
		if ($_SESSION['limit']['nextCategory']) {
			echo '<div class="settingsSetDiv">
			<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="getSitesTable(' . ($_SESSION['limit']['categoryHandler'] + $limit) . ', true);">
				<div style="float: left;"><p>Następne strony</p></div>				
			</div>';
		}
	}

	sitesCategoriesContent($base, $keys, $limit);
?>

