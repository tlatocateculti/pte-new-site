<?php 
	require_once('./utilityFunctions.php');
// 	require_once "../../scripts/database.php";
	require_once "../../config.php";
// 	require_once "../mailFunctions.php";
	require_once "../../scripts/utilityFunctions.php";
	require_once "../scripts/forms.php";
	//confirmSession();
	//$currentForm = 0;
	
	//funkcja tworzy pojedynczy dormularz dla edycji/dodawania wizyty;
	//$data - zbiór wartości dla kolejnych edytowanych pól
// 	function createNewsForm($data = '', $currentForm = 0, $roll = false) {
// 	//print_r($data);
// 		$date = new DateTime("now");
// 		$date->setTimezone(new DateTimezone("Europe/Warsaw"));	
// 		echo "<div name='visitForms' id='form-{$currentForm}' style='overflow: hidden;'>";
// 		//data dodaje się automatycznie
// 		//createTextInput("dateAdd-{$currentForm}", ($data === '' ? $date->format("Y/m/d H:i") : $data[0]['dateAdd'][$currentForm]), 'Data dodania', 'Format: YYYY/MM/DD HH:MM', 'afterBlock beforeBlock');
// 		echo createNewTextInput("dateExpired-{$currentForm}", ($data === '' ? '' : $data[0]['dateExpired'][$currentForm]), 'Data wygaśnięcia', 'Format: YYYY/MM/DD HH:MM', 'afterBlock beforeBlock', 'Można okreslić (opcjonalnie) datę wygaśnięcia wiadomości. Jeżeli takowa zostanie ustalona to po powyższym terminie wiadomość automatycznie będzie wyłączana (przestanie się wyświetlać).');
// // 		createTextInput("dateExpired-{$currentForm}", ($data === '' ? '' : $data[0]['dateExpired'][$currentForm]), 'Data wygaśnięcia (opcjonalnie)', 'Format: YYYY/MM/DD HH:MM', 'afterBlock beforeBlock');
// 		echo createNewTextInput("topic-{$currentForm}", ($data === '' ? '' : $data[0]['topic'][$currentForm]), 'Temat wiadomości', 'Wpisz temat wiadomości', 'afterBlock beforeBlock', 'Temat wiadomości jaki będzie dla niej wyświetlany. Należy pamiętać, że brak tematu jest domupszczalny jednak taka wiadomości może nie wyglądać atrakcyjnie.');
// // 		createTextInput("topic-{$currentForm}", ($data === '' ? '' : $data[0]['topic'][$currentForm]), 'Temat wiadomości', 'Wpisz temat wiadomości', 'afterBlock beforeBlock');
// 		echo createNewSelectInput("show-{$currentForm}", array(1 => 'Tak', 2 => 'Nie'), ($data === '' ? '' : $data[0]['show'][$currentForm]), 'Wiadomość jest pokazywana', '', 'afterBlock beforeBlock', 'Wiadomości, która ma nie być pokazywana będzie widoczna w panelu edycji wiadomości jednak nie ukaże sie na stronie');
// // 		createSelectInput("show-{$currentForm}", array(1 => 'Tak', 2 => 'Nie'), ($data === '' ? '' : $data[0]['show'][$currentForm]), 'Wiadomość jest pokazywana', '', 'afterBlock beforeBlock');
// 		echo createNewTextArea("summary-{$currentForm}", ($data === '' ? '' : $data[0]['summary'][$currentForm]), 'Skrót wiadomości', 'Wpisz skrót wiadomości (do 150 znaków)', 'afterBlock beforeBlock', 6, 100, 150, array('change' => 'summaryKeyUpEvent(this);'), 'To pole pozwala na ustawienie opisu skróconej informacji dotyczącej dodawanej/edytowanej wiadomości. Ustawinie go nie jest obligatoryjne (uzupełnia się samo w z informacji pełnej - pierwsze 150 znaków) jednak zawarta W nim informacja może mieć pozytywny (bądź negatywny) wpływ na pozycję w wyszukiwarkach internetowych.');
// // 		createTextArea("summary-{$currentForm}", ($data === '' ? '' : $data[0]['summary'][$currentForm]), 'Skrót wiadomości', 'Wpisz skrót wiadomości (do 150 znaków)', 'afterBlock beforeBlock', 6, 100, 150, array('change' => 'summaryKeyUpEvent(this);'));
// 		echo createNewTextAreaEditor("full_text-{$currentForm}", ($data === '' ? '' : $data[0]['full_text'][$currentForm]), 'Treść wiadomości', 'afterBlock beforeBlock', ''/* array('mouseover' => 'fullTextKeyUpEvent(this, ' . $currentForm . ');')*/, 'Ten edytor pozwala na edycję treści właściwej. Umożliwia dodawanie paragrafów, nagłówków, obrazów oraz innych elementów tekstu bez znajomości języka HTML. Prócz menu głównego (szare przyciski) każdyu z dodawanych elementów posiada edycję prawym przyciskiem myszy. Ponadto istnieje możliwość skopiowania i wklejenia danych np. z programu Word/Excel/LibreOffice. Rozwiązanuie kopiowania może jednak negatywnie odbić się na pozycjonowaniu strony (edytory te robią bałagan w kodzie HTML, a co za tym idzie sprawiają kłopot robotom przeszukującym sieć). Zawsze można jednak poprawić kod HTML (widoczny w drugim, dolnym oknie).');
// // 		createTextArea("full_text-{$currentForm}", ($data === '' ? '' : $data[0]['full_text'][$currentForm]), 'Treść wiadomości', 'Wpisz pełną treść wiadomości (65535 znaków)', 'afterBlock beforeBlock', 6, 100, '', array('keyup' => 'fullTextKeyUpEvent(this, ' . $currentForm . ');'));
// 
// // 		$warnings = '';
// 		if ($data !== '') {
// // 			if ($data[0]['show'][$currentForm] == 0)
// // 				$warnings .= 'TO OGŁOSZENIE NIE JEST WYŚWIETLANE!<br/>';
// // 			createLabelInfo("warningInfo-{$currentForm}", $warnings, 'UWAGI: ', 'expired beforeBlock afterBlock');
// 		
// 			createHiddenInput("news_id-{$currentForm}", $data[0]['news_id'][$currentForm]);
// 		}
// 		createHiddenInput("user_id-{$currentForm}", 1);
// 		createHiddenInput("dateAdd-{$currentForm}", ($data === '' ? $date->format("Y/m/d H:i") : $data[0]['dateAdd'][$currentForm]));
// 		echo "</div>";
// 	}

	function createNewsForm($data = '') {
		$date = new DateTime("now");
		$date->setTimezone(new DateTimezone("Europe/Warsaw"));	
		echo "<div id='siteForm' style='overflow: hidden;'>";
		//createTextInput("addDate", ($data === '' ? $date->format("Y/m/d H:i") : $data['addDate'][0]), 'Data dodania', 'Format: YYYY/MM/DD HH:MM', 'afterBlock beforeBlock');
		echo createNewTextAreaEditor("text", ((!isset($data['text'])) ? '' : (isset($data['b64']) ? rawurldecode(urldecode(base64_decode($data['text']))) : $data['text'])), 'Treść strony', 'afterBlock beforeBlock', '', 'Ten edytor pozwala na edycję treści właściwej. Umożliwia dodawanie paragrafów, nagłówków, obrazów oraz innych elementów tekstu bez znajomości języka HTML. Prócz menu głównego (szare przyciski) każdyu z dodawanych elementów posiada edycję prawym przyciskiem myszy. Ponadto istnieje możliwość skopiowania i wklejenia danych np. z programu Word/Excel/LibreOffice. Rozwiązanuie kopiowania może jednak negatywnie odbić się na pozycjonowaniu strony (edytory te robią bałagan w kodzie HTML, a co za tym idzie sprawiają kłopot robotom przeszukującym sieć). Zawsze można jednak poprawić kod HTML (widoczny w drugim, dolnym oknie).');
		//createButtonPickUp('Otwórz galerię','Wybierz zdjęcie do dodania', array('click' => "openSiteGellaryWindow();"));
		
		
		echo '<div class="addSiteMiniGallery">
			<div class="addSiteMiniGalleryContent" ' . (isset($data['gallery']) ? ('style="width: ' . (320 + ( 315 * count($data['gallery']))) . 'px;"') : '' ) . '>';
		if (isset($data['gallery'])) {
			for ($i = 0; $i < count($data['gallery']); $i++) {
				echo '<div onclick="openSiteGellaryWindow(this);" class="addSiteMiniPhoto" data-mini="' . $data['gallery'][$i]['mini'] . '" data-full="' . $data['gallery'][$i]['full'] . '" data-id="' . $data['gallery'][$i]['id'] . '">
					<img src="' . $data['gallery'][$i]['mini'] . '"/>
					<i onclick="removeMiniSite(event);" class="fa fa-times-circle-o deleteMini" aria-hidden="true"></i>
				</div>';
			}
		}
		echo '		<div onclick="openSiteGellaryWindow(this);" class="addSiteMiniPhoto">
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					<p style="">DODAJ MINIATURĘ ZDJĘCIA</p>';
					//<!--<i onclick="removeMiniSite(event);" class="fa fa-times-circle-o deleteMini" aria-hidden="true"></i>-->
		echo '		</div>
			</div>
			<!--<div class="addSiteMiniGalleryScroll" onmousemove="getMouseScrollMove(event);" onmousedown="getMouseScrollAction(false, event);" onmouseup="getMouseScrollAction(true, event);"><div></div></div>-->
		</div>';
		echo createNewTextInput("newsHeader", (!isset($data['newsHeader']) ? '' : $data['newsHeader']), 'Nagłówek wiadomości', 'Krótki i treściwy (celem dobrego wyświetlania na kaflu)', 'afterBlock beforeBlock', 'Opis strony. Wyświetlany jest w tytule strony na pasku przeglądarki. Dobry opis znaczni wpływa na indeksowanie stron.');
		echo createNewTextInput("shortText", (!isset($data['shortText']) ? '' : $data['shortText']), 'Streszczenie', 'Maksymalnie 50/60 znaków. Reszta tekstu po kliknięciu (w przypadku braku tekstu wyświetli się tylko nagłówek z datą)', 'afterBlock beforeBlock', 'Opis strony. Wyświetlany jest w tytule strony na pasku przeglądarki. Dobry opis znaczni wpływa na indeksowanie stron.');
		echo createNewTextInput("description", (!isset($data['description']) ? '' : $data['description']), 'Komentarz wiadomości', 'Wprowadzony tekst będzie widoczny jedynie w panelu administracyjnym!', 'afterBlock beforeBlock', 'Opis strony. Wyświetlany jest w tytule strony na pasku przeglądarki. Dobry opis znaczni wpływa na indeksowanie stron.');
		echo createNewSelectInput("active", array(1 => 'Tak', 0 => 'Nie'), (!isset($data['active']) ? '' : $data['active']), 'Wiadomość aktywna', '', 'afterBlock beforeBlock', 'Strona może zostać czasowo wyłączona. Jeżeli zostanie wybrana opcja Nie to strona będzie widoczna jedynie w panelu administracyjnym, nie pojawi się ona natomiast na stronie dla użytkowników. Dzięki temu można posiadać kilka stron WWW z różną treścią, które można włączać/wyłączać w dogodnym momencie.');
		createHiddenInput("user_id", 1);
		createHiddenInput("addDate", (!isset($data['addDate']) ? $date->format("Y-m-d H:i") : $data['addDate']));
		if (isset($data['site_id'])) 
			createHiddenInput("site_id", $data['site_id']);
		echo "</div>";
	}
	
	function createSiteForm($data = '') {
		$date = new DateTime("now");
		$date->setTimezone(new DateTimezone("Europe/Warsaw"));	
		echo "<div id='siteForm' style='overflow: hidden;'>";
		//createTextInput("addDate", ($data === '' ? $date->format("Y/m/d H:i") : $data['addDate'][0]), 'Data dodania', 'Format: YYYY/MM/DD HH:MM', 'afterBlock beforeBlock');
		echo createNewTextAreaEditor("text", ($data === '' ? '' : (isset($data['b64']) ? rawurldecode(urldecode(base64_decode($data['text']))) : $data['text'])), 'Treść strony', 'afterBlock beforeBlock', '', 'Ten edytor pozwala na edycję treści właściwej. Umożliwia dodawanie paragrafów, nagłówków, obrazów oraz innych elementów tekstu bez znajomości języka HTML. Prócz menu głównego (szare przyciski) każdyu z dodawanych elementów posiada edycję prawym przyciskiem myszy. Ponadto istnieje możliwość skopiowania i wklejenia danych np. z programu Word/Excel/LibreOffice. Rozwiązanuie kopiowania może jednak negatywnie odbić się na pozycjonowaniu strony (edytory te robią bałagan w kodzie HTML, a co za tym idzie sprawiają kłopot robotom przeszukującym sieć). Zawsze można jednak poprawić kod HTML (widoczny w drugim, dolnym oknie).');
		//createButtonPickUp('Otwórz galerię','Wybierz zdjęcie do dodania', array('click' => "openSiteGellaryWindow();"));
		

		//echo createNewSelectInput("subsites", array(0 => 'Nie', 1 => 'Tak'), (!isset($data['siteBelong']) ? '' : ((strpos($data['siteBelong'], 'sub_')  === 0) ? 1 : 0)), 'Podstrona z kategorii', ['onchange' => 'siteChangeSiteBelong(this);'], 'afterBlock beforeBlock', 'Jeżeli zaznaczone zostanie tak strona zostanie dodana do podstron kategorii. Stronę taką można dodawać jako odnośnik np. w kategoriach.');
		//echo createNewEditSelectInput("mainbtncategory", (!isset($data['mainbtncategory']) ? '' : $data['mainbtncategory']), (!isset($data['mainbtncategoryID']) ? '' : $data['mainbtncategoryID']), 'Główna kategoria przycisku', 'Kategoria dla strony', 'afterBlock beforeBlock', 'Główna kategoria oferty. Do niej można podłączać kategorie produktów.');
		//echo createNewEditSelectInput("btncategory", (!isset($data['btncategory']) ? '' : $data['btncategory']), (!isset($data['btncategoryID']) ? '' : $data['btncategoryID']), 'Kategoria przycisku', 'Kategoria dla strony', 'afterBlock beforeBlock', 'Kategoria menu, w której wyświetlana jest podstrona.');
		//echo createNewTextInput("btnname", (!isset($data['btnname']) ? '' : $data['btnname']), 'Etykieta przycisku', 'Nazwa przycisku prowadzącego do strony', 'afterBlock beforeBlock', 'Podany tekst zostanie wstawiony do przycisku prowadzącym do tej strony');
		echo createNewTextInput("header", (!isset($data['header']) ? '' : $data['header']), 'Nagłówek podstrony', 'Wprowadzony tekst będzie widoczny jedynie w panelu administracyjnym!', 'afterBlock beforeBlock', 'Opis strony. Wyświetlany jest w tytule strony na pasku przeglądarki. Dobry opis znaczni wpływa na indeksowanie stron.');
		echo createNewTextInput("description", (!isset($data['description']) ? '' : $data['description']), 'Komentarz strony', 'Wprowadzony tekst będzie widoczny jedynie w panelu administracyjnym!', 'afterBlock beforeBlock', 'Opis strony. Wyświetlany jest w tytule strony na pasku przeglądarki. Dobry opis znaczni wpływa na indeksowanie stron.');
		if (!isset($data['siteBelong']))
			echo createNewTextInput("siteBelong", (!isset($data['siteBelong']) ? 'index' : $data['siteBelong']), 'Adres strony', 'Nazwa strony', 'afterBlock beforeBlock', 'Podana tutaj nazwa identyfikuje stronę, do której zostanie dowiązana edytowana treść. Identyfikator strony można łątwo rozpoznać - wyświetla się po znaku zapytania w pasku adresu strony WWW (np. m_contact, m_main itd.). Dodanie stron z innym identyfikatorem nie skutkuje żadnymi konsekwencjami poza tym, że takowa strona po prostu się nie wyświetli.');
		else {
			createHiddenInput("siteBelong", $data['siteBelong']);
		}
		//ponizsze powinno byc mozliwe przez dodawanie w panelu administracyjnym
		$val = ['default' => (!isset($data['siteCategory']) ? 'main' : $data['siteCategory']), 'Główna' => "main", 'Archiwum' => "archive", 'Kursy' => "courses", 'Materiały' => "materials"];
		echo createNewRadioButton("siteCategory", $val, 'Kategoria strony', 'W której kategoria ma być wyświetlana strona', 'afterBlock beforeBlock', 'Domyślnie treści dodawane są do strony głównej. Strony można przenosić do archiwum (osobna strona), kursów bądź materiałów (przeważnie odnośniki do dodanych materiałów)');
		//echo createNewSelectInput("active", array(1 => 'Tak', 0 => 'Nie'), (!isset($data['active']) ? '' : $data['active']), 'Strona aktualnie w użyciu', '', 'afterBlock beforeBlock', 'Strona może zostać czasowo wyłączona. Jeżeli zostanie wybrana opcja Nie to strona będzie widoczna jedynie w panelu administracyjnym, nie pojawi się ona natomiast na stronie dla użytkowników. Dzięki temu można posiadać kilka stron WWW z różną treścią, które można włączać/wyłączać w dogodnym momencie.');
		createHiddenInput("user_id", 1);
		createHiddenInput("addDate", (!isset($data['addDate']) ? $date->format("Y/m/d H:i") : $data['addDate']));
		if (isset($data['site_id'])) 
			createHiddenInput("site_id", $data['site_id']);
		createHiddenInput("active", "1");
		echo "</div>";
	}
	
	function indexOf($table, $search) {
		$maxCount = count($table);
		$max = (((int) ($maxCount--/2)) + 1);
		for ($i = 0; $i < $max; $i++) {
			if ($table[$i] == $search)
				return $i;
			if ($table[$maxCount - $i] == $search)
				return $maxCount - $i;
		}
		return -1;
	}
	
	//$i = 0;
	$siteID = @explode(',', $_POST['fields'])[0];
	
	
// 			$cats = readEntries('../../json', 'categories.json');
// 			$catsCat = array_keys($cats);
// 			$btnNameExist = false;
// 			$catNoNameExists = false;
// 			//print_r($cats);
// 			for ($i = 0; $i < count($catsCat); $i++) {
// 				$files['mainbtncategory'][$i] = $catsCat[$i];
// 				$subCats = array_keys($cats[$catsCat[$i]]);
// 				for ($j = 0; $j < count($subCats); $j++) {
// 					$files['btncategory'][@count($files['btncategory'])] = $subCats[$j];
// 					if ($subCats[$j] === '[brak kategorii]') $catNoNameExists = true;
// 					if (!$btnNameExist) {
// 						for ($k = 0; $k < count($cats[$catsCat[$i]][$subCats[$j]]['buttons']); $k++) {
// 							if (isset($cats[$catsCat[$i]][$subCats[$j]]['buttons'][$k]['site']))
// 								if ($cats[$catsCat[$i]][$subCats[$j]]['buttons'][$k]['site'] === $siteID) {
// 									$files['btnname'] = $cats[$catsCat[$i]][$subCats[$j]]['buttons'][$k]['name'];
// 									$files['btncategoryID'] = count($files['btncategory']) - 1;
// 									$files['mainbtncategoryID'] = $i;
// 									$btnNameExist = true;
// 									break;
// 								}
// 						}
// 					}
// 				}
// 			}
// 			if (!$catNoNameExists) $files['btncategory'][count($files['btncategory'])] = '[brak kategorii]';

	if (isset($_GET['s'])) {
		$files = readEntries('../../json', 'sites.json', '', $siteID);
		$files['site_id'] = $siteID;
		createSiteForm($files);
	}
	else {
		$files = readEntries('../../json', 'news.json', '', $siteID);
		$files['site_id'] = $siteID;
		createNewsForm($files);
	}

?>

<style>
	.afterBlock:after, .beforeBlock:before {
		content: '';
		overflow: auto;
		display: table;
		clear: both;
	}
	.afterBlock p input ~ span {
		float: right;
	}
	.selectList {
		overflow: hidden;
	}
	.expired {
		color: red;
	}
	#galleryWindow {
        display: none;
        position: absolute;
        background: white;
        border: 1px solid black;
    }
    #galleriesDiv {
        overflow-y: auto;
        height: 90%;
       /* position: relative;*/
    }
    
    
    	table {
		position: relative;
		width: 100%;
		overflow: hidden;
		table-layout: fixed;
	}
	th,td {
		width: 100px;
		overflow: hidden;
		word-break: break-all;
	}
	th:first-child {
		width: 30px;
	}
	th:last-child {
		width: 60px;
	}
	#content {
		overflow-x: visible;
	}
	.gallerySetDiv {
		border: 1px solid black;
		overflow: hidden;
		width: 95%;
		height: 40px;
	}
	.gallerySetName {
		font-size: 20px;
		font-weight: bold;
		text-align: center;
		background: gray;
		margin: 0;
		padding-top: 10px;
		height: 40px;
		cursor: pointer;
	}
	
	.photosSet {
		overflow: hidden;
		overflow-y: auto;
		height: 420px;
	}
	
	.photosSet:after {
		content: '';
		clear: both;
		display: table;
	}
	
	.photoMiniSet {
		width: 300px;
		height: 200px;
		float: left;

	}
	.actionButtons {
        display: none;
	}
</style>

<button onclick="<?php if (!isset($_GET['s'])) echo 'saveNews();'; else echo 'saveSite();'; ?>">
<?php if (isset($_POST['fields'])) echo 'Zapisz zmiany'; else {
	if (!isset($_GET['s'])) 
		echo 'Dodaj wiadomość'; 
	else
		echo 'Dodaj stronę';
	}?></button>
<button onclick="<?php if (!isset($_GET['s'])) echo 'getSitesTable(0, true);'; else echo 'getSitesTable();'; ?>">Anuluj</button>

<div id='galleryWindow'>
	<link rel="stylesheet" href="./css/gallery.css"/>
	<style>
		.buttonIconDiv {display: none;}
		#galleryAddNew {display: none;}
	</style>
	<div id='galleriesDiv'></div>
	<button id="addButtonGalleryWindow" onclick='addPhotoToText(this);'>Dodaj obraz</button>
	<button onclick='closeWindow(this);'>Anuluj</button>
</div>
