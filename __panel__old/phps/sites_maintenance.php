<?php
	require_once('./utilityFunctions.php');
	//confirmSession();
	//pliki wymagane, jednak nie ma sensu podłaczać ich wielokrotnie
	require_once "../../config.php";
	require_once "../../scripts/utilityFunctions.php";
	
	if (!isset($_SESSION['limit']['siteHandler']))
		$_SESSION['limit']['siteHandler'] = 0;
	if (isset($_POST['startNum'])) 
		$_SESSION['limit']['siteHandler'] = $_POST['startNum'];
		
	$keys = [];
	$limit = 50;
	$totalNews = 0;
	
	$sitesShow = false;
	
	if (isset($_GET['s'])) {
		if ($_GET['s'] == 1)
			$sitesShow = true;
	}
	
	function createActionButtons($hide = 0, $n = [], $icons = [], $actions = []) {
		$btn = createNodeButtons($hide, $n, $icons, [], $actions);
		$btnTxt = '';
		for ($i = 0; $i < count($btn); $i++) {
			$btnTxt .= "<div style=\"position: relative;\"";
			if (isset($btn[$i]['style']))
				$btnTxt .= " class=\"{$btn[$i]['style']}\"";
			if (is_array($btn[$i]['events'])) {
				$keys = array_keys($btn[$i]['events']);
				for ($j = 0; $j < count($keys); $j++) {
					$btnTxt .= " {$keys[$j]}='" . $btn[$i]['events'][$keys[$j]] . ";'";
				}
			}
			$btnTxt .= ">";
			if (isset($btn[$i]['icon'])) {
				$btnTxt .= "<p";
				if ($btn[$i]['name'])
					$btnTxt .= " data-tip='{$btn[$i]['name']}'";
				$btnTxt .= ">{$btn[$i]['icon']}</p>";
			}
			else 
				$btnTxt .= "<p>{$btn[$i]['name']}</p>";
			$btnTxt .= "</div>";
		}
		return $btnTxt;
	}
	
	function newsCreateTableContent(&$base, &$limit) {
		echo '<div class="settingsSetDiv">
			<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="sitesTableAddEdit(false, true, this);">
				<div style="float: left;"><p>Dodaj nową wiadomość</p></div>
				<div style="float: right; width: 170px; height: 100%; overflow:hidden; font-size: 15px;">' . 
				createActionButtons(false, ['Dodaj wiadomość'], ['<i class="fa fa-plus" aria-hidden="true"></i>'], ['onclick' => array('sitesTableAddEdit(false, true, this)')]) .'</div>
			</div>
		</div>';
		echo "<div>Filtruj<input type='search' name='filter' onchange=''/></div>";
		echo '<div id="newsPanel">';
		$btnIcons = [0 => '<i class="fa fa-cog" aria-hidden="true"></i>', 2 => '<i class="fa fa-trash" aria-hidden="true"></i>'];
		$btnName = [0 => 'Edytuj wiadomość', 2 => 'Usuń wiadomość (NA ZAWSZE)'];
		$btnActions = ['onclick' => array("event.stopPropagation(); sitesTableAddEdit(true, true, this)", 
						"event.stopPropagation(); showHidesites(true, this)", 
						"event.stopPropagation(); sitesTableDelete(true, this)") ];
					
		//$file = readEntries('../../json', 'news.json');					
		$file = readEntries('../json', 'news.json');
// 		echo "<pre>";
// 		print_r($file[0]['active']);
// 		echo "</pre>";
		if (count($file) > $_SESSION['limit']['siteHandler'] + 50) 
			$_SESSION['limit']['nextSite'] = true;
		else
			$_SESSION['limit']['nextSite'] = false;
		$fKeys = @array_keys($file);
		for ($i = 0; $i < count($fKeys); $i++) {
			if ($file[$fKeys[$i]]['active'] == 1) {
				$btnIcons[1] = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
				$btnName[1] = 'Ukryj wiadomość';
			}
			else {
				$btnIcons[1] = '<i class="fa fa-eye" aria-hidden="true"></i>';
				$btnName[1] = 'Pokaż wiadomość';
			}
			echo '<div class="settingsSetDiv">
				<div class="settingsBody" data-description="' . $file[$fKeys[$i]]['description'] . '" data-active="' . $file[$fKeys[$i]]['active'] . '" data-id="' . $fKeys[$i] . '" data-adddate="' . $file[$fKeys[$i]]['addDate'] . '" onclick="sitesTableAddEdit(true, true, this);">
					<div style="float: left;"><p>' . ($file[$fKeys[$i]]['description'] !== '' ? $file[$fKeys[$i]]['description'] : '{brak opisu}')  . ($file[$fKeys[$i]]['active'] == 1 ? ' (aktywna)' : ' (nieaktwyna)') . '</p></div>
					<div class="buttonIconDiv">' . 
					createActionButtons($file[$fKeys[$i]]['active'], $btnName, $btnIcons, $btnActions) .'</div>
					<div style="display: none;">' . (isset($file[$fKeys[$i]]['b64']) ? rawurldecode(urldecode(base64_decode($file[$fKeys[$i]]['text']))) : $file[$fKeys[$i]]['text']) . '</div>
				</div>
			</div>';
		}
// 		for ($i = 0; $i < count($base->getResults(0)['news_id']); $i++) {
// 			if ($base->getResults(0)['show'][$i] == 1) {
// 				$btnIcons[1] = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
// 				$btnName[1] = 'Ukryj wiadomość';
// 			}
// 			else {
// 				$btnIcons[1] = '<i class="fa fa-eye" aria-hidden="true"></i>';
// 				$btnName[1] = 'Pokaż wiadomość';
// 			}
// 			echo '<div class="settingsSetDiv">
// 				<div class="settingsBody" data-summary="' . $base->getResults(0)['summary'][$i] . '" data-show="' . $base->getResults(0)['show'][$i] . '" data-userid="' . $base->getResults(0)['user_id'][$i] . '" data-id="' . $base->getResults(0)['news_id'][$i] . '" data-dateadd="' . $base->getResults(0)['dateAdd'][$i] . '" data-dateexp="' . $base->getResults(0)['dateExpired'][$i] . '" onclick="sitesTableAddEdit(true, true, this);">
// 					<div style="float: left;"><p>' . ($base->getResults(0)['topic'][$i] !== '' ? $base->getResults(0)['topic'][$i] : '{brak tematu}')  . ($base->getResults(0)['show'][$i] == 1 ? ' (aktywna)' : ' (nieaktwyna)') . '</p></div>
// 					<div class="buttonIconDiv">' . 
// 					createActionButtons($base->getResults(0)['show'][$i], $btnName, $btnIcons, $btnActions) .'</div>
// 					<div style="display: none;">' . $base->getResults(0)['full_text'][$i] . '</div>
// 				</div>
// 			</div>';
// 		}
		echo '</div>';
		if ($_SESSION['limit']['nextSite']) {
// 		<div style="float: right; width: 170px; height: 100%; overflow:hidden; font-size: 15px;">' . 
// 				createActionButtons(false, ['Dodaj wiadomość'], ['<i class="fa fa-plus" aria-hidden="true"></i>'], ['onclick' => array('getSitesTable(' . ($_SESSION['limit']['siteHandler'] + $limit) . ', true)')]) .'</div>
// 			</div>
			echo '<div class="settingsSetDiv">
			<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="getSitesTable(' . ($_SESSION['limit']['siteHandler'] + $limit) . ', true);">
				<div style="float: left;"><p>Następne wiadomości</p></div>				
			</div>';
		}
	}
	
// 	function sitesDatabaseMaintenance(&$base, &$keys, &$totalNews) {
// 		//setExpiredDate($base);
// 		if (isset($_POST['deleteId'])) {
// 			$visitDelId = explode(',', $_POST['deleteId']);
// // 			$base->buildConditionQuery(array('sites', 'site_id'), $visitDelId, DataEnum::IN);
// // 			$base->delete('sites', $base->getConditions());
// // 			//$base->update('visits', array('deleted' => 1), $base->getConditions());
// // 			$base->flushConditions();
// 		}
// // 		$totalNews = $base->countQuery(array('sites' => array('id')));
// // 		if(isset($_SESSION['limit']['siteHandler'])) {
// // 			if($_SESSION['limit']['siteHandler'] == -1)
// // 				$_SESSION['limit']['siteHandler'] = $totalNews - 50;	
// // 		}
// // 		else {
// // 			$_SESSION['limit']['siteHandler'] = 0;
// // 		}
// // 		if ($totalNews > $_SESSION['limit']['siteHandler'] + 50) 
// // 			$_SESSION['limit']['nextSite'] = true;
// // 		else
// // 			$_SESSION['limit']['nextSite'] = false;
// // 		$base->queryTable(array('sites' => array('*')), "", 50, $_SESSION['limit']['siteHandler']);
// // // 		$base->queryTable(array('sites' => array('addDate', 'description', 'siteBelong', 'active', 'user_id', 'site_id')), "", 50, $_SESSION['limit']['siteHandler']);
// // 		//$_SESSION['limit']['siteHandler'] += 10;
// // 		if ($base->getResults(0) == -1)
// // 			return;
// // 		
// // 		$keys = array_keys($base->getResults(0));
// 	}
	
// 	function sitesCreateTableHeader() {
// 		echo "<th>LP</th>\r\n";
// 		echo "<th>Data dodania</th>\r\n";
// 		echo "<th style='width:250px;'>Komentarz</th>\r\n"; 
// 		echo "<th>Strona docelowa</th>\r\n";
// 		echo "<th>Zaznacz</th>\r\n";
// 	}
	
	function sitesCreateTableContent(&$base, &$keys,&$limit) {
		echo '<div class="settingsSetDiv">
			<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="sitesTableAddEdit(false, false, this);">
				<div style="float: left;"><p>Dodaj nową stronę</p></div>
				<div style="float: right; width: 170px; height: 100%; overflow:hidden; font-size: 15px;">' . 
				createActionButtons(false, ['Dodaj stronę'], ['<i class="fa fa-plus" aria-hidden="true"></i>'], ['onclick' => array('sitesTableAddEdit(false, false, this)')]) .'</div>
			</div>
		</div>';
		echo "<div>Filtruj<input type='search' name='filter' onchange=''/></div>";
		echo '<div id="sitesPanel">';
		$btnIcons = [0 => '<i class="fa fa-cog" aria-hidden="true"></i>', 2 => '<i class="fa fa-trash" aria-hidden="true"></i>'];
		$btnName = [0 => 'Edytuj stronę', 2 => 'Usuń stronę (NA ZAWSZE)'];
		$btnActions = ['onclick' => array("event.stopPropagation(); sitesTableAddEdit(true, false, this)", 
						"event.stopPropagation(); showHidesites(false, this)", 
						"event.stopPropagation(); sitesTableDelete(false, this)") ];

		//$files = getJSONFiles('../../json/sites');
		$file = [];
		if (count($files) === 0) {
			$file = readEntries('../../json', 'sites.json');
// 			if (count($file) > $_SESSION['limit']['siteHandler'] + 50) 
// 				$_SESSION['limit']['nextSite'] = true;
// 			else
// 				$_SESSION['limit']['nextSite'] = false;
		}
		else {
// 			if (count($file) > $_SESSION['limit']['siteHandler'] + 50) 
// 				$_SESSION['limit']['nextSite'] = true;
// 			else
// 				$_SESSION['limit']['nextSite'] = false;
			foreach($files as $fJSON) {
				$readJSON = readEntries('../../json/sites', $fJSON);
				$cName = array_keys($readJSON)[0];
				$file[$cName] = $readJSON[$cName];
			}
		}
		$fKeys = array_keys($file);
		for ($i = 0; $i < count($fKeys); $i++) {
			if ($file[$fKeys[$i]]['active'] == 1) {
				$btnIcons[1] = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
				$btnName[1] = 'Ukryj stronę';
			}
			else {
				$btnIcons[1] = '<i class="fa fa-eye" aria-hidden="true"></i>';
				$btnName[1] = 'Pokaż stronę';
			}
			echo '<div class="settingsSetDiv">
				<div class="settingsBody" data-description="' . $file[$fKeys[$i]]['description'] . '" data-active="' . $file[$fKeys[$i]]['active'] . '" data-sitebelong="' . $file[$fKeys[$i]]['siteBelong'] . '" data-id="' . $fKeys[$i] . '" data-adddate="' . $file[$fKeys[$i]]['addDate'] . '" onclick="sitesTableAddEdit(true, false, this);">
					<div style="float: left;"><p>' .
					        (!isset($file[$fKeys[$i]]['siteCategory']) ? '[Główna] ' : '[' . $file[$fKeys[$i]]['siteCategory'] . '] ') .
						($file[$fKeys[$i]]['description'] !== '' ? $file[$fKeys[$i]]['description'] : '{brak opisu}')  . 
						($file[$fKeys[$i]]['active'] == 1 ? ' (aktywna)' : ' (nieaktwyna)') . '</p></div>
					<div class="buttonIconDiv">' . 
					createActionButtons($file[$fKeys[$i]]['active'], $btnName, $btnIcons, $btnActions) .'</div>
					<div style="display: none;">' . (isset($file[$fKeys[$i]]['b64']) ? rawurldecode(urldecode(base64_decode($file[$fKeys[$i]]['text']))) : $file[$fKeys[$i]]['text']) . '</div>
				</div>
			</div>';
		}
		echo '</div>';
// 		if ($_SESSION['limit']['nextSite']) {
// 			echo '<div class="settingsSetDiv">
// 			<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="getSitesTable(' . ($_SESSION['limit']['siteHandler'] + $limit) . ', true);">
// 				<div style="float: left;"><p>Następne strony</p></div>				
// 			</div>';
// 		}
	}
//ROZPOCZYNA SIĘ GENEROWANIE STRONY WWW
//nieaktywne i prawdopodobnie do wyrzucenia - po co czytać plik kilkukrotnie (to nie baza danych)
// 	if (!$sitesShow) 
// 		newsDatabaseMaintenance($base, $totalNews);
// 	else
// 		sitesDatabaseMaintenance($base, $keys, $totalNews);
?>
<?php
	if (!$sitesShow) 
		newsCreateTableContent($base, $keys, $limit);
	else
		sitesCreateTableContent($base, $keys, $limit);
?>

