<?php 
	require_once('./utilityFunctions.php');
	require_once "../../scripts/utilityFunctions.php";
// 	require_once "../../scripts/database.php";
	require_once "../../config.php";
	//confirmSession();
?>
<?php
	echo "<p>Niniejsza część konfiguracji jest KRYTYCZNA dla strony! Jakikolwiek błąd spowoduje, że nie będzie można podłączyć
	się do bazy danych (przestanie działać rejestracja wizyt, katalog pacjentów oraz przychodni) oraz nikt nie będzie mógł 
	przesyłać do nas swojej poczty zarówno przez panel, jak i z potwierdzeniem wizyt. Dlatego usilnie zaleca się nie zmieniać tych ustawie
	chyba, że jest się świadomym ryzyka!</p>
	<p>Ponieważ zapisane tutaj dane nie powinny wpaść w niepowołane ręce (np. domorosłego włamywacza) zawartość tej tabeli danych
	nie jest przesyłana. Strona pozwala na trzymanie kilku konfiguracji połączeń jednak w danej chwili może być aktywna tylko jedna 
	taka konfiguracja. Każda konfiguracja ma swoją nazwę (i ta jest przesyłana). Z menu można wybrać pożądaną konfigurację i ją
	aktywować bądź usunąć. Usunięcie powoduje trwałe wyrzucenie konfiguracji - nie będzie można z niej korzystać w przyszłości!</p>
	<p>Podane w formularzu dane zostaną przesłane do bazy jako nowe bądź jako edycja już istniejącej konfiguracji (przy wybraniu 
	odpowiedniej opcji). </p>";
	
	
?>
	<style>
	.afterBlock:after, .beforeBlock:before {
		content: '';
		overflow: auto;
		display: table;
		clear: both;
	}
	</style>
	<link rel="stylesheet" href="./css/elements.css"/>
	<div id='settingsCommunicationDiv'></div>
		<div class="settingsSetDiv" style="height: 40px;">
			<div class="headerSetName" style="color: white; font-weight: 900;" onclick="toggleSettingsDiv(this);">
				<p>Ustawienia dostępu do bazy danych i przesyłania poczty</p>
			</div>
			<div class="sBody">
				<?php
// 					$base = new Database();
// 					if (@$base->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME)) {
// 					
// 						echo "<div id='settingsBaseList'>";
// 						echo "<div><span>Lista aktualnych ustawień konfiguracyjnych</span><select onchange='' name='setList'>";
// 						
// 						$base->queryTable(array('site_config' => array('configName', 'id', 'active')), '', -1);
// 						if ($base->getResults(0) != -1) {
// 							for ($i = 0; $i < count($base->getResults(0)['id']); $i++) {
// 								echo "<option value='" . $base->getResults(0)['id'][$i] . "'>" . $base->getResults(0)['configName'][$i] . ($base->getResults(0)['active'][$i] == 1 ? " (*)" : "") . "</option>";
// 							}
// 						}
// 						echo "</select><button onclick='selectSettings();'>Wybierz konfigurację</button><button onclick='deleteSettings();'>Skasuj konfigurację</button><button onclick=''>Pokaż konfigurację</button>";
// 						echo '<i class="fa fa-question-circle" aria-hidden="true" data-text="Lista pozwala na przełączanie się pomiędzy dostępnymi na serwerze konfiguracjami bazy danych i/lub serwera pocztowego wykorzystywanego przez formularz.
// Gwiazką oznaczona jest aktualnie wykorzystywana konfiguracja. Konfiguracje można zmieniać poprzez poniższy formularz - wystarczy wpisać dokładnie taką samą nazwę konfiguracji jaka widnieje na liście i zaznaczyć opcję `Zastąp konfigurację na liście powyżej` Bez zaznaczenia zostanie dodana nowa konfiguracja o identycznej nazwie (konfiguracje są unikatowe względem identyfikatorów wewnętrznych, nazwy mogą się powtarzać).
// Wybranie nowej konfiguracji sprowadza się do wyboru odpiwiedniej nazwy i wybrania przycisku `Wybierz konfigurację`. W analogiczny spsób dokonuje się usunięcia konfiguracji. Zapisywanie i zarządzanie konfigurcjami może być przydatne np. w przypadku awarii serwera główengo i konieczności skorzystania z serwera zapasowego bądź chwilowej zmian uprawnień do baz danych.
// Domyślnie konfiguracja nie jest przesyłana (bezpieczeństwo). By uzyskać przechowywane informacje należy wybrać przycisk `Pokaż konfigurację` i dokonać potwierdzenia uprawnień." onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div></div>';
// 						$base->flushResults();
// 					}
				?>
				<div class='afterBlock beforeBlock'><span>Nazwa użytkownika bazy danych</span><input type='text' name='baseUser'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Użytkownik posiadający uprawnienia do bazy danych dostępnej na serwerze" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Hasło użytkownika bazy danych</span><input type='password' name='basePass'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Hasło dla powyższego użytkownika" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Nazwa bazy danych</span><input type='text' name='baseName'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Nazwa wykorzystywanej bazy danych (nie adres serwera!)" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Nazwa użytkownika serwera pocztowego</span><input type='text' name='siteLogin'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Użytkownik poczty elektornicznej dostępnej na uprawnionym serwerze. Nazwą może być nazwa użtykownika, może być też adres poczty elektronicznej (zależne od dostawcy)" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Hasło użytkownika serwera pocztowego</span><input type='password' name='sitePass'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Hasło dla powyższego użtywkonika" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Nazwa serwera pocztowego</span><input type='text' name='siteServer'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Adres serwera posiadający uprawnienia do przesyłania wiadomości pocztowych. Przeważnie w tym polu podaje się adres SMTP, który należy podać w takiej postaci, w jakie podej go dostawda (np. mail.poczta.pl, ssl://bezpieczna.poczta.pl itd)" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Port serwera pocztowego</span><input type='text' name='sitePort'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Port serwera poczty elektronicznej. Przeważnie poczta autoryzwona działą na porcie 465 (taki zostaje domyślnie przyjęty w przypadku nie wypełnienia tego pola)." onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Nazwa konfiguracji</span><input type='text' name='configName'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Nazwa, pod jaką zostanie zapisana konfiguracja w bazie danych. Należy pamiętać, że konfiguracja będzie domyślnie wykorzsytana!" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Zastąp konfigurację wybraną na liście powyżej</span><input type='checkbox' name='reconfig'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Jeżeli w bazie istnieje konfiguracja o nazwie podanej powyżej to zostanie ona zastąpiona" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><button onclick='appendSettings();'>Zatwierdź</button><button onclick="resetSettings('settingsForm');">Wyczyść pola</button></div>
			</div>
		</div>
	
		<div class="settingsSetDiv" style="height: 40px;">
			<div class="headerSetName" style="color: white; font-weight: 900;" onclick="toggleSettingsDiv(this);">
				<p>Ustawienia adresu zwrotnego dla formularzy</p>
			</div>
			<div class="sBody">
				<?php 
				$email = getSettingEmail();
				if ($email != false)
					$email = explode('||', $email);
				?>
				<div class='afterBlock beforeBlock'><span>Adres e-poczty</span><input type='text' name='email' value='<?php if (isset($email[0])) echo $email[0]; ?>'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Adres poczty, na którą ma być przesyłana korespondencja z formularza" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span></span><input type='checkbox' name='onemail' <?php if (isset($email[1])) if ($email[1] == 1) echo " checked='checked'";?>/>Dane z formularza przesyłane przez adres serwera pośredniczącego<i class="fa fa-question-circle" aria-hidden="true" data-text="Jeżeli zaznaczenie jest aktywne to poczta dostarcza z formularza będzie widziana jako przesłana przez adres pocztowy, który został podany przy konfiguracji strony. Po odznaczeniu poczta docierać będzie w taki sposób, że będzie ona widoczna jakby osoby przesyłały ją w tradycyjny sposób (z ich adresem pocztowym)" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div><button onclick='appendEmailSettings();'>Zatwierdź</button><button onclick="resetSettings('emailSettingsForm');">Wyczyść pola</button></div>
			</div>
		</div>
		
		<div class="settingsSetDiv" style="height: 40px;">
			<div class="headerSetName" style="color: white; font-weight: 900;" onclick="toggleSettingsDiv(this);">
				<p>Ustawienia super-użytkownika</p>
			</div>
			<div class="sBody">
				<div><p>Lista super użytkowników (identyfikacja po adresach poczty). Należy pamiętać, że może być ich maksymalnie trzech.</p>
				<?php 
					//trzeba pomyslec o funkcji wczytujacej login - po co robic to w dwóch miejsach (utilityFunctions ma taka sama, powtórzoną formułę)
					$sRead = readLoginFile(__DIR__ . '/../../data/login.dat');
					//print_r($sRead);
					for ($i = 0; $i < count($sRead); $i++) {
						$data = explode('||', decodePhrase($sRead[$i]));
						echo "<p data-id='{$i}' style='clear: both; float: inherit;'>{$data[2]}<button onclick='deleteSU(this);'>Usuń</button></p>";
					}
// 					if ($f = @fopen(__DIR__ . '/../../data/login.dat', 'r')) { 
// 						$sRead = fread($f, filesize(__DIR__ . "../../data/login.dat"));
// 						fclose($f);
// 						$sRead = explode('|##|', $sRead);
// 						for ($i = 0; $i < count($sRead); $i++) {
// 							$data = explode('||', decodePhrase($sRead[$i]));
// 							echo "<p data-id='{$i}' style='clear: both; float: inherit;'>{$data[2]}<button onclick='deleteSU(this);'>Usuń</button></p>";
// 						}
// 					}
				?>
				</div>
				<div><p>Ze względu na bezpieczeństwo konfiguracji strony może istnieć do 3 super użytkowników. Strona prócz tego może posiadać więcej uprawnionych użytkowników do korzystania z panelu administracyjnego.
				Jednak super użutkownik może np. dostać się do panelu nawet w przypadku, gdy dostęp do bazy SQL będzie niemożlowiwy. Ponadto super użytkownik może usuwać użytkowników z bazy SQL (użytkownicy SQL nie mają
				takiej możliwości). Super użytkownik może zmieniać uprawnienia każdemu użytkownikowi oraz ma dostęp do niektórych, normalnie ukrytych opcji.</p></div>
				<div class='afterBlock beforeBlock'><span>Login</span><input type='password' name='login'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Nazwa super użytkownika. W celach bezpieczeństwa nazwa ta jest zakrywana tak samo jako hasło" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Hasło</span><input type='password' name='pass'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Hasło super użytkownika. Jeżeli wybrana jest autoryzacja po adresie poczttowym to hasło może być użyteczne np. do szczytania konfiguracji strony" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Potwierdzenie hasła</span><input type='password' name='repass'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Powtórzenie hasła w celu weryfikacji jego poprawności" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Adres poczty</span><input type='text' name='passMail'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Adres poczty super użytkownika. Na adres ten będzie przeysłany adres kierujący do panelu administracyjnego bądź zapomniane hasło" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><span>Adres poczty (potwierdzenie)</span><input type='text' name='rePassMail'/><i class="fa fa-question-circle" aria-hidden="true" data-text="Weryfikacja poprawności poczty" onmouseover="showInfoTip(event);" onmouseout="showInfoTip(event, true);"></i></div>
				<div class='afterBlock beforeBlock'><button onclick='appendUserSettings();'>Zatwierdź</button><button onclick="resetSettings('mainUserSettingsForm');">Wyczyść pola</button></div>
			</div>
		</div>
		
		<div class="settingsSetDiv" style="height: 40px;">
			<div class="headerSetName" style="color: white; font-weight: 900;" onclick="toggleSettingsDiv(this);">
				<p>Mapa strony</p>
			</div>
			<div class="sBody">
				<div><p>Mapa strony (tutaj plik sitemap.xml) ułatwia robotom wyszukiwarek (np. Google czy Bing) katalogować stronę oraz podstrony naszej witryny.
				Mapa strony zawiera wszystkie dostępne podstrony (wraz z ich adresami) oraz takie dodatkowe informacje jak np. priorytet odnośnika (czy to on powinien pojawić się w wynikach wyszukiwania),
				czas ostatniej modyfikacji (przeważnie czas odświeżenia witryny) i/lub ifnormacje, z jaką częstotoliwością zmienia się wskazana podstrona WWW (np. dziennie, miesięcznie, rocznie, nigdy).
				Zaleca się co jakiś czas dokonywać aktualizacji mapy strony (np. po dodaniu nowych informacji) by plim był zawsze jak najświeższy. Aktualnie nie jest możliwe dokonywanie automatycznych 
				aktualizacji (mogłyby doprowadzić do chaosu działania całej wtiryny i zadziałać na niekorzyść katalogowania strony). Poniżej powinna prezentować się zawartość pliku mapy strony WWW (o ile 
				została takowa utworzona). Jeżeli nie - dobrym rozwiązaniem jest utworzyć nowy plik mapy (Generuj mapę). Mape można zmieniać ręcznie (edytując jej zawartość) jednak jest to nie zalecane (chyba, że
				ma się wiedzę na temat działania wyszukiwarek).</p>
				</div>
				<div class='afterBlock beforeBlock'><span>Zawartość pliku sitemap.xml</span><textarea style='width: 80%; min-height: 300px;' name='sitemap'>
				<?php 
					$f = @fopen("../../sitemap.xml", 'r');
					if ($f) {
						$sRead = fread($f, filesize(__DIR__ . "/../../sitemap.xml"));
						fclose($f);
						echo $sRead;
					}
				?>
				</textarea></div>
				<div class='afterBlock beforeBlock'><button onclick='generateSiteMap();'>Generuj mapę</button><button onclick="saveSiteMap();">Zapisz mapę</button></div>
			</div>
		</div>
		
		<div class="settingsSetDiv" style="height: 40px;">
			<div class="headerSetName" style="color: white; font-weight: 900;" onclick="toggleSettingsDiv(this);">
				<p>Słowa kluczowe i tytuły stron</p>
			</div>
			<div class="sBody">
				<div><p>Większość wyszukiwarek (w zasadzie wszystkie znaczące) odczytują i interpretują tak zwane 'słowa kluczowe'. Każda wyszukiwarka przypisuje sobie te słowa do 
				aktualnie skanowanej strony. W przypadku, gdy dana osoba wpisze takowe słowa w pole wyszukiwania wyszukiwarka powinna yuwzględnić w wynikach naszą stronę. Typowo
				przyjęło się podawanie do 20 słów kluczowych, oddzielanych przecinkami. W obecnej chwili dopuszczana jest większa ilość słów kluczowych oraz możliwe jest tworzenie
				wyrażeń kluczowych (np. składające się z dœóch lub trzech słów). Należy mieć na uwadze, że same słowa są jedynie dodatkiem do analizowanej treści strony. Jeżeli np. słowa
				te nie będą mieć odzwierciedlenia w treści strony (np. będziemy reklamować się jako sklep, w rzeczywistości natmiast prowadzimy ranking sklepów) to nasza strona
				może zająć niższą pozycję w wyszukiwarkach. Ponadto na ranking naszej strony mają wpływ także inne czynniki (tytuł strony, mapa strony, podłączenie strony do innych stron itp.)</p>
				</div>
				<div class='afterBlock beforeBlock'><span>Słowa kluczowe</span><input type='text' name='keywords' value='<?php $f=@fopen(__DIR__ . "/../../data/keywords.txt", 'r');if($f){$sRead = fread($f, filesize(__DIR__ . "/../../data/keywords.txt"));fclose($f);echo $sRead;}?>'/></div>
				<div><p>Równie ważnym czynnikiem jest tytuł wyświetlanej strony. Najlepiej jest gdy każda podstrona ma swoją własną, unikatową nazwę. Nie jest to oczywiście wymagane jednak
				potrafi znacznie podnieść ranking naszej strony. Niestety, ustawienie pożądanego, uniwersalnego wzoru tytułu strony wymaga dołączenia pewnych zmiennych. Można pozostawić wpisany już wzór,
				zmienić go bądź usunąć. Lista dostępnych zmiennych: </p>
				<ul>
					<li>[podstrona] - wyświetli nazwę podstony</li>
					<li>[kategoria] - wyświetli nazwę wybranej kategorii, o ile takowa istnieje (strony kursów oraz szkoleń)</li>
					<li>[podkategoria] - wyświetli nazwę podkategorii, o ile takowa istnieje (strony kursów oraz szkoleń)</li>
					<li>[kurs] - wyświeli nazwę kursu/szkolenia, o ile zostało wybrane (strony kursów oraz szkoleń)</li>
				</ul>
				</div>
				<div class='afterBlock beforeBlock'><span>Słowa kluczowe</span><input type='text' name='siteTitle' value='<?php $f=@fopen(__DIR__ . "/../../data/title.txt", 'r');if($f){$sRead = fread($f, filesize(__DIR__ . "/../../data/title.txt"));fclose($f);echo $sRead;}?>'/></div>
				<div class='afterBlock beforeBlock'><button onclick="saveKeywords();">Zapisz ustawienia</button></div>
			</div>
		</div>
		
		<div class="settingsSetDiv" style="height: 40px;">
			<div class="headerSetName" style="color: white; font-weight: 900;" onclick="toggleSettingsDiv(this);">
				<p>Sprawdzenie i naprawa bazy danych strony WWW</p>
			</div>
			<div class="sBody">
				<div><p>Jeżeli strona/podstrony WWW nie wyświetlają się prawidłowo (posiadają błędy, strona z podstronami nie ładuje się prawidłowo, na stronach nie ma odpowiednich nazw/tekstu)
				to naprawa bazy danych może pomóc. Naprawa składa się z kilku etapów: stworzenia kopii zapasowej, otworzenia bazy w trybie odczytu, sprawdzenia uszkodzeń, próby naprawy oraz
				sprawdzenia integralności bazy po naprawie. W przypadku powodzenia wyświetlony zostanie stosowany komunikat bądź informacja o napotkanym błędzie w przypadku porażki.</p>
				</div>
				<div><p id="repairBaseOutput"></p>
				</div>
				<div class='afterBlock beforeBlock'><button onclick="repairBase();">Rozpocznij naprawę bazy</button></div>
			</div>
		</div>
	</div>
<!--	<fieldset>
		<legend onclick="showHideForm('emailSettingsForm');">Ustawienia adresu zwrotnego dla formularzy</legend>
		<div id='emailSettingsForm'>
			<?php 
// 				$email = getSettingEmail();
// 				if ($email != false)
// 					$email = explode('||', $email);
			?>
			<div><span>Adres e-poczty</span><input type='text' name='email' value='<?php if (isset($email[0])) echo $email[0]; ?>'/></div>
			<div class='afterBlock beforeBlock'><span></span><input type='checkbox' name='onemail' <?php if (isset($email[1])) if ($email[1] == 1) echo " checked='checked'";?>/>Przesyłaj dane z formularza poprzez pojedynczy adres (podany w konfiguracji przesyłania poczty)</div>
			<div><button onclick='appendEmailSettings();'>Zatwierdź</button><button onclick="resetSettings('emailSettingsForm');">Wyczyść pola</button></div>
		</div>
	</fieldset>-->
<!--	<fieldset>
		<legend onclick="showHideForm('mainUserSettingsForm');">Ustawienia super-użytkownika</legend>
		<div id='mainUserSettingsForm'>
			<div><span>Login</span><input type='password' name='login'/></div>
			<div><span>Hasło</span><input type='password' name='pass'/></div>
			<div><span>Potwierdzenie hasła</span><input type='password' name='repass'/></div>
			<div><span>Adres poczty (na wypadek zgubienia hasła)</span><input type='text' name='passMail'/></div>
			<div><span>Adres poczty (potwierdzenie)</span><input type='text' name='rePassMail'/></div>
			<div><button onclick='appendUserSettings();'>Zatwierdź</button><button onclick="resetSettings('mainUserSettingsForm');">Wyczyść pola</button></div>
		</div>
		
		
	
	</fieldset>-->
	<!--<fieldset>
		<legend onclick="showHideForm('updateSiteForm');">Aktualizacja strony</legend>
		<div id='updateSiteForm'>
			<div><span>Login</span><input type='password' name='login'/></div>
			<div><span>Hasło</span><input type='password' name='pass'/></div>
			<div><span>Potwierdzenie hasła</span><input type='password' name='repass'/></div>
			<div><span>Adres poczty (na wypadek zgubienia hasła)</span><input type='text' name='passMail'/></div>
			<div><span>Adres poczty (potwierdzenie)</span><input type='text' name='rePassMail'/></div>
			<div><button onclick='appendUserSettings();'>Zatwierdź</button><button onclick="resetSettings('mainUserSettingsForm');">Wyczyść pola</button></div>
		</div>
	</fieldset>-->
<div id="tipLabel"><p></p></div>