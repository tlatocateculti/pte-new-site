<?php 
	require_once "./utilityFunctions.php";
	require_once "../../scripts/utilityFunctions.php";
// 	require_once "../../scripts/database.php";
	require_once "../../config.php";
	//confirmSession();
?>
<?php
// 	if (isset($_POST['startNum'])) 
// 		$_SESSION['limit']['galleries'] = $_POST['startNum'];
		//to byl chyba duplikat tego ponizej
// 	if (!isset($_SESSION['limit']['galleries']))
// 		$_SESSION['limit']['galleries'] = 0;
	//$bs = new Database();
	//$bs->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME);
	
	//przechowuje informacje o ilości rekordów w bazie danych
	//$totalGalleries = $bs->countQuery(array('galleries' => array('gallery_id')));
// 	$limit = 10;

// 	if(isset($_SESSION['limit']['galleries'])) {
// 		//warunek sie spełni, gdy użytkownik będzie chciał wyświetlić ostatnią stronę użytkowników
// 		if($_SESSION['limit']['galleries'] == -1)
// 			$_SESSION['limit']['galleries'] = $totalGalleries - 10;
// 				
// 	}
// 	else {
// 		$_SESSION['limit']['galleries'] = 0;
// 	}
	
	function createActionButtons($hide = 0, $n = [], $icons = [], $actions = []) {
		$btn = createNodeButtons($hide, $n, $icons, [], $actions);
		$btnTxt = '';
		for ($i = 0; $i < count($btn); $i++) {
			$btnTxt .= "<div style=\"position: relative;\"";
			if (isset($btn[$i]['style']))
				$btnTxt .= " class=\"{$btn[$i]['style']}\"";
			if (is_array($btn[$i]['events'])) {
				$keys = array_keys($btn[$i]['events']);
				for ($j = 0; $j < count($keys); $j++) {
					$btnTxt .= " {$keys[$j]}='" . $btn[$i]['events'][$keys[$j]] . ";'";
				}
			}
			$btnTxt .= ">";
			if (isset($btn[$i]['icon'])) {
				$btnTxt .= "<p";
				if ($btn[$i]['name'])
					$btnTxt .= " data-tip='{$btn[$i]['name']}'";
				$btnTxt .= ">{$btn[$i]['icon']}</p>";
			}
			else 
				$btnTxt .= "<p>{$btn[$i]['name']}</p>";
			$btnTxt .= "</div>";
		}
		return $btnTxt;
	}
	
// 	function readGalleryEntries($folder) {
// 		return json_decode(file_get_contents($folder . '/content.json'), true)[0]['photos'];
// // 		$d = dir($folder);
// // 		$out = [];
// // 		$name = [];
// // 		while($entry = $d->read()) {
// // 			if ($entry === 'galleries.content' || $entry === 'md5' || $entry === 'intro.jpg' || $entry === 'content.json')
// // 				continue;
// // 			else if (strpos($entry, '.')) {
// // 				if (strpos($entry, 'mini'))
// // 					$out[count($out)] = $entry;
// // 				else
// // 					$name[count($name)] = $entry;
// // 				
// // 			}
// // 	// 		else
// // 	// 			$dirContent[$i++] = $entry;
// // 		}
// // 		//print_r($out);
// // 		//generateMiniImages($name, $out, $folder);
// // 		//generateContentFile($name);
// // 		$outImages = [];
// // 		for ($i = 0; $i < count($name); $i++)  {
// // 			$outImages[$i]['photo'] = $name[$i];
// // 			$cmpStr = substr($name[$i], 0, -4);
// // 			for ($j =0; $j < count($out);$j++) 
// // 				if (@substr($out[$j], 0, -9) === $cmpStr) {
// // 					$outImages[$i]['mini'] = $out[$j];
// // 					unset($out[$j]);
// // 					break;
// // 				}
// // 			if (!isset($outImages[$i]['mini']))
// // 				$outImages[$i]['mini'] = '';
// // 		}
// // 		return $outImages;
// 	}
	
	//funkcja wymaga optymalizacji!!
	function elementsTable($gid = 0) {
		/*$gallery_id*/;
// 		$bs->queryTable(array('galleries' => array('name', 'gallery_folder', 'gallery_id', 'description', 'active')), "", 10, $_SESSION['limit']['galleries']);
// 		$bs->queryTable(array('galleries' => array('name', 'gallery_folder', 'gallery_id', 'description', 'active')), "", -1);
// 		$gallery_id = $bs->getResults(0);
// 		$bs->flushResults();
		$images = readGalleryEntries("../../json");
// 		echo '<div id="galleryAddNew" class="gallerySetDiv">
// 				<div class="gallerySetName" style="color: white; font-weight: 900;" onclick="galleriesTableAddEdit();">
// 					<div style="float: left;"><p>Dodaj nową galerię</p></div>
// 					<div style="float: right; width: 170px; height: 100%; overflow:hidden; font-size: 15px;">' . 
// 					createActionButtons(false, ['Dodaj galrię'], ['<i class="fa fa-plus" aria-hidden="true"></i>'], ['onclick' => array('galleriesTableAddEdit()')]) .'</div>
// 				</div>
// 			</div>';
		$btnIcons = [0 => '<i class="fa fa-plus" aria-hidden="true"></i>', 1 => '<i class="fa fa-pencil" aria-hidden="true"></i>', 2 => '<i class="fa fa-eraser" aria-hidden="true"></i>', 3 => '<i class="fa fa-cog" aria-hidden="true"></i>' /*, 5 => '<i class="fa fa-trash" aria-hidden="true"></i>'*/];
		$btnName = [0 => 'Dodaj nowe zdjęcia do galerii', 1 => 'Edytuj zaznaczone zdjęcia z galerii', 2 => 'Usuń zaznaczone zdjęcia z galerii', 3 => 'Edytuj galerię' /*, 5 => 'Usuń galerię (NA ZAWSZE)'*/];
		$btnActions = ['onclick' => array("event.stopPropagation(); galleriesTableAddEdit(true , false, this)", 
						"event.stopPropagation(); galleriesTableAddEdit(true, true, this)", 
						"event.stopPropagation(); galleryPhotosTableDelete(this)", 
						"event.stopPropagation(); galleriesTableAddEdit(false, true, this)"
						/*"event.stopPropagation(); showHideGallery(this)", 
						"event.stopPropagation(); galleriesTableDelete(this)"*/) ];
		$basicCSS = "height: 200px; background: gray;";
		//for ($i = 0; $i < count($gallery_id['gallery_id']); $i++) {
			echo '<div id="set-1" class="gallerySetDiv"';
			//if ($gallery_id['gallery_id'][$i] == $gid) 
			echo ' style="height: 580px;"';
			echo '>';
// 			if (1) {
// 				$btnIcons[4] = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
// 				$btnName[4] = 'Ukryj galerię';
// 			}
// 			else {
// 				$btnIcons[4] = '<i class="fa fa-eye" aria-hidden="true"></i>';
// 				$btnName[4] = 'Pokaż galerię';
// 			}
				echo '<div class="gallerySetName" onclick="gallerySetClick(this);" data-desc="Opis zdjęcia"><div style="float: left;"><p>Galeria główna'  . (1 ? ' (aktywna)' : ' (nieaktywna)') . '</p></div>
<div data-gallery-icon-action="1" class="buttonIconDiv">' . createActionButtons(1, $btnName, $btnIcons, $btnActions) .'</div></div>';
// 				$bs->buildConditionQuery(array('gallery_elements', 'gallery_id'), array($gallery_id['gallery_id'][$i]), DataEnum::EQUAL);
// 				$bs->flushResults();
// 				$bs->queryTable(array('gallery_elements' => array('*')), $bs->getConditions(), -1);
// 				if ($bs->getResults($set + 1) != -1 && $bs->getResults($set + 1) != 0)
// 					$set++;
// 				$bs->flushConditions();
				echo '<div class="photosSet">';
					echo '<div style="display: block;" data-name="gInfo"><p style="clear:both; float: inherit;">Opis galerii: BRAK</p></div>';
					//$base_files = [];
					//print_r($set);
					//if ($bs->getResults(0) != -1 && $bs->getResults(0) != 0) {
						for ($j = 0; $j < count($images); $j++) {
							$images[$j]['photo'] = explode('/', $images[$j]['photo'])[2];
							if ($images[$j]['mini'] !== '') $images[$j]['mini'] = explode('/', $images[$j]['mini'])[2];
							//$bsCount = count($base_files);
							echo "<div onclick='galleryPhotoClick(this);' data-id='" . $j . "' class='photoMiniSet'>";
// 							echo "<div onclick='galleryPhotoClick(this);' data-id='" . $images[$j]['photo'] . "||" . $images[$j]['mini'] . "' class='photoMiniSet'>";
							$dp = "height: 200px; background: gray url('../galleries/";
							if ($images[$j]['mini'] !== '') {
								$dp .= $images[$j]['mini'];
// 								$base_files[$bsCount++] = $bs->getResults(0)['path_mini'][$j];
// 								$base_files[$bsCount] = $bs->getResults(0)['path'][$j];
							}
							else {
								$dp .= $images[$j]['photo'];
								//$base_files[$bsCount] = $bs->getResults(0)['path'][$j];
							}
							$dp .= "'); width: 100%; height: 100%; background-size: 100% 100%;";
							
							echo "<div style=\"" . $dp . "\" data-full=\"galleries/" . $images[$j]['photo'] . (($images[$j]['mini'] !== '') ? "\" data-mini=\"galleries/" . $images[$j]['mini'] : "") .  "\" data-photo=\"{$dp}\"></div>";
							echo "</div>";
						}
					//}
					//else 
					//	echo '<p>BRAK DODANYCH ZDJĘĆ!</p>';
//PONIŻSZY KOD ODPOWIADA ZA USUWANIE NIEPOTRZEBNYCH PLIKÓW Z FOLDERU WYBRANEJ GALERII!!
				removeGalleryFiles('../../galleries/nocategories');
				echo '</div>';
				echo '<div class="infoDiv">
<p>Uprawnienia folderu galerii ' . 
substr(sprintf('%o', fileperms('../../galleries/')), -4) . ', zapis: ' .
((is_writable('../../galleries/')) ? ' Dostępny' : ' Brak dostępu') .'</p>
</div>';
				//$set++;
			echo "</div>";
		//}			
	}
	
	//////////////////////////////////////////////
	//print_r($_GET);
// 	if (!isset($_GET['s']))
// 		setsTable($bs);
// 	else {
		if (isset($_POST['gid']))
			elementsTable($_POST['gid']);
		else
			elementsTable();
// 	}