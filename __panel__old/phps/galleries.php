<style>
/*	table {
		position: relative;
		width: 100%;
		overflow: hidden;
		table-layout: fixed;
	}
	th,td {
		width: 100px;
		overflow: hidden;
		word-break: break-all;
	}
	th:first-child {
		width: 30px;
	}
	th:last-child {
		width: 60px;
	}*/
	#content {
		overflow-x: visible;
	}
	.actionButtons {
		position: relative;
	}
	.gallerySetDiv {
		border: 1px solid black;
		overflow: hidden;
		width: 95%;
		height: 40px;
	}
	.gallerySetName {
		font-size: 20px;
		font-weight: bold;
		text-align: center;
		background: gray;
		margin: 0;
		padding-top: 10px;
		height: 40px;
		cursor: pointer;
	}
	.gallerySetName p {
		margin: 0;
	}
	.gallerySetName::after {
		content: '';
		display: table;
		clear: both;
	}
	
	.photosSet {
		overflow: hidden;
		overflow-y: auto;
		height: 490px;
	}
	.photosSet:after {
		content: '';
		clear: both;
		display: table;
	}
	
	.photoMiniSet {
		width: 300px;
		height: 200px;
		float: left;

	}	
	.elemActionBtn {
		display: none;
	}
	.buttonIconDiv {
		position: relative; 
		z-index: 150; 
		float: right; 
		width: 170px; 
		height: 100%; 
		overflow:hidden; 
		font-size: 15px;
	}
</style>
<script src="./js/galleries.js"></script>
<div id="tableContent"></div>
<?php //if (!isset($editGalleryContent)) : ?>
<!--<div id='actionButtons'>
	<h3>Operacje na zaznaczonych polach:</h3>
	<button onclick="galleriesTableAddEdit();">Dodaj nową galerię</button>-->
<!-- 	<button onclick="galleriesTableAddEdit(false, true);">Edycja</button> -->
<!-- 	<button onclick="galleriesTableDelete();">Usunięcie</button> -->

<!-- </div> -->
<div id="tipLabel"></div>
<?php //endif; ?>
<script>
// 	getGalleriesTable(<?php if (isset($editGalleryContent)) echo 'true'; ?>);
</script>