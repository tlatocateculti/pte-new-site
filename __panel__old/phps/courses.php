<?php
/*
 * PLIK BEDZIE ZAWIERAŁ ZARZĄDZANIE AKTUALNOŚCIAMI!!!
 * PÓŹNIEJ TRZEBA ZMIENIĆ JEGO NAZWĘ!!
 * TYCZY JEST TO TAKŻE PLIKÓW SPOKREWNIONYCH!
 */
?>
<style>
	table {
		position: relative;
		width: 100%;
		overflow: hidden;
		table-layout: fixed;
	}
	th,td {
		width: 100px;
		overflow: hidden;
		word-break: break-all;
	}
	th:first-child {
		width: 30px;
	}
	th:last-child {
		width: 60px;
	}
	#content {
		overflow-x: visible;
	}
</style>
<link rel="stylesheet" href="./css/elements.css"/>
<div id="tableContent"></div>
<div id='actionButtons'>
<h3>Operacje na zaznaczonych polach:</h3>
<button onclick="<?php if(!isset($trainingShow)) echo 'coursesTableAddEdit();'; else echo 'coursesTableAddEdit(true);'; ?>">DODAJ</button>
<button onclick="<?php if(!isset($trainingShow)) echo 'coursesTableAddEdit(false, true);'; else echo 'coursesTableAddEdit(true, true);'; ?>">Edycja</button>
<button onclick="coursesTableDelete();">Usunięcie</button>
<!--<button onclick="newsTableConfirm();">Potwierdź</button>
<button onclick="newsTableDisable();">Zablokuj/odblokuj</button></div>-->
<div id="tipLabel"><p></p></div>
<div id="siteEditDiv"></div>