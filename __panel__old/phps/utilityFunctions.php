<?php

function readLoginFile($file) {
	require_once(__DIR__ . "/../../scripts/utilityFunctions.php");
	if ($f = @fopen($file, 'r')) { 
		$sRead = fread($f, filesize($file));
		fclose($f);
		$sRead = explode('|##|', $sRead);
		return $sRead;
	}
	else 
		return [];
}

function confirmSession() {
	require_once('../../config.php');
	
	@session_start();
	if (!isset($_SESSION['login'])) {
		if (!isset($_SESSION['loginTry']))
			$_SESSION['loginTry'] = 3;
		else 
			$_SESSION['loginTry']--;
		if ($_SESSION['loginTry'] == 0 && !isset($_SESSION['login'])) {
			unset($_SESSION['unconfirm']);
			unset($_SESSION['loginTry']);
			header('Location: ' . $_BASE_LOCATION[0]);
		}
	}
	if ($_SERVER['QUERY_STRING'] == "showLogout") {
		$_SESSION['unconfirm'] = false;
		unset($_SESSION['login']);
		header('Location: ' . $_BASE_LOCATION[0]);
		//header('Location: ' . $_BASE_LOCATION[0]);
	}	
	//w tym pliku to już nie działa!!! - nowa wersja jest w pliku katalog wyżej!
// 	if (isset($_POST['pass']) && isset($_POST['login'])) {
// 		//require_once(__DIR__ . "/../scripts/utilityFunctions.php");
// 		if ($f = @fopen(__DIR__ . '/../data/login.dat', 'r')) { 
// 			$sRead = fread($f, filesize("../data/login.dat"));
// 			fclose($f);
// 			//jak to jest liczone trzeba sobie z kodu wywnioskować (na razie)
// 			$data = explode('||', decodePhrase($sRead));
// 			$l = md5($_POST['login'] . $data[2] . $_POST['pass']);
// 			$p = md5($_POST['login'] . $_POST['pass']);
// 			if ($p != $data[0] || $l != $data[1]) 
// 				$_SESSION['unconfirm'] = false;
// 			else {
// 				$_SESSION['unconfirm'] = true;
// 				unset($_SESSION['loginTry']);
// 				$_SESSION['login'] = $_POST['login'];
// 			}
// 		}
// 		else 
// 			$_SESSION['unconfirm'] = false;
// 
// 		
// 	}
	//TUTAJ TRZEBA TO WSZYSTKO POPRAWIĆ - ZA DUŻO WARUNKÓW I SIĘ POGUBIĘ DNIA PEWNEGO!
	if (!isset($_SESSION['unconfirm']))
		header('Location: ' . $_BASE_LOCATION[0]);
	else if (!$_SESSION['unconfirm']) {
		//header('Location: ' . $_BASE_LOCATION[0]);
		$_SESSION['unconfirm'] = false;
	}
}

// function imScale($res, $w, $h) {
// 	$sizeW = imagesx($res);
// 	$sizeH = imagesy($res);
// 	if ($sizeH == 0) return $res;
// 	$ratio = $sizeW/$sizeH;
// 	$wPrev = $w;
// 	$hPrev = $h;
// 	$xStart = 0;
// 	$yStart = 0;
// 	if ($ratio > 1) { 
// 		$h = $w / $ratio;
// 		if ($h != $hPrev)
// 			$yStart = ($hPrev - $h) / 2;
// 	}
// 	else if ($ratio < 1) { 
// 		$w = $h * $ratio;
// 		if ($w != $wPrev) 
// 			$xStart = ($wPrev - $w) / 2;
// 	}
// 	$im = imagecreatetruecolor($wPrev, $hPrev);
// 	$opacity = imagecolorallocatealpha($im, 0, 0, 0, 127);
// 	imagefill($im, 0, 0, $opacity);
// 	imagecopyresampled($im, $res, $xStart, $yStart, 0, 0, $w, $h, $sizeW, $sizeH);
// 	return $im;
// }
// 
// function generateMiniImages($img, &$miniImg, $path) {	
// 	//echo 'MINIATURY!!';
// 	//print_r($img);
// 	for ($i = 0; $i < count($img); $i++) {
// 		$hasMini = false;
// 		for ($j = 0; $j < count($miniImg); $j++) {
// 			if (strpos($miniImg[$j], $img[$i]) !== false) {
// 				$hasMini = true;
// 				break;
// 			}			
// 		}
// 		if (!$hasMini) {
// 			//$hasAlpha = false;
// 			$imgnameArray = explode('.', $img[$i]);
// 			
// 			if ($imgnameArray[1] == 'png') {				
// 				$im = @imagecreatefrompng("{$path}/{$img[$i]}");
// 				//$hasAlpha = true;
// 			}
// 			else {
// 				$im = @imagecreatefromjpeg("{$path}/{$img[$i]}");
// 			}
// 			$im2 = imScale($im, 300, 200);
// // 			$im2 = @imagescale($im, 300, 200);
// // 			if ($hasAlpha) {
// 				@imageAlphaBlending($im2, true);
// 				@imageSaveAlpha($im2, true);
// // 			}
// 			
// 			@imagepng($im2, "{$path}/{$imgnameArray[0]}_mini.png");
// 			$miniImg[] = "{$imgnameArray[0]}_mini.png";
// 			@imagedestroy($im2);
// 			@imagedestroy($im);
// 		}
// 	}
// }

function readGalleryEntries($folder, $id = -1) {
	return readEntries($folder, 'gallery.json', 'photos', $id);
// 	$o = json_decode(file_get_contents($folder . '/content.json'), true)[0]['photos'];
// 	if ($id != -1 && $id < count($o))
// 		return $o[$id];
// 	return $o;
}

function removeGalleryFiles($path, $existed_files = '', $deleteSelected = false) {
//'../galleries/' . $bs->getResults(0)['gallery_folder'][$i]
	if (!$deleteSelected) {
		$folder_files = @scandir($path);
		for ($z = 0; $z < count($folder_files); $z++) {
			if ($folder_files[$z] == '..' || $folder_files[$z] == '.') {
				$folder_files[$z] = '';
				continue;
			}
			@unlink($path . '/' . $folder_files[$z]);
		}
	}
	else {
		for ($i = 0; $i < count($existed_files); $i++) {
			@unlink($path . '/' . $existed_files[$i]);
		}
	}
}

// function save_tmp_file($dir, $n, $data, $mode = 'a') {	
// 	if(!file_exists($dir)) {
// 		mkdir($dir, 0755, true);
// 	}
// 	$f = fopen(__DIR__ . '/' . $dir . $n, $mode);
// 	//echo $n;
// 	//$f.lock();
// 	//$pass = encodePhrase($data);
// 	fwrite($f, $data);
// 	//$f.unlock();
// 	fclose($f);
// 	
// }
// 
// function saveFile($dir, $n, $count) {
// 	echo $count;
// 	$fi = fopen(__DIR__ . '/' . $dir . $n . '.tmp_' . 0, 'r');
// 	$fo = fopen(__DIR__ . '/' . $dir . $n, 'w');
// 	$sRead = fread($fi, filesize(__DIR__ . '/' . $dir . $n . '.tmp_' . 0));
// 	$sRead = explode(',', $sRead)[1];
// 	//fwrite($fo, $sRead);
// 	fclose($fi);
// 	unlink(__DIR__ . '/' . $dir . $n . '.tmp_' . 0);
// 	for ($i = 1; $i < $count; $i++) {
// 		echo $fi = fopen(__DIR__ . '/' . $dir . $n . '.tmp_' . $i, 'r');
// 		$sRead .= fread($fi, filesize(__DIR__ . '/' . $dir . $n . '.tmp_' . $i));
// 		//fwrite($fo, $sRead);
// 		fclose($fi);
// 		unlink(__DIR__ . '/' . $dir . $n . '.tmp_' . $i);
// 		echo ' ' . $i;
// 	}
// 	$sRead = str_replace(' ','+',$sRead);
// 	//echo $sRead;
// 	fwrite($fo, base64_decode($sRead));
// 	fclose($fo);
// }

function createNodeButtons($hide = false, $name = [], $icons = [], $commEvent = [], $csmEvent = []) {
	$ret = [];
	if (count($name) > 0) $res['name'] = $name;
	else { 
		$res['name'] = ['Zapisz zmiany', 'Edytuj nazwę', 'Ukryj', 'Usuń (NA ZAWSZE!)'];
		if ($hide) $res['name'][2] = 'Pokaż';
	}
	if (count($icons) > 0 ) $res['icon'] = $icons;
	else {
		$res['icon'] = ['<i class="fa fa-pencil" aria-hidden="true"></i>', 
				'<i class="fa fa-cog" aria-hidden="true"></i>',
				'<i class="fa fa-eye-slash" aria-hidden="true"></i>',
				'<i class="fa fa-trash" aria-hidden="true"></i>'];
		if ($hide) $res['icon'][2] = '<i class="fa fa-eye" aria-hidden="true"></i>';
	}
	$res['style'] = ['buttonListStyle hideBtn', 'buttonListStyle'];
	if (count($commEvent) > 0) $res['commonEvents'] = $commEvent;
	else $res['commonEvents'] = ['onmouseout' => "hideTip(\"tipLabel\")", 'onmouseover' => "showTip(this, event)"];
	if (count($csmEvent) > 0) $res['customEvents'] = $csmEvent;
	else $res['customEvents'] = ['onclick' => array("saveListNode(this, 1)", "editListNode(this)", "showHideNode(this)", "deleteListNode(this)") ];
	$sMax = count($res['style']) - 1;
	for ($i = 0; $i < count($res['name']); $i++) {
		$ret[$i] =  array('name' => $res['name'][$i], 'icon' => $res['icon'][$i], 'style' => (($sMax < $i) ? $res['style'][$sMax] : $res['style'][$i]), 'events' => $res['commonEvents'] );
		foreach (array_keys($res['customEvents']) as $k) {
			if ($res['customEvents'][$k][$i] != '') $ret[$i]['events'][$k] = $res['customEvents'][$k][$i];
		}
	}
	return $ret;
}
