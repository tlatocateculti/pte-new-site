-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 01, 2017 at 08:11 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pte_main`
--
CREATE DATABASE IF NOT EXISTS `pte_main` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
USE `pte_main`;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id_category` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  UNIQUE KEY `id_category` (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id_city` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_polish_ci NOT NULL,
  UNIQUE KEY `id_city` (`id_city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id_company` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `short_name` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `nip` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `id_country` bigint(20) unsigned NOT NULL,
  `id_zipCode` bigint(20) unsigned NOT NULL,
  `id_street` bigint(20) unsigned NOT NULL,
  `estate_number` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `local_number` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `site_address` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_polish_ci NOT NULL,
  UNIQUE KEY `id_company` (`id_company`),
  KEY `id_country` (`id_country`),
  KEY `id_zipCode` (`id_zipCode`),
  KEY `id_street` (`id_street`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id_country` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  UNIQUE KEY `id_country` (`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id_file` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file_path` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `file_realName` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `file_size` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `file_description` text COLLATE utf8_polish_ci NOT NULL,
  `file_displayName` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `id_category` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `id_file` (`id_file`),
  KEY `id_category` (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `files_categories`
--

DROP TABLE IF EXISTS `files_categories`;
CREATE TABLE IF NOT EXISTS `files_categories` (
  `id_file` bigint(20) unsigned NOT NULL,
  `id_category` bigint(20) unsigned NOT NULL,
  `id_group` bigint(20) unsigned NOT NULL,
  KEY `id_file` (`id_file`),
  KEY `id_category` (`id_category`),
  KEY `id_group` (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id_group` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  UNIQUE KEY `id_group` (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `streets`
--

DROP TABLE IF EXISTS `streets`;
CREATE TABLE IF NOT EXISTS `streets` (
  `id_street` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  UNIQUE KEY `id_street` (`id_street`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `second_names` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `id_company` bigint(20) unsigned DEFAULT NULL,
  `short_name` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `person_id` varchar(20) COLLATE utf8_polish_ci DEFAULT NULL,
  `nip` varchar(20) COLLATE utf8_polish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `street_prefix` tinyint(1) DEFAULT NULL,
  `id_street` bigint(20) unsigned DEFAULT NULL,
  `estate_number` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `local_number` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `id_zipCode` bigint(20) unsigned DEFAULT NULL,
  `id_country` bigint(20) unsigned DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_polish_ci NOT NULL,
  UNIQUE KEY `id_user` (`id_user`),
  KEY `id_street` (`id_street`),
  KEY `id_company` (`id_company`),
  KEY `id_zipCode` (`id_zipCode`),
  KEY `id_country` (`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id_user` bigint(20) unsigned NOT NULL,
  `id_group` bigint(20) unsigned NOT NULL,
  KEY `id_user` (`id_user`),
  KEY `id_group` (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zipCodes`
--

DROP TABLE IF EXISTS `zipCodes`;
CREATE TABLE IF NOT EXISTS `zipCodes` (
  `id_zipCode` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `id_city` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `id_zipCode` (`id_zipCode`),
  KEY `id_city` (`id_city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `fk-comapny_street` FOREIGN KEY (`id_street`) REFERENCES `streets` (`id_street`),
  ADD CONSTRAINT `fk_comapny_zipCode` FOREIGN KEY (`id_zipCode`) REFERENCES `zipCodes` (`id_zipCode`),
  ADD CONSTRAINT `fk_company_country` FOREIGN KEY (`id_country`) REFERENCES `countries` (`id_country`);

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `fk_files_category` FOREIGN KEY (`id_category`) REFERENCES `categories` (`id_category`);

--
-- Constraints for table `files_categories`
--
ALTER TABLE `files_categories`
  ADD CONSTRAINT `fk_fc_category` FOREIGN KEY (`id_category`) REFERENCES `categories` (`id_category`),
  ADD CONSTRAINT `fk_fc_file` FOREIGN KEY (`id_file`) REFERENCES `files` (`id_file`),
  ADD CONSTRAINT `fk_fc_group` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id_group`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_user_company` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`),
  ADD CONSTRAINT `fk_user_country` FOREIGN KEY (`id_country`) REFERENCES `countries` (`id_country`),
  ADD CONSTRAINT `fk_user_street` FOREIGN KEY (`id_street`) REFERENCES `streets` (`id_street`),
  ADD CONSTRAINT `fk_user_zipCode` FOREIGN KEY (`id_zipCode`) REFERENCES `zipCodes` (`id_zipCode`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_ug_group` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id_group`),
  ADD CONSTRAINT `fk_ug_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`);

--
-- Constraints for table `zipCodes`
--
ALTER TABLE `zipCodes`
  ADD CONSTRAINT `fk_zipCode_city` FOREIGN KEY (`id_city`) REFERENCES `cities` (`id_city`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
